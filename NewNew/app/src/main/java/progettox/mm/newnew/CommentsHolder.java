package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Admin on 25/5/2015.
 */
public class CommentsHolder implements Serializable {

    //public ParseFile profilePic;
    public String PlayerID;
    private String PlayerObjectID;
    public String Message;
    public String ObjectID;
    private int NoOfComments;
    private int NoOfLikes;
    private Boolean IsLiked;
    private Date CreatedAt;

    public CommentsHolder(String Username, String UserObjectID, String Comment, final Activity activity){
        this.PlayerID = Username;
        this.PlayerObjectID = UserObjectID;
        this.Message = Comment;



        //load user avatar if it is not in cache
//        if(!IsBitmapCached(this.PlayerID, activity)){
//            ParseFile Picture = Img;
//            Picture.getDataInBackground(new GetDataCallback() {
//                @Override
//                public void done(byte[] data, ParseException e) {
//
//                    if (e == null) {
//                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
//                                data.length);
//                        storeImage(PictureBmp, PlayerID, activity);
//
//                    } else {
//
//                    }
//
//                }
//            });
//        }

    }

    public void insertObjectID(String ObjID){
        this.ObjectID = ObjID;
    }

    public String getObjectID(){return this.ObjectID;}
    public String getPlayerObjectID() { return this.PlayerObjectID;}

    public CommentsHolder(ParseObject Obj, final Activity activity){
        //this.profilePic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
        //this.profilePic = Obj.getParseFile("Avatar_Pic");
        this.PlayerID = Obj.getParseObject("User_objectId").getString("username");
        this.PlayerObjectID = Obj.getParseObject("User_objectId").getObjectId();
        this.ObjectID = Obj.getObjectId();
        this.CreatedAt = Obj.getCreatedAt();
        this.NoOfComments = Obj.getInt("No_of_Comments");
        this.NoOfLikes = Obj.getInt("No_of_Likes");
        this.IsLiked = false;
        //this.PlayerID = Obj.getString("username");
        if (Obj.getString("Comments") != null){
            this.Message = Obj.getString("Comments");
        }
        else
        {
            this.Message = "";
        }

        //load user avatar if it is not in cache
        if(!IsBitmapCached(this.PlayerObjectID, activity)){
            ParseFile Picture = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        storeImage(PictureBmp, PlayerObjectID, activity);

                    } else {

                    }

                }
            });
        }

    }

    public String getDate() {
        String temp = this.CreatedAt.toString();
        int index = temp.indexOf("GMT");


        return temp.substring(0, index);
    }

    public int getNoOfLikes() {return this.NoOfLikes;}
    public int getNoOfComments() {return this.NoOfComments;}

    public void setIsLiked(Boolean like){this.IsLiked = like;}
    public Boolean getIsLiked() {return this.IsLiked;}
    //public ParseFile getProfilePic(){ return this.profilePic;}
    public String getPlayerID(){ return this.PlayerID;}
    public String getComment(){ return this.Message;}
    //public void setProfilePic(ParseFile Obj){ this.profilePic = Obj;}
    public void setPlayerID(String ID){this.PlayerID = ID;}
    public void setComment(String Comment){this.Message = Comment;}

    public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public boolean IsBitmapCached(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return false;
    }

}