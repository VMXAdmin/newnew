package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 27/7/2015.
 */
public class activity_image_secondary extends AppCompatActivity {

    private String mPlayerID;
    private String mPlayer_objectId;
    private CommentsAdapter mCommentsAdapter;
    private List<CommentsHolder> mCommentsArray;
    private String mPost_objectId;
    private String mImage_objectId;
    private String mImage_User_objectId;
    //private String mComment_User_Name;
    //private String mComment_Date;
    //private String mComment_Body;
    private Boolean mImage_IsLiked;
    private int mNo_of_Likes;
    private int mNo_of_Comments;
    private ProgressDialog mDialogImageLoad;
    private TextView mLoadingCaption;
    private TextView mTxtLikes;
    private TextView mTxtComments;
    private ImageButton mBtnLikes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_image_secondary);
        Initialize();
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        loadandUpdateImageLkesComments();
        updateUI();
        checkIfYouLiked();
        loadComments();
    }

    private void Initialize(){
        App appState = ((App) getApplicationContext());
        mPost_objectId = appState.getGlobalFeedID();
        mPlayerID = appState.getPlayerID();
        mPlayer_objectId = appState.getPlayer_objectId();
        mLoadingCaption = (TextView) findViewById(R.id.lbl_loading_images);

        mDialogImageLoad = new ProgressDialog(this);
        mDialogImageLoad.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialogImageLoad.setMessage("Saving your comment..."); //TODO
        mDialogImageLoad.setIndeterminate(true);
        mDialogImageLoad.setCanceledOnTouchOutside(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPost_objectId = extras.getString("Post_ObjectID");
            mImage_objectId = extras.getString("Image_ObjectID");
            mImage_User_objectId = extras.getString("User_ObjectID");
            //mComment_User_Name = extras.getString("User_Name");
            //mComment_Date = extras.getString("Comment_Date");
            //mComment_Body = extras.getString("Comment_Body");
            mNo_of_Comments = extras.getInt("Comment_Comments");
            mNo_of_Likes = extras.getInt("Comment_Likes");
            //mComment_IsLiked = extras.getBoolean("Comment_IsLiked");
        }
    }

    private void loadandUpdateImageLkesComments(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        query.whereEqualTo("objectId", mImage_objectId);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size()>0)
                        updateLikesComments(scoreList.get(0).getInt("No_of_Likes"),
                                scoreList.get(0).getInt("No_of_Comments"));


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updateLikesComments(int countLikes, int countComments){
        mTxtLikes.setText(String.valueOf(countLikes));
        mTxtComments.setText(String.valueOf(countComments));
    }

    private void updateUI(){

        mTxtLikes = (TextView) findViewById(R.id.no_of_likes);
        mTxtComments = (TextView) findViewById(R.id.no_of_comments);
//        mTxtLikes.setText(String.valueOf(mNo_of_Likes));
//        mTxtComments.setText(String.valueOf(mNo_of_Comments));
//        mTxtLikes.setText(0);
//        mTxtComments.setText(0);

        //set ONCLICK listener for the like button
        mBtnLikes = (ImageButton) findViewById(R.id.btn_likes);
        mBtnLikes.setTag(mBtnLikes.getId(), mImage_objectId);
        mBtnLikes.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_action_new));
        mBtnLikes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        likeImage(String.valueOf(view.getTag(view.getId())));

                    }
                });

        ImageView img = (ImageView) findViewById(R.id.iv_image);
        img.setImageBitmap(GlobalPost.LoadPNG(
                mPost_objectId + "_" + mImage_objectId,this));

    }

    private void checkIfYouLiked(){
        if(mNo_of_Likes> 0){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GF_Img_Likes");
            ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Images", mImage_objectId);
            query.whereEqualTo("Image_objectId", obj);
            obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
            query.whereEqualTo("Image_objectId", obj);
            //query.include("User_objectId");
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        if(scoreList.size() > 1){
                            mImage_IsLiked = true;
                            mBtnLikes.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                                    R.drawable.ic_action_newed));
                        }

                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    public void loadComments(){
        //Log.d("PARSE", "Fetching of User in progress");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GF_Img_Cmts");
        ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Images", mImage_objectId);
        query.whereEqualTo("Image_objectId", obj);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList.get(0));
                    updateLVandAdapter(scoreList);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updateLVandAdapter(List<ParseObject> scoreList){

        mCommentsArray = ConvertJSONtoArray(scoreList);

        //Load Comments into ListView
        ArrayList<CommentsHolder> arrayOfUsers = new ArrayList<CommentsHolder>();
        mCommentsAdapter = new CommentsAdapter(this, arrayOfUsers, this);
        mCommentsAdapter.addAll(mCommentsArray);
        ListView listView = (ListView) findViewById(R.id.lvComments);
        listView.setAdapter(mCommentsAdapter);
        listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
    }

    private void likeImage(final String Image_objectId){

        mNo_of_Likes++;
        updateLikesDisplay();

        ParseObject NewPlayer = new ParseObject("GF_Img_Likes");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Master", mPost_objectId));
        NewPlayer.put("Image_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Images", Image_objectId));
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                updateCommentsMaster_Likes(Image_objectId);
                //UpdateParseObjectWithLikes(FeedID);
            }
        });

    }

    private void updateCommentsMaster_Likes(String Image_ObjID){
        //update COMMENTS MASTER table
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        query.whereEqualTo("objectId", Image_ObjID);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size()>0){
                        int NoofLikes = scoreList.get(0).getInt("No_of_Likes");
                        NoofLikes++;
                        scoreList.get(0).put("No_of_Likes", NoofLikes);
                        scoreList.get(0).saveInBackground(new SaveCallback() {
                            public void done(ParseException e) {
                                //LoadPostComments();
                            }
                        });
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void btnClick_SubmitNewComment(View view){
        hideSoftKeyboard();
        mDialogImageLoad.show();
        EditText txtComment = (EditText) findViewById(R.id.txt_new_comment);
        //insert new word into History table
        final ParseObject GlobalFeed_Cmt_Comments = new ParseObject("GF_Img_Cmts");


        GlobalFeed_Cmt_Comments.put("GlobalFeed_objectId", ParseObject.createWithoutData("GlobalFeed_Master", mPost_objectId));
        GlobalFeed_Cmt_Comments.put("Image_objectId",
                ParseObject.createWithoutData("GlobalFeed_Images", mImage_objectId));
        GlobalFeed_Cmt_Comments.put("User_objectId",
                ParseObject.createWithoutData("_User", mPlayer_objectId));
        GlobalFeed_Cmt_Comments.put("Comments", txtComment.getText().toString());

        GlobalFeed_Cmt_Comments.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    mDialogImageLoad.dismiss();
                    mNo_of_Comments++;
                    updateCommentsMaster();
                    loadComments();

                    updateCommentsDisplay();
                    //TODO
                    //addGroupNotifications(mPostID);
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

        //Clear the comment column after inserting a new entry
        txtComment.setText("");


    }

    private void updateCommentsMaster(){
        //update COMMENTS MASTER table
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        query.whereEqualTo("objectId", mImage_objectId);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size()>0){
                        scoreList.get(0).put("No_of_Comments", mNo_of_Comments);
                        scoreList.get(0).saveInBackground();
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updateLikesDisplay(){

        mTxtLikes.setText(String.valueOf(mNo_of_Likes));

    }

    private void updateCommentsDisplay(){

        mTxtComments.setText(String.valueOf(mNo_of_Comments));

    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public ArrayList<CommentsHolder> ConvertJSONtoArray(List<ParseObject> jsonObjects){
        ArrayList<CommentsHolder> GameHistory = new ArrayList<CommentsHolder>();

        try{
            for (int i = 0; i < jsonObjects.size(); i++) {
                //if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)) {
                GameHistory.add(new CommentsHolder(jsonObjects.get(i), this));

                //}
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }

        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    public class CommentsAdapter extends ArrayAdapter<CommentsHolder> {

        private Activity activity;

        public CommentsAdapter(Context context, ArrayList<CommentsHolder> users, Activity act) {
            super(context, 0, users);
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            CommentsHolder user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_comments_secondary, parent, false);
            }

            // Populate the Post'er's Image and Name
            TextView userName = (TextView) convertView.findViewById(R.id.txt_PlayerId);
            TextView comment = (TextView) convertView.findViewById(R.id.txt_Comment);
            //ParseImageView imgIcon = (ParseImageView) convertView.findViewById(R.id.user_profile_pic);
            ImageView imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);

            try{
                userName.setText(user.getPlayerID());
                comment.setText(user.getComment());
                imgIcon.setImageBitmap(user.loadImageFromStorage(user.getPlayerObjectID(), activity));
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }


            // Return the completed view to render on screen
            return convertView;
        }

    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

//            Bitmap temp = g.loadImageFromStorage(fileName, activity);
//            if (temp != null){
//                return g.loadImageFromStorage(fileName, activity);
//            }
//            else
//            {
//                return null;
//            }
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
