package progettox.mm.newnew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


public class GameSessionDetailsActivity extends ActionBarActivity {



    private List<ParseObject> PlayerHistoryResults;
    private List<PlayerHistory> PlayerHistoryArray;
    public String PlayerID;
    public String Player_objectId;
    public String GameSessionID;
    public String GameName;
    public String mGameSessionID_Notify;
    public String mLastChar;
    public int mWordCount;
    public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    private int mRequestCode = 100;
    private ProgressDialog mDialog;
    public App appState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_session_detalls);


        //Get the bundle
        //Bundle bundle = getIntent().getExtras();

        //Extract the data…
        //PlayerID = bundle.getString("PlayerID");
        //Player_objectId = bundle.getString("Player_objectId");
        //GameSessionID = bundle.getString("SessionID");


        appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();
        appState.setPageNumber(6);

        GameSessionID = appState.getGameSessionID();
        mGameSessionID_Notify = appState.getGameSessionID_Notify();
        mLastChar = appState.getFirstChar();
        mWordCount = appState.getWordCount();
        GameName = appState.getGameSessionName();

        //SET activity Title
        setTitle(GameName);

        //since we're in the games territory, we want to go back to game session page
        //in main menu when user presses back
        appState.setGameNotification(true);

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Retrieving Game Sessions, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
        GetPlayerHistoryData();


        ImageButton playButton = (ImageButton) findViewById(R.id.play_action);
        playButton.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view) {
                goToGamePlayPage();

            }

        });


    }

    @Override
    protected void onResume() {
        super.onResume();  // Alway
        hideSoftKeyboard();
    }
    @Override
    protected void onStart() {
        super.onStart();  // Always call the superclass method first
        hideSoftKeyboard();

        final View activityRootView = findViewById(R.id.activityRoot);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
                    //... do something here
                    hideSoftKeyboard();
                }
            }
        });

        //check if user opened the game which the push notification sent
        if(mGameSessionID_Notify != null){
            if(mGameSessionID_Notify.equals(GameSessionID)){
                appState.setGameSessionID_Notify("");
            }
        }

    }

    public void goToGamePlayPage(){
//        Intent intent = new Intent(this, GamePlayActivity.class);
//        startActivity(intent);


        Intent intent = new Intent(this, GamePlayActivity.class);
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("LastChar", mLastChar);
        bundle.putInt("WordCount", mWordCount);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivityForResult(intent, mRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        //Do your work here in ActivityA
        //getExtras().get("Type"));
//        String message = data.getStringExtra(EXTRA_MESSAGE);
//        if(message != null){
//            if(message.equals("Success")){
//                finish();
//            }
//        }

        if(requestCode == mRequestCode && resultCode == RESULT_OK){
            String editTextString = data.getStringExtra("Status");
            if(editTextString.equals("Success")){
                Intent intent = new Intent();
                intent.putExtra("Status", "WordSuccess");
                setResult(RESULT_OK, intent);
                finish();
            }
            else {


            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerHistoryArray = fromJson(data);

            Log.i("Finally", PlayerHistoryArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerHistory> arrayOfUsers = new ArrayList<PlayerHistory>();
            // Create the adapter to convert the array to views
            UsersAdapter adapter = new UsersAdapter(this, arrayOfUsers);
            adapter.addAll(PlayerHistoryArray);
            // Attach the adapter to a ListView
            ListView listView = (ListView) findViewById(R.id.lvItems);
            listView.setAdapter(adapter);
            mDialog.dismiss();
            hideSoftKeyboard();
            ///////////
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    PlayerHistory listItem = PlayerHistoryArray.get(position);
//                    Log.i("Item selected is ", listItem.GameName);
//                }
//            });
            ///////////


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void GetPlayerHistoryData(){

        Log.d("PARSE", "Fetching of User in progress");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_History");
        query.whereEqualTo("GameSession_ObjectID", GameSessionID);
        Log.d("ObjectId is", GameSessionID);
        query.include("GameSession_objectId");
        //query.addDescendingOrder("createAt");
        //query.orderByAscending("createAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0){
                        PlayerHistoryResults = scoreList;
                        displayJSONResults(scoreList);

                    }
                    else
                    {
                        mDialog.dismiss();
                        //TODO to display textView message saying no entries has been made yet
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerHistory> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerHistory> GameHistory = new ArrayList<PlayerHistory>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerHistory(jsonObjects.get(i), i+1));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //to set the criteria if the player wants to submit a new word
        //GameHistory.get(0).setWordCriteria();
        return GameHistory;
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    public class PlayerHistory {
        public String objectId;
        public String GameSession_ObjectID;
        public String Player_ObjectID;
        public String PlayerID;
        public String Word;
        public int Stars;
        public Date CreatedAt;
        public String GameName;
        public String CurrentPlayerID;
        public int WordCount;
        public int TotalPlayers;
        public String LastCharacter;
        public int Points;
        public int index;

        public void setIndex(int count){
            index = count;
        }

        public void setWordCriteria(){
            mLastChar = this.LastCharacter;
            mWordCount = this.WordCount;
        }

        public PlayerHistory(ParseObject Obj, int c) {
            this.objectId = Obj.getObjectId();
            this.GameSession_ObjectID = Obj.getString("GameSession_ObjectID");
            this.Player_ObjectID = Obj.getString("Player_ObjectID");
            this.PlayerID = Obj.getString("PlayerID");
            this.Word = Obj.getString("Word");
            this.Stars = Obj.getInt("Stars");
            this.Points = Obj.getInt("Points");
            this.CreatedAt = Obj.getCreatedAt();
            this.GameName = Obj.getParseObject("GameSession_objectId").getString("Name");
            this.WordCount = Obj.getParseObject("GameSession_objectId").getInt("WordCount");
            this.TotalPlayers = Obj.getParseObject("GameSession_objectId").getInt("TotalPlayers");
            this.CurrentPlayerID = Obj.getParseObject("GameSession_objectId").getString("CurrentPlayerID");
            this.LastCharacter = Obj.getParseObject("GameSession_objectId").getString("LastChar");
            this.index = c;


        }

    }

    public class UsersAdapter extends ArrayAdapter<PlayerHistory> {
        public UsersAdapter(Context context, ArrayList<PlayerHistory> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            PlayerHistory user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_gamesessiondetails, parent, false);
            }
            // Lookup view for data population
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            TextView wordIndex = (TextView) convertView.findViewById(R.id.wordIndex);
            TextView diamond = (TextView) convertView.findViewById(R.id.Diamond);

            wordIndex.setText(String.valueOf(user.index));
            diamond.setText(String.valueOf(user.Stars));
            // Populate the data into the template view using the data object
            tvName.setText(user.Word);
            //tvName.setHint(user.GameSession_ObjectID);
            String SubHeader = "玩者: " + user.PlayerID + " 分数: " + user.Points;
            tvHome.setText(SubHeader);
            //tvName.setContentDescription(user.GameSession_ObjectID);


            //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
            View PrimaryColumn = convertView.findViewById(R.id.primary_target);
            PrimaryColumn.setTag(PrimaryColumn.getId(), user.objectId);
            //Then we set the onClick function here to pull the GameSession_objectId upon each click
            PrimaryColumn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(MainActivity.this,
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();

                        }
                    });

            View ShareButton = convertView.findViewById(R.id.secondary_action);
            ShareButton.setTag(ShareButton.getId(), user.GameSession_ObjectID);
            ShareButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            String outputText = "Share " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(MainActivity.this,
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                        }
                    });

            // Return the completed view to render on screen
            return convertView;
        }

    }
}
