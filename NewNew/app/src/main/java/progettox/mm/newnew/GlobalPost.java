package progettox.mm.newnew;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 25/5/2015.
 */
public class GlobalPost implements Serializable {
    public String objectId;
    //public ParseFile profilePic; //profile avatar
    public String PlayerID;
    public String PlayerObjectID;
    public String Name;
    public String Message;
    public ArrayList<String> Images;
    private Hashtable<String, Integer> mImage_Likes;
    private Hashtable<String, Integer> mImage_Comments;
    public ArrayList<CommentsHolder> Comments;
    public ArrayList<CommentsHolder> Likes;
    public int noOfLikes;
    public int noOfComments;
    public Date CreatedAt;
    public String CreatedAtDisplay;
    public int noOfImages;
    private int Type;
    private Boolean isLiked;

    //for group
    private Boolean isGroup;
    private String GroupName;
    private String Group_ObjectID;

    //this is to put the header on each group post
    public GlobalPost(String ObjId, String PlayerID, String PlayerObjectID, String GroupName, Boolean isGroup,
                      String Group_ObjId, int type, String Message, String Date, Date createdAt){
        this.objectId = ObjId;
        //this.profilePic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
        this.PlayerID = PlayerID;
        this.PlayerObjectID = PlayerObjectID;
        this.Name = "";
        this.CreatedAt = createdAt;
        this.Message = "";
        this.noOfComments = 0;
        this.noOfImages = 0;
        this.noOfLikes = 0;
        this.Type = type;
        this.isLiked = false;
        this.Message = Message;
        this.CreatedAtDisplay = Date;
        this.isGroup = false;
        if(isGroup) {
            this.GroupName = GroupName;
            this.isGroup = true;
            this.Group_ObjectID = Group_ObjId;

        }


    }

    public GlobalPost(ParseObject Obj, String PlayerID){
        this.objectId = Obj.getObjectId();
        //this.profilePic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
        this.PlayerID = PlayerID;
        this.Name = Obj.getString("Name");
        this.CreatedAt = Obj.getCreatedAt();
        this.Message = Obj.getString("Message");
        this.noOfComments = 0;
        this.noOfImages = 0;
        this.noOfLikes = 0;
        this.isLiked = false;
    }

    public GlobalPost(ParseObject Obj, final Activity activity) {
        this.objectId = Obj.getObjectId();
        //this.profilePic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
        this.PlayerID = Obj.getParseObject("User_objectId").getString("username");
        this.PlayerObjectID = Obj.getParseObject("User_objectId").getObjectId();
        this.Name = Obj.getString("Name");
        this.CreatedAt = Obj.getCreatedAt();
        this.Message = Obj.getString("Message");
        this.noOfComments = 0;
        this.noOfImages = 0;
        this.noOfLikes = 0;
        this.Type = Obj.getInt("Type");
        this.isLiked = false;
        this.mImage_Likes = new Hashtable<>();
        this.mImage_Comments = new Hashtable<>();

        //group
        this.isGroup = Obj.getBoolean("IsGroup");
        if(this.isGroup) {
            this.GroupName = Obj.getParseObject("Group_objectId").getString("Name");
            this.Group_ObjectID = Obj.getParseObject("Group_objectId").getObjectId();

            //load user avatar if it is not in cache
            if(!IsBitmapCached(this.Group_ObjectID, activity)){
                ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Image");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Group_ObjectID, activity);

                        } else {

                        }

                    }
                });
            }

            //load user avatar if it is not in cache
            if(!IsBitmapCached(this.Group_ObjectID + "_avatar", activity)){
                ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Avatar");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Group_ObjectID+"_avatar", activity);

                        } else {

                        }

                    }
                });
            }

        }


        //load user avatar if it is not in cache
        if(!IsBitmapCached(this.PlayerObjectID, activity)){
            ParseFile Picture = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        storeImage(PictureBmp, PlayerObjectID, activity);

                    } else {

                    }

                }
            });
        }

    }

    public Date getCreatedAt(){ return this.CreatedAt;}

    public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    public static Boolean CheckPNGCache(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        //catch (FileNotFoundException e)
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }


    public static void cachePNG(Bitmap bitmapImage, String FileName, Activity activity){
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void StorePNG(Bitmap bitmapImage, String FileName, Activity activity){
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Bitmap LoadPNG(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }

        return null;
    }

    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }

        return null;
    }

    private boolean IsBitmapCached(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        //catch (FileNotFoundException e)
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void insertImageArray(List<ParseObject> Obj, final Activity activity){
        if (noOfImages == 0)
        {
            this.Images = new ArrayList<String>();
        }

        for(int i = 0; i< Obj.size();i++){
            this.Images.add( Obj.get(i).getObjectId());

            //
            //load post images if it is not in cache
            final String Filename = this.objectId + "_" + Obj.get(i).getObjectId();
            if(!IsBitmapCached(Filename ,activity)){
                ParseFile Picture = Obj.get(i).getParseFile("Image");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Filename, activity);

                        } else {

                        }

                    }
                });
            }

            this.noOfImages++;
        }

    }

    public void insertImages(ParseObject Obj, final Activity activity){
        if (noOfImages == 0)
        {
            this.Images = new ArrayList<String>();
        }
        this.Images.add( Obj.getObjectId());

        //
        //load post images if it is not in cache
        final String Filename = this.objectId + "_" + Obj.getObjectId();
        if(!IsBitmapCached(Filename ,activity)){
            ParseFile Picture = Obj.getParseFile("Image");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        storeImage(PictureBmp, Filename, activity);

                    } else {

                    }

                }
            });
        }
        //


        this.noOfImages++;
    }

    public void setImage_NoofLikes(String ImageObjID, int count){
        if(this.mImage_Likes == null)
            this.mImage_Likes = new Hashtable<>();
        this.mImage_Likes.put(ImageObjID, count);
    }

    public void setImage_NoofComments(String ImageObjID, int count){
        if(this.mImage_Comments == null)
            this.mImage_Comments = new Hashtable<>();
        this.mImage_Comments.put(ImageObjID, count);
    }

    public void insertImagesThumbnail(ParseObject Obj, final Activity activity){
        if (noOfImages == 0)
        {
            this.Images = new ArrayList<String>();
        }
        this.Images.add( Obj.getObjectId());

        //get NO OF LIKES and NO OF COMMENTS for each image
        this.mImage_Likes.put(Obj.getObjectId(), Obj.getInt("No_of_Likes"));
        this.mImage_Comments.put(Obj.getObjectId(), Obj.getInt("No_of_Comments"));
        //
        //load post images if it is not in cache
        final String Filename = this.objectId + "_" + Obj.getObjectId() + "_Thumb";
        if(!IsBitmapCached(Filename ,activity)){
            ParseFile Picture = Obj.getParseFile("Image_Thumbnail");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        storeImage(PictureBmp, Filename, activity);

                    } else {

                    }

                }
            });
        }
        //


        this.noOfImages++;
    }

//    public void updateNoOfLikes(int size){
//        this.noOfLikes = size;
//    }
//
//    public void updateNoOfComments(int size){
//        this.noOfComments = size;
//    }

    public void setLiked() {
        this.isLiked = true;
    }

    public void insertLikes(ParseObject Obj, Activity activity, String PlayerObjID){
        if (noOfLikes == 0)
        {
            this.Likes = new ArrayList<CommentsHolder>();
        }
        this.Likes.add( new CommentsHolder(Obj, activity));
        this.noOfLikes++;

        //check if YOU liked it before
        if (Obj.getParseObject("User_objectId").getObjectId().equals(PlayerObjID))
            this.isLiked = true;
    }

    public void clearLikes(){
        this.noOfLikes = 0;
        if(this.Likes != null)
            this.Likes.clear();
    }

    public void clearComments(){
        this.noOfComments = 0;
        if(this.Comments != null)
            this.Comments.clear();
    }

    public void insertComments(ParseObject Obj, Activity activity){
        if (noOfComments == 0)
        {
            this.Comments = new ArrayList<CommentsHolder>();
        }

        String ObjID = Obj.getObjectId();
        for(int i=0;i<this.Comments.size();i++){
            if(ObjID.equals(Comments.get(i).getObjectID())){
                return;
            }
        }

        this.Comments.add( new CommentsHolder(Obj, activity));
        this.noOfComments++;
    }

    public void setNoOfComments(int count){
        this.noOfComments = count;
    }
    public void setNoOfLikes(int count){
        this.noOfLikes = count;
    }

    public void addNewComment(CommentsHolder Obj){
            if (noOfComments == 0)
            {
                this.Comments = new ArrayList<CommentsHolder>();
            }
            this.Comments.add(Obj);
            this.noOfComments++;
        }

    public ArrayList<String> getImages(){
        return this.Images;
    }



    public int getNoOfImages(){
        return this.noOfImages;
    }

    public int getNoOfLikes(){ return this.noOfLikes; }

    public Boolean getIsLiked() {
        if(this.isLiked != null)
            return this.isLiked;
        else
            return false;
    }

    public int getNoOfComments(){
        return this.noOfComments;
//        if(this.Comments != null)
//            return this.Comments.size();
//        else
//            return 0;
    }

    public String getDate(){

        String temp = this.CreatedAt.toString();
        int index = temp.indexOf("GMT");


        return temp.substring(0, index);

    }

    public static String getDateFromCreatedAt(Date date){
        String temp = date.toString();
        int index = temp.indexOf("GMT");


        return temp.substring(0, index);
    }


    public ArrayList<CommentsHolder> getComments(){
        return this.Comments;
    }
    public ArrayList<CommentsHolder> getLikes(){
        return this.Likes;
    }
    public String getUsername(){return this.PlayerID;}
    public String getObjectId() {return this.objectId;}
    public String getPostName(){return this.Name;}
    public String getMessage(){return this.Message;}
    public String getPlayerObjectID(){return this.PlayerObjectID;}
    public String getPlayerID(){return this.PlayerID;}
    public String getGroupName(){return this.GroupName;}
    public Boolean getIsGroup(){return this.isGroup;}
    public String getGroup_ObjectID(){return this.Group_ObjectID;}
    public int getPostType(){return this.Type;}
    public int getImage_NoOfLikes(String image_ObjID) {return mImage_Likes.get(image_ObjID);}
    public int getImage_NoOfComments(String image_ObjID) {return mImage_Comments.get(image_ObjID);}

    GlobalPost(){

    }

//        public ArrayList<PlayerHistory> fromJson(List<ParseObject> jsonObjects) {
//            ArrayList<PlayerHistory> users = new ArrayList<PlayerHistory>();
//            for (int i = 0; i < jsonObjects.size(); i++) {
////                try {
////                    //users.add(new PlayerHistory(jsonObjects.getJSONObject(i)));
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
//            }
//            return users;
//        }
}
