package progettox.mm.newnew;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseImageView;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;

public class chatListAdapter extends ArrayAdapter<ChatDetailsHolder> {
    private String mUserId;
    private Activity activity;
    private List<ChatDetailsHolder> mMessages;

    public chatListAdapter(Context context, String userId, List<ChatDetailsHolder> messages, Activity act) {
        super(context, 0, messages);
        this.mUserId = userId;
        this.activity = act;
        this.mMessages = messages;
    }

    public void updateReceiptList(List<ChatDetailsHolder> updatedMessages) {
        this.mMessages = updatedMessages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.chat_item, parent, false);
            // final ViewHolder holder = new ViewHolder();

//            ParseImageView imageLeft = (ParseImageView)convertView.findViewById(R.id.ivProfileLeft);
//            ParseImageView imageRight = (ParseImageView)convertView.findViewById(R.id.ivProfileRight);
//            TextView body = (TextView)convertView.findViewById(R.id.tvBody);
            convertView.setTag(convertView.getId());
        }

//        ParseImageView imageLeft = (ParseImageView)convertView.findViewById(R.id.ivProfileLeft);
//        ParseImageView imageRight = (ParseImageView)convertView.findViewById(R.id.ivProfileRight);
        ImageView imageLeft = (ImageView)convertView.findViewById(R.id.ivProfileLeft);
        ImageView imageRight = (ImageView)convertView.findViewById(R.id.ivProfileRight);
        TextView body = (TextView)convertView.findViewById(R.id.tvBody);
        LinearLayout chatBody = (LinearLayout) convertView.findViewById(R.id.layout_chat_body);
        final ImageView chatImage = (ImageView)convertView.findViewById(R.id.ivImage);
        final ChatDetailsHolder message = (ChatDetailsHolder)getItem(position);
        //final ChatDetailsHolder message = mMessages.get(position);


        final boolean isMe = message.getUserObjectID().equals(mUserId);



        // Show-hide image based on the logged-in user.
        // Display the profile image to the right for our user, left for other users.
        int bodyGravity;
        if (isMe) {
            bodyGravity = Gravity.RIGHT;
            imageRight.setVisibility(View.VISIBLE);
            imageLeft.setVisibility(View.INVISIBLE);
            body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            //chatBody.setBackground(activity.getResources().getDrawable(R.drawable.bg_chat_bubble_self));
            chatBody.setGravity(Gravity.END);
            LinearLayout.LayoutParams llp = (LinearLayout.LayoutParams) body.getLayoutParams();
            body.setBackgroundResource(R.drawable.bg_chat_bubble_self);

            llp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
            body.setLayoutParams(llp);

        } else {
            bodyGravity = Gravity.LEFT;
            imageLeft.setVisibility(View.VISIBLE);
            imageRight.setVisibility(View.INVISIBLE);
            body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            //chatBody.setBackground(activity.getResources().getDrawable(R.drawable.bg_chat_bubble_sender));
            chatBody.setGravity(Gravity.START);
            //body.setGravity(Gravity.START);
            //body.setBackgroundResource(R.drawable.bg_chat_bubble_left);
            LinearLayout.LayoutParams llp = (LinearLayout.LayoutParams) body.getLayoutParams();
            body.setBackgroundResource(R.drawable.bg_chat_bubble_sender);

//            llp.setMargins(50, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
            llp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
            body.setLayoutParams(llp);
        }

        //final ParseImageView profileView = isMe ? imageRight : imageLeft;
        final ImageView profileView = isMe ? imageRight : imageLeft;

        profileView.setImageBitmap(message.loadImageFromStorage(message.getUserObjectID(), activity));
        //Picasso.with(getContext()).load(getProfileUrl(message.getProfilePic())).into(profileView);

        //TODO DONT use png as the indicator for image, very BAD practice
        if(message.getBody().equals("*Image")){
            body.setVisibility(View.GONE);
            chatImage.setVisibility(View.VISIBLE);
            final Bitmap image = message.loadImageFromStorage(message.getobjectId(), activity);

            if(image == null)
                this.notifyDataSetChanged();

            chatImage.setImageBitmap(image);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            params.weight = 1.0f;
//            params.gravity = bodyGravity;
//            chatImage.setLayoutParams(params);
//            chatImage.setPadding(50,0,0,0);

            chatImage.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            showFullThumbnail(image);
                            //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                        }
                    });
        }
        else{
            chatImage.setVisibility(View.GONE);
            body.setVisibility(View.VISIBLE);
            body.setText(message.getBody());
        }

        return convertView;
    }

    // Create a gravatar image based on the hash value obtained from userId
    private static String getProfileUrl(final String userId) {
        String hex = "";
        try {
            final MessageDigest digest = MessageDigest.getInstance("MD5");
            final byte[] hash = digest.digest(userId.getBytes());
            final BigInteger bigInt = new BigInteger(hash);
            hex = bigInt.abs().toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "http://www.gravatar.com/avatar/" + hex + "?d=identicon";
    }

    final class ViewHolder {
        public ImageView imageLeft;
        public ImageView imageRight;
        public TextView body;
    }

    public void showFullThumbnail(Bitmap IMG){
        final Dialog nagDialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        ivPreview.setImageBitmap(IMG);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }
}
