package progettox.mm.newnew;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 22/6/2015.
 */
public class globalfeed_main extends AppCompatActivity {
    public String PlayerID;
    public String Player_objectId;
    private View myFragmentView;
    //public List<GlobalPost> mGlobalPost_List;
    //public Hashtable<String, GlobalPost> mGlobalPost_List;
    private Map<String, GlobalPost> mGlobalPost_List = Collections.synchronizedMap(new LinkedHashMap<String, GlobalPost>());
    public GlobalPostAdapter mAdapter;
    //public CommentsAdapter mCommentsAdapter;
    private ProgressDialog mDialogImageLoad;
    public int mScreenHeight;
    public int mScreenWidth;
    private boolean mIsDirty;
    private LoadImageTask mLoadImageTask;
    private LoadImageTaskDrawable mLoadImageTaskDrawable;
    private Button mBtnHome;
    private LinearLayout mBottomFrame;
    private ListView mListView;
    private TextView mLoadingFeedCaption;
    private TextView mLoadingMoreFeedCaption;
    private App mAppState;
    private Date mLastPostDate;
    private String[] mPostObjectIDs;
    private int mTotalFeedCount; //this will keep track of how many feeds the app loads as the user scrolls down and down sumore
    private int mFeedCategory;
    private List<String> mFriendArray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_globalfeed_main);
//        if (getArguments() != null) {
//            PlayerID = getArguments().getString("PlayerID");
//            Player_objectId = getArguments().getString("Player_objectId");
//        }

        mAppState = ((App)getApplicationContext());
        PlayerID = mAppState.getPlayerID();
        Player_objectId = mAppState.getPlayer_objectId();
        mFeedCategory = 0;
        mDialogImageLoad = new ProgressDialog(this);
        mDialogImageLoad.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialogImageLoad.setMessage("Loading your image.");
        mDialogImageLoad.setIndeterminate(true);
        mDialogImageLoad.setCanceledOnTouchOutside(false);

        mLoadingFeedCaption = (TextView) findViewById(R.id.lbl_loading_feed);
        mLoadingMoreFeedCaption = (TextView) findViewById(R.id.lbl_loading_more_feed);

        mBtnHome = (Button) findViewById(R.id.firstPage_action);
        mBtnHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToPostNew();
            }
        });

        mBottomFrame = (LinearLayout) findViewById(R.id.layout_bottom_frame);

        //blank out INVITE and SHARE buttons
        Button mInvite = (Button) findViewById(R.id.secondPage_action);
        Button mShare = (Button) findViewById(R.id.thirdPage_action);
        Button mMore = (Button) findViewById(R.id.fourthPage_action);
        mInvite.setVisibility(View.GONE);
        mMore.setVisibility(View.GONE);

        mShare.setText(getResources().getText(R.string.lbl_feed_refresh));
        mShare.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                LoadGlobalFeedAll();
                mIsDirty = true;

            }
        });

        mTotalFeedCount = 0;

        Bundle bundle = getIntent().getExtras();
        mFeedCategory = bundle.getInt("Feed_Category");
        setTitleName(mFeedCategory);


    }

    private void setTitleName(int Type){
        if(Type == 0)
            setTitle(getResources().getText(R.string.title_globalfeed));
        else if(Type == 1)
            setTitle(getResources().getText(R.string.title_friendfeed));
        else if(Type == 2)
            setTitle(getResources().getText(R.string.title_tutor_feed));
    }

    private void goToPostNew(){
        Intent createNewPost = new Intent(this, createNewGlobalFeed.class);
        createNewPost.putExtra("GroupID", "global");
        createNewPost.putExtra("GroupName", "Global Feed");
        //Type 1 is GLOBAL FEED, Type 2 is GROUP FEED, Type 3 is USER/INDIVIDUAL FEED
        createNewPost.putExtra("Type", 1);
        startActivity(createNewPost);
    }

    public void OpenTargetFeed(String ObjID){
        int Type = mGlobalPost_List.get(ObjID).getPostType();
        //2 is Group
        //if(Type == mAppState.getPostGroupInt()) {
            String Group_objectId = mGlobalPost_List.get(ObjID).getGroup_ObjectID();
            String Group_name = mGlobalPost_List.get(ObjID).getGroupName();
            //TODO check of isTutor Group or not, now is temporarily set to false
//            ((MainMenuActivity)getActivity()).GoToFeedGroup(Group_objectId, Group_name,
//                    true, false);
            Intent intent = new Intent(this, activity_feed_targeted.class);
            intent.putExtra("GroupObjectID", Group_objectId);
            intent.putExtra("GroupName", Group_name);
            intent.putExtra("IsGroup", true);
            if(Type == 6)
                intent.putExtra("IsTutor", true);
            else
                intent.putExtra("IsTutor", false);
            startActivity(intent);
//        }
//        //3 is  friend
//        else if(Type == 3){
//
//        }
    }


    public void likeFeed(final String FeedID){

        //updat display first to let user know it is responsive
        mGlobalPost_List.get(FeedID).noOfLikes++;
        mGlobalPost_List.get(FeedID).setLiked();
        updateGlobalFeedAdapter();

        ParseObject NewPlayer = new ParseObject("GlobalFeed_Likes");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Master", FeedID));
        NewPlayer.put("GlobalFeed_ObjectID", FeedID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
                //UpdateParseObjectWithLikes(FeedID);
            }
        });
    }

    public void OpenFeedDetails(String FeedID){
        //((MainMenuActivity)getActivity()).GotoGlobalFeedDetails(FeedID);
        App appState = ((App)getApplicationContext());
        appState.setGlobalFeedID(FeedID);

        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, globalfeed_secondary_activity.class);
        bundle.putString("Feed_ObjectID", FeedID);
        bundle.putString("User_Name", mGlobalPost_List.get(FeedID).getPlayerID());
        bundle.putString("User_ObjectID", mGlobalPost_List.get(FeedID).getPlayerObjectID());
        bundle.putString("Message", mGlobalPost_List.get(FeedID).getMessage());
        bundle.putInt("Type", mGlobalPost_List.get(FeedID).getPostType());
        bundle.putBoolean("IsGroup", mGlobalPost_List.get(FeedID).getIsGroup());
        bundle.putString("Group_Name", mGlobalPost_List.get(FeedID).getGroupName());
        bundle.putString("Group_ObjectID", mGlobalPost_List.get(FeedID).getGroup_ObjectID());
        bundle.putString("CreatedAtDisplay", mGlobalPost_List.get(FeedID).getDate());
        bundle.putSerializable("CreatedAt", mGlobalPost_List.get(FeedID).getCreatedAt());

        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void OpenUserFeedPage(String Feed_objectId){
//        ((MainMenuActivity)getActivity()).OpenUserFeedPage(
//                mGlobalPost_List.get(Feed_objectId).getPlayerID(),
//                mGlobalPost_List.get(Feed_objectId).getPlayerObjectID());
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", mGlobalPost_List.get(Feed_objectId).getPlayerObjectID());
        intent.putExtra("Player_Name", mGlobalPost_List.get(Feed_objectId).getPlayerID());
        intent.putExtra("IsGroup", false);
        startActivity(intent);

    }

    public void btnClick_GotoFeedDetails(){

    }

    private boolean checkDeviceCache(){

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        //SharedPreferences.Editor ed = sp.edit();
//        ed.putString("mHTChatItemList", "");
//        ed.apply();

        //check if we have cache of CHAT data
        String JSON = sp.getString("mGlobalPost_List", "");
        if(JSON == null || JSON.equals("")){
            //here we start to load and populate the user's chat data and history
            //preLoadFeedData();
            return false;
        }

        return true;
    }

    public class FeedComparator implements Comparator<GlobalPost>
    {
        public int compare(GlobalPost left, GlobalPost right) {
            //return left.name.compareTo(right.name);
            return left.getCreatedAt().compareTo(right.getCreatedAt());
        }
    }

    private void preLoadFeedData(){
        //here we just reload the JSON into our hashTable item
        //mGlobalPost_List = new List<GlobalPost>();
        Gson gson = new Gson();
        //Type stringStringMap = new TypeToken<Hashtable<String, GlobalPost>>(){}.getType();
        Type stringStringMap = new TypeToken<Map<String, GlobalPost>>(){}.getType();

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        String JSON = sp.getString("mGlobalPost_List", "");
        mGlobalPost_List = gson.fromJson(JSON, stringStringMap);
        mTotalFeedCount = mGlobalPost_List.size();
        //mLastPostDate = mGlobalPost_List.get(mGlobalPost_List.size()-1).getCreatedAt();
        ArrayList<GlobalPost> tempList = new ArrayList<>();
        tempList.addAll(mGlobalPost_List.values());
        if(tempList.size() > 0){
            mLastPostDate = tempList.get(tempList.size()-1).getCreatedAt();
        }


        ArrayList<GlobalPost> arrayOfUsers = new ArrayList<GlobalPost>();
        mAdapter = new GlobalPostAdapter(this, arrayOfUsers, this);
        mAdapter.addAll(mGlobalPost_List.values());

        mListView = (ListView) findViewById(R.id.lvItems);
        mListView.setAdapter(mAdapter);

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

//                if(mLastFirstVisibleItem<firstVisibleItem)
//                {
//                    //Log.i("SCROLLING DOWN","TRUE");
//                    if(mBottomFrame.getVisibility() == View.VISIBLE)
//                        mBottomFrame.setVisibility(View.GONE);
//
//                }
//                if(mLastFirstVisibleItem>firstVisibleItem)
//                {
//                    //Log.i("SCROLLING UP","TRUE");
//                    if(mBottomFrame.getVisibility() == View.GONE)
//                        mBottomFrame.setVisibility(View.VISIBLE);
//                }
                mLastFirstVisibleItem=firstVisibleItem;

                if(mTotalFeedCount > 0){
                    int loadingTriggerCount = mTotalFeedCount - 7;
                    if(firstVisibleItem >= loadingTriggerCount
                            && loadingTriggerCount > 0){
                        mTotalFeedCount+=5;
                        loadGlobalFeedNext();
                    }
                }

            }
        });

    }

    public void checkForFeedUpdates(){

        //check for 1st record in LHM, if the createdAt date is older than the lastest, we grab new ones
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Master");
        query.whereEqualTo("Type", mAppState.getPostGroupInt()); //2
        query.whereEqualTo("Type", mAppState.getPostGlobalInt()); //1
        query.whereEqualTo("Type", mAppState.getPostGroupClosedInt()); //4

        ParseQuery<ParseObject> query1 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> query2 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> query3 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> mainQuery;
        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        query1.whereEqualTo("Type", mAppState.getPostGroupInt()); //2
        query2.whereEqualTo("Type", mAppState.getPostGlobalInt()); //1
        query3.whereEqualTo("Type", mAppState.getPostGroupClosedInt()); //4

        queries.add(query1);
        queries.add(query2);
        queries.add(query3);
        //mainQuery = ParseQuery.or(queries);
//        if(mFeedCategory == 0) {
//            mainQuery = ParseQuery.or(queries);
//        }
        if (mFeedCategory == 1) {
            if(mFriendArray == null) return;
            mainQuery = ParseQuery.getQuery("GlobalFeed_Master");
            mainQuery.whereEqualTo("Type", mAppState.getPostFriendInt());
            mainQuery.whereContainedIn("User_ObjectID", mFriendArray);
        }
        else if (mFeedCategory == 2){
            mainQuery = ParseQuery.getQuery("GlobalFeed_Master");
            mainQuery.whereEqualTo("Type", mAppState.getPostTutor());
        }
        else{
            mainQuery = ParseQuery.or(queries);
        }



        mainQuery.orderByDescending("createdAt");
        mainQuery.setLimit(1);
        //mainQuery.whereLessThan("createdAt", mLastPostDate);
        final Date latestCachedEntryDate = mGlobalPost_List.entrySet().iterator().next().getValue().getCreatedAt();
        final String latestCachcedObjectID = mGlobalPost_List.entrySet().iterator().next().getValue().getObjectId();
        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "The getFirst request failed.");
//                    if(!scoreList.get(0).getCreatedAt().equals(latestCachedEntryDate)){
//                        initiateFeedRefresh();
//                    }
                    if(scoreList.size()> 0){
                        if(!latestCachcedObjectID.equals(scoreList.get(0).getObjectId())){
                            initiateFeedRefresh();
                        }
                    }
                    else
                        mLoadingFeedCaption.setVisibility(View.GONE);


                } else {
                    Log.d("score", "Retrieved the object.");
                }
            }
        });
    }

    private void initiateFeedRefresh(){
        LoadGlobalFeedAll();
        this.mIsDirty = true;
    }

    public void updateComments(String PostID){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        query.whereEqualTo("GlobalFeed_ObjectID", PostID);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updateGlobalNewsFeedObject(scoreList, 3);
                    ;
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void checkForLikesUpdates(){
        for (Map.Entry<String, GlobalPost> entry : mGlobalPost_List.entrySet()) {
            String i = entry.getKey();
            //ArrayList<String> value = entry.getValue();
            // now work with key and value...
            final String PostID = mGlobalPost_List.get(i).getObjectId();

            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Likes");
            query.whereEqualTo("GlobalFeed_ObjectID", PostID);
            query.countInBackground(new CountCallback() {
                @Override
                public void done(int i, ParseException e) {
                    if(mGlobalPost_List.get(PostID) != null){

                        //means there are comments for this post
                        if(i > 0){
                            if(mGlobalPost_List.get(PostID).getLikes() == null){
                                mGlobalPost_List.get(PostID).setNoOfLikes(i);
                                mIsDirty = true;
                                mAdapter.notifyDataSetChanged();
                            }
                            else{
                                if(mGlobalPost_List.get(PostID).getNoOfLikes() != i){
                                    mGlobalPost_List.get(PostID).setNoOfLikes(i);
                                    //updateComments(PostID);
                                    mAdapter.notifyDataSetChanged();
                                    mIsDirty = true;
                                }
                            }

                        }

                    }
                }
            });
        }
    }

    public void checkForCommentUpdates(){

        for (Map.Entry<String, GlobalPost> entry : mGlobalPost_List.entrySet()) {
            String i = entry.getKey();
            //ArrayList<String> value = entry.getValue();
            // now work with key and value...
            final String PostID = mGlobalPost_List.get(i).getObjectId();

            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
            query.whereEqualTo("GlobalFeed_ObjectID", PostID);
            query.countInBackground(new CountCallback() {
                @Override
                public void done(int i, ParseException e) {
                    if(mGlobalPost_List.get(PostID) != null){

                        //means there are comments for this post
                        if(i > 0){
                            if(mGlobalPost_List.get(PostID).getComments() == null){
                                mGlobalPost_List.get(PostID).setNoOfComments(i);
                                mIsDirty = true;
                                mAdapter.notifyDataSetChanged();
                            }
                            else{
                                if(mGlobalPost_List.get(PostID).getNoOfComments() != i){
                                    mGlobalPost_List.get(PostID).setNoOfComments(i);
                                    //updateComments(PostID);
                                    mAdapter.notifyDataSetChanged();
                                    mIsDirty = true;
                                }
                            }

                        }

                    }
                }
            });
        }


    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        retrieveFriendList();
//        if(checkDeviceCache()){
//
//            preLoadFeedData();
//            checkForFeedUpdates();
//            checkForCommentUpdates();
//            checkForLikesUpdates();
//        }else{
            LoadGlobalFeedAll();
            this.mIsDirty = true;

       // }



        App appState = ((App)getApplicationContext());
        appState.setPageNumber(3);

    }

    private void retrieveFriendList(){
        Gson gson = new Gson();
        //Type stringStringMap = new TypeToken<Hashtable<String, GlobalPost>>(){}.getType();
        Type stringStringMap = new TypeToken<List<String>>(){}.getType();

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        String JSON = sp.getString("Friend_List", "");
        mFriendArray = gson.fromJson(JSON, stringStringMap);
    }

    private void loadGlobalFeedNext(){
        //mTotalFeedCount += 5;
        mLoadingMoreFeedCaption.setVisibility(View.VISIBLE);
        ParseQuery<ParseObject> query1 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> query2 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> query3 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> mainQuery;
        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        query1.whereEqualTo("Type", mAppState.getPostGroupInt()); //2
        query2.whereEqualTo("Type", mAppState.getPostGlobalInt()); //1
        query3.whereEqualTo("Type", mAppState.getPostGroupClosedInt()); //4

        queries.add(query1);
        queries.add(query2);
        queries.add(query3);
        //mainQuery = ParseQuery.or(queries);

        if (mFeedCategory == 1) {
            if(mFriendArray == null) return;
            mainQuery = ParseQuery.getQuery("GlobalFeed_Master");
            mainQuery.whereEqualTo("Type", mAppState.getPostFriendInt());
            mainQuery.whereContainedIn("User_ObjectID", mFriendArray);
        }
        else if (mFeedCategory == 2){
            mainQuery = ParseQuery.getQuery("GlobalFeed_Master");
            mainQuery.whereEqualTo("Type", mAppState.getPostTutor());
        }
        else{
            mainQuery = ParseQuery.or(queries);
        }

        mainQuery.setLimit(10);
        mainQuery.whereLessThan("createdAt", mLastPostDate);

        mainQuery.include("User_objectId");
        mainQuery.include("Group_objectId");
        mainQuery.orderByDescending("createdAt");
        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    if(scoreList.size() > 0) {
                        UpdateMasterData(scoreList);
                        mLoadingMoreFeedCaption.setVisibility(View.GONE);
                    }
                    else {
                        mLoadingFeedCaption.setVisibility(View.GONE);
                        mLoadingMoreFeedCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void LoadGlobalFeedAll(){

        mLoadingFeedCaption.setText(getResources().getText(R.string.lbl_feed_loading));
        mLoadingFeedCaption.setVisibility(View.VISIBLE);
        ParseQuery<ParseObject> query1 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> query2 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> query3 = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> mainQuery;
        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        query1.whereEqualTo("Type", mAppState.getPostGroupInt()); //2
        query2.whereEqualTo("Type", mAppState.getPostGlobalInt()); //1
        query3.whereEqualTo("Type", mAppState.getPostGroupClosedInt()); //4

        queries.add(query1);
        queries.add(query2);
        queries.add(query3);

        if (mFeedCategory == 1) {
            if(mFriendArray == null){
                ClearListView();
                mLoadingFeedCaption.setText(getResources().getText(R.string.txt_no_friends));
                return;
            }
            mainQuery = ParseQuery.getQuery("GlobalFeed_Master");
            mainQuery.whereEqualTo("Type", mAppState.getPostFriendInt());
            mainQuery.whereContainedIn("User_ObjectID", mFriendArray);
        }
        else if (mFeedCategory == 2){
            mainQuery = ParseQuery.getQuery("GlobalFeed_Master");
            mainQuery.whereEqualTo("Type", mAppState.getPostTutor());
        }
        else{
            mainQuery = ParseQuery.or(queries);
        }


        mainQuery.setLimit(10);

        mainQuery.include("User_objectId");
        mainQuery.include("Group_objectId");
        mainQuery.orderByDescending("createdAt");
        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");


                    if(scoreList.size() > 0)
                        FeedMasterDataIntoObjects(scoreList);
                    else
                        mLoadingFeedCaption.setVisibility(View.GONE);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void UpdateMasterData(List<ParseObject> data){
        //get last date
        //TODO take care if the rows of data is not 5, will hit if at end of posts

        if(data.size() < 1){
            mTotalFeedCount = mGlobalPost_List.size();
            return;
        }

        mLastPostDate = data.get(data.size()-1).getCreatedAt();

        mPostObjectIDs = new String[data.size()];
        for (int i = 0; i < data.size(); i++) {
            //if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)) {
            mGlobalPost_List.put(data.get(i).getObjectId(),
                    new GlobalPost(data.get(i), this));
            mPostObjectIDs[i] = data.get(i).getObjectId();

            //}
        }

        mTotalFeedCount = mGlobalPost_List.size();

        mAdapter.notifyDataSetChanged();
        LoadPostImages();
    }

    public void ClearListView(){

        mGlobalPost_List.clear();
        ArrayList<GlobalPost> arrayOfUsers = new ArrayList<GlobalPost>();
        mAdapter = new GlobalPostAdapter(this, arrayOfUsers, this);
        mAdapter.addAll(mGlobalPost_List.values());

        mListView = (ListView) findViewById(R.id.lvItems);
        mListView.setAdapter(mAdapter);
    }

    public void FeedMasterDataIntoObjects(List<ParseObject> data){
        //First create the main globalfeed objects
        if(mGlobalPost_List != null)
            mGlobalPost_List.clear();

        mGlobalPost_List = ConvertJSONtoArray(data);
        mTotalFeedCount = mGlobalPost_List.size();

        //get last date
        mLastPostDate = data.get(data.size()-1).getCreatedAt();

        ArrayList<GlobalPost> arrayOfUsers = new ArrayList<GlobalPost>();
        // Create the adapter to convert the array to views
        mAdapter = new GlobalPostAdapter(this, arrayOfUsers, this);
        mAdapter.addAll(mGlobalPost_List.values());

        mListView = (ListView) findViewById(R.id.lvItems);
        mListView.setAdapter(mAdapter);
        LoadPostImages();

        //Next we have to load Any Images that might have been attached

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

//                if(mLastFirstVisibleItem<firstVisibleItem)
//                {
//                    //Log.i("SCROLLING DOWN","TRUE");
//                    if(mBottomFrame.getVisibility() == View.VISIBLE)
//                        mBottomFrame.setVisibility(View.GONE);
//
//                }
//                if(mLastFirstVisibleItem>firstVisibleItem)
//                {
//                    //Log.i("SCROLLING UP","TRUE");
//                    if(mBottomFrame.getVisibility() == View.GONE)
//                        mBottomFrame.setVisibility(View.VISIBLE);
//                }
                mLastFirstVisibleItem=firstVisibleItem;

                if(mTotalFeedCount > 0){
                    int loadingTriggerCount = mTotalFeedCount - 7;
                    if(firstVisibleItem >= loadingTriggerCount
                            && loadingTriggerCount > 0){
                        mTotalFeedCount+=5;
                        loadGlobalFeedNext();
                    }
                }

            }
        });

    }

//    public void FeedCommentsData(){
//
//        ArrayList<CommentsHolder> arrayOfUsers = new ArrayList<CommentsHolder>();
//        mCommentsAdapter = new CommentsAdapter(getActivity(), arrayOfUsers);
//
//    }

    public void LoadPostImages(){

//        String[] ObjIDs = new String[mGlobalPost_List.size()];
//        for(int i = 0; i< ObjIDs.length; i++){
//            Obj
//        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        //ArrayList<ParseObject> obj = new ArrayList<ParseObject>();
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master", "RRoYFVnc7W"));
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master","pFN5oLckJB"));

        //String[] names = {"RRoYFVnc7W", "pFN5oLckJB"};
        query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(mPostObjectIDs));
        query.include("GlobalFeed_Master");
        query.orderByAscending("Index");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updateGlobalNewsFeedObject(scoreList, 1);
                    //LoadPostLikes();

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void LoadPostLikes(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Likes");
        //ArrayList<ParseObject> obj = new ArrayList<ParseObject>();
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master", "RRoYFVnc7W"));
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master","pFN5oLckJB"));

        //String[] names = {"RRoYFVnc7W", "pFN5oLckJB"};
        //query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(names));
        //query.include("GlobalFeed_Master");
        query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(mPostObjectIDs));
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updateGlobalNewsFeedObject(scoreList, 2);
                    //LoadPostComments();
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void LoadPostComments(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        //ArrayList<ParseObject> obj = new ArrayList<ParseObject>();
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master", "RRoYFVnc7W"));
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master","pFN5oLckJB"));

        //String[] names = {"RRoYFVnc7W", "pFN5oLckJB"};
        //query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(names));
        //query.include("GlobalFeed_Master");
        query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(mPostObjectIDs));
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updateGlobalNewsFeedObject(scoreList, 3);
                    ;
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void updateGlobalNewsFeedObject(List<ParseObject> jsonObjects, int type){
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {

                if (type == 1){
                    String ObjID = jsonObjects.get(i).getParseObject("GlobalFeed_objectId").getObjectId();
                    //for (int j = 0; j < mGlobalPost_List.size(); j++){
                    //if(ObjID.equals(mGlobalPost_List.get(j).objectId)){
                    mGlobalPost_List.get(ObjID).insertImagesThumbnail(jsonObjects.get(i), this);
                    //}
                    //}

                }
                else if(type == 2){
                    String ObjID = jsonObjects.get(i).getParseObject("GlobalFeed_objectId").getObjectId();
                    //for (int j = 0; j < mGlobalPost_List.size(); j++){
                    //if(ObjID.equals(mGlobalPost_List.get(j).objectId)){
                    mGlobalPost_List.get(ObjID).insertLikes(jsonObjects.get(i), this, Player_objectId);

                    //}
                    // }

                }
                else if(type == 3){
                    String ObjID = jsonObjects.get(i).getParseObject("GlobalFeed_objectId").getObjectId();
                    //for (int j = 0; j < mGlonbalPost_List.size(); j++){
                    //if(ObjID.equals(mGlobalPost_List.get(j).objectId)){
                    mGlobalPost_List.get(ObjID).insertComments(jsonObjects.get(i), this);
                    // }
                    // }
                }





            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(type == 1) LoadPostLikes();
        if(type == 2) LoadPostComments();
        if(type == 3)  updateGlobalFeedAdapter();
    }

    public void updateGlobalFeedAdapter(){
        mAdapter.clear();
        mAdapter.addAll(mGlobalPost_List.values());
        mAdapter.notifyDataSetChanged();
        mLoadingFeedCaption.setVisibility(View.GONE);
        saveCache();

        //here we can start to cache the news feed object

    }



    public Map<String, GlobalPost> ConvertJSONtoArray(List<ParseObject> jsonObjects){
        //Hashtable<String, GlobalPost> GameHistory = new Hashtable<String, GlobalPost>();
        Map<String, GlobalPost> GameHistory = Collections.synchronizedMap(new LinkedHashMap<String, GlobalPost>());
        mPostObjectIDs = new String[jsonObjects.size()];
        for (int i = 0; i < jsonObjects.size(); i++) {
            //if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)) {
            GameHistory.put(jsonObjects.get(i).getObjectId(),
                    new GlobalPost(jsonObjects.get(i), this));
            mPostObjectIDs[i] = jsonObjects.get(i).getObjectId();

            //}
        }
        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }
//
//    private class InnerWebViewClient extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }
//
//
//    }

    private void DisplayFullImage(Bitmap bmp){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        //Bitmap IMG = GlobalPost.LoadPNG(imgFileName, this);
        ivPreview.setImageBitmap(bmp);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void DisplayFullImage(String imgFileName){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        Bitmap IMG = GlobalPost.LoadPNG(imgFileName, this);
        ivPreview.setImageBitmap(IMG);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void LoadFullImage(ParseObject Obj, final String Filename){

        final Activity act = this;
        //store image into cache
        if(!GlobalPost.CheckPNGCache(Filename, this)){
            ParseFile Picture = Obj.getParseFile("Image");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        GlobalPost.StorePNG(PictureBmp, Filename, act);
                        DisplayFullImage(PictureBmp);

                    } else {

                    }

                }
            });
        }
        else
        {
            DisplayFullImage(Filename);
        }

    }

    public void showFullThumbnail(final String FeedObjID, final String ImageObjID){

        final String Filename = FeedObjID + "_" + ImageObjID;

        if(GlobalPost.CheckPNGCache(Filename, this)){
            DisplayFullImage(Filename);
        }
        else{
            mDialogImageLoad.show();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
            query.whereEqualTo("objectId", ImageObjID);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                        if(scoreList.size()>0)
                            LoadFullImage(scoreList.get(0), Filename);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }




    }

    static class ViewHolder {

        ImageButton btnComments;
        ImageButton btnLikes;
        TextView userName;
        TextView postDate;
        ImageView imgIcon;
        TextView postMessage;
        ImageView[] PostImages = new ImageView[4];
        TextView likes;
        TextView comments;
        LinearLayout groupPost;
        TextView groupName;
    }

    public class GlobalPostAdapter extends ArrayAdapter<GlobalPost> {
        Activity activity;
        public GlobalPostAdapter(Context context, ArrayList<GlobalPost> users, Activity act) {
            super(context, 0, users);
            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final GlobalPost user = getItem(position);
            ViewHolder holder = null;
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_globalfeed_main_v3, parent, false);
                holder = new ViewHolder();
                holder.btnComments = (ImageButton) convertView.findViewById(R.id.btn_comments);
                holder.btnLikes = (ImageButton) convertView.findViewById(R.id.btn_likes);
                holder.userName = (TextView) convertView.findViewById(R.id.user_name);
                holder.imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);
                holder.postMessage = (TextView) convertView.findViewById(R.id.post_message);
                holder.postDate = (TextView) convertView.findViewById(R.id.post_date);
                holder.PostImages[0] = (ImageView) convertView.findViewById(R.id.post_pic1);
                holder.PostImages[1] = (ImageView) convertView.findViewById(R.id.post_pic2);
                holder.PostImages[2] = (ImageView) convertView.findViewById(R.id.post_pic3);
                holder.PostImages[3] = (ImageView) convertView.findViewById(R.id.post_pic4);
                holder.groupPost = (LinearLayout) convertView.findViewById(R.id.layout_group_post);
                holder.groupName = (TextView) convertView.findViewById(R.id.post_target_group);
                holder.likes = (TextView) convertView.findViewById(R.id.no_of_likes);
                holder.comments = (TextView) convertView.findViewById(R.id.no_of_comments);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }

            //Tag the Comments Button
            //Button btnComments = (Button) convertView.findViewById(R.id.btn_comments);
            holder.btnLikes.setTag(holder.btnLikes.getId(), user.objectId);
            if(!user.getIsLiked()){
                try{
                    mLoadImageTaskDrawable = new LoadImageTaskDrawable(holder.btnLikes, R.drawable.ic_action_new, activity);
                    mLoadImageTaskDrawable.execute(holder.btnLikes);
                }
                catch(Exception e){
                    Log.i("Feed_Main", e.getMessage());
                }
                holder.btnLikes.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            likeFeed(String.valueOf(view.getTag(view.getId())));

                            }
                        });
            }
            else
            {
//                holder.btnLikes.setImageBitmap(BitmapFactory.decodeResource(
//                        getResources(), R.drawable.ic_feed_liked));
                try{
                    mLoadImageTaskDrawable = new LoadImageTaskDrawable(holder.btnLikes, R.drawable.ic_action_newed, activity);
                    mLoadImageTaskDrawable.execute(holder.btnLikes);
                }
                catch(Exception e){
                    Log.i("Feed_Main", e.getMessage());
                }

                holder.btnLikes.setClickable(false);

            }





            //Tag the Comments Button
            //Button btnComments = (Button) convertView.findViewById(R.id.btn_comments);
            holder.btnComments.setTag(holder.btnComments.getId(), user.objectId);
            holder.btnComments.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(getActivity(),
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                            OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                        }
                    });

            // Populate the Post'er's Image and Name
            //TextView userName = (TextView) convertView.findViewById(R.id.user_name);
            //ParseImageView imgIcon = (ParseImageView) convertView.findViewById(R.id.user_profile_pic);
            //ImageView imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);
            holder.userName.setText(user.getPlayerID());
            holder.postDate.setText(user.getDate());
            //holder.userName.setTag(holder.userName.getId(), user.getPlayerObjectID());
            holder.userName.setTag(holder.userName.getId(), user.getObjectId());
            holder.userName.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                            //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                            OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                        }
                    });

            try{
//                holder.imgIcon.setImageBitmap(RoundedImageView.getCircleBitmap(
//                        user.loadImageFromStorage(user.getUsername(),
//                                getActivity())));

                mLoadImageTask = new LoadImageTask(holder.imgIcon, user, user.getPlayerObjectID(), activity);
                mLoadImageTask.execute(holder.imgIcon);
                holder.imgIcon.setTag(holder.imgIcon.getId(), user.getObjectId());
                holder.imgIcon.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                                OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                            }
                        });
            }
            catch(Exception e){
                Log.i("Feed_Main", e.getMessage());
            }

            //enable group post alert if this post is a group post
            //TODO instead of using getIsGroup, use getTYPE()
            if(user.getIsGroup()){
                holder.groupPost.setVisibility(View.VISIBLE);
                holder.groupName.setText(user.getGroupName());
                holder.groupName.setTag(holder.groupName.getId(), user.getObjectId());
                holder.groupName.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(getActivity(),
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                                OpenTargetFeed(String.valueOf(view.getTag(view.getId())));

                            }
                        });
            }
            else
            {
                holder.groupPost.setVisibility(View.GONE);
            }

            // Populate the Post's name and message
            holder.postMessage.setTag(holder.postMessage.getId(), user.objectId);
            holder.postMessage.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(getActivity(),
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                            OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                        }
                    });

            holder.postMessage.setText(user.Message);

//            if(user.getNoOfLikes() > 0)
//                //holder.likes.setText(String.valueOf(user.getLikes().size()));
//                holder.likes.setText(String.valueOf(user.noOfLikes));
//            else
//                holder.likes.setText("0");

            holder.likes.setText(String.valueOf(user.getNoOfLikes()));
            holder.comments.setText(String.valueOf(user.getNoOfComments()));
//            if (user.getComments() != null)
//            {
//                //comments.setText(String.valueOf(user.getNoOfComments()));
//                holder.comments.setText(String.valueOf(user.getComments().size()));
//            }
//            else
//            {
//                holder.comments.setText("0");
//            }

            //only load the images if they are present
            if(user.getNoOfImages() > 0){

                for (int i = 0; i<4; i++){
                    holder.PostImages[i].setVisibility(View.GONE);

                }

                for (int i = 0; i<user.getNoOfImages(); i++){
                    //in images preview, only able to preview up to 4 images for now
                    //as i've fixed 4 ImageViews in the layout
                    if(i >= 4){
                        continue;
                    }
                    holder.PostImages[i].setVisibility(View.VISIBLE);
                    final String imageFilename = user.getObjectId() + "_" + user.getImages().get(i) + "_Thumb";
                    final int count = i;
                    //final String imageFilenameFull = user.getObjectId() + "_" + user.getImages().get(i);
                    try{

                        mLoadImageTask = new LoadImageTask(holder.PostImages[i], user, imageFilename, activity);
                        mLoadImageTask.execute(holder.PostImages[i]);


                    }
                    catch(Exception e){
                        Log.i("Feed_Main", e.getMessage());
                    }

                    final ImageView PIM =  holder.PostImages[i];
                    if(PIM.getDrawable() == null){

                    }
                    PIM.setTag(PIM.getId(), user.objectId);
                    PIM.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //TODO to pass in filename, then let the function load the image
                                    showFullThumbnail(user.getObjectId(), user.getImages().get(count));
                                    //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                                }
                            });

                }
            }
            else{
                for (int i = 0; i<4; i++){
                    holder.PostImages[i].setVisibility(View.GONE);

                }

            }

            // Return the completed view to render on screen
            return convertView;
        }

    }

    public class CommentsAdapter extends ArrayAdapter<CommentsHolder> {

        Activity activity;

        public CommentsAdapter(Context context, ArrayList<CommentsHolder> users, Activity act) {
            super(context, 0, users);
            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            CommentsHolder user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_globalfeed_comments, parent, false);
            }

            // Populate the Post'er's Image and Name
            TextView userName = (TextView) convertView.findViewById(R.id.txt_PlayerId);
            TextView comment = (TextView) convertView.findViewById(R.id.txt_Comment);
//            ParseImageView imgIcon = (ParseImageView) convertView.findViewById(R.id.user_profile_pic);
            ImageView imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);
            userName.setText(user.getPlayerID());
            comment.setText(user.getComment());
//            imgIcon.setParseFile(user.getProfilePic());
//            imgIcon.loadInBackground();
            imgIcon.setImageBitmap(user.loadImageFromStorage(user.getPlayerID(), activity));


            // Return the completed view to render on screen
            return convertView;
        }

    }


    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            //saveCache();
        }

    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            //saveCache();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            //saveCache();
        }

    }

    private void saveCache(){
        Gson gson = new Gson();
        //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
        String JSON = gson.toJson(mGlobalPost_List);

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("mGlobalPost_List", JSON);
        ed.apply();

        this.mIsDirty = false;
    }


    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private GlobalPost g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, GlobalPost globalPost, String FileName, Activity act) {
            v = view;
            g = globalPost;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    public class LoadImageTaskDrawable extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private GlobalPost g;
        private int fileName;
        private Activity activity;

        LoadImageTaskDrawable(ImageView view, int imageID, Activity act) {
            v = view;
            fileName = imageID;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            return BitmapFactory.decodeResource(getResources(), fileName);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.menu_global_feed, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.home){
            finish();
            return true;
        }

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_global_feed) {
//            mFeedCategory = 0;
//            setTitleName(mFeedCategory);
//            LoadGlobalFeedAll();
//            mIsDirty = true;
//        }
//        if (id == R.id.action_friend_feed) {
//            mFeedCategory = 1;
//            setTitleName(mFeedCategory);
//            LoadGlobalFeedAll();
//            mIsDirty = true;
//        }
//        if (id == R.id.action_tutor_feed) {
//            mFeedCategory = 2;
//            setTitleName(mFeedCategory);
//            LoadGlobalFeedAll();
//            mIsDirty = true;
//        }

        return super.onOptionsItemSelected(item);
    }
}
