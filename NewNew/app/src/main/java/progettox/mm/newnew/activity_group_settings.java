package progettox.mm.newnew;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 16/6/2015.
 */
public class activity_group_settings extends AppCompatActivity {

    private String mPlayer_objectId;
    private String mPlayer_Name;
    private String mTutor_objectId;
    private String mTutor_Name;
    private Boolean mIsGroup;
    private String mGroupName;
    private Boolean mIsTutor;
    private String mGroup_objectId;
    private String mGroup_description;
    private int mGroup_type;
    private String mGroup_date;
    private int mGroup_noOfMembers;
    private String mGroupCreator_objectID;
    private Button mBtnPersonalMessage;

    private void Initialize() {

        App appState = ((App) getApplicationContext());
        mPlayer_Name = appState.getPlayerID();
        mPlayer_objectId = appState.getPlayer_objectId();

        mIsGroup = false;
        mIsTutor = false;
        mGroup_noOfMembers = 0;

        //group_settings_name
        //group_settings_date
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //mPlayer_objectId = extras.getString("PlayerObjectID");
            //check for group
            Boolean isGroup = extras.getBoolean("IsGroup");
            Boolean isTutor = extras.getBoolean("IsTutor");

            if (isGroup) {
                mIsGroup = true;
                mGroupName = extras.getString("Group_Name");
                mGroup_objectId = extras.getString("Group_ObjectID");
            }

            if (isTutor) {
                mIsTutor = true;
                mGroupName = extras.getString("Group_Name");
                mGroup_objectId = extras.getString("Group_ObjectID");
                mTutor_Name = extras.getString("Tutor_Name");
                mTutor_objectId = extras.getString("Tutor_ObjectID");
            }

            mGroupCreator_objectID = extras.getString("Creator_ObjectID");
            mGroup_description = extras.getString("Group_Desc");
            mGroup_date = extras.getString("Group_Date");
            mGroup_type = extras.getInt("Group_Type");
        }

        //display UI
        TextView groupName = (TextView) findViewById(R.id.group_settings_name);
        TextView groupDesc = (TextView) findViewById(R.id.txt_group_desc);
        TextView groupType = (TextView) findViewById(R.id.txt_group_type);
        TextView groupDate = (TextView) findViewById(R.id.group_settings_date);
        LinearLayout groupMembers = (LinearLayout) findViewById(R.id.layout_group_members);
        groupMembers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToGroupMembersPage();
            }
        });
        LinearLayout adminPanel = (LinearLayout) findViewById(R.id.layout_admin_settings);
        adminPanel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToAdminPanelPage();
            }
        });

        LinearLayout groupImages = (LinearLayout) findViewById(R.id.layout_group_images);
        groupImages.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToGroupImagesPage();
            }
        });

        groupName.setText(mGroupName);
        groupDesc.setText(mGroup_description);
        groupType.setText(appState.getGroupType(mGroup_type));
        groupDate.setText(mGroup_date);

        mBtnPersonalMessage = (Button) findViewById(R.id.btn_open_pm);
        mBtnPersonalMessage.setVisibility(View.GONE);
        if (mIsTutor)
        {
            mBtnPersonalMessage.setVisibility(View.VISIBLE);
            mBtnPersonalMessage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    goToNewPM();
                }
            });
        }



        //constraints
        if(!mPlayer_objectId.equals(mGroupCreator_objectID))
            adminPanel.setVisibility(View.GONE);
    }

    private void goToNewPM(){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("objectId", mGroupCreator_objectID);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    if(objects.size() > 0){

                        GoToNewPMPage(objects.get(0).getUsername());
                    }
                    else {

                    }
                } else {
                    // Something went wrong.
                }
            }
        });


    }

    private void GoToNewPMPage(String name){
        Intent groupMembersActivity = new Intent(this, activity_new_pm.class);
        groupMembersActivity.putExtra("User_ObjectID", mGroupCreator_objectID);
        groupMembersActivity.putExtra("User_Name", name);
        startActivity(groupMembersActivity);

    }

    private void goToGroupImagesPage(){
        Intent groupMembersActivity = new Intent(this, activity_group_images.class);
        groupMembersActivity.putExtra("Group_ObjectID", mGroup_objectId);
        groupMembersActivity.putExtra("Profile_Type", 2);
        startActivity(groupMembersActivity);
    }

    private void goToAdminPanelPage(){
        Intent groupMembersActivity = new Intent(this, activity_group_admin_panel.class);
        groupMembersActivity.putExtra("Group_ObjectID", mGroup_objectId);
        groupMembersActivity.putExtra("Profile_Type", 2);
        startActivity(groupMembersActivity);
    }

    private void goToGroupMembersPage(){
        Intent groupMembersActivity = new Intent(this, activity_group_settings_members.class);
        groupMembersActivity.putExtra("Group_ObjectID", mGroup_objectId);
        groupMembersActivity.putExtra("Profile_Type", 2);
        startActivity(groupMembersActivity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_settings);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        Initialize();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        loadNoOfGroupMembers();
    }

    private void updateNoOfMembers(int count){
        TextView groupNoOfMembers = (TextView) findViewById(R.id.txt_group_totalmembers);
        groupNoOfMembers.setText(String.valueOf(count));
    }

    private void loadNoOfGroupMembers(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        query.whereEqualTo("Group_ObjectID", mGroup_objectId);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                updateNoOfMembers(i);
            }
        });

    }
}
