package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Admin on 28/7/2015.
 */
public class activity_group_join extends AppCompatActivity {

    //private List<Group> mGroupArray;
    private String mPlayer_objectId;
    private String mPlayer_Name;
    private LoadImageTask mLoadImageTask;
    private TextView txtLoadingCaption;
    //private ListView mListView;
    private GroupAdapter mGroupAdapter ;
    public App mAppState;
    private LinearLayout mBottomLayout;
    private Button mBtnFriends;
    private Button mBtnNew;
    private Button mBtnNear;
    private Button mBtnTop;
    private HashMap<String, Group> mGroupArray;
    private ListView mListView;

    //baidu maps api usages
    private static final String TAG = "dzt";
    private LocationClient mLocationClient;
    private LocationClientOption.LocationMode tempMode = LocationClientOption.LocationMode.Battery_Saving;
    private String tempcoor="bd09ll";
    private ParseGeoPoint mGeoLocation;

    private Boolean retrieveGeoLocation(){
        mGeoLocation = mAppState.getGeoLocPoint();
        if(mGeoLocation != null)
            return true;
        else
            return false;
    }

    private void Initialize() {

        mAppState = ((App) getApplicationContext());
        mPlayer_Name = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        //Initialize BAIDU map geo location grab
        mLocationClient = ((App)getApplication()).mLocationClient; // Class LocationClient
        InitLocation();
        mLocationClient.start();
        retrieveGeoLocation();



        mBottomLayout = (LinearLayout) findViewById(R.id.layout_join_panel);
        mBottomLayout.setVisibility(View.VISIBLE);

        mListView = (ListView) findViewById(R.id.lv_groups);
        txtLoadingCaption = (TextView)findViewById(R.id.lbl_loading_caption);

        mBtnFriends = (Button) findViewById(R.id.firstPage_action);
        mBtnFriends.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getGroupList(1);
                        toggleButtonAppearance(1);
                    }
                });

        mBtnNew = (Button) findViewById(R.id.secondPage_action);
        mBtnNew.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getGroupList(2);
                        toggleButtonAppearance(2);
                    }
                });

        mBtnNear = (Button) findViewById(R.id.thirdPage_action);
        mBtnNear.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getGroupList(3);
                        toggleButtonAppearance(3);
                    }
                });

        mBtnTop = (Button) findViewById(R.id.forthPage_action);
        mBtnTop.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getGroupList(4);
                        toggleButtonAppearance(4);

                    }
                });

        toggleButtonAppearance(1);
    }

    private void toggleButtonAppearance(int position){
        if(position == 1){
            mBtnFriends.setTextColor(getResources().getColor(R.color.names));
            mBtnNew.setTextColor(getResources().getColor(R.color.white));
            mBtnNear.setTextColor(getResources().getColor(R.color.white));
            mBtnTop.setTextColor(getResources().getColor(R.color.white));
        }
        else if(position == 2){
            mBtnFriends.setTextColor(getResources().getColor(R.color.white));
            mBtnNew.setTextColor(getResources().getColor(R.color.names));
            mBtnNear.setTextColor(getResources().getColor(R.color.white));
            mBtnTop.setTextColor(getResources().getColor(R.color.white));
        }
        else if(position == 3){
            mBtnFriends.setTextColor(getResources().getColor(R.color.white));
            mBtnNew.setTextColor(getResources().getColor(R.color.white));
            mBtnNear.setTextColor(getResources().getColor(R.color.names));
            mBtnTop.setTextColor(getResources().getColor(R.color.white));
        }
        else if(position == 4){
            mBtnFriends.setTextColor(getResources().getColor(R.color.white));
            mBtnNew.setTextColor(getResources().getColor(R.color.white));
            mBtnNear.setTextColor(getResources().getColor(R.color.white));
            mBtnTop.setTextColor(getResources().getColor(R.color.names));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_groups);
        Initialize();

        //by default is the FRIENDS' GROUP option being selected
        getGroupList(1);



    }

    private void InitLocation(){
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);//���ö�λģʽ
        option.setCoorType(tempcoor);//���صĶ�λ����ǰٶȾ�γ�ȣ�Ĭ��ֵgcj02
        int span=1000;
        try {
            //span = Integer.valueOf(frequence.getText().toString());
        } catch (Exception e) {
            // TODO: handle exception
        }
        option.setScanSpan(span);//���÷���λ����ļ��ʱ��Ϊ5000ms
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
    }

    private void filterGroupList(List<ParseObject> friendList){

        try{
            if(friendList.size() > 0){
                for (int i = 0; i<mGroupArray.size(); i++){
                    for(int j = 0; j<friendList.size();j++){
                        String group_ObjectID = friendList.get(j).getParseObject("Group_objectId").getObjectId();
                        if(mGroupArray.get(group_ObjectID) != null){

                            if(friendList.get(j).getInt("Member_Status") == 1)
                            {
                                //mGroupArray.get(group_ObjectID).setMemberStatus(2);
                                mGroupArray.remove(group_ObjectID);
                            }
                            else
                            {
                                mGroupArray.get(group_ObjectID).setMemberStatus(1);

                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }





        updateListViewAdapter();
    }

    private void getSelfGroupList(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        query.whereEqualTo("Member_ObjectID", mPlayer_objectId);
        //query.whereEqualTo("Member_Status", 1);
        query.include("Group_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    //if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        filterGroupList(groupList);
//                    }
//                    else
//                    {
//                        txtLoadingCaption.setVisibility(View.GONE);
//                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getGroupList(int type){

        txtLoadingCaption.setVisibility(View.VISIBLE);
        //mListView.setEnabled(false);
        mListView.setVisibility(View.INVISIBLE);
        if(type == 4){

            ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
            ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
            query.whereEqualTo("Player_objectId", obj);
            query.whereEqualTo("Status", 1);
            query.include("Friend_objectId");

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> friendList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + friendList.size() + " scores");
                        //PlayerHistoryResults = scoreList;

                        if(friendList.size()>0)
                            getGroupsOne(friendList);
                        else
                            txtLoadingCaption.setVisibility(View.GONE);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
        else if(type == 2 || type == 1){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Master");
            query.orderByDescending("createdAt");
            //TODO to implement when SMART LOADING comes in
            //query.setLimit(25);

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> friendList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + friendList.size() + " scores");
                        //PlayerHistoryResults = scoreList;

                        if(friendList.size()>0)
                            getGroupsTwo(friendList);
                        else
                            txtLoadingCaption.setVisibility(View.GONE);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
        else if(type == 3){
            if(retrieveGeoLocation()){
                ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Master");
                query.whereNear("GeoLocation", mGeoLocation);
                //TODO to implement when SMART LOADING comes in
                //query.setLimit(25);

                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> friendList, ParseException e) {
                        if (e == null) {
                            Log.d("score", "Retrieved " + friendList.size() + " scores");
                            //PlayerHistoryResults = scoreList;

                            if(friendList.size()>0)
                                getGroupsTwo(friendList);
                            else
                                txtLoadingCaption.setVisibility(View.GONE);
                        } else {
                            Log.d("score", "Error: " + e.getMessage());
                        }
                    }
                });
            }

        }





    }

    private void getGroupsOne(List<ParseObject> friendList){

        ArrayList<String> temp = new ArrayList<>();
        for (int i = 0; i < friendList.size(); i++) {
            try {
                temp.add(friendList.get(i).getParseObject("Friend_objectId").getObjectId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        //query.whereEqualTo("Member_ObjectID", mUser_ObjectID);
        query.whereContainedIn("Member_ObjectID", temp);
        query.whereEqualTo("Member_Status", 1);
        query.include("Group_objectId");
        query.include("Member_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        formGroupList(groupList);
                    }
                    else
                    {
                        txtLoadingCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getGroupsTwo(List<ParseObject> friendList){

        ArrayList<String> temp = new ArrayList<>();
        for (int i = 0; i < friendList.size(); i++) {
            try {
                temp.add(friendList.get(i).getObjectId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        //query.whereEqualTo("Member_ObjectID", mUser_ObjectID);
        query.whereContainedIn("Group_ObjectID", temp);
        query.whereEqualTo("Member_Status", 1);
        query.include("Group_objectId");
        query.include("Member_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        formGroupList(groupList);
                    }
                    else
                    {
                        txtLoadingCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void formGroupList(List<ParseObject> groupList){
        mGroupArray = new HashMap<>();

        String group_objectId = "";

        try{
            for(int i=0;i<groupList.size();i++){
                group_objectId = groupList.get(i).getParseObject("Group_objectId").getObjectId();
                if(mGroupArray.get(group_objectId) == null){
                    mGroupArray.put(group_objectId, new Group(groupList.get(i), this));
                    mGroupArray.get(group_objectId).insertNewMember(
                            groupList.get(i).getParseObject("Member_objectId").getObjectId(),
                            groupList.get(i).getParseObject("Member_objectId").getString("username"));
                }
                else{
                    mGroupArray.get(group_objectId).insertNewMember(
                            groupList.get(i).getParseObject("Member_objectId").getObjectId(),
                            groupList.get(i).getParseObject("Member_objectId").getString("username"));
                }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }



        getSelfGroupList();



    }

    private void updateListViewAdapter(){
        ArrayList<Group> arrayOfUsers = new ArrayList<Group>();
        // Create the adapter to convert the array to views
        mGroupAdapter = new GroupAdapter(this, R.layout.group_browser_item, arrayOfUsers, this);
        mGroupAdapter.addAll(mGroupArray.values());
        // Attach the adapter to a ListView
        //ListView listView = (ListView) findViewById(R.id.lv_groups);
        mListView.setAdapter(mGroupAdapter);
        mListView.setEnabled(true);
        //mDialog.dismiss();
        txtLoadingCaption.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
    }

    public static class GroupHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtDesc;
        TextView txtNoOfMembers;
        Button rightButton;
        LinearLayout linearLayout;

    }

    public class GroupAdapter extends ArrayAdapter<Group> {
        private Activity activity;

        public GroupAdapter(Context context, int layoutResourceId, ArrayList<Group> data, Activity act) {
            super(context, layoutResourceId, data);

            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;
            Group item = getItem(position);
            GroupHolder holder = null;

            if (row == null) {

                row = LayoutInflater.from(getContext()).inflate(R.layout.item_individual_groups, parent, false);
                holder = new GroupHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.ivProfileLeft);
                holder.txtTitle = (TextView) row.findViewById(R.id.tvName);
                holder.txtDesc = (TextView) row.findViewById(R.id.tvDesc);
                holder.txtNoOfMembers = (TextView) row.findViewById(R.id.tvMembers);
                holder.rightButton = (Button) row.findViewById(R.id.txt_action_member);
                holder.linearLayout = (LinearLayout) row.findViewById(R.id.ll_group_item);

                row.setTag(holder);

            } else {
                holder = (GroupHolder) row.getTag();
            }

            //set clickable
            holder.linearLayout.setTag(holder.linearLayout.getId(), item);
            //holder.linearLayout.setTag(mGroupNameTag, item.getGroupName());

            holder.linearLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            GoToFeedGroup((Group) view.getTag(view.getId()));

                        }
                    });


            holder.txtTitle.setText(item.getGroupName());
            holder.txtDesc.setText(item.getGroupDescription());
            holder.txtNoOfMembers.setText(String.valueOf(item.getTotalNoOfMembersHT()) + " " +
                    getResources().getText(R.string.lbl_members));

            try {

                mLoadImageTask = new LoadImageTask(holder.imgIcon, item.getObjectID(), activity);
                mLoadImageTask.execute(holder.imgIcon);


            } catch (Exception e) {
                Log.i("Feed_Main", e.getMessage());
            }

            try{
                if (item.getMemberStatus() == 2) {
                    holder.rightButton.setVisibility(View.GONE);
                    holder.rightButton.setClickable(false);
                } else if (item.getMemberStatus() == 1) {
                    holder.rightButton.setVisibility(View.VISIBLE);
                    holder.rightButton.setText(getResources().getText(R.string.btn_joined_group));
                    holder.rightButton.setTextColor(getResources().getColor(R.color.inputText));
                    holder.rightButton.setBackgroundColor(getResources().getColor(R.color.white));
                    holder.rightButton.setClickable(false);
                } else if (item.getMemberStatus() == 0) {
                    holder.rightButton.setVisibility(View.VISIBLE);
                    holder.rightButton.setText(getResources().getText(R.string.btn_group_join));
                    holder.rightButton.setTag(holder.rightButton.getId(), item);
                    holder.rightButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView temp = (TextView) view;
                            temp.setText(getResources().getText(R.string.btn_joined_group));
                            temp.setClickable(false);
                            temp.setTextColor(getResources().getColor(R.color.inputText));
                            temp.setBackgroundColor(getResources().getColor(R.color.white));
                            joinGroup((Group) view.getTag(view.getId()));
                        }
                    });


                }
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }


            return row;
        }

    }

    public void GoToFeedGroup(Group Obj){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("GroupObjectID", Obj.getObjectID());
        intent.putExtra("GroupName", Obj.getGroupName());
        intent.putExtra("IsGroup", true);
        intent.putExtra("IsTutor", Obj.getIsTutorGroup());
        startActivity(intent);
    }

    private void joinGroup(final Group Obj){

        final ParseObject gameScore = new ParseObject("GlobalFeed_Group_Members");
        gameScore.put("Group_ObjectID", Obj.getObjectID());
        gameScore.put("Group_objectId", ParseObject.createWithoutData("GlobalFeed_Group_Master",
                Obj.getObjectID()));
        gameScore.put("Member_ObjectID", mPlayer_objectId);
        gameScore.put("Member_objectId", ParseObject.createWithoutData("_User",
                mPlayer_objectId));
        gameScore.put("Member_Status", 0);
        gameScore.put("IsNotify", true); //by default, group notifications are set to ON
        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
                sendJoinGroupNotification(Obj.getCreatorObjectID(), Obj.getObjectID());
            }
        });
    }

    private void sendJoinGroupNotification(String groupCreator, String GroupObjID){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", groupCreator));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 7);
        NewPlayer.put("IsAccepted", false); //By default is False
        NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", GroupObjID));
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });


    }


    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

//            Bitmap temp = g.loadImageFromStorage(fileName, activity);
//            if (temp != null){
//                return g.loadImageFromStorage(fileName, activity);
//            }
//            else
//            {
//                return null;
//            }
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
