package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 1/7/2015.
 */
public class activity_user_profile extends AppCompatActivity{

    private String mPlayer_objectId;
    private String mPlayer_Name;
    private static final int CAMERA_REQUEST = 1888;
    private static final int AttachFileRequestCode = 2888;
    private Bitmap mUserPhoto;
    private ImageView userProfilePic;
    private ParseFile mTempAvatar;
    private ProgressDialog mDialog;
    private ParseFile mUserPic_Thumbnail;
    private ParseFile mUserPic_Fullsize;
    static final int REQUEST_TAKE_PHOTO = 1;
    private String mCurrentPhotoPath;
    public int mScreenHeight;
    public int mScreenWidth;
    private App mAppState;
    private LinearLayout LayoutUsernameChange;
    private EditText txtNewName;
    private Button btnSaveName;
    private TextView userName;

    private void Initialize(){

        mAppState = ((App)getApplicationContext());
        mPlayer_Name = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Updating your profile pic.");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        userProfilePic = (ImageView) findViewById(R.id.user_profile_pic);
        userName = (TextView) findViewById(R.id.lbl_my_name);

        userName.setText(mPlayer_Name);
        userProfilePic.setImageBitmap(GlobalPost.LoadPNG(mPlayer_objectId + "_Full", this));

        txtNewName = (EditText) findViewById(R.id.txt_new_username);
        btnSaveName = (Button) findViewById(R.id.btn_save_username);
        btnSaveName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                saveNewusernametoParse();
            }
        });

        LayoutUsernameChange = (LinearLayout) findViewById(R.id.layout_username);
        LayoutUsernameChange.setVisibility(View.GONE);

        Button changeUsername = (Button) findViewById(R.id.btn_change_username);
        changeUsername.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EnableChangeNamelayout();

            }
        });

        ImageButton cameraBtn = (ImageButton) findViewById(R.id.camera_action);
        cameraBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                dispatchTakePictureIntent();
                //testLogin();
            }
        });

        ImageButton attachBtn = (ImageButton) findViewById(R.id.attach_action);
        attachBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent,AttachFileRequestCode);
            }
        });

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;

    }

    private void EnableChangeNamelayout(){
        LayoutUsernameChange.setVisibility(View.VISIBLE);

    }

    private void saveNewusernametoParse(){
        final String newUID = txtNewName.getText().toString();

        ParseUser user = ParseUser.getCurrentUser();
        user.put("username", newUID);
        user.put("Canonical_username", newUID.toLowerCase());
        user.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                // If successful add file to user and signUpInBackground
                if(null == e){
                    txtNewName.setText("");
                    LayoutUsernameChange.setVisibility(View.GONE);
                    userName.setText(newUID);
                    mAppState.setPlayerID(newUID);
                }

            }
        });
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        if(storageDir.isDirectory()){
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );


        }
        else{
            storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        }
// Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.getAbsolutePath();
        galleryAddPic();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //...
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        if(mUserPhoto != null){

            //savedInstanceState.putBitmapArrayList("PostImage",mUserPhotoArray);
            savedInstanceState.putParcelable("PostImage", mUserPhoto);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Initialize();
        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            //Bitmap bmp = savedInstanceState.getParcelable("PostImage");
            Bitmap temp = savedInstanceState.getParcelable("PostImage");
            if(temp != null){
                mUserPhoto = temp;
                //imageView.setImageBitmap(mUserPhoto);
            }

        } else {
            // Probably initialize members with default values for a new instance
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            mDialog.show();
//            Bitmap UserPhoto = (Bitmap) data.getExtras().get("data");
//            userProfilePic.setImageBitmap(UserPhoto);
//            updateUserAvatar(UserPhoto);

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(mCurrentPhotoPath);
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

            File file = new File(mCurrentPhotoPath);

            if(file.exists()){

                //int imageWidth = mScreenWidth * 0.8;
                Bitmap UserPhoto = BitmapFactory.decodeFile(file.getAbsolutePath());
                int oriImageWidth =  UserPhoto.getWidth();
                int oriImageHeight = UserPhoto.getHeight();

                int imageWidth = UserPhoto.getWidth();
                int imageHeight = UserPhoto.getHeight();
                //check if photo exceeds
                if(UserPhoto.getWidth() >= mScreenWidth){
                    Double imageWidthDb = mScreenWidth * 0.8;
                    Double imageHeightDb = UserPhoto.getHeight() * 0.8;
                    if(imageHeightDb >= mScreenHeight){
                        imageHeightDb = mScreenHeight * 0.8;

                    }
                    imageWidth = imageWidthDb.intValue();
                    imageHeight = imageHeightDb.intValue();
//                    UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                            file.getAbsolutePath(), imageWidth, imageHeight);


                }

                BitmapFactory.Options bounds = new BitmapFactory.Options();
                bounds.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), bounds);

                BitmapFactory.Options opts = new BitmapFactory.Options();
                //Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                //BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                opts.inSampleSize = BitmapManager.calculateInSampleSize(oriImageWidth, oriImageHeight,
                        imageWidth, imageHeight);

                opts.inJustDecodeBounds = false;
                //return BitmapFactory.decodeResource(res, resId, options);
                Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
//                Bitmap bm = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                            file.getAbsolutePath(), imageWidth, imageHeight);

                ExifInterface exif = null;
                try{
                    exif = new ExifInterface(file.getAbsolutePath());
                }
                catch(Exception e){

                }

                String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

                int rotationAngle = 0;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

                Matrix matrix = new Matrix();
                matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
                //UserPhoto = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
                UserPhoto = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                userProfilePic.setImageBitmap(UserPhoto);

                //add to Parse background as well
                saveParseImagesToServerCache(UserPhoto, file.getAbsolutePath());

            }
        }

        if (requestCode == AttachFileRequestCode && resultCode == RESULT_OK) {

            mDialog.show();
//            Uri selectedImageUri = data.getData();
//            String[] fileColumn = {MediaStore.Images.Media.DATA};
//            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
//                    null, null, null);
//            imageCursor.moveToFirst();
//
//            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
//            String picturePath = imageCursor.getString(fileColumnIndex);
//
//            Bitmap UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(), picturePath, 400, 400);
//            userProfilePic.setImageBitmap(UserPhoto);
//            updateUserAvatar(UserPhoto);

            Uri selectedImageUri = data.getData();
            String[] fileColumn = {MediaStore.Images.Media.DATA};
            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
                    null, null, null);
            imageCursor.moveToFirst();

            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
            String picturePath = imageCursor.getString(fileColumnIndex);

            Bitmap UserPhoto = BitmapFactory.decodeFile(picturePath);
            //check if photo exceeds
            if(UserPhoto.getWidth() >= mScreenWidth){
                Double imageWidthDb = mScreenWidth * 0.8;
                Double imageHeightDb = UserPhoto.getHeight() * 0.8;
                if(imageHeightDb >= mScreenHeight){
                    imageHeightDb = mScreenHeight * 0.8;

                }

                int imageWidth = imageWidthDb.intValue();
                int imageHeight = imageHeightDb.intValue();
                UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
                        picturePath, imageWidth, imageHeight);


            }
            userProfilePic.setImageBitmap(UserPhoto);

            //add to Parse background as well
            saveParseImagesToServerCache(UserPhoto, picturePath);

        }
    }

    private void saveParseImagesToServerCache(final Bitmap imgFull, String ImagePath){

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        imgFull.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] dataStream = stream.toByteArray();

        String imgFileName = "Image";
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        mUserPic_Fullsize = new ParseFile (imgFileName + ".jpg", dataStream);
        mUserPic_Fullsize.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                // If successful add file to user and signUpInBackground
                if(null == e){
                    savetoParseFull(imgFull);
                }

            }
        });

        //prepare the thumbnail image
        final Bitmap bm = BitmapManager.decodeSampledBitmapFromPath(ImagePath, 100, 100, this);

        ExifInterface exif = null;
        try{
            exif = new ExifInterface(ImagePath);
        }
        catch(Exception e){

        }

        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        //UserPhoto = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        final Bitmap temp = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);



        stream = new ByteArrayOutputStream();
        temp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        dataStream = stream.toByteArray();

        //String imgFileName = "Image";
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        mUserPic_Thumbnail = new ParseFile (imgFileName + ".jpg", dataStream);
        mUserPic_Thumbnail.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                // If successful add file to user and signUpInBackground
                if(null == e){
                    savetoParseThumb(temp);
                }

            }
        });
//        ArrayList<ParseFile> mParsePhotoArray = new ArrayList<>();
//        mParsePhotoArray.add(mUserPic_Fullsize);
//        mParsePhotoArray.add(mUserPic_Thumbnail);

    }

    private void saveImageThumbnail(String Filename){
        Bitmap temp = BitmapManager.decodeSampledBitmapFromPath(Filename, 100, 100, this);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        temp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] dataStream = stream.toByteArray();

        String imgFileName = "Image";
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        mUserPic_Thumbnail = new ParseFile (imgFileName + ".jpg", dataStream);
        mUserPic_Thumbnail.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                // If successful add file to user and signUpInBackground
                if(null == e){

                }

            }
        });



    }

//    private void updateUserAvatar(final Bitmap img){
//        String Username = mPlayer_Name;
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        //Bitmap tempAvatar = mUserPhoto;
//        img.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        byte[] data = stream.toByteArray();
//        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
//        mTempAvatar = new ParseFile(Username + ".png", data);
//        mTempAvatar.saveInBackground(new SaveCallback() {
//            public void done(ParseException e) {
//                // If successful add file to user and signUpInBackground
//                if(null == e)
//                    savetoParse(img);
//            }
//        });
//    }

    private void savetoParseThumb(Bitmap img){

        mDialog.dismiss();
        ParseUser user = ParseUser.getCurrentUser();
        //ParseUser user = ParseUser.
        user.put("Avatar_Pic", mUserPic_Thumbnail);
        user.saveInBackground();

        GlobalPost.StorePNG(img, mPlayer_objectId, this);
        userProfilePic.setImageBitmap(img);

    }

    private void addNewPost(){
        //final Activity act = this;

        //insert new word into History table
        final ParseObject gameScore = new ParseObject("GlobalFeed_Master");
        gameScore.put("Message", getResources().getText(R.string.lbl_new_profile_pic));
        gameScore.put("User_ObjectID", mPlayer_objectId);
        gameScore.put("User_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));

        gameScore.put("Friend_ObjectID", mPlayer_objectId);
        gameScore.put("Friend_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        gameScore.put("IsGroup", false);
        gameScore.put("Type", mAppState.getPostFriendInt());

        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    AddImagesToParse(gameScore.getObjectId());
                    getUserFriendList(gameScore.getObjectId());
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });
    }

    private void AddImagesToParse(final String ObjectId){

        final List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        ParseObject NewPlayer;

        for(int i = 0; i< 1;i++){
            NewPlayer = new ParseObject("GlobalFeed_Images");
            NewPlayer.put("GlobalFeed_ObjectID", ObjectId);
            NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData("GlobalFeed_Master",
                    ObjectId)); //same for all
            NewPlayer.put("Submitter_objectId", ParseObject.createWithoutData("_User",
                    mPlayer_objectId));
            NewPlayer.put("Index", i);
            NewPlayer.put("Image", mUserPic_Fullsize);
            NewPlayer.put("Image_Icon", mUserPic_Fullsize);
            NewPlayer.put("Image_Thumbnail", mUserPic_Thumbnail);


            NewPlayer.put("Member_objectId", ParseObject.createWithoutData("_User",
                    mPlayer_objectId)); //same for all

            PlayerList.add(NewPlayer);
        }

        final Activity act = this;

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if(e == null){
                    //do something
                    Log.i("Add new notifications", "SUCCESS!");
                    //mGlobalPost_List.get(ObjectId).insertImageArray(PlayerList, act);
                    //mDialog.dismiss();
                    //cacheGlobalPost();
                    //finish();

                }
                else
                {
                    Log.i("Save Image Error", e.getMessage());
                }


            }
        });

    }

    private void getUserFriendList(final String FeedObjID){
        //get friendList
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    SendFriendsNotification(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


    }

    private void SendFriendsNotification(List<ParseObject> friendList){

        List<ParseObject> FriendList = new ArrayList<ParseObject>();
        //ArrayList<ParseObject> FriendList = new ArrayList<>();
        ParseObject NewPlayer;
        for(int i = 0; i < friendList.size();i++){
            NewPlayer = new ParseObject("Notifications");

            //NewPlayer.put("Group_ObjectID", GroupID);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", friendList.get(i).getParseObject("Friend_objectId").getObjectId()));
            NewPlayer.put("IsSeen", false);
            NewPlayer.put("Type", 11);
            NewPlayer.put("IsAccepted", false); //By default is False
//            NewPlayer.put("Feed_objectId", ParseObject.createWithoutData(
//                    "GlobalFeed_Master", FeedObjID));
            NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                    "_User", mPlayer_objectId));
            FriendList.add(NewPlayer);
        }


        ParseObject.saveAllInBackground(FriendList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new notifications", "SUCCESS!");

            }
        });
    }

    private void savetoParseFull(Bitmap img){

        mDialog.dismiss();
        ParseUser user = ParseUser.getCurrentUser();
        //ParseUser user = ParseUser.
        user.put("Avatar_Pic_Full", mUserPic_Fullsize);
        user.saveInBackground();

        String Filename = mPlayer_objectId + "_Full";
        GlobalPost.StorePNG(img, Filename, this);
        userProfilePic.setImageBitmap(img);
        addNewPost();
    }

    private void completeUpdateAvatar(){
        Toast.makeText(this,
                "User Image updated successfully",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
    //TODO to change to "_FULL"
        userProfilePic.setImageBitmap(GlobalPost.LoadPNG(mPlayer_objectId, this));
    }
}
