package progettox.mm.newnew;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class CreateNewGameActivity extends ActionBarActivity {



    private List<ParseObject> PlayerHistoryResults;
    private List<PlayerHistory> PlayerHistoryArray;
    public String PlayerID;
    public String Player_objectId;
    public String GameSessionID;
    public String mLastChar;
    public int mWordCount;
    public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    private int mRequestCode = 100;
    public Spinner mSpinner_letter;
    public Spinner mSpinner_turnInterval;
    public Spinner mSpinner_timeInterval;
    public EditText mGameName;
    public App appState;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_game_activity);

        //Get the bundle
        //Bundle bundle = getIntent().getExtras();

        //Extract the data…
        //PlayerID = bundle.getString("PlayerID");
        //Player_objectId = bundle.getString("Player_objectId");
        //GameSessionID = bundle.getString("SessionID");

        appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();

        populateSpinners();

        //
        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Retrieving Game Sessions, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        mGameName = (EditText) findViewById(R.id.txt_gameName);


    }

    private void populateSpinners(){
        //populate spinner with No of Letters
        mSpinner_letter = (Spinner) findViewById(R.id.spinner_letter);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_spinner_letter = ArrayAdapter.createFromResource(this,
                R.array.letters_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_spinner_letter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mSpinner_letter.setAdapter(adapter_spinner_letter);

        //populate spinner with No of Turn Intervals
        mSpinner_turnInterval = (Spinner) findViewById(R.id.spinner_turnInterval);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_spinner_turnInterval = ArrayAdapter.createFromResource(this,
                R.array.turnInterval_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_spinner_turnInterval.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mSpinner_turnInterval.setAdapter(adapter_spinner_turnInterval);

        //populate spinner with No of Time Intervals
        mSpinner_timeInterval = (Spinner) findViewById(R.id.spinner_timeInterval);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_spinner_timeInterval = ArrayAdapter.createFromResource(this,
                R.array.timeinterval_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_spinner_timeInterval.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mSpinner_timeInterval.setAdapter(adapter_spinner_timeInterval);


    }

    private void addNewGame(){



        String name = mGameName.getText().toString();
        int noOfLetters = Integer.parseInt(mSpinner_letter.getSelectedItem().toString());
        int turnInterval = Integer.parseInt(mSpinner_turnInterval.getSelectedItem().toString());
        int timeInterval = Integer.parseInt(mSpinner_timeInterval.getSelectedItem().toString());

        //generate random first character for the 1st game
        Random generator = new Random();
        String[] chars = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
                "M", "N", "O", "P", "R", "S", "T", "U", "V", "W", "Y"};
        String firstChar = chars[generator.nextInt(chars.length - 1)];

        //we need to plus 1 here because to account for the game creator
        int noOfPlayers = appState.getSelectedFriendList().length + 1;

        parseAddNewGame(name, noOfLetters, noOfPlayers, turnInterval, timeInterval, firstChar);

    }

    public void btnClick_addNewGame(View view){
        mDialog.show();
        addNewGame();
    }

    public void btnClick_goToAddFriendsActivity(View view){
       Intent intent = new Intent(this, FriendListActivity.class);
       startActivity(intent);
    }


    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerHistoryArray = fromJson(data);

            Log.i("Finally", PlayerHistoryArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerHistory> arrayOfUsers = new ArrayList<PlayerHistory>();
            // Create the adapter to convert the array to views
            UsersAdapter adapter = new UsersAdapter(this, arrayOfUsers);
            adapter.addAll(PlayerHistoryArray);
            // Attach the adapter to a ListView
            ListView listView = (ListView) findViewById(R.id.lvItems);
            listView.setAdapter(adapter);

            ///////////
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    PlayerHistory listItem = PlayerHistoryArray.get(position);
//                    Log.i("Item selected is ", listItem.GameName);
//                }
//            });
            ///////////


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void parseAddPlayers(String ObjectId){

        //update GameSessionID
        appState.setGameSessionID(ObjectId);

        List<ParseObject> PlayerList = new ArrayList<ParseObject>();

        //Add the Player who is creating the game frst
        ParseObject NewPlayer = new ParseObject("GameSession_PlayerList");
        NewPlayer.put("Player_ObjectID", Player_objectId);
        NewPlayer.put("PlayerID", PlayerID);
        NewPlayer.put("GameSession_ObjectID", ObjectId); //same for all
        NewPlayer.put("CurrentPlayerID", PlayerID); //same for all
        NewPlayer.put("GameSession_objectId", ParseObject.createWithoutData("GameSession_Master",
                ObjectId)); //same for all
        NewPlayer.put("TurnIndex", 1);
        PlayerList.add(NewPlayer);

        //followed by all the other friends he/she selected to join the game
        for (int i = 0; i<appState.getSelectedFriendList().length; i++){
            //insert new word into History table
            NewPlayer = new ParseObject("GameSession_PlayerList");
            NewPlayer.put("Player_ObjectID", appState.getSelectedFriendObjectIDList()[i]);
            NewPlayer.put("PlayerID", appState.getSelectedFriendList()[i]);
            NewPlayer.put("GameSession_ObjectID", ObjectId); //same for all
            NewPlayer.put("CurrentPlayerID", PlayerID); //same for all
            NewPlayer.put("GameSession_objectId", ParseObject.createWithoutData("GameSession_Master",
                    ObjectId)); //same for all
            NewPlayer.put("TurnIndex", i+2);
            PlayerList.add(NewPlayer);
        }

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new game completed", "SUCCESS!");
                uponConmpleteAddNewGame();
            }
        });

    }

    public void uponConmpleteAddNewGame(){
        Intent intent = new Intent();
        intent.putExtra("Status", "Success");
        setResult(RESULT_OK, intent);
        mDialog.dismiss();
        finish();
    }


    private void parseAddNewGame(String Name, int NoOfLetters, int NoOfPlayers, int TurnInterval, int TimeInterval, String FirstChar){


        //insert new word into History table
        final ParseObject gameScore = new ParseObject("GameSession_Master");
        gameScore.put("Name", Name);
        gameScore.put("CurrentPlayer_ObjectID", Player_objectId);
        gameScore.put("CurrentPlayerID", PlayerID);
        //gameScore.put("GameSession_objectId", ParseObject.createWithoutData("GameSession_Master",GameSessionID));
        gameScore.put("CurrentPlayerIndex", 1);
        gameScore.put("WordCount", NoOfLetters);
        gameScore.put("TotalPlayers", NoOfPlayers);
        gameScore.put("GameStatus", "ACTIVE");
        gameScore.put("IntervalTimeInMinutes", TimeInterval);
        gameScore.put("IntervalTurns", TurnInterval);
        gameScore.put("LastChar", FirstChar);
        //TODO in case we still use pubNUb
        //.put("Pubnub_Channel", 0);
        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Saved successfully.
                    GameSessionID = gameScore.getObjectId();

                    parseAddPlayers(gameScore.getObjectId());

                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerHistory> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerHistory> GameHistory = new ArrayList<PlayerHistory>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerHistory(jsonObjects.get(i), i+1));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //to set the criteria if the player wants to submit a new word
        GameHistory.get(0).setWordCriteria();
        return GameHistory;
    }

    public class PlayerHistory {
        public String objectId;
        public String GameSession_ObjectID;
        public String Player_ObjectID;
        public String PlayerID;
        public String Word;
        public int Stars;
        public Date CreatedAt;
        public String GameName;
        public String CurrentPlayerID;
        public int WordCount;
        public int TotalPlayers;
        public String LastCharacter;
        public int Points;
        public int index;

        public void setIndex(int count){
            index = count;
        }

        public void setWordCriteria(){
            mLastChar = this.LastCharacter;
            mWordCount = this.WordCount;
        }

        public PlayerHistory(ParseObject Obj, int c) {
            this.objectId = Obj.getObjectId();
            this.GameSession_ObjectID = Obj.getString("GameSession_ObjectID");
            this.Player_ObjectID = Obj.getString("Player_ObjectID");
            this.PlayerID = Obj.getString("PlayerID");
            this.Word = Obj.getString("Word");
            this.Stars = Obj.getInt("Stars");
            this.Points = Obj.getInt("Points");
            this.CreatedAt = Obj.getCreatedAt();
            this.GameName = Obj.getParseObject("GameSession_objectId").getString("Name");
            this.WordCount = Obj.getParseObject("GameSession_objectId").getInt("WordCount");
            this.TotalPlayers = Obj.getParseObject("GameSession_objectId").getInt("TotalPlayers");
            this.CurrentPlayerID = Obj.getParseObject("GameSession_objectId").getString("CurrentPlayerID");
            this.LastCharacter = Obj.getParseObject("GameSession_objectId").getString("LastChar");
            this.index = c;


        }

    }

    public class UsersAdapter extends ArrayAdapter<PlayerHistory> {
        public UsersAdapter(Context context, ArrayList<PlayerHistory> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            PlayerHistory user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_gamesessiondetails, parent, false);
            }
            // Lookup view for data population
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            TextView wordIndex = (TextView) convertView.findViewById(R.id.wordIndex);
            TextView diamond = (TextView) convertView.findViewById(R.id.Diamond);

            wordIndex.setText(String.valueOf(user.index));
            diamond.setText(String.valueOf(user.Stars));
            // Populate the data into the template view using the data object
            tvName.setText(user.Word);
            //tvName.setHint(user.GameSession_ObjectID);
            String SubHeader = "玩者: " + user.PlayerID + " 分数: " + user.Points;
            tvHome.setText(SubHeader);
            //tvName.setContentDescription(user.GameSession_ObjectID);


            //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
            View PrimaryColumn = convertView.findViewById(R.id.primary_target);
            PrimaryColumn.setTag(PrimaryColumn.getId(), user.objectId);
            //Then we set the onClick function here to pull the GameSession_objectId upon each click
            PrimaryColumn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(MainActivity.this,
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();

                        }
                    });

            View ShareButton = convertView.findViewById(R.id.secondary_action);
            ShareButton.setTag(ShareButton.getId(), user.GameSession_ObjectID);
            ShareButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            String outputText = "Share " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(MainActivity.this,
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                        }
                    });

            // Return the completed view to render on screen
            return convertView;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        if (!mNavigationDrawerFragment.isDrawerOpen()) {
//            // Only show items in the action bar relevant to this screen
//            // if the drawer is not showing. Otherwise, let the drawer
//            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.main_menu, menu);
//            restoreActionBar();
//            return true;
//        }
        getMenuInflater().inflate(R.menu.global_next, menu);;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_next) {

            if(mGameName.getText().toString().equals("")){
                AlertDialogs.ShowAlertEmptyName(this);
                return false;
            }

            if(appState.getSelectedFriendList() == null){
                AlertDialogs.ShowAlertEmptyContacts(this);
                return  false;
            }
            else
            {
                int noOfPlayers = appState.getSelectedFriendList().length;

                if(noOfPlayers == 0){
                    AlertDialogs.ShowAlertEmptyContacts(this);
                    return  false;
                }
            }


            mDialog.show();
            addNewGame();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public static class AlertDialogs{

        public AlertDialogs(){

        }

        public static void ShowAlertEmptyName(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter a name for your new game")
                    .setTitle("Invalid Game Name");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }


        public static void ShowAlertEmptyContacts(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please have a few friends for your new group")
                    .setTitle("Invalid Game Friends");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }
}
