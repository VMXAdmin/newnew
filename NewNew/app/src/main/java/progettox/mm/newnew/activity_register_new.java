package progettox.mm.newnew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 24/6/2015.
 */
public class activity_register_new extends AppCompatActivity {

    private EditText mUserEmail;
    private EditText mUserName;
    //private RadioGroup mRadioGrpGender;
    //private DatePicker mBirthDate;
    private EditText mUserPwd;
    private EditText mUserPwdConf;
    public Spinner mSpinner_day;
    public Spinner mSpinner_mth;
    public Spinner mSpinner_yr;
    private Date mBirthDate;
    private ParseFile mTempAvatar;
    private int mGender;
    private ProgressDialog mDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_new_main);
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        Initialize();

    }

    private void prepareTempAvatar(final String imgFileName){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        final Bitmap tempAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.img_blank_black);
        tempAvatar.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] data = stream.toByteArray();
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        mTempAvatar = new ParseFile (imgFileName + ".png", data);
        mTempAvatar.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                // If successful add file to user and signUpInBackground
                if(null == e)
                    saveImagetoParse(imgFileName, tempAvatar);
            }
        });
    }

    private void saveImagetoParse(String ObjID, Bitmap Pic ){
        ParseUser user = ParseUser.getCurrentUser();
        //ParseUser user = ParseUser.
        user.put("Avatar_Pic", mTempAvatar);
        user.saveInBackground();

        GlobalPost.StorePNG(Pic, ObjID, this);

    }

    private void savetoParse(){
        //TODO to put Country and State

        recordBirthDetails();

        final String Username = mUserName.getText().toString();
        final String Password = mUserPwd.getText().toString();
        String Email = mUserEmail.getText().toString();
        Date bday = mBirthDate;
        int Gender = mGender;

        final ParseUser user = new ParseUser();
        user.setUsername(mUserName.getText().toString());
        user.setPassword(mUserPwd.getText().toString());
        user.setEmail(mUserEmail.getText().toString());


        user.put("birthday", mBirthDate);
        user.put("gender", mGender);

        //now to insert canonical name with all lower case characters
        user.put("Canonical_username", mUserName.getText().toString().toLowerCase());

        //now to put a temp user image


        //user.put("Avatar_Pic", mTempAvatar);

        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    mDialog.dismiss();
                    prepareTempAvatar(user.getObjectId());
                    GotoGameActivity(mUserName.getText().toString(),
                            mUserPwd.getText().toString(),
                            user.getObjectId());
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });
    }

    private void ParseUserLogin(String usernametxt, String passwordtxt){
        // Send data to Parse.com for verification
        ParseUser.logInInBackground(usernametxt, passwordtxt,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            // If user exist and authenticated, send user to Welcome.class
//                            Intent intent = new Intent(
//                                    LoginSignupActivity.this,
//                                    Welcome.class);
//                            startActivity(intent);
//                            Toast.makeText(getApplicationContext(),
//                                    "Successfully Logged in",
//                                    Toast.LENGTH_LONG).show();
//                            finish();
                        } else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "No such user exist, please signup",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void Initialize(){

        mUserEmail = (EditText) findViewById(R.id.txt_user_email);
        mUserName = (EditText) findViewById(R.id.txt_user_name);
        //mRadioGrpGender = (RadioGroup) findViewById(R.id.radiogrp_group_gender);
        //mBirthDate = (DatePicker) findViewById(R.id.dp_user_bday);
        mUserPwd = (EditText) findViewById(R.id.txt_user_pwd);
        //mRadioGroup.check(R.id.radio_public);
        mSpinner_day = (Spinner) findViewById(R.id.spinner_day);
        mSpinner_mth = (Spinner) findViewById(R.id.spinner_mth);
        mSpinner_yr = (Spinner) findViewById(R.id.spinner_yr);

        ArrayAdapter<CharSequence> adapter_spinner_day = ArrayAdapter.createFromResource(this,
                R.array.letters_day_array, R.layout.item_spinner_day);
        // Specify the layout to use when the list of choices appears
        adapter_spinner_day.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mSpinner_day.setAdapter(adapter_spinner_day);

        ArrayAdapter<CharSequence> adapter_spinner_mth = ArrayAdapter.createFromResource(this,
                R.array.letters_month_array, R.layout.item_spinner_day);
        // Specify the layout to use when the list of choices appears
        adapter_spinner_mth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mSpinner_mth.setAdapter(adapter_spinner_mth);



        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,
                R.layout.item_spinner_day, computeYearSpinner());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        mSpinner_yr.setAdapter(spinnerArrayAdapter);

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Creating your new profile, thanks!");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);
    }

    private String[] computeYearSpinner(){

        int count = 100;
        Calendar C = Calendar.getInstance();
        int index = C.get(Calendar.YEAR) - count;
        String[] temp = new String[count];

        for(int i=0;i<count;i++){
            temp[i] = String.valueOf(index);
            index++;
        }

        return temp;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.global_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_next) {
            goToNextScreen();
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToNextScreen(){
        mDialog.show();
        savetoParse();
    }

    public void GotoGameActivity(String UID, String PWD, String UOBJID){


        //register into Shared Object to be retrieved later///////////
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("PlayerID", UID);
        ed.putString("Player_ObjectID", UOBJID);
        ed.putString("PlayerPWD", PWD);
        ed.apply();
        //register into Shared Object to be retrieved later///////////

        //The 1st line is to register into Parse's Installation table to faciliate proper Push Notifications
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("username", UID);
        installation.saveInBackground();

        App appState = ((App)getApplicationContext());
        appState.setPlayerID(UID);
        appState.setPlayer_objectId(UOBJID);

        Intent intent = new Intent(this, MainMenuActivity.class);
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("PlayerID", UID);
        bundle.putString("Player_objectId", UOBJID);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void recordBirthDetails(){
        int Day = Integer.parseInt(mSpinner_day.getSelectedItem().toString());
        int Mth = Integer.parseInt(mSpinner_mth.getSelectedItem().toString());
        int Yr = Integer.parseInt(mSpinner_yr.getSelectedItem().toString());

        Log.i("day is ", String.valueOf(Day));
        Log.i("mth is ", String.valueOf(Mth));
        Log.i("year is ", String.valueOf(Yr));

        Calendar cal = Calendar.getInstance();
        cal.set(Yr, Mth, Day);
        mBirthDate = cal.getTime();

        String temp =
                new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_male:
                if (checked)
                    mGender = 1;
                break;
            case R.id.radio_female:
                if (checked)
                    mGender = 0;
                break;

        }
    }


}
