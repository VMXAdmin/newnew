package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 11/7/2015.
 */
public class activity_group_settings_members extends AppCompatActivity {

    private List<PlayerFriend> PlayerFriendsArray;
    public String PlayerID;
    public String Player_objectId;
    public String mUser_ObjectID;
    public String mUser_Name;
    public AppInfoAdapter adapter ;
    public App mAppState;
    private String mGroup_objectId;
    private LoadImageTask mLoadImageTask;
    private int mProfileType; //1 is user, 2 is group, 3 is tutor

    private void Initialize(){
        Bundle extras = getIntent().getExtras();
        mProfileType = 1;
        if (extras != null){
            mProfileType = extras.getInt("Profile_Type");
            if(mProfileType == 2)
                mGroup_objectId = extras.getString("Group_ObjectID");
            else if(mProfileType == 1){
                mUser_ObjectID = extras.getString("User_ObjectID");
                mUser_Name = extras.getString("User_Name");
            }

        }

        mAppState = ((App)getApplicationContext());
        PlayerID = mAppState.getPlayerID();
        Player_objectId = mAppState.getPlayer_objectId();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_invite_listview);
        Initialize();

        if(mProfileType == 2)
            getMembersList();
        else if(mProfileType == 1)
            getUserFriendList();


    }

    private void getFriendsList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User",Player_objectId);
        query.whereEqualTo("Player_objectId", obj);
        //query.whereEqualTo("Status", 1);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    filterMembersList(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    private void filterMembersList(List<ParseObject> friendList){

        try{
            if(friendList.size() > 0){
                for (int i = 0; i<PlayerFriendsArray.size(); i++){
                    for(int j = 0; j<friendList.size();j++){
                        String Friend_ObjectID = friendList.get(j).getParseObject("Friend_objectId").getObjectId();
                        if(Friend_ObjectID.equals(PlayerFriendsArray.get(i).getFriendObjectID())){

                            if(friendList.get(j).getInt("Status") == 1)
                            {
                                PlayerFriendsArray.get(i).setFriendStatus(1);
                                PlayerFriendsArray.get(i).setIsFriend(true);
                            }
                            else
                            {
                                PlayerFriendsArray.get(i).setFriendStatus(0);
                                PlayerFriendsArray.get(i).setIsFriend(true);
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }

        updateListViewAdapter();
    }

    private void updateListViewAdapter(){
        // Construct the data source
        ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
        // Create the adapter to convert the array to views
        adapter = new AppInfoAdapter(this, R.layout.item_group_settings_members, arrayOfUsers, this);
        adapter.addAll(PlayerFriendsArray);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.listApplication);
        listView.setAdapter(adapter);

    }

    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerFriendsArray = fromJson(data);
            getFriendsList();

//            // Construct the data source
//            ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
//            // Create the adapter to convert the array to views
//            adapter = new AppInfoAdapter(this, R.layout.item_group_settings_members, arrayOfUsers, this);
//            adapter.addAll(PlayerFriendsArray);
//            // Attach the adapter to a ListView
//            ListView listView = (ListView) findViewById(R.id.listApplication);
//            listView.setAdapter(adapter);


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void getUserFriendList(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User", mUser_ObjectID);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    if(friendList.size()>0)
                        displayJSONResults(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getMembersList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master",mGroup_objectId);
        query.whereEqualTo("Group_objectId", obj);
        query.whereEqualTo("Member_Status", 1);
        query.include("Member_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    if(friendList.size()>0)
                        displayJSONResults(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    private String getUserName(String ObjID){
        String temp = "";
        try{
            for (int i = 0; i<PlayerFriendsArray.size(); i++){
                if(PlayerFriendsArray.get(i).getFriendObjectID().equals(ObjID)){
                    temp = PlayerFriendsArray.get(i).getFriendID();
                    break;
                }

            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }


        return temp;
    }

    private void addFriend(final String Friend_ObjectID){
        List<ParseObject> PlayerList = new ArrayList<ParseObject>();

        ParseObject gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", PlayerID);
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", Player_objectId));
        gameScore.put("FriendID", getUserName(Friend_ObjectID));
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", Friend_ObjectID));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", getUserName(Friend_ObjectID));
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", Friend_ObjectID));
        gameScore.put("FriendID", PlayerID);
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", Player_objectId));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {

                sendAddFriendNotification(Friend_ObjectID);
            }
        });
    }

    private void sendAddFriendNotification(String Friend_ObjectID){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", Friend_ObjectID));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 3);
        NewPlayer.put("IsAccepted", false); //By default is False

        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    private void OpenUserFeedPage(String Friend_objectId){
//        ((MainMenuActivity)getActivity()).OpenUserFeedPage(
//                mGlobalPost_List.get(Feed_objectId).getPlayerID(),
//                mGlobalPost_List.get(Feed_objectId).getPlayerObjectID());
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", Friend_objectId);
        intent.putExtra("Player_Name", getUserName(Friend_objectId));
        intent.putExtra("IsGroup", false);
        startActivity(intent);

    }

    public static class AppInfoHolder
    {
        //ImageView imgIcon;
        ImageView imgIcon;
        TextView txtTitle;
        Button chkSelect;

    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.menu_source_icon);
                holder.txtTitle = (TextView) row.findViewById(R.id.menu_source_name);
                holder.chkSelect = (Button) row.findViewById(R.id.txt_action_member);

                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.FriendID);
            holder.txtTitle.setTag(holder.txtTitle.getId(), appinfo.getFriendObjectID());
            holder.txtTitle.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                            //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                            OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                        }
                    });

            //holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.Friend_objectId, activity));
            try{

                mLoadImageTask = new LoadImageTask(holder.imgIcon, appinfo.Friend_objectId,
                        activity);
                mLoadImageTask.execute(holder.imgIcon);
                holder.imgIcon.setTag(holder.imgIcon.getId(), appinfo.getFriendObjectID());
                holder.imgIcon.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                                OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                            }
                        });

            }
            catch(Exception e){
                Log.i("Group Settings Members", e.getMessage());
            }

            try{
//dtermine whether it is your friend, not your friend and yourself
                if(appinfo.getFriendObjectID().equals(Player_objectId)){
                    holder.chkSelect.setVisibility(View.INVISIBLE);
                    holder.chkSelect.setClickable(false);
                }
                else if(appinfo.getIsFriend()){
                    if(appinfo.getFriendType() == 1){
                        holder.chkSelect.setVisibility(View.INVISIBLE);
                        holder.chkSelect.setClickable(false);
                    }
                    else if(appinfo.getFriendType() == 0)
                    {
                        holder.chkSelect.setVisibility(View.VISIBLE);
                        holder.chkSelect.setClickable(false);
                        holder.chkSelect.setText(getResources().getText(R.string.group_members_friend_requested));
                        holder.chkSelect.setBackgroundColor(getResources().getColor(R.color.white));
                    }

                }
                else
                {
                    holder.chkSelect.setVisibility(View.VISIBLE);
                    holder.chkSelect.setText(getResources().getText(R.string.group_members_notfriend));
                    holder.chkSelect.setTag(holder.chkSelect.getId(), appinfo.getFriendObjectID());
                    holder.chkSelect.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView temp = (TextView) view;
                            temp.setText(getResources().getText(R.string.group_members_friend_requested));
                            temp.setClickable(false);
                            temp.setBackgroundColor(getResources().getColor(R.color.white));
                            addFriend(String.valueOf(view.getTag(view.getId())));

                        }
                    });
                }
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }



            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //holder.PostImages[i].setImageBitmap(g.loadImageFromStorage(fileName, getActivity()));
            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
            //v = params[0];
            //return mFakeImageLoader.getImage();
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
