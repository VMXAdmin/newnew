package progettox.mm.newnew;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Admin on 12/6/2015.
 */
public class CreateNewGlobalFeedGroup extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;
    private static final int AttachFileRequestCode = 2888;
    private ImageView mGroupPhoto;
    private Bitmap mUserPhoto;
    private EditText mGroupName;
    private EditText mGroupDesc;
    private EditText mGroupLocation;
    private int mGroupType;
    private RadioGroup mRadioGroup;
    private App mAppState;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.globalfeed_group_new);

        Initialize();

        if (savedInstanceState != null) {
            // Restore value of members from saved state
            //mUserPhoto = savedInstanceState.getByteArray("PostImage");
            Bitmap bmp = savedInstanceState.getParcelable("FeedGroupImage");
            if(bmp != null){
                mUserPhoto = bmp;
                mGroupPhoto.setImageBitmap(mUserPhoto);
            }

        }

//        LocationManager locationManager = (LocationManager)
//                getSystemService(this.LOCATION_SERVICE);
//
//        LocationListener locationListener = new MyLocationListener();
//        locationManager.requestLocationUpdates(
//                LocationManager.GPS_PROVIDER, 5000, 10, locationListener);

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(mUserPhoto != null){
            savedInstanceState.putParcelable("FeedGroupImage",mUserPhoto);
        }
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    private void Initialize(){
        mAppState = ((App)getApplicationContext());
        mGroupPhoto = (ImageView) findViewById(R.id.imgview_group_photo);
        mGroupName = (EditText) findViewById(R.id.txt_group_name);
        mGroupDesc = (EditText) findViewById(R.id.txt_group_desc);
        mGroupLocation = (EditText) findViewById(R.id.txt_group_locale);
        mRadioGroup = (RadioGroup) findViewById(R.id.radiogrp_group_type);
        mRadioGroup.check(R.id.radio_public);
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.global_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.home){
            finish();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_next) {
            goToNextScreen();
        }

        return super.onOptionsItemSelected(item);
    }


    private void goToNextScreen(){
        Intent intent = new Intent(this, CreateNewGlobalFeedGroup2.class);
        Bundle bundle = new Bundle();
        bundle.putString("GroupName", mGroupName.getText().toString());
        bundle.putString("GroupDesc", mGroupDesc.getText().toString());
        bundle.putInt("GroupType", mGroupType);
        //TODO to put Country and State
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_public:
                if (checked)
                    mGroupType = mAppState.getPostGroupInt();
                    break;
            case R.id.radio_friends:
                if (checked)
                    mGroupType = mAppState.getPostGroupClosedInt();
                    break;
            case R.id.radio_closed:
                if (checked)
                    mGroupType = mAppState.getPostGroupSecretInt();
                    break;
        }
    }

    public void onClickCameraBtn(View view){
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    public void onClickAttachBtn(View view){
        final Intent galleryIntent = new Intent(
                Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, AttachFileRequestCode);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            Uri selectedImageUri = data.getData();
            String[] fileColumn = {MediaStore.Images.Media.DATA};
            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
                    null, null, null);
            imageCursor.moveToFirst();

            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
            String picturePath = imageCursor.getString(fileColumnIndex);

            mUserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(), picturePath, 100, 100);
            mGroupPhoto.setImageBitmap(mUserPhoto);

//            Bitmap UserPhoto = (Bitmap) data.getExtras().get("data");
//            Uri selectedImageUri = BitmapManager.getImageUri(getApplicationContext(), UserPhoto,
//                    Bitmap.CompressFormat.JPEG);
//
//            String[] fileColumn = {MediaStore.Images.Media.DATA};
//            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
//                    null, null, null);
//            imageCursor.moveToFirst();
//
//            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
//            String picturePath = imageCursor.getString(fileColumnIndex);
//
//            mGroupPhoto.setImageBitmap(
//                    BitmapManager.decodeSampledBitmapFromResource(getResources(), picturePath, 100, 100));


        }

        if (requestCode == AttachFileRequestCode && resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            String[] fileColumn = {MediaStore.Images.Media.DATA};
            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
                    null, null, null);
            imageCursor.moveToFirst();

            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
            String picturePath = imageCursor.getString(fileColumnIndex);

            mUserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(), picturePath, 100, 100);
            mGroupPhoto.setImageBitmap(mUserPhoto);
        }
    }

//    private class MyLocationListener implements LocationListener {
//
//        @Override
//        public void onLocationChanged(Location loc) {
//            //editLocation.setText("");
//            //pb.setVisibility(View.INVISIBLE);
////            Toast.makeText(
////                    getBaseContext(),
////                    "Location changed: Lat: " + loc.getLatitude() + " Lng: "
////                            + loc.getLongitude(), Toast.LENGTH_SHORT).show();
//            String longitude = "Longitude: " + loc.getLongitude();
//            Log.v("Longtitude", longitude);
//            String latitude = "Latitude: " + loc.getLatitude();
//            Log.v("Latitude", latitude);
//
//        /*------- To get city name from coordinates -------- */
//            String cityName = null;
//            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
//            List<Address> addresses;
//            try {
//                addresses = gcd.getFromLocation(loc.getLatitude(),
//                        loc.getLongitude(), 1);
//                if (addresses.size() > 0)
//                    System.out.println(addresses.get(0).getLocality());
//                cityName = addresses.get(0).getLocality();
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
//            String s = longitude + "\n" + latitude + "\n\nMy Current City is: "
//                    + cityName;
//            Log.i("Coordinates", s);
//            //editLocation.setText(s);
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {}
//
//        @Override
//        public void onProviderEnabled(String provider) {}
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {}
//    }

}
