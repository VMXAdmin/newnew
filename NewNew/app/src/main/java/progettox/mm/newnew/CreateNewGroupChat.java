package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Admin on 21/5/2015.
 */
public class CreateNewGroupChat extends ActionBarActivity {
    private List<ParseObject> PlayerHistoryResults;
    private List<PlayerFriend> PlayerFriendsArray;
    public String PlayerID;
    public String Player_objectId;
    public String mFeedSessionID;
    public String[] FriendList;
    public String[] FriendObjIDList;

    public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    private int mRequestCode = 100;

    public EditText mGameName;
    public App appState;
    private ProgressDialog mDialog;
    private static final int CAMERA_REQUEST = 1888;
    private static final int AttachFileRequestCode = 2888;
    private ImageView imageView;
    private Bitmap mUserPhoto;
    public AppInfoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_groupchat_form);

        appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Creating your new group chat, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        setTitle("New Chat Group");

        //initialize camera button
        this.imageView = (ImageView)this.findViewById(R.id.img1);
        ImageButton cameraBtn = (ImageButton) findViewById(R.id.camera_action);
        cameraBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        ImageButton attachBtn = (ImageButton) findViewById(R.id.attach_action);
        attachBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent,"Select File"), AttachFileRequestCode);

                final Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //galleryIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                startActivityForResult(galleryIntent, AttachFileRequestCode);
            }
        });


// Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            //mUserPhoto = savedInstanceState.getByteArray("PostImage");
            Bitmap bmp = savedInstanceState.getParcelable("PostImage");
            if(bmp != null){
                mUserPhoto = bmp;
                imageView.setImageBitmap(mUserPhoto);
            }

        } else {
            // Probably initialize members with default values for a new instance
        }


    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(mUserPhoto != null){
//            int bytes = mUserPhoto.getByteCount();
//            ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
//            mUserPhoto.copyPixelsToBuffer(buffer); //Move the byte data to the buffer
//
//            byte[] array = buffer.array(); //Get the underlying array containing the data.
//            savedInstanceState.putByteArray("PostImage", array);
            savedInstanceState.putParcelable("PostImage",mUserPhoto);
        }


        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    private void getCheckedFriends(){


        StringBuilder result = new StringBuilder();
        StringBuilder resultObjId = new StringBuilder();
        int count = 0;
        //for(int i=0;i<adapter.mCheckStates.size();i++)
        for(int i=0;i<adapter.data.size();i++)
        {
            if(adapter.mCheckStates.get(i))
            {
                count++;
                result.append(PlayerFriendsArray.get(i).FriendID);
                result.append("\n");
                //FriendList[0] = PlayerFriendsArray.get(i).FriendID;
                resultObjId.append(PlayerFriendsArray.get(i).Friend_objectId);
                resultObjId.append("\n");

            }

        }

        if (count == 1){
            int friendsCount = 1;
            FriendList = new String[friendsCount];
            FriendObjIDList = new String[friendsCount];
            appState.SelectedFriendList(friendsCount);
            appState.SelectedFriendObjectIDList(friendsCount);
            FriendList[0] = result.substring(0, result.indexOf("\n"));
            FriendObjIDList[0] = resultObjId.substring(0, resultObjId.indexOf("\n"));

            appState.setSelectedFriendList(FriendList);
            appState.setSelectedFriendObjectIDList(FriendObjIDList);

        }
        else
        {
            //Toast.makeText(this, result, 1000).show();
            StringTokenizer tokens = new StringTokenizer(result.toString(), "\n");
            StringTokenizer tokensObjID = new StringTokenizer(resultObjId.toString(), "\n");
            int friendsCount = tokens.countTokens();
            FriendList = new String[friendsCount];
            FriendObjIDList = new String[friendsCount];
            appState.SelectedFriendList(friendsCount);
            appState.SelectedFriendObjectIDList(friendsCount);

            for(int i=0;i<friendsCount;i++) {
                FriendList[i] = tokens.nextToken();
                FriendObjIDList[i] = tokensObjID.nextToken();
            }

            //store into global class

            appState.setSelectedFriendList(FriendList);
            appState.setSelectedFriendObjectIDList(FriendObjIDList);
        }




    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            mUserPhoto = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(mUserPhoto);


        }

        if (requestCode == AttachFileRequestCode) {
            Uri selectedImageUri = data.getData();
            String[] fileColumn = {MediaStore.Images.Media.DATA};
            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
                    null, null, null);
            imageCursor.moveToFirst();

            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
            String picturePath = imageCursor.getString(fileColumnIndex);

            mUserPhoto = BitmapFactory.decodeFile(picturePath);
            imageView.setImageBitmap(mUserPhoto);
        }
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        //imageView.setImageBitmap(mUserPhoto);

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        //imageView.setImageBitmap(mUserPhoto);
        getFriendsList();
    }

    private void getFriendsList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User",Player_objectId);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        //query.whereEqualTo("PlayerID", PlayerID);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    displayJSONResults(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), i+1, this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerFriendsArray = fromJson(data);

            Log.i("Finally", PlayerFriendsArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
            // Create the adapter to convert the array to views
            adapter = new AppInfoAdapter(this, R.layout.friends_invite_item, arrayOfUsers, this);
            adapter.addAll(PlayerFriendsArray);
            // Attach the adapter to a ListView
            ListView listView = (ListView) findViewById(R.id.listApplication);
            listView.setAdapter(adapter);
            mDialog.dismiss();
            ///////////
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    PlayerFriend listItem = PlayerFriendsArray.get(position);
//                    Log.i("Item selected is ", listItem.GameName);
//                }
//            });
            ///////////


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    public void btnClick_goBack(View view){
        finish();
    }



    private void AddImagesToParse(String ObjectId){

        if(mUserPhoto == null){
            mDialog.dismiss();
            finish();
            return;
        }

        //update GameSessionID
        appState.setGameSessionID(ObjectId);

        //List<ParseObject> PlayerList = new ArrayList<ParseObject>();

        //Add the Player who is creating the game frst
        ParseObject NewPlayer = new ParseObject("GlobalFeed_Images");
        NewPlayer.put("GlobalFeed_ObjectID", ObjectId);

        NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData("GlobalFeed_Master",
                ObjectId)); //same for all


        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mUserPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] data = stream.toByteArray();
        ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        imgFile.saveInBackground();

        NewPlayer.put("Image", imgFile);

        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Saved successfully.
//                    mFeedSessionID = gameScore.getObjectId();
//
//                    AddImagesToParse(gameScore.getObjectId());
                    mDialog.dismiss();
                    finish();

                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

        //followed by all the other friends he/she selected to join the game
//        for (int i = 0; i<appState.getSelectedFriendList().length; i++){
//            //insert new word into History table
//            NewPlayer = new ParseObject("GameSession_PlayerList");
//            NewPlayer.put("Player_ObjectID", appState.getSelectedFriendObjectIDList()[i]);
//            NewPlayer.put("PlayerID", appState.getSelectedFriendList()[i]);
//            NewPlayer.put("GameSession_ObjectID", ObjectId); //same for all
//            NewPlayer.put("CurrentPlayerID", PlayerID); //same for all
//            NewPlayer.put("GameSession_objectId", ParseObject.createWithoutData("GameSession_Master",
//                    ObjectId)); //same for all
//            NewPlayer.put("TurnIndex", i+2);
//            PlayerList.add(NewPlayer);
//        }

//        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {
//
//            @Override
//            public void done(ParseException e) {
//                // TODO Auto-generated method stub
//                //do something
//                Log.i("Add new game completed", "SUCCESS!");
//                uponConmpleteAddNewGame();
//            }
//        });



    }

    public void uponConmpleteAddNewGame(){
        Intent intent = new Intent();
        intent.putExtra("Status", "Success");
        setResult(RESULT_OK, intent);
        mDialog.dismiss();
        finish();
    }

    private void parseAddPlayers(final String ObjectId, final String ChatName){

        //update GameSessionID
        //appState.setGameSessionID(ObjectId);
        //getCheckedFriends();

        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        ParseObject NewPlayer = new ParseObject("Chat_UserList");
        //Add the Player who is creating the game frst
        NewPlayer.put("Chat_ObjectID", ObjectId);
        NewPlayer.put("Chat_objectId", ParseObject.createWithoutData(
                "Chat_Master", ObjectId));
        NewPlayer.put("User_ObjectID", Player_objectId);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        PlayerList.add(NewPlayer);

        //followed by all the other friends he/she selected to join the game
        for (int i = 0; i<FriendObjIDList.length; i++){
            //insert new word into History table
            NewPlayer = new ParseObject("Chat_UserList");
            NewPlayer.put("Chat_ObjectID", ObjectId);
            NewPlayer.put("Chat_objectId", ParseObject.createWithoutData(
                    "Chat_Master", ObjectId));
            NewPlayer.put("User_ObjectID", FriendObjIDList[i]);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", FriendObjIDList[i]));
            PlayerList.add(NewPlayer);
        }

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new game completed", "SUCCESS!");
                //uponConmpleteAddNewGame();
                GotoChatActivity(ObjectId, ChatName);
            }
        });

    }

    private void updatePushChannel(String ChatID){
        final String chatID = ChatID;
        ParseQuery query = new ParseQuery("Chat_Master");


// Retrieve the object by id
        query.getInBackground(chatID, new GetCallback<ParseObject>() {
            public void done(ParseObject gameScore, ParseException e) {
                if (e == null) {
                    // Now let's update it with some new data. In this case, only cheatMode and score
                    // will get sent to the Parse Cloud. playerName hasn't changed.
                    gameScore.put("Channel", "Chat_" + chatID);
                    gameScore.saveInBackground();
                }
            }
        });
    }


    private void addNewChatGroup(){

        //get Views
        EditText Name = (EditText) findViewById(R.id.txt_group_name);
        ImageView Image = (ImageView) findViewById(R.id.img1);
        final String ChatName = Name.getText().toString();
        //insert new word into History table
        final ParseObject ChatMaster = new ParseObject("Chat_Master");
        ChatMaster.put("Name", ChatName);
        ChatMaster.put("IsGroup", true);

        if(mUserPhoto != null){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mUserPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] data = stream.toByteArray();
            ParseFile imgFile = new ParseFile ("GroupImage.png", data);
            imgFile.saveInBackground();
            ChatMaster.put("Picture", imgFile);
        }
        else
        {
            //mUserPhoto
            return;
        }

        ChatMaster.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Saved successfully.
                    mFeedSessionID = ChatMaster.getObjectId();
                    parseAddPlayers(ChatMaster.getObjectId(), ChatName);
                    updatePushChannel(ChatMaster.getObjectId());

                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

    }

    public static class AlertDialogs{

        public AlertDialogs(){

        }

        public static void ShowAlertEmptyName(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter a name for your new group")
                    .setTitle("Invalid Group Name");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        public static void ShowAlertEmptyImage(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please have an image for your new group")
                    .setTitle("Invalid Group Image");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        public static void ShowAlertEmptyContacts(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please have a few friends for your new group")
                    .setTitle("Invalid Group Contacts");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    public static class AppInfoHolder
    {
        ImageView imgIcon;
        //ParseImageView imgIcon;
        TextView txtTitle;
        CheckBox chkSelect;

    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.menu_source_icon);
                holder.txtTitle = (TextView) row.findViewById(R.id.menu_source_name);
                holder.chkSelect = (CheckBox) row.findViewById(R.id.menu_source_check_box);

                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.FriendID);
            holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.Friend_objectId, activity));
//            holder.imgIcon.setParseFile(appinfo.profilePic);
//            holder.imgIcon.loadInBackground(new GetDataCallback() {
//                public void done(byte[] data, ParseException e) {
//                    // The image is loaded and displayed!
//                    //int oldHeight = imageView.getHeight();
//                    //int oldWidth = imageView.getWidth();
//                    Log.v("LOG!!!!!!", "imageView height = ");      // DISPLAYS 90 px
//                    Log.v("LOG!!!!!!", "imageView width = " );        // DISPLAYS 90 px
//                }
//            });
            //holder.imgIcon.setImageDrawable(appinfo.icon);
            // holder.chkSelect.setChecked(true);
            holder.chkSelect.setTag(position);
            holder.chkSelect.setChecked(mCheckStates.get(position, false));
            holder.chkSelect.setOnCheckedChangeListener(this);
            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }

    public class PlayerFriend {
        public String objectId;
        public String PlayerID;
        public String FriendID;
        public String Friend_objectId;

        public Drawable icon;
        public String applicationName;
        public ParseFile profilePic;

        public PlayerFriend(ParseObject Obj, int c, final Activity activity) {
            this.objectId = Obj.getObjectId();

            this.PlayerID = Obj.getString("PlayerID");
            this.FriendID = Obj.getString("FriendID");
            this.profilePic = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
            this.Friend_objectId = Obj.getParseObject("Friend_objectId").getObjectId();
            //profilePic
            //public static Drawable createFromResourceStream (Resources res, TypedValue value, InputStream is, String srcName, BitmapFactory.Options opts)

            if(!IsBitmapCached(this.Friend_objectId, activity)){
                ParseFile Picture = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Friend_objectId, activity);

                        } else {

                        }

                    }
                });
            }


        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return false;
        }

        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return null;
        }


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        if (!mNavigationDrawerFragment.isDrawerOpen()) {
//            // Only show items in the action bar relevant to this screen
//            // if the drawer is not showing. Otherwise, let the drawer
//            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.main_menu, menu);
//            restoreActionBar();
//            return true;
//        }
        getMenuInflater().inflate(R.menu.global_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_next) {

            EditText Name = (EditText) findViewById(R.id.txt_group_name);
            EditText Content = (EditText) findViewById(R.id.txt_post_content);

            if(Name.getText().toString().equals("")){
                AlertDialogs.ShowAlertEmptyName(this);
                return false;
            }

            if(mUserPhoto == null){
                AlertDialogs.ShowAlertEmptyImage(this);
                return false;
            }

            getCheckedFriends();
            if(FriendList == null){
                AlertDialogs.ShowAlertEmptyContacts(this);
                return false;
            }
            else
            {
                if(FriendList.length == 0){
                    AlertDialogs.ShowAlertEmptyContacts(this);
                    return false;
                }
            }



            mDialog.show();
            addNewChatGroup();
            //getCheckedFriends();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public void GotoChatActivity(String ChatID, String ChatName){
        mDialog.dismiss();
        Intent intent = new Intent(this, ChatActivity.class);

        //intent.putExtra("myObject", new Gson().toJson(mHTChatItemList.get(ChatID)));
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("ChatID", ChatID);
        bundle.putString("ChatUsername", ChatName);
        //bundle.putString("Player_objectId", UOBJID);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }


}
