package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Admin on 29/7/2015.
 */
public class activity_individual_find extends AppCompatActivity{
    private List<PlayerFriend> mPlayerFriendsArray;
    public String mPlayerID;
    public String mPlayer_objectId;
    public AppInfoAdapter mAdapter ;
    public App mAppState;
    private LoadImageTask mLoadImageTask;
    private ListView mListView;
    private TextView txtLoadingCaption;
    private LinearLayout mBottomLayout;
    private HashMap<String, PlayerFriend> mGroupArray;
    private Button mBtnGroup;
    private Button mBtnReco;
    private Button mBtnNear;

    //baidu maps api usages
    private static final String TAG = "dzt";
    private LocationClient mLocationClient;
    private LocationClientOption.LocationMode tempMode = LocationClientOption.LocationMode.Battery_Saving;
    private String tempcoor="bd09ll";
    private ParseGeoPoint mGeoLocation;

    private Boolean retrieveGeoLocation(){
        mGeoLocation = mAppState.getGeoLocPoint();
        if(mGeoLocation != null)
            return true;
        else
            return false;
    }

    private void InitLocation(){
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);//���ö�λģʽ
        option.setCoorType(tempcoor);//���صĶ�λ����ǰٶȾ�γ�ȣ�Ĭ��ֵgcj02
        int span=1000;
        try {
            //span = Integer.valueOf(frequence.getText().toString());
        } catch (Exception e) {
            // TODO: handle exception
        }
        option.setScanSpan(span);//���÷���λ����ļ��ʱ��Ϊ5000ms
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
    }

    private void Initialize(){

        mAppState = ((App)getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        //Initialize BAIDU map geo location grab
        mLocationClient = ((App)getApplication()).mLocationClient; // Class LocationClient
        InitLocation();
        mLocationClient.start();
        retrieveGeoLocation();

        mBottomLayout = (LinearLayout) findViewById(R.id.layout_join_panel);
        mBottomLayout.setVisibility(View.VISIBLE);

        mListView = (ListView) findViewById(R.id.lv_groups);
        txtLoadingCaption = (TextView)findViewById(R.id.lbl_loading_caption);
        txtLoadingCaption.setText(getResources().getText(R.string.lbl_frn_find_loading));

        mBtnGroup = (Button) findViewById(R.id.firstPage_action);
        mBtnGroup.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getGroups(1);
                        toggleButtonAppearance(1);
                    }
                });

        mBtnReco = (Button) findViewById(R.id.secondPage_action);
        mBtnReco.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getGroups(2);
                        toggleButtonAppearance(2);
                    }
                });

        mBtnNear = (Button) findViewById(R.id.thirdPage_action);
        mBtnNear.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getGroups(3);
                        toggleButtonAppearance(3);
                    }
                });

        toggleButtonAppearance(1);
    }

    private void toggleButtonAppearance(int position){
        if(position == 1){
            mBtnGroup.setTextColor(getResources().getColor(R.color.names));
            mBtnReco.setTextColor(getResources().getColor(R.color.white));
            mBtnNear.setTextColor(getResources().getColor(R.color.white));

        }
        else if(position == 2){
            mBtnGroup.setTextColor(getResources().getColor(R.color.white));
            mBtnReco.setTextColor(getResources().getColor(R.color.names));
            mBtnNear.setTextColor(getResources().getColor(R.color.white));

        }
        else if(position == 3){
            mBtnGroup.setTextColor(getResources().getColor(R.color.white));
            mBtnReco.setTextColor(getResources().getColor(R.color.white));
            mBtnNear.setTextColor(getResources().getColor(R.color.names));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_find);
        Initialize();
        getGroups(1);
    }

    private void getGroups(int type){

        txtLoadingCaption.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
        if(type == 1){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
            //ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master",mGroup_objectId);
            query.whereEqualTo("Member_ObjectID", mPlayer_objectId);
            query.whereEqualTo("Member_Status", 1);
            query.include("Member_objectId");

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> friendList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + friendList.size() + " scores");
                        //PlayerHistoryResults = scoreList;
                        if(friendList.size()>0)
                            getGroupMembers(friendList);
                        else
                            txtLoadingCaption.setVisibility(View.GONE);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
        else if(type == 2){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Master");
            //ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master",mGroup_objectId);
            query.whereEqualTo("User_ObjectID", mPlayer_objectId);
            //query.include("Member_objectId");

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> friendList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + friendList.size() + " scores");
                        //PlayerHistoryResults = scoreList;
                        if(friendList.size()>0)
                            getFeedCommentors(friendList);
                        else
                            txtLoadingCaption.setVisibility(View.GONE);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
        else if(type == 3){

            if(retrieveGeoLocation()){
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereNear("GeoLoc", mGeoLocation);
                //TODO to implement when SMART LOADING comes in
                //query.setLimit(25);

                query.findInBackground(new FindCallback<ParseUser>() {
                    public void done(List<ParseUser> objects, ParseException e) {
                        if (e == null) {
                            Log.d("score", "Retrieved " + objects.size() + " scores");
                            //PlayerHistoryResults = scoreList;

                            if(objects.size()>0)
                                getUsers(objects);
                            else
                                txtLoadingCaption.setVisibility(View.GONE);
                        } else {
                            Log.d("score", "Error: " + e.getMessage());
                        }
                    }
                });
            }
        }

    }


    private void getFeedCommentors(List<ParseObject> friendList){
        ArrayList<String> temp = new ArrayList<>();
        for (int i = 0; i < friendList.size(); i++) {
            try {
                temp.add(friendList.get(i).getObjectId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        //query.whereEqualTo("Member_ObjectID", mUser_ObjectID);
        query.whereContainedIn("GlobalFeed_ObjectID", temp);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        formMemberListReco(groupList);
                    }
                    else
                    {
                        txtLoadingCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getGroupMembers(List<ParseObject> friendList){
        ArrayList<String> temp = new ArrayList<>();
        for (int i = 0; i < friendList.size(); i++) {
            try {
                temp.add(friendList.get(i).getString("Group_ObjectID"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        //query.whereEqualTo("Member_ObjectID", mUser_ObjectID);
        query.whereContainedIn("Group_ObjectID", temp);
        query.whereEqualTo("Member_Status", 1);
        query.include("Group_objectId");
        query.include("Member_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        formMemberList(groupList);
                    }
                    else
                    {
                        txtLoadingCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getUsers(List<ParseUser> groupList){
        mPlayerFriendsArray = fromJsonParseUser(groupList);

        mGroupArray = new HashMap<>();

        String member_objectId = "";

        try{
            for(int i=0;i<groupList.size();i++){
                member_objectId = groupList.get(i).getObjectId();
                if(member_objectId.equals(mPlayer_objectId))
                    continue;

                if(mGroupArray.get(member_objectId) == null){
                    mGroupArray.put(member_objectId, new PlayerFriend(groupList.get(i), this));
//                mGroupArray.get(member_objectId).insertNewGroup(
//                        groupList.get(i).getParseObject("Group_objectId").getObjectId(),
//                        groupList.get(i).getParseObject("Group_objectId").getString("Name"));
                }
//            else{
//                mGroupArray.get(member_objectId).insertNewGroup(
//                        groupList.get(i).getParseObject("Group_objectId").getObjectId(),
//                        groupList.get(i).getParseObject("Group_objectId").getString("Name"));
//            }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }



        getSelfFriendList();
    }

    private void formMemberListReco(List<ParseObject> groupList){
        mPlayerFriendsArray = fromJson(groupList);

        mGroupArray = new HashMap<>();

        String member_objectId = "";
        try{
            for(int i=0;i<groupList.size();i++){
                member_objectId = groupList.get(i).getParseObject("User_objectId").getObjectId();
                if(member_objectId.equals(mPlayer_objectId))
                    continue;

                if(mGroupArray.get(member_objectId) == null){
                    mGroupArray.put(member_objectId, new PlayerFriend(groupList.get(i), this));
//                mGroupArray.get(member_objectId).insertNewGroup(
//                        groupList.get(i).getParseObject("Group_objectId").getObjectId(),
//                        groupList.get(i).getParseObject("Group_objectId").getString("Name"));
                }
//            else{
//                mGroupArray.get(member_objectId).insertNewGroup(
//                        groupList.get(i).getParseObject("Group_objectId").getObjectId(),
//                        groupList.get(i).getParseObject("Group_objectId").getString("Name"));
//            }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }


        getSelfFriendList();



    }

    private void formMemberList(List<ParseObject> groupList){
        mPlayerFriendsArray = fromJson(groupList);

        mGroupArray = new HashMap<>();

        String member_objectId = "";
        try{
            for(int i=0;i<groupList.size();i++){
                member_objectId = groupList.get(i).getParseObject("Member_objectId").getObjectId();
                if(member_objectId.equals(mPlayer_objectId))
                    continue;

                if(mGroupArray.get(member_objectId) == null){
                    try{
                        mGroupArray.put(member_objectId, new PlayerFriend(groupList.get(i), this));
                        mGroupArray.get(member_objectId).insertNewGroup(
                                groupList.get(i).getParseObject("Group_objectId").getObjectId(),
                                groupList.get(i).getParseObject("Group_objectId").getString("Name"));
                    }
                    catch(Exception e){
                        Log.i("Find", e.getMessage());
                    }

                }
                else{
                    mGroupArray.get(member_objectId).insertNewGroup(
                            groupList.get(i).getParseObject("Group_objectId").getObjectId(),
                            groupList.get(i).getParseObject("Group_objectId").getString("Name"));
                }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }


        getSelfFriendList();



    }

    private void getSelfFriendList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User",mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        //query.whereEqualTo("Status", 1);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    filterMembersList(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    private void filterMembersList(List<ParseObject> friendList){

        try{
            if(friendList.size() > 0){
                for (int i = 0; i<mGroupArray.size(); i++){

                    for(int j = 0; j<friendList.size();j++){
                        String Friend_ObjectID = friendList.get(j).getParseObject("Friend_objectId").getObjectId();


                        if((mGroupArray.get(Friend_ObjectID) != null)){



                            if(friendList.get(j).getInt("Status") == 1)
                            {
                                //                            PlayerFriendsArray.get(i).setFriendStatus(1);
                                //                            PlayerFriendsArray.get(i).setIsFriend(true);
                                mGroupArray.remove(Friend_ObjectID);
                            }
                            else
                            {
                                mGroupArray.get(Friend_ObjectID).setFriendStatus(0);
                                mGroupArray.get(Friend_ObjectID).setIsFriend(true);
                            }


//                        if(PlayerFriendsArray.get(i).getFriendObjectID().equals(Player_objectId))
//                            PlayerFriendsArray.get(i).setIsFriend(false);
                        }
                    }
                }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }




        updateListViewAdapter();
    }

    private void updateListViewAdapter(){
        // Construct the data source
        ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
        // Create the adapter to convert the array to views
        //mAdapter = new AppInfoAdapter(this, R.layout.item_group_settings_members, arrayOfUsers, this);
        mAdapter = new AppInfoAdapter(this, R.layout.item_individual_groups, arrayOfUsers, this);
        mAdapter.addAll(mGroupArray.values());
        // Attach the adapter to a ListView
        //mListView = (ListView) findViewById(R.id.listApplication);
        mListView.setAdapter(mAdapter);
        txtLoadingCaption.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                //GameHistory.add(new PlayerFriend(jsonObjects.get(i), i,this));
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    public ArrayList<PlayerFriend> fromJsonParseUser(List<ParseUser> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                //GameHistory.add(new PlayerFriend(jsonObjects.get(i), i,this));
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    private String getUserName(String ObjID){
        String temp = "";
        try{
            for (int i = 0; i<mPlayerFriendsArray.size(); i++){
                if(mPlayerFriendsArray.get(i).getFriendObjectID().equals(ObjID)){
                    temp = mPlayerFriendsArray.get(i).getFriendID();
                    break;
                }

            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }


        return temp;
    }

    private void addFriend(final String Friend_ObjectID){
        List<ParseObject> PlayerList = new ArrayList<ParseObject>();

        ParseObject gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", mPlayerID);
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        gameScore.put("FriendID", getUserName(Friend_ObjectID));
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", Friend_ObjectID));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", getUserName(Friend_ObjectID));
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", Friend_ObjectID));
        gameScore.put("FriendID", mPlayerID);
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {

                sendAddFriendNotification(Friend_ObjectID);
            }
        });
    }

    private void sendAddFriendNotification(String Friend_ObjectID){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", Friend_ObjectID));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 3);
        NewPlayer.put("IsAccepted", false); //By default is False

        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    private void OpenUserFeedPage(String Friend_objectId){
//        ((MainMenuActivity)getActivity()).OpenUserFeedPage(
//                mGlobalPost_List.get(Feed_objectId).getPlayerID(),
//                mGlobalPost_List.get(Feed_objectId).getPlayerObjectID());
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", Friend_objectId);
        intent.putExtra("Player_Name", getUserName(Friend_objectId));
        intent.putExtra("IsGroup", false);
        startActivity(intent);

    }

    public static class AppInfoHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtDesc;
        TextView txtNoOfMembers;
        Button rightButton;
        LinearLayout linearLayout;
    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.ivProfileLeft);
                holder.txtTitle = (TextView) row.findViewById(R.id.tvName);
                holder.txtDesc = (TextView) row.findViewById(R.id.tvDesc);
                holder.txtNoOfMembers = (TextView) row.findViewById(R.id.tvMembers);
                holder.rightButton = (Button) row.findViewById(R.id.txt_action_member);
                holder.linearLayout = (LinearLayout) row.findViewById(R.id.ll_group_item);

                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.FriendID);
            holder.txtTitle.setTag(holder.txtTitle.getId(), appinfo.getFriendObjectID());
            holder.txtTitle.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                            //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                            OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                        }
                    });


            holder.txtDesc.setText(appinfo.getFirstGroup());
            if(appinfo.getTotalNoOfGroups() == 0){
                holder.txtNoOfMembers.setVisibility(View.GONE);
            }
            else{
                holder.txtNoOfMembers.setVisibility(View.VISIBLE);
                holder.txtNoOfMembers.setText(appinfo.getTotalNoOfGroups() +
                        " " + getResources().getText(R.string.lbl_mutual_groups));
            }

            //holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.Friend_objectId, activity));
            try{

                mLoadImageTask = new LoadImageTask(holder.imgIcon, appinfo.Friend_objectId,
                        activity);
                mLoadImageTask.execute(holder.imgIcon);
                holder.imgIcon.setTag(holder.imgIcon.getId(), appinfo.getFriendObjectID());
                holder.imgIcon.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                                OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                            }
                        });

            }
            catch(Exception e){
                Log.i("Group Settings Members", e.getMessage());
            }

            try{
                //dtermine whether it is your friend, not your friend and yourself
                if(appinfo.getFriendObjectID().equals(mPlayer_objectId)){
                    holder.rightButton.setVisibility(View.INVISIBLE);
                    holder.rightButton.setClickable(false);
                }
                else if(appinfo.getIsFriend()){
                    if(appinfo.getFriendType() == 1){
                        holder.rightButton.setVisibility(View.INVISIBLE);
                        holder.rightButton.setClickable(false);
                    }
                    else if(appinfo.getFriendType() == 0)
                    {
                        holder.rightButton.setVisibility(View.VISIBLE);
                        holder.rightButton.setClickable(false);
                        holder.rightButton.setText(getResources().getText(R.string.group_members_friend_requested));
                        holder.rightButton.setBackgroundColor(getResources().getColor(R.color.white));
                    }

                }
                else
                {
                    holder.rightButton.setVisibility(View.VISIBLE);
                    holder.rightButton.setText(getResources().getText(R.string.group_members_notfriend));
                    holder.rightButton.setTag(holder.rightButton.getId(), appinfo.getFriendObjectID());
                    holder.rightButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView temp = (TextView) view;
                            temp.setText(getResources().getText(R.string.group_members_friend_requested));
                            temp.setClickable(false);
                            temp.setBackgroundColor(getResources().getColor(R.color.white));
                            addFriend(String.valueOf(view.getTag(view.getId())));

                        }
                    });
                }
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }


            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //holder.PostImages[i].setImageBitmap(g.loadImageFromStorage(fileName, getActivity()));
            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
            //v = params[0];
            //return mFakeImageLoader.getImage();
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
