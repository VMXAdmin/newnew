package progettox.mm.newnew;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetDataCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

/**
 * Created by Admin on 19/5/2015.
 */
public class ChatActivity extends AppCompatActivity {
    private static final String TAG = ChatActivity.class.getName();
    private static String sUserId;
    private String mChatID;
    private String mChatUsername;
    private String mChatUserObjectID;
    private String mChannel;
    public String mPlayerID;
    public String mOtherPlayerID;
    public String mPlayer_objectId;
    public String mActionType;
    public ParseFile mProfilePic;
    private boolean mIsDirty;

    public static final String USER_ID_KEY = "User_ObjectID";
    private static final int MAX_CHAT_MESSAGES_TO_SHOW = 20;

    private EditText etMessage;
    private Button btSend;

    private ListView lvChat;
//    private ArrayList<Message> mMessages;
    private ArrayList<ChatDetailsHolder> mMessages;
    private Hashtable<String, progettox.mm.newnew.ChatItem> mHTChatItemList;

    private chatListAdapter mAdapter;
    private ProgressDialog mDialog;
    private static final int CAMERA_REQUEST = 1888;
    static final int REQUEST_TAKE_PHOTO = 1;
    private String mCurrentPhotoPath;
    private Bitmap mUserPhoto;
    public ChatActivity(){}
    private Activity mActivity;
    private App mAppState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mActivity = this;

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            //mUserPhoto = savedInstanceState.getByteArray("PostImage");
            Bitmap bmp = savedInstanceState.getParcelable("ChatImage");
            mCurrentPhotoPath = savedInstanceState.getString("ImagePath");
            if(bmp != null){
                mUserPhoto = bmp;
                //imageView.setImageBitmap(mUserPhoto);
            }

        } else {
            // Probably initialize members with default values for a new instance
        }

    }


    public void Initialize(){
        mAppState = ((App)getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();
        mAppState.setPageNumber(10);

        mIsDirty = false;

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Just another load away, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);
        //mDialog.show();


        Bundle extras = getIntent().getExtras();
        if (extras != null){
            mChatID = extras.getString("ChatID");
            mChatUsername = extras.getString("ChatUsername");
            mChatUserObjectID = extras.getString("ChatUserObjectID");
            mChannel = extras.getString("Channel");
            mActionType = extras.getString("ChatAlertType");
        }

        setChatID(mChatID);
        setTitle(mChatUsername);
        sUserId = mPlayer_objectId;

        startWithCurrentUser();
        //receiveMessage();
        if(mActionType != null){
            if(mActionType.equals("PUSH")) {
                checkChatCache();
                receiveMessage();
            }
            else
                checkChatCache();
                checkForChatUpdates();

        }
        else
        {
            checkChatCache();
            checkForChatUpdates();
        }

        ImageButton cameraBtn = (ImageButton) findViewById(R.id.btCamera);
        cameraBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                dispatchTakePictureIntent();
            }
        });

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        if(storageDir.isDirectory()){
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );


        }
        else{
            storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        }
// Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.getAbsolutePath();
        galleryAddPic();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //...
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(mCurrentPhotoPath);
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

            File file = new File(mCurrentPhotoPath);

            if(file.exists()){

                //int imageWidth = mScreenWidth * 0.8;
                Bitmap UserPhoto = BitmapFactory.decodeFile(file.getAbsolutePath());
                int oriImageWidth =  UserPhoto.getWidth();
                int oriImageHeight = UserPhoto.getHeight();

                int imageWidth = 1024;
                int imageHeight = 576;

                //check if image is horizontal or vertical
                if(oriImageWidth > oriImageHeight){
                    imageWidth = 1024;
                    imageHeight = 576;
                }
                else{
                    imageWidth = 576;
                    imageHeight = 1024;
                }

                BitmapFactory.Options bounds = new BitmapFactory.Options();
                bounds.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), bounds);

                BitmapFactory.Options opts = new BitmapFactory.Options();
                //Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                //BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                opts.inSampleSize = BitmapManager.calculateInSampleSize(oriImageWidth, oriImageHeight,
                        imageWidth, imageHeight);

                opts.inJustDecodeBounds = false;
                //return BitmapFactory.decodeResource(res, resId, options);
                Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
//                Bitmap bm = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                            file.getAbsolutePath(), imageWidth, imageHeight);

                ExifInterface exif = null;
                try{
                    exif = new ExifInterface(file.getAbsolutePath());
                }
                catch(Exception e){

                }

                String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

                int rotationAngle = 0;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

                Matrix matrix = new Matrix();
                matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
                //UserPhoto = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
                mUserPhoto = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mUserPhoto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] dataStream = stream.toByteArray();
                final ParseFile imgFile = new ParseFile ("Img1.png", dataStream);
                imgFile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        saveNewMessageIntoChatHistory(imgFile);
                    }
                });
            }





            //mUserPhoto = (Bitmap) data.getExtras().get("data");

            //here we save the photo to Parse
//            final Message message = new Message();
//            message.setUserId(sUserId);
//            message.setBody("png");
//            message.setUserObjectId(sUserId);
//            message.setChatId(mChatID);
//            message.setChatObjectId(mChatID);

//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            mUserPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            byte[] dataStream = stream.toByteArray();
//            final ParseFile imgFile = new ParseFile ("Img1.png", dataStream);
//            imgFile.saveInBackground();

//            message.put("Image", imgFile);
//
//
//
//            message.saveInBackground(new SaveCallback() {
//                @Override
//                public void done(ParseException e) {
//
//
//                    StoreChatImageIntoLocalDir(imgFile, message.getObjectId());
////                    receiveMessage();
//                    updateViaPush();
//                }
//            });
        }


    }

    private void saveNewMessageIntoChatHistory(final ParseFile obj){
        final Message message = new Message();
        message.setUserId(sUserId);
        message.setBody("*Image");
        message.setUserObjectId(sUserId);
        message.setChatId(mChatID);
        message.setChatObjectId(mChatID);

        message.put("Image", obj);



        message.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                storeChatImage(mUserPhoto, message.getObjectId(), mActivity);
                //StoreChatImageIntoLocalDir(obj, message.getObjectId());
//                    receiveMessage();
                updateViaPush();
            }
        });

    }

    private void storeChatImage(Bitmap bitmapImage, String FileName, Activity activity) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();

            receiveMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(mUserPhoto != null){
//            int bytes = mUserPhoto.getByteCount();
//            ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
//            mUserPhoto.copyPixelsToBuffer(buffer); //Move the byte data to the buffer
//
//            byte[] array = buffer.array(); //Get the underlying array containing the data.
//            savedInstanceState.putByteArray("PostImage", array);
            savedInstanceState.putParcelable("ChatImage",mUserPhoto);
            savedInstanceState.putString("ImagePath", mCurrentPhotoPath);
        }


        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void checkForChatUpdates(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_History");
        query.whereEqualTo("Chat_ObjectID", mChatID);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                if(mHTChatItemList != null){
                    if(mHTChatItemList.get(mChatID) != null){
                        if(mHTChatItemList.get(mChatID).getChatDetails() != null){
                            if(mHTChatItemList.get(mChatID).getChatDetails().size() != i){
                                receiveMessage();
                                mIsDirty = true;
                                //mDialog.show();
                            }
                        }
                    }
                }

            }
        });
    }

    private void checkChatCache(){
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);

        //check if we have cache of CHAT data
        String JSON = sp.getString("mHTChatItemList", "");
        if(JSON == null || JSON.equals("") || JSON.equals("null")){
            //here we start to load and populate the user's chat data and history
            //initializeAdapter();
            receiveMessage();
        }
        else
        {
            //here we just reload the JSON into our hashTable item
            mHTChatItemList = new Hashtable<String, progettox.mm.newnew.ChatItem>();
            Gson gson = new Gson();
            Type stringStringMap = new TypeToken<Hashtable<String, progettox.mm.newnew.ChatItem>>(){}.getType();
            mHTChatItemList = gson.fromJson(JSON, stringStringMap);

            //mMessages = mHTChatItemList.get(mChatID).getChatDetails();
            if(mHTChatItemList.get(mChatID) != null){
                if(mHTChatItemList.get(mChatID).getChatDetails() != null){
                    if(mHTChatItemList.get(mChatID).getChatDetails().size() > 0){
                        //mAdapter.addAll(mMessages);
                        mAdapter = new chatListAdapter(ChatActivity.this, sUserId, mHTChatItemList.get(mChatID).getChatDetails(), this);
                        //mAdapter.addAll(mHTChatItemList.get(mChatID).getChatDetails());
                        lvChat.setAdapter(mAdapter);
                        lvChat.invalidate(); // redraw listview
                        lvChat.setSelection(mAdapter.getCount()-1);
                        mDialog.dismiss();

                        mIsDirty = false;
                    }
                }
            }



        }
    }

//    @Override
//    public void onBackPressed()
//    {
//        // code here to show dialog
//        //super.onBackPressed();  // optional depending on your needs
//        //Intent i = new Intent(this, MainMenuActivity.class);
//        //i.setFlags(Intent.FLAG
// _ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK);
//        //i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
//        //i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK);
//        //startActivity(i);
//
//        if(mActionType.equals("PUSH")){
//            Intent i = new Intent(this, MainMenuActivity.class);
//            startActivity(i);
//        }
//        else{
//            finish();
//        }
//
//    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        Initialize();

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        //check to see if we entered via Push
        Bundle extras = getIntent().getExtras();
        //String ActionType ="";
        if (extras != null){
            mChatID = extras.getString("ChatID");
            mChatUsername = extras.getString("ChatUsername");
            mChannel = extras.getString("Channel");
            mActionType = extras.getString("ChatAlertType");
        }

        setChatID(mChatID);
        setTitle(mChatUsername);
        receiveMessage();

    }


    // Get the userId from the cached currentUser object
    private void startWithCurrentUser() {
        //sUserId = ParseUser.getCurrentUser().getObjectId();
        sUserId = mPlayer_objectId;
        setupMessagePosting();

    }

    private void setChatID(String ID){
        App appState = ((App)getApplicationContext());
        appState.setChatID(ID);
    }

    private void initializeAdapter(){
        mAdapter = new chatListAdapter(ChatActivity.this, sUserId, mHTChatItemList.get(mChatID).getChatDetails(), this);
        lvChat.setAdapter(mAdapter);
    }

    // Setup button event handler which posts the entered message to Parse
    private void setupMessagePosting() {
        etMessage = (EditText) findViewById(R.id.etMessage);
        btSend = (Button) findViewById(R.id.btSend);
        lvChat = (ListView) findViewById(R.id.lvChat);
        mMessages = new ArrayList<ChatDetailsHolder>();
        //mAdapter = new chatListAdapter(ChatActivity.this, sUserId, mMessages, this);
//        mAdapter = new chatListAdapter(ChatActivity.this, sUserId, mHTChatItemList.get(mChatID).getChatDetails(), this);
//        lvChat.setAdapter(mAdapter);
        btSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String body = etMessage.getText().toString();
                // Use Message model to create new messages now
                Message message = new Message();
                message.setUserId(sUserId);
                message.setBody(body);
                message.setUserObjectId(sUserId);
                message.setChatId(mChatID);
                message.setChatObjectId(mChatID);
                message.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        receiveMessage();
                        updateViaPush();
                    }
                });
                etMessage.setText("");



                //TODO test out web api
                testChatWebAPI();
            }
        });
    }

    private void updateViaPush(){

        //if(mMessages.get(0).getIsGroup())
        //{
//            HashMap<String, Object> params = new HashMap<>();
//            params.put("GN", mChatUsername);
//            params.put("CI", mChatID);
//            params.put("CH", mChannel);
//
//
//            ParseCloud.callFunctionInBackground("GROUPCHATPUSH", params, new FunctionCallback<Object>() {
//            @Override
//            public void done(Object o, ParseException e) {
//
//            }
//        });

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_UserList");
            query.whereEqualTo("Chat_ObjectID", mChatID);
            query.include("User_objectId");
            //query.addDescendingOrder("createAt");
            //query.orderByAscending("createAt");
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + scoreList.size() + " scores");
                        if(scoreList.size() > 0){
                            Push(scoreList);
                        }
                        else
                        {
                            mDialog.dismiss();
                            //TODO to display textView message saying no entries has been made yet
                        }

                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
    }

    public void Push(List<ParseObject> ListObj){

        //TODO if the group is only 2 ppl, the receipient ID here becomes the group name
        //TODO causing the push not able to be sent to the recipient
        if(ListObj.size() < 3){
//            ParsePush parsePush = new ParsePush();
//            ParseQuery pQuery = ParseInstallation.getQuery(); // <-- Installation query
//            pQuery.whereEqualTo("username", mOtherPlayerID); // <-- you'll probably want to target someone that's not the current user, so modify accordingly
//            parsePush.sendMessageInBackground("CHAT " + mChatID + " " + mPlayerID, pQuery);

            HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("recipientId", mChatUsername);
            params.put("message", "CHAT " + mChatID + " " + mPlayerID);
            ParseCloud.callFunctionInBackground("sendPushToUser", params, new FunctionCallback<String>() {
                @Override
                public void done(String o, ParseException e) {
                    if (e == null) {
                        // Push sent successfully
                        Log.i("Cloud Push", " was successfully sent");
                    }
                }
            });
        }
        else
        {
            for (int i = 0; i<ListObj.size();i++){

                if(!ListObj.get(i).getParseObject("User_objectId").getString("username").equals(mPlayerID))
                {
//                    ParsePush parsePush = new ParsePush();
//                    ParseQuery pQuery = ParseInstallation.getQuery(); // <-- Installation query
//                    pQuery.whereEqualTo("username", ListObj.get(i).getParseObject("User_objectId").getString("username")); // <-- you'll probably want to target someone that's not the current user, so modify accordingly
//                    parsePush.sendMessageInBackground("CHAT " + mChatID + " " + mChatUsername, pQuery);

                    HashMap<String, Object> params = new HashMap<String, Object>();
                    params.put("recipientId", ListObj.get(i).getParseObject("User_objectId").getString("username"));
                    params.put("message", "CHAT " + mChatID + " " + mChatUsername);
                    ParseCloud.callFunctionInBackground("sendPushToUser", params, new FunctionCallback<String>() {
                        @Override
                        public void done(String o, ParseException e) {
                            if (e == null) {
                                // Push sent successfully
                                Log.i("Cloud Push", " was successfully sent");
                            }
                        }
                    });
                }

            }
        }



    }

    // Query messages from Parse so we can load them into the chat adapter
    public void receiveMessage() {
        // Construct query to execute
        setChatID(mChatID);
        ParseQuery<Message> query = ParseQuery.getQuery(Message.class);
        // Configure limit and sort order
        //TODO to put limit on no of messages to load
        //query.setLimit(MAX_CHAT_MESSAGES_TO_SHOW);

        query.orderByAscending("createdAt");
        query.whereEqualTo("Chat_ObjectID", mChatID);
        query.include("User_objectId");
        query.include("Chat_objectId");
        // Execute query to fetch all messages from Parse asynchronously
        // This is equivalent to a SELECT query with SQL
        query.findInBackground(new FindCallback<Message>() {
            public void done(List<Message> messages, ParseException e) {
                if (e == null) {
                    //mMessages.clear();
                    //mMessages.addAll(messages);
                    //mAdapter.notifyDataSetChanged(); // update adapter
                    //lvChat.invalidate(); // redraw listview
                    if(messages != null)
                    {
                        if(messages.size() > 0){
                            mIsDirty = true;
                            updateHTObject(messages);
                            UpdateMessages(messages);
                        }
                    }

                } else {
                    Log.d("message", "Error: " + e.getMessage());
                }
            }
        });
    }


    private void updateHTObject(List<Message> messages){


        //1st, we need to add a new ChatItem to the HT
        //String ChatMasterID = messages.get(0).getParseObject("Chat_objectId").getObjectId();
        String ChatMasterID = mChatID;
//        for (int i=0;i<messages.size();i++){
//            try{
//
//            }catch(Exception e){
//                Log.e("Error", e.getMessage())
//            }
//        }
//
        if(mHTChatItemList == null){
            mHTChatItemList = new Hashtable<String, ChatItem>();
            mHTChatItemList.put(ChatMasterID,
                    new ChatItem(messages.get(0), mHTChatItemList.size()+1, this));
        }
        else{
            if(mHTChatItemList.get(ChatMasterID) == null){
                mHTChatItemList.put(ChatMasterID,
                        new ChatItem(messages.get(0), mHTChatItemList.size()+1, this));
            }
        }




        //need to input ChatName into the object
        //if(!messages.get(0).getParseObject("Chat_objectId").getBoolean("IsGroup")){
        mHTChatItemList.get(ChatMasterID).updateChatName(mChatUsername, mChatUserObjectID);
        //}


    }

    public void UpdateMessages(List<Message> messages){
        //mAdapter.setNotifyOnChange(false);

        int lastViewedPosition = lvChat.getFirstVisiblePosition();
        View v = lvChat.getChildAt(0);
        int topOffset = (v == null) ? 0 : v.getTop();


        mMessages.clear();
        //mAdapter.clear();
        if(mHTChatItemList.get(mChatID).getChatDetails()!= null) {
            mHTChatItemList.get(mChatID).getChatDetails().clear();
        }
        else {
            mHTChatItemList.get(mChatID).createNewList();
        }

        String ChatPlayer = "";
        for (int i=0;i<messages.size();i++){
            mMessages.add(new ChatDetailsHolder(messages.get(i), this));

            //mHTChatItemList.get(mChatID).getChatDetails().add(new ChatDetailsHolder(messages.get(i)));
            if (!sUserId.equals(messages.get(i).getString("User_ObjectID"))){
                ChatPlayer = messages.get(i).getParseObject("User_objectId").getString("username");
            }
        }

        if(mHTChatItemList.get(mChatID) == null){
            //ChatItem CI = new ChatItem()
            //mHTChatItemList.put()
        }




        mOtherPlayerID = ChatPlayer;

        mHTChatItemList.get(mChatID).getChatDetails().addAll(mMessages);

        if (mAdapter == null){
            mAdapter = new chatListAdapter(ChatActivity.this, sUserId, mHTChatItemList.get(mChatID).getChatDetails(), this);
            lvChat.setAdapter(mAdapter);
        }

//        setTitle(ChatPlayer);
        //mMessages.addAll(messages);

        //mAdapter.updateReceiptList(mMessages);
        //mAdapter.clear();
        //mAdapter.addAll(mHTChatItemList.get(mChatID).getChatDetails());
        //mAdapter.updateReceiptList(mMessages);


        mAdapter.notifyDataSetChanged(); // update adapter
        lvChat.setSelectionFromTop(lastViewedPosition, topOffset);
        lvChat.invalidate(); // redraw listview
        //lvChat.setSelection(mAdapter.getCount()-1);
        //lvChat.smoothScrollToPosition(mAdapter.getCount()-1);
        mDialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        setChatID("");

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);

            SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();

            this.mIsDirty = false;
        }

    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first
        setChatID("");

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);

            SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();

            this.mIsDirty = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        setChatID("");

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);

            SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();

            this.mIsDirty = false;
        }
    }

    private void testChatWebAPI(){
        String testURL = "http://192.168.1.181:1028/AngularJSAuthentication.API/Api/ChatHistory/Add";
        insertChat newTask = new insertChat(testURL);
        newTask.execute();
    }

    public class insertChat extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        insertChat(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {

                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_feed_liked);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] bitmapdata = stream.toByteArray();
                String imgAzure = Base64.encodeToString(bitmapdata,0);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("objectId", 0);
                jsonParam.put("Chat_objectId", 8);
                jsonParam.put("User_objectId", 9);
                jsonParam.put("Body", "KONG LIANG");
                jsonParam.put("IsImage", "true");
                jsonParam.put("Image", imgAzure);
                jsonParam.put("FileName", null);
                java.util.Date utilDate = new java.util.Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                jsonParam.put("createdAt", sqlDate);
                jsonParam.put("updatedAt", sqlDate);

                String URLArgs = jsonParam.toString();


                //URL url = new URL("http://192.168.1.181:8080/AngularJSAuthentication.API/token");
                URL url = new URL(URLHttp);

                urlConn = (HttpURLConnection) url.openConnection();
                //urlConn = url.openConnection();
                urlConn.setDoInput (true);
                urlConn.setDoOutput (true);
                urlConn.setUseCaches (false);
                urlConn.setRequestProperty("Content-Type","application/json");
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                urlConn.connect();

                OutputStream os = urlConn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                //URLArgs = "grant_type=password&username=sridhar&password=sridhar&client_id=Android&client_secret=abc%40123";
                writer.write(URLArgs);
                writer.flush();
                writer.close();
                os.close();


                //Get Response
//                InputStream is = urlConn.getInputStream();
//                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//                String lines;
//                StringBuffer response = new StringBuffer();
//                while((lines = rd.readLine()) != null) {
//                    response.append(lines);
//                    response.append('\r');
//                }
//                rd.close();
                //return response.toString();


                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }




                //InputStream in = new BufferedInputStream(urlConn.getInputStream());

//                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    result.append(line);
//                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONObject obj = new JSONObject(result);

                //JSONArray data = obj.getJSONArray(obj.getString("d"));
                String testArray = obj.getString("access_token");
                //mAppSate.setToken(testArray);
                //JSONArray data = new JSONArray(testArray);
                //String friendID = data.getString("friendID");
                //String playerID = data.getString("playerID");

                Log.d("My App", obj.toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }
}
