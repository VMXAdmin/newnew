package progettox.mm.newnew;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

//import com.baidu.mapapi.SDKInitializer;
//import com.baidu.mapapi.map.LocationData;
//import com.baidu.mapapi.map.MKMapViewListener;
//import com.baidu.mapapi.map.MapController;
//import com.baidu.mapapi.map.MapView;
//import com.baidu.mapapi.map.MyLocationOverlay;
//import com.baidu.platform.comapi.basestruct.GeoPoint;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.app.Activity;
import android.os.Bundle;

public class MapMainActivity extends AppCompatActivity {

    public LocationClient mLocationClient = null;
    public BDLocationListener myListener = new MyLocationListener();

    private double latitude, longitude;
    private static final String TAG = "CouponOrderActivity";
    private static final String TAG_SUCCESS = "success";
    //UI
    Button requestLocButton = null;
    boolean isRequest = false;//Whether the manual trigger request
    boolean isFirstLoc = true;//Whether the first positioning

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //SDKInitializer.initialize(getApplicationContext());
        super.onCreate(savedInstanceState);

        setContentView(R.layout.friends_invite_listview);
        setTitle("Positioning function");

        requestLocButton = (Button)findViewById(R.id.button1);
        requestLocButton.setOnClickListener(btnClickListener);

//Map initialization
        //mMapView = (MapView)findViewById(R.id.bmapView);
//        mMapController = mMapView.getController(); //Get a map of the controller
//        mMapController.setZoom(13);//Set map zoom level;
//        mMapController.enableClick(true);//Set map whether responses to click event.
//        mMapView.setBuiltInZoomControls(true);//Display a built-in zoom control

//Positioning initialization
        mLocationClient = new LocationClient(getApplicationContext()); // 声明LocationClient类
        //mLocationClient.setAccessKey("BpGYSXjAPeGID253M5UpDw0K");
        mLocationClient.registerLocationListener(myListener); // 注册监听函数

        //setLocationOption();
        mLocationClient.start();// 开始定位


//The orientation layer initialization
        //myLocationOverlay = new locationOverlay(mMapView);
//Positioning data
        //myLocationOverlay.setData(locData);
//Add the orientation layer
        //mMapView.getOverlays().add(myLocationOverlay);
        //myLocationOverlay.enableCompass();
//Modify the positioning data refresh layer effect
        //mMapView.refresh();

    }

    //The positioning button monitor
    OnClickListener btnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
// TODO Auto-generated method stub
//Manual positioning request
            requestLocClick();
        }
    };

//    private void setLocationOption() {
//        LocationClientOption option = new LocationClientOption();
//        option.setOpenGps(true);
//        option.setIsNeedAddress(true); // 返回的定位结果包含地址信息
//        option.setAddrType("all"); // 返回的定位结果包含地址信息
//        option.setCoorType("bd09ll"); // 返回的定位结果是百度经纬度,默认值gcj02
//        option.setScanSpan(5000); // 设置发起定位请求的间隔时间为5000ms
////        option.disableCache(true); // 禁止启用缓存定位
////        option.setPoiNumber(5); // 最多返回POI个数
////        option.setPoiDistance(1000); // poi查询距离
////        option.setPoiExtraInfo(true); // 是否需要POI的电话和地址等详细信息
//        mLocationClient.setLocOption(option);
//    }

    public class MyLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null)
                return;
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            Log.i(TAG,
                    "latitude in onReceiveLocation-> "
                            + String.valueOf(latitude));
            Log.i(TAG,
                    "longitude in onReceiveLocation-> "
                            + String.valueOf(longitude));

        }

        public void onReceivePoi(BDLocation poiLocation) {
            // 将在下个版本中去除poi功能
            if (poiLocation == null) {
                return;
            }
        }
    }

    //Inheritance of MyLocationOverlay overrides the dispatchTap click the processing
//    public class locationOverlay extends MyLocationOverlay{
//
//        public locationOverlay(MapView mapView) {
//            super(mapView);
//// TODO Auto-generated constructor stub
//        }
//    }

    //Manual trigger a location request
    public void requestLocClick(){
        isRequest = true;
        //mLocClient.requestLocation();
        Toast.makeText(MapMainActivity.this, "Locating...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.global_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {
            //getFriendInvites();
            //createNewFeedGroup();
            isRequest = true;
            mLocationClient.requestLocation();
            Toast.makeText(MapMainActivity.this, "Locating...", Toast.LENGTH_SHORT).show();
            //return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        if (mLocationClient != null){
            mLocationClient.stop();
        }
        //mMapView.destroy();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        //mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        //mMapView.onResume();
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //mMapView.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //mMapView.onRestoreInstanceState(savedInstanceState);
    }

}
