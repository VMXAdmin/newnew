package progettox.mm.newnew;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 10/6/2015.
 */
public class activity_feed_targeted extends AppCompatActivity {
    //public String PlayerID;
    private String mPlayer_objectId;
    private String mPlayer_Name;
    private String mUser_objectId;
    private String mUser_Name;
    private String mGroupOwner_objectID;
    private Boolean mIsGroup;
    private Boolean mIsMember;
    private Boolean mIsUser;
    private String mGroupName;
    private Boolean mIsTutor;
    private String mGroup_objectId;
    private String mGroup_description;
    private int mGroup_type; //0 is open, 1 is closed, 2 is secret
    private String mGroup_date;
    public Hashtable<String, GlobalPost> mGlobalPost_List;
    public GlobalPostAdapter mAdapter;
    public int mScreenHeight;
    public int mScreenWidth;
    private boolean mIsDirty;
    private LoadImageTask mLoadImageTask;
    private ProgressDialog mDialogImageLoad;
    private ImageView userAvatar;
    private LinearLayout userBackground;
    private TextView txtUserName ;
    private TextView txtFeedTag;
    ArrayList<String> FeedIDs = new ArrayList<>();
    private int mSdkVersion;
    private TextView mLoadingFeedCaption;
    //bottom menun buttons
    private Button mBtnMenuMore;
    private Button mBtnHome;
    private LinearLayout mBottomLayout;
    private ListView listView;

    public activity_feed_targeted(){}

    private void OpenSettingsPage(){

        if(mIsGroup){
            Intent intent = new Intent(this, activity_group_settings.class);
            //Bundle bundle = new Bundle();
            intent.putExtra("IsGroup", mIsGroup);
            intent.putExtra("IsTutor", mIsTutor);
            intent.putExtra("Group_Name", mGroupName);
            intent.putExtra("Group_ObjectID", mGroup_objectId);
            intent.putExtra("Group_Desc", mGroup_description);
            intent.putExtra("Group_Type", mGroup_type);
            intent.putExtra("Group_Date", mGroup_date);
            intent.putExtra("Tutor_Name", mUser_Name);
            intent.putExtra("Tutor_ObjectID", mUser_objectId);
            intent.putExtra("Creator_ObjectID", mGroupOwner_objectID);

            startActivity(intent);
        }

        if(mIsUser){
            Intent intent = new Intent(this, activity_individual_settings.class);
            intent.putExtra("User_ObjectID", mUser_objectId);
            intent.putExtra("User_Name", mUser_Name);
            startActivity(intent);
        }


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.globalfeed_group_main);

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;

        mDialogImageLoad = new ProgressDialog(this);
        mDialogImageLoad.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialogImageLoad.setMessage("Loading your image.");
        mDialogImageLoad.setIndeterminate(true);
        mDialogImageLoad.setCanceledOnTouchOutside(false);

        mLoadingFeedCaption = (TextView) findViewById(R.id.lbl_loading_feed);
        mBottomLayout = (LinearLayout) findViewById(R.id.layout_bottom_frame);
        mBottomLayout.setVisibility(View.GONE);
        mIsGroup = false;
        mIsUser = false;

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            //mPlayer_objectId = extras.getString("PlayerObjectID");

            //check for group
            Boolean isGroup = extras.getBoolean("IsGroup");
            if(isGroup != null){
                if (isGroup){
                    mIsGroup = true;
                    mIsUser = false;
                    mGroup_objectId = extras.getString("GroupObjectID");
                    mIsTutor = extras.getBoolean("IsTutor");
                    mGroupName = extras.getString("GroupName");
                    setTitle(mGroupName);
                }
                else
                {
                    mUser_objectId = extras.getString("PlayerObjectID");
                    mUser_Name = extras.getString("Player_Name");
                    mIsUser = true;
                    mIsGroup = false;
                }
            }
        }

        Initialize();


    }

    private void checkGroupImage(){

    }

    private void Initialize(){

        App appState = ((App)getApplicationContext());
        mPlayer_Name = appState.getPlayerID();
        mPlayer_objectId = appState.getPlayer_objectId();

        checkGroupImage();

        //load user avatar and background
        userAvatar = (ImageView) findViewById(R.id.img_user_avatar);
        userBackground = (LinearLayout) findViewById(R.id.layout_user_background);
        txtUserName = (TextView) findViewById(R.id.txt_feed_name);
        txtFeedTag = (TextView) findViewById(R.id.txt_feed_tag);
        mSdkVersion = android.os.Build.VERSION.SDK_INT;

        userBackground.setVisibility(View.GONE);



        mIsMember = false;

        //Here we check to see if you are already in the group
        if (mIsGroup){
            checkIsMember();
        }
        else{
            checkIsFriend();
        }



    }

    private void prepBottomButtons(Boolean status, int memberStatus){
        mBtnMenuMore = (Button) findViewById(R.id.fourthPage_action);
        mBtnMenuMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                OpenSettingsPage();
            }
        });

        mBtnHome = (Button) findViewById(R.id.firstPage_action);
        mBtnHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToPostNew();
            }
        });

        Button btnInvite = (Button) findViewById(R.id.secondPage_action);
        btnInvite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToInviteFriends();
            }
        });

        //for individual page, no need to have function to reco friends
        if(mIsUser == true || memberStatus == 0){
            btnInvite.setVisibility(View.GONE);
        }

        if (mIsGroup){
            if(!status){
                mBtnHome.setText(getResources().getText(R.string.lbl_join));
                mBtnHome.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        addGroup();
                    }
                });
            }
            else{
                if(memberStatus == 0){
                    mBtnHome.setText(getResources().getText(R.string.lbl_join_sent));
                    mBtnHome.setClickable(false);
                }
            }
        }
        else{
            if(!status){
                mBtnHome.setText(getResources().getText(R.string.lbl_add_friend));
                mBtnHome.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        addFriend();
                    }
                });
            }
            else{
                if(memberStatus == 0){
                    mBtnHome.setText(getResources().getText(R.string.lbl_join_sent));
                    mBtnHome.setClickable(false);
                }

            }
        }
    }

    private void addGroup(){

        final ParseObject gameScore = new ParseObject("GlobalFeed_Group_Members");
        gameScore.put("Group_ObjectID", mGroup_objectId);
        gameScore.put("Group_objectId", ParseObject.createWithoutData("GlobalFeed_Group_Master",
                mGroup_objectId));
        gameScore.put("Member_ObjectID", mPlayer_objectId);
        gameScore.put("Member_objectId", ParseObject.createWithoutData("_User",
                mPlayer_objectId));
        gameScore.put("Member_Status", 0);
        gameScore.put("IsNotify", true); //by default, group notifications are set to ON
        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                RequestCompleted();
                sendJoinGroupNotification();
            }
        });
    }

    private void sendJoinGroupNotification(){

        //First we need to find out who is the group admin/creator
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Master");
        query.whereEqualTo("objectId", mGroup_objectId);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size() > 0)
                        sendJoinGroupNotification_2(scoreList.get(0).getString("Creator_ObjectID"));
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


    }

    private void sendJoinGroupNotification_2(String groupCreator){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", groupCreator));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 7);
        NewPlayer.put("IsAccepted", false); //By default is False
        NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", mGroup_objectId));
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    private void sendAddFriendNotification(){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", mUser_objectId));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 3);
        NewPlayer.put("IsAccepted", false); //By default is False

        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    private void addFriend(){

        List<ParseObject> PlayerList = new ArrayList<ParseObject>();

        ParseObject gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", mPlayer_Name);
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        gameScore.put("FriendID", mUser_Name);
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", mUser_objectId));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", mUser_Name);
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", mUser_objectId));
        gameScore.put("FriendID", mPlayer_Name);
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                RequestCompleted();
                sendAddFriendNotification();
            }
        });

//        gameScore.saveInBackground(new SaveCallback() {
//            public void done(ParseException e) {
//                RequestCompleted();
//                sendAddFriendNotification();
//            }
//        });







    }

    private void RequestCompleted(){
        mBtnHome.setText(getResources().getText(R.string.lbl_join_sent));
        mBtnHome.setClickable(false);
        Toast.makeText(this,
                getResources().getText(R.string.toast_add_friend),
                Toast.LENGTH_SHORT).show();
    }

    private void checkIsFriend(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");

        ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        obj = ParseObject.createWithoutData("_User", mUser_objectId);
        query.whereEqualTo("Friend_objectId", obj);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size() > 0){
                        //second arg is to determine whether have you requested to be a friend
                        //as 0 means requested, 1 means friend, no record means no connection
                        prepBottomButtons(true, scoreList.get(0).getInt("Status"));
                    }
                    else{
                        prepBottomButtons(false, 0);
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void OpenUserFeedPage(String Feed_objectId){

        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", mGlobalPost_List.get(Feed_objectId).getPlayerObjectID());
        intent.putExtra("Player_Name", mGlobalPost_List.get(Feed_objectId).getPlayerID());
        intent.putExtra("IsGroup", false);
        startActivity(intent);

    }

    private void checkIsMember(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");

        //check whether it is group or individual
        if(mIsGroup){
            query.whereEqualTo("Group_ObjectID", mGroup_objectId);
            query.whereEqualTo("Member_ObjectID", mPlayer_objectId);
            //query.whereEqualTo("Member_Status", 1);
        }
        else{
            query.whereEqualTo("User_ObjectID", mPlayer_objectId);
        }

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size() > 0){
                        //second arg is to determine whether have you requested to be a member
                        //as 0 means requested, 1 means active member, no record means no connection
                        prepBottomButtons(true, scoreList.get(0).getInt("Member_Status"));
                    }
                    else{
                        prepBottomButtons(false, 0);
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void goToPostNew(){
        Intent createNewPost = new Intent(this, createNewGlobalFeed.class);
        //Type 1 is GLOBAL FEED, Type 2 is GROUP FEED, Type 3 is USER/INDIVIDUAL FEED
        if(mIsGroup){
            createNewPost.putExtra("GroupID", mGroup_objectId);
            createNewPost.putExtra("GroupName", mGroupName);
            createNewPost.putExtra("Type", 2);
            createNewPost.putExtra("GroupType", mGroup_type);
            createNewPost.putExtra("IsTutor", mIsTutor);
        }
        else
        {
            if(mIsUser){
                createNewPost.putExtra("UserID", mUser_objectId);
                createNewPost.putExtra("UserName", mUser_Name);
                createNewPost.putExtra("Type", 3);
            }
        }

        startActivity(createNewPost);
    }

    private void goToInviteFriends(){
        Intent createNewPost = new Intent(this, activity_group_invite_friends.class);
        //Type 1 is GLOBAL FEED, Type 2 is GROUP FEED, Type 3 is USER/INDIVIDUAL FEED
        if(mIsGroup){
            createNewPost.putExtra("Group_ObjectID", mGroup_objectId);
            createNewPost.putExtra("Group_Name", mGroupName);
            createNewPost.putExtra("Group_Type", 2);
        }

        startActivity(createNewPost);
    }


    public void OpenFeedDetails(String FeedID){
        //GotoGlobalFeedDetails(FeedID);

        App appState = ((App)getApplicationContext());
        appState.setGlobalFeedID(FeedID);

        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, globalfeed_secondary_activity.class);

        try{
            bundle.putString("Feed_ObjectID", FeedID);
            bundle.putString("User_Name", mGlobalPost_List.get(FeedID).getPlayerID());
            bundle.putString("User_ObjectID", mGlobalPost_List.get(FeedID).getPlayerObjectID());
            bundle.putString("Message", mGlobalPost_List.get(FeedID).getMessage());
            bundle.putInt("Type", mGlobalPost_List.get(FeedID).getPostType());
            bundle.putBoolean("IsGroup", mGlobalPost_List.get(FeedID).getIsGroup());
            bundle.putString("Group_Name", mGlobalPost_List.get(FeedID).getGroupName());
            bundle.putString("Group_ObjectID", mGlobalPost_List.get(FeedID).getGroup_ObjectID());
            bundle.putString("CreatedAtDisplay", mGlobalPost_List.get(FeedID).getDate());
            bundle.putSerializable("CreatedAt", mGlobalPost_List.get(FeedID).getCreatedAt());
        }catch(Exception e){
            Log.e("Error", e.getMessage());
            return;
        }

        intent.putExtras(bundle);
        startActivity(intent);
    }

    private boolean checkDeviceCache(){

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        //SharedPreferences.Editor ed = sp.edit();
//        ed.putString("mHTChatItemList", "");
//        ed.apply();

        //check if we have cache of CHAT data
        String JSON = sp.getString("mGlobalPost_List", "");
        if(JSON == null || JSON.equals("")){
            //here we start to load and populate the user's chat data and history
            //preLoadFeedData();
            return false;
        }

        return true;
    }

    public class FeedComparator implements Comparator<GlobalPost>
    {
        public int compare(GlobalPost left, GlobalPost right) {
            //return left.name.compareTo(right.name);
            return left.getCreatedAt().compareTo(right.getCreatedAt());
        }
    }

    private void preLoadFeedData(){
        //here we just reload the JSON into our hashTable item
        //mGlobalPost_List = new List<GlobalPost>();
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Hashtable<String, GlobalPost>>(){}.getType();

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        String JSON = sp.getString("mGlobalPost_List", "");
        mGlobalPost_List = gson.fromJson(JSON, stringStringMap);

        //Collections.sort(user.getChatDetails(), new ChatDetailsComparator());
        // sort based on created Date
//        List<GlobalPost> list = new ArrayList<GlobalPost>(mGlobalPost_List.values());
//        Collections.sort(list, new FeedComparator());
//        Collection<GlobalPost> userCollection = new HashSet<GlobalPost>(list);
//        mGlobalPost_List.values().clear();
//        mGlobalPost_List.values().addAll(userCollection);
        //

        ArrayList<GlobalPost> arrayOfUsers = new ArrayList<GlobalPost>();
        mAdapter = new GlobalPostAdapter(this, arrayOfUsers, this);
        mAdapter.addAll(mGlobalPost_List.values());

        ListView listView = (ListView) findViewById(R.id.lvItems);
        listView.setAdapter(mAdapter);

    }

    public void checkForFeedUpdates(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Master");
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                if(mGlobalPost_List.size() != i){
                    LoadGlobalFeedAll();
                    mIsDirty = true;
                }
            }
        });
    }

    public void updateComments(String PostID){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        query.whereEqualTo("GlobalFeed_ObjectID", PostID);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    if(scoreList.size()>0)
                        updateGlobalNewsFeedObject(scoreList, 3);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void checkForCommentUpdates(){

        Enumeration e = mGlobalPost_List.keys();
        while (e.hasMoreElements()) {
            final String i = (String) e.nextElement();
            //txt.append("\n"+output.get(i));
            final String PostID = mGlobalPost_List.get(i).getObjectId();

            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
            query.whereEqualTo("GlobalFeed_ObjectID", PostID);
            query.countInBackground(new CountCallback() {
                @Override
                public void done(int i, ParseException e) {
                    if(mGlobalPost_List.get(PostID) != null){
                        if(mGlobalPost_List.get(PostID).getComments() != null){
                            if(mGlobalPost_List.get(PostID).getComments().size() != i){
                                updateComments(PostID);
                                mIsDirty = true;
                            }
                        }
                    }
                }
            });
        }


    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
//
//        LoadGlobalFeedAll();
//
//        App appState = ((App)getApplicationContext());
//        appState.setPageNumber(3);
    }

    @Override
    public void onResume() {
        super.onResume();

        LoadGlobalFeedAll();

        App appState = ((App)getApplicationContext());
        appState.setPageNumber(3);
    }

    public void LoadGlobalFeedAll(){

        mLoadingFeedCaption.setVisibility(View.VISIBLE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> queryUserTimeline = ParseQuery.getQuery("GlobalFeed_Master");
        ParseQuery<ParseObject> mainQuery;
        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        //check whether it is group or individual
        if(mIsGroup){
            query.whereEqualTo("Group_ObjectID", mGroup_objectId);
            mainQuery = query;
        }
        else{
            query.whereEqualTo("User_ObjectID", mUser_objectId);
            ParseObject obj = ParseObject.createWithoutData("_User", mUser_objectId);
            queryUserTimeline.whereEqualTo("Friend_objectId", obj);
            queries.add(query);
            queries.add(queryUserTimeline);
            mainQuery = ParseQuery.or(queries);
        }

        mainQuery.include("User_objectId");
        mainQuery.include("Group_objectId");
        mainQuery.orderByDescending("createdAt");
        mainQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    //if(scoreList.size() > 0)
                    FeedMasterDataIntoObjects(scoreList);


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }



    public void FeedMasterDataIntoObjects(List<ParseObject> data){
        //First create the main globalfeed objects
        mGlobalPost_List = ConvertJSONtoArray(data);

        if(data.size() > 0 && mIsGroup == true){

            try{
                mGroup_description = data.get(0).getParseObject("Group_objectId").getString("Description");
                mGroup_type = data.get(0).getParseObject("Group_objectId").getInt("Type");
                mGroup_date = GlobalPost.getDateFromCreatedAt(data.get(0).getParseObject("Group_objectId").getCreatedAt());
                mGroupOwner_objectID = data.get(0).getParseObject("Group_objectId").getString("Creator_ObjectID");
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }

        }

        ArrayList<GlobalPost> arrayOfUsers = new ArrayList<GlobalPost>();
        // Create the adapter to convert the array to views
        mAdapter = new GlobalPostAdapter(this, arrayOfUsers, this);
        mAdapter.addAll(mGlobalPost_List.values());

        listView = (ListView) findViewById(R.id.lvItems);

        //construct Header View for the group feed
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View sectionHeading = inflater.inflate(R.layout.item_feed_group_header, listView, false);
        TextView groupName = (TextView) sectionHeading.findViewById(R.id.txt_feed_name);
        ImageView imgAvatarIcon = (ImageView) sectionHeading.findViewById(R.id.img_user_avatar);
        TextView groupTag = (TextView) sectionHeading.findViewById(R.id.txt_feed_tag);
        LinearLayout headerLayout = (LinearLayout) sectionHeading.findViewById(R.id.layout_user_background);

        if(!mIsGroup) {
            groupName.setText(mUser_Name);
            setTitle(mUser_Name);
        }
        else {
            groupName.setText(mGroupName);
            setTitle(mGroupName);
        }

        if(mIsGroup){
            if(mIsTutor){
                imgAvatarIcon.setVisibility(View.VISIBLE);
                imgAvatarIcon.setImageBitmap(GlobalPost.LoadPNG(mGroup_objectId + "_avatar", this));
                if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, this));
                    headerLayout.setBackgroundDrawable(d );
                } else {
                    Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, this));
                    headerLayout.setBackground( d);
                }



                groupTag.setText("TUTOR/INSTRUCTOR");
            }
            else
            {
                imgAvatarIcon.setVisibility(View.INVISIBLE);
                if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, this));
                    headerLayout.setBackgroundDrawable(d );
                } else {
                    Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, this));
                    headerLayout.setBackground( d);
                }
                groupTag.setText("");

                if(GlobalPost.LoadPNG(mGroup_objectId, this) == null){
                    if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        headerLayout.setBackgroundResource(R.drawable.bg_feed_generic);
                    } else {
                        //Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, activity));
                        //headerHolder.headerLayout.setBackground(R.id.layout_user_background)
                        headerLayout.setBackgroundResource(R.drawable.bg_feed_generic);
                    }
                }
            }

        }
        else {
            imgAvatarIcon.setVisibility(View.VISIBLE);
            imgAvatarIcon.setImageBitmap(GlobalPost.LoadPNG(mUser_objectId, this));
            if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                headerLayout.setBackgroundDrawable( getResources().getDrawable(R.drawable.bg_feed_generic) );
            } else {
                headerLayout.setBackground( getResources().getDrawable(R.drawable.bg_feed_generic));
            }
            groupTag.setText("");

        }

        if (listView.getHeaderViewsCount() == 0)
            listView.addHeaderView(sectionHeading);

        listView.setAdapter(mAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

//                if(mLastFirstVisibleItem<firstVisibleItem)
//                {
//                    //Log.i("SCROLLING DOWN","TRUE");
//                    if(mBottomLayout.getVisibility() == View.VISIBLE)
//                        mBottomLayout.setVisibility(View.GONE);
//
//                }
//                if(mLastFirstVisibleItem>firstVisibleItem)
//                {
//                    //Log.i("SCROLLING UP","TRUE");
//                    if(mBottomLayout.getVisibility() == View.GONE)
//                        mBottomLayout.setVisibility(View.VISIBLE);
//                }
                mLastFirstVisibleItem=firstVisibleItem;

            }
        });

        LoadPostImages(data);

    }



    public void LoadPostImages(List<ParseObject> data){


        for(int i=0;i<data.size();i++){
            if(FeedIDs != null)
                FeedIDs.add(data.get(i).getObjectId());
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        query.whereContainedIn("GlobalFeed_ObjectID", FeedIDs);
        query.include("GlobalFeed_Master");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updateGlobalNewsFeedObject(scoreList, 1);
                    //LoadPostLikes();

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public void LoadPostLikes(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Likes");
        query.whereContainedIn("GlobalFeed_ObjectID", FeedIDs);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updateGlobalNewsFeedObject(scoreList, 2);
                    //LoadPostComments();
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        //TODO to change to only counting the likes and comments
        //TODO but tricky as need to loop one by one to count each post
//        query.countInBackground(new CountCallback() {
//            @Override
//            public void done(int i, ParseException e) {
//                if(mGlobalPost_List.size() != i){
//                    LoadGlobalFeedAll();
//                    mIsDirty = true;
//                }
//            }
//        });
    }

    public void LoadPostComments(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        query.whereContainedIn("GlobalFeed_ObjectID", FeedIDs);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updateGlobalNewsFeedObject(scoreList, 3);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void updateGlobalNewsFeedObject(List<ParseObject> jsonObjects, int type){


        for (int i = 0; i < jsonObjects.size(); i++) {
            try {

                if (type == 1){
                    String ObjID = jsonObjects.get(i).getParseObject("GlobalFeed_objectId").getObjectId();
                    //mGlobalPost_List.get(ObjID).insertImages(jsonObjects.get(i), this);
                    mGlobalPost_List.get(ObjID).insertImagesThumbnail(jsonObjects.get(i), this);
                }
                else if(type == 2){
                    String ObjID = jsonObjects.get(i).getParseObject("GlobalFeed_objectId").getObjectId();
                    mGlobalPost_List.get(ObjID).insertLikes(jsonObjects.get(i), this, mPlayer_objectId);
                }
                else if(type == 3){
                    String ObjID = jsonObjects.get(i).getParseObject("GlobalFeed_objectId").getObjectId();
                    mGlobalPost_List.get(ObjID).insertComments(jsonObjects.get(i), this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(type == 1) LoadPostLikes();
        if(type == 2) LoadPostComments();
        if(type == 3)  updateGlobalFeedAdapter();

    }

    public void updateGlobalFeedAdapter(){

        mAdapter.clear();
        mAdapter.addAll(mGlobalPost_List.values());
        //TODO to reenable save cache after fine tuning
        //saveCache();
        mLoadingFeedCaption.setVisibility(View.GONE);
        mBottomLayout.setVisibility(View.VISIBLE);
    }



    public Hashtable<String, GlobalPost> ConvertJSONtoArray(List<ParseObject> jsonObjects){
        Hashtable<String, GlobalPost> GameHistory = new Hashtable<String, GlobalPost>();

        for (int i = 0; i < jsonObjects.size(); i++) {
            //if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)) {
            GameHistory.put(jsonObjects.get(i).getObjectId(),
                    new GlobalPost(jsonObjects.get(i), this));

        }
        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    private void DisplayFullImage(Bitmap bmp){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);

        ivPreview.setImageBitmap(bmp);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void DisplayFullImage(String imgFileName){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        Bitmap IMG = GlobalPost.LoadPNG(imgFileName, this);
        ivPreview.setImageBitmap(IMG);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void LoadFullImage(ParseObject Obj, final String Filename){

        final Activity act = this;
        //store image into cache
        if(!GlobalPost.CheckPNGCache(Filename, this)){
            ParseFile Picture = Obj.getParseFile("Image");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        GlobalPost.StorePNG(PictureBmp, Filename, act);
                        DisplayFullImage(PictureBmp);

                    } else {
                        Log.e("Error", e.getMessage());
                    }

                }
            });
        }
        else
        {
            DisplayFullImage(Filename);
        }

    }

    public void showFullThumbnail(final String FeedObjID, final String ImageObjID){
        final String Filename = FeedObjID + "_" + ImageObjID;

        if(GlobalPost.CheckPNGCache(Filename, this)){
            DisplayFullImage(Filename);
        }
        else{
            mDialogImageLoad.show();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
            query.whereEqualTo("objectId", ImageObjID);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                        if(scoreList.size()>0)
                            LoadFullImage(scoreList.get(0), Filename);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    static class ViewHeaderHolder {
        ImageView imgAvatarIcon;
        TextView groupName;
        TextView groupTag;
        LinearLayout headerLayout;
    }

    static class ViewHolder {


        ImageButton btnComments;
        TextView userName;
        ImageView imgIcon;
        TextView postMessage;
        ImageView[] PostImages = new ImageView[4];
        TextView likes;
        TextView comments;
        LinearLayout groupPost;
        TextView groupName;
    }

    public class GlobalPostAdapter extends ArrayAdapter<GlobalPost> {

        private Activity activity;
        // some fields

        public GlobalPostAdapter(Context context, ArrayList<GlobalPost> users, Activity act) {
            super(context, 0, users);
            this.activity = act;
        }

        @Override
        public int getItemViewType(int position) {
            return (position == this.getCount() - 1) ? 1 : 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final GlobalPost user = getItem(position);
            Log.i("Listview Position is ", String.valueOf(position));
            ViewHolder holder = null;
            ViewHeaderHolder headerHolder = null;
            int theType = getItemViewType(position);

            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {

//                if(position == 0){
//                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_feed_group_header, parent, false);
//                    headerHolder = new ViewHeaderHolder();
//
//                    headerHolder.groupName = (TextView) convertView.findViewById(R.id.txt_feed_name);
//                    headerHolder.imgAvatarIcon = (ImageView) convertView.findViewById(R.id.img_user_avatar);
//                    headerHolder.groupTag = (TextView) convertView.findViewById(R.id.txt_feed_tag);
//                    headerHolder.headerLayout = (LinearLayout) convertView.findViewById(R.id.layout_user_background);
//
//                    convertView.setTag(headerHolder);
//                    Log.i("Create Header ", String.valueOf(position));
//                }
//                else{
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_globalfeed_main_v3, parent, false);
                    holder = new ViewHolder();
                    holder.btnComments = (ImageButton) convertView.findViewById(R.id.btn_comments);
                    holder.userName = (TextView) convertView.findViewById(R.id.user_name);
                    holder.imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);
                    holder.postMessage = (TextView) convertView.findViewById(R.id.post_message);
                    holder.PostImages[0] = (ImageView) convertView.findViewById(R.id.post_pic1);
                    holder.PostImages[1] = (ImageView) convertView.findViewById(R.id.post_pic2);
                    holder.PostImages[2] = (ImageView) convertView.findViewById(R.id.post_pic3);
                    holder.PostImages[3] = (ImageView) convertView.findViewById(R.id.post_pic4);
                    holder.groupPost = (LinearLayout) convertView.findViewById(R.id.layout_group_post);
                    holder.groupName = (TextView) convertView.findViewById(R.id.post_target_group);
                    holder.likes = (TextView) convertView.findViewById(R.id.no_of_likes);
                    holder.comments = (TextView) convertView.findViewById(R.id.no_of_comments);
                    convertView.setTag(holder);
                    Log.i("Create Item ", String.valueOf(position));
                //}


            }
            else
            {
//                if(position == 0) {
//                    Log.i("Retrieve Header ", String.valueOf(position));
//                    headerHolder = (ViewHeaderHolder) convertView.getTag();
//                }
//                else
//                {
                    //try{
                    Log.i("Retrieve item ", String.valueOf(position));
                        holder = (ViewHolder)convertView.getTag();
//                    }
//                    catch (Exception e){
//                        headerHolder = (ViewHeaderHolder)convertView.getTag();
//                    }
                //}

            }



//            if(position == 0){
//                if(!mIsGroup) {
//                    headerHolder.groupName.setText(user.PlayerID);
//                    setTitle(user.PlayerID);
//                }
//                else {
//                    headerHolder.groupName.setText(user.getGroupName());
//                    setTitle(user.getGroupName());
//                }
//
//                if(mIsGroup){
//                    if(mIsTutor){
//                        headerHolder.imgAvatarIcon.setVisibility(View.VISIBLE);
//                        headerHolder.imgAvatarIcon.setImageBitmap(GlobalPost.LoadPNG(mGroup_objectId + "_avatar", activity));
//                        if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                            Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, activity));
//                            headerHolder.headerLayout.setBackgroundDrawable(d );
//                        } else {
//                            Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, activity));
//                            headerHolder.headerLayout.setBackground( d);
//                        }
//
//
//
//                        headerHolder.groupTag.setText("TUTOR/INSTRUCTOR");
//                    }
//                    else
//                    {
//                        headerHolder.imgAvatarIcon.setVisibility(View.INVISIBLE);
//                        if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                            Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, activity));
//                            headerHolder.headerLayout.setBackgroundDrawable(d );
//                        } else {
//                            Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, activity));
//                            headerHolder.headerLayout.setBackground( d);
//                        }
//                        headerHolder.groupTag.setText("");
//
//                        if(GlobalPost.LoadPNG(mGroup_objectId, activity) == null){
//                            if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                                headerHolder.headerLayout.setBackgroundResource(R.drawable.bg_feed_generic);
//                            } else {
//                                //Drawable d = new BitmapDrawable(getResources(), GlobalPost.LoadPNG(mGroup_objectId, activity));
//                                //headerHolder.headerLayout.setBackground(R.id.layout_user_background)
//                                headerHolder.headerLayout.setBackgroundResource(R.drawable.bg_feed_generic);
//                            }
//                        }
//                    }
//
//                }
//                else {
//                    headerHolder.imgAvatarIcon.setVisibility(View.VISIBLE);
//                    headerHolder.imgAvatarIcon.setImageBitmap(GlobalPost.LoadPNG(mUser_objectId, activity));
//                    if(mSdkVersion < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                        headerHolder.headerLayout.setBackgroundDrawable( getResources().getDrawable(R.drawable.bg_feed_generic) );
//                    } else {
//                        headerHolder.headerLayout.setBackground( getResources().getDrawable(R.drawable.bg_feed_generic));
//                    }
//                    headerHolder.groupTag.setText("");
//
//                }
//
//            }
//            else{
                //Tag the Comments Button
                //Button btnComments = (Button) convertView.findViewById(R.id.btn_comments);
                holder.btnComments.setTag(holder.btnComments.getId(), user.objectId);
                holder.btnComments.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                            }
                        });


//                if(!mIsGroup) {
//                    txtUserName.setText(user.PlayerID);
//                    setTitle(user.PlayerID);
//                }
//                else {
//                    txtUserName.setText(user.getGroupName());
//                    setTitle(user.getGroupName());
//
//                }

            if(mIsUser){

                if(user.getIsGroup()){
                    holder.groupPost.setVisibility(View.VISIBLE);
                    holder.groupName.setText(user.getGroupName());
                }


                holder.groupName.setTag(holder.groupName.getId(), user.getObjectId());
                holder.groupName.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(getActivity(),
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                                OpenTargetFeed(String.valueOf(view.getTag(view.getId())));

                            }
                        });
            }
            else
            {
                holder.groupPost.setVisibility(View.GONE);
            }

                holder.userName.setText(user.PlayerID);
                holder.userName.setTag(holder.userName.getId(), user.getObjectId());
                holder.userName.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                                OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                            }
                        });

                try{

                    mLoadImageTask = new LoadImageTask(holder.imgIcon, user, user.getPlayerObjectID(),
                            activity);
                    mLoadImageTask.execute(holder.imgIcon);
                    holder.imgIcon.setTag(holder.imgIcon.getId(), user.getObjectId());
                    holder.imgIcon.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                    //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                                    OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                                }
                            });

                }
                catch(Exception e){
                    Log.i("Feed_Main", e.getMessage());
                }

                holder.postMessage.setTag(holder.postMessage.getId(), user.objectId);
                holder.postMessage.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(getActivity(),
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                                OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                            }
                        });

                holder.postMessage.setText(user.Message);

            holder.likes.setText(String.valueOf(user.getNoOfLikes()));
            holder.comments.setText(String.valueOf(user.getNoOfComments()));
//                if(user.getLikes() != null)
//                    holder.likes.setText(String.valueOf(user.getLikes().size()));
//                else
//                    holder.likes.setText("0");

//                if (user.getComments() != null)
//                {
//
//                    holder.comments.setText(String.valueOf(user.getComments().size()));
//                }
//                else
//                {
//                    holder.comments.setText("0");
//                }

                //only load the images if they are present
                if(user.getNoOfImages() > 0){
                    for (int i = 0; i<user.getNoOfImages(); i++){
                        //in images preview, only able to preview up to 4 images for now
                        //as i've fixed 4 ImageViews in the layout
                        if(i >= 4){
                            continue;
                        }

                        holder.PostImages[i].setVisibility(View.VISIBLE);
                        String imageFilename = user.getObjectId() + "_" + user.getImages().get(i) + "_Thumb";
                        final int count = i;
                        final Bitmap image = user.loadImageFromStorage(imageFilename, activity);
                        try{

                            mLoadImageTask = new LoadImageTask(holder.PostImages[i], user, imageFilename, activity);
                            mLoadImageTask.execute(holder.PostImages[i]);


                        }
                        catch(Exception e){
                            Log.i("Feed_Main", e.getMessage());
                        }

                        final ImageView PIM =  holder.PostImages[i];
                        if(PIM.getDrawable() == null){

                        }
                        PIM.setTag(PIM.getId(), user.objectId);
                        PIM.setOnClickListener(
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //showFullThumbnail(image);
                                        showFullThumbnail(user.getObjectId(), user.getImages().get(count));
                                    }
                                });
                    }
                }
                else{
                    for (int i = 0; i<4; i++){
                        holder.PostImages[i].setVisibility(View.GONE);
                    }
                }
            //}


            // Return the completed view to render on screen
            return convertView;
        }

    }

    public void OpenTargetFeed(String ObjID){
        int Type = mGlobalPost_List.get(ObjID).getPostType();
        //2 is Group
        if(Type == 2){
            try{
                String Group_objectId = mGlobalPost_List.get(ObjID).getGroup_ObjectID();
                String Group_name = mGlobalPost_List.get(ObjID).getGroupName();
                //TODO check of isTutor Group or not, now is temporarily set to false
                GoToFeedGroup(Group_objectId, Group_name,
                        true, false);
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }

        }
        //3 is  friend
        else if(Type == 3){

        }
    }

    public void GoToFeedGroup(String objID, String groupName, Boolean isGroup, Boolean isTutor){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("GroupObjectID", objID);
        intent.putExtra("GroupName", groupName);
        intent.putExtra("IsGroup", isGroup);
        intent.putExtra("IsTutor", isTutor);
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            //TODO
           //saveCache();
        }

    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            //TODO
            //saveCache();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            //TODO
            //saveCache();
        }

    }

    private void saveCache(){
        Gson gson = new Gson();
        //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
        String JSON = gson.toJson(mGlobalPost_List);

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("mGlobalPost_List", JSON);
        ed.apply();

        this.mIsDirty = false;
    }


    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private GlobalPost g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, GlobalPost globalPost, String FileName, Activity act) {
            v = view;
            g = globalPost;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        // Respond to the action bar's Up/Home button
        if (id == android.R.id.home)
        {
            //NavUtils.navigateUpFromSameTask(this);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
