package progettox.mm.newnew;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 20/7/2015.
 */
public class activity_individual_settings extends AppCompatActivity {

    private String mChat_objectId;
    private String mPlayer_objectId;
    private String mPlayer_Name;
    private String mUser_objectId;
    private String mUser_Name;
    private int mNoOfFriends;
    private TextView mTvUserFriends;
    private TextView mTvUserGroups;
    private TextView mTvUserPhotos;
    private Button mBtnChat;
    private Button mBtnPersonalMessage;
    private Button mBtnAddFriend;
    private Boolean mIsMe;

    private void Initialize() {

        App appState = ((App) getApplicationContext());
        mPlayer_Name = appState.getPlayerID();
        mPlayer_objectId = appState.getPlayer_objectId();
        mNoOfFriends = 0;
        mIsMe = false;
        mChat_objectId = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mUser_objectId = extras.getString("User_ObjectID");
            mUser_Name = extras.getString("User_Name");
        }

        //display UI
        TextView txtUserName = (TextView) findViewById(R.id.individual_settings_name);
        TextView txtUserJoinDate = (TextView) findViewById(R.id.individual_settings_date);
        TextView txtViewUserFeed = (TextView) findViewById(R.id.txt_user_feed);
        //TextView mTvUserPhotos = (TextView) findViewById(R.id.txt_user_photos);

        LinearLayout userFeed = (LinearLayout) findViewById(R.id.layout_news_feed);
        userFeed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToUserFeed();
            }
        });
        LinearLayout userFriends = (LinearLayout) findViewById(R.id.layout_user_friends);
        userFriends.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToUserFriends();
            }
        });

        LinearLayout userImages = (LinearLayout) findViewById(R.id.layout_user_images);
        userImages.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToUserPhotos();
            }
        });

        LinearLayout userGroups = (LinearLayout) findViewById(R.id.layout_user_groups);
        userGroups.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToUserGroups();
            }
        });

        ImageView userProfilePic = (ImageView) findViewById(R.id.user_profile_pic);
        userProfilePic.setImageBitmap(GlobalPost.LoadPNG(mUser_objectId, this));

        txtUserName.setText(mUser_Name);
//        groupDesc.setText(mGroup_description);
//        groupType.setText(appState.getGroupType(mGroup_type));
//        groupDate.setText(mGroup_date);

        mTvUserFriends = (TextView) findViewById(R.id.tv_user_friends);
        mTvUserGroups = (TextView) findViewById(R.id.tv_user_groups);
        mTvUserPhotos = (TextView) findViewById(R.id.txt_user_photos);

        setTitle(mUser_Name + "'s Info");
        txtViewUserFeed.setText(getResources().getText(R.string.lbl_view) + " " +
                mUser_Name + "'s " + getResources().getText(R.string.lbl_feed));

        //prep buttons
        mBtnChat = (Button) findViewById(R.id.btn_open_chat);
        mBtnChat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onFriendTouched();
            }
        });

        mBtnPersonalMessage = (Button) findViewById(R.id.btn_open_pm);
        mBtnPersonalMessage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToNewPM();
            }
        });
        mBtnAddFriend = (Button) findViewById(R.id.btn_friend_request);
        mBtnAddFriend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendAddFriendRequest();
            }
        });
        mBtnChat.setVisibility(View.GONE);
        mBtnPersonalMessage.setVisibility(View.GONE);
        mBtnAddFriend.setVisibility(View.GONE);

        if(mPlayer_objectId.equals(mUser_objectId)){
            mBtnChat.setVisibility(View.GONE);
            mBtnPersonalMessage.setVisibility(View.GONE);
            mBtnAddFriend.setVisibility(View.GONE);
            mIsMe = true;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_settings);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        Initialize();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        loadNoOfFriends();
        loadNoOfPhotos();
        loadNoOfGroups();
        if(!mIsMe)
            checkIsFriend();
    }

    private void sendAddFriendNotification(){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", mUser_objectId));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 3);
        NewPlayer.put("IsAccepted", false); //By default is False

        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    private void AddFriendRequestCompleted(){
        mBtnAddFriend.setText(getResources().getText(R.string.btn_added_friend));
        mBtnAddFriend.setBackgroundColor(getResources().getColor(R.color.white));
        mBtnAddFriend.setClickable(false);
        mBtnAddFriend.setTextColor(getResources().getColor(R.color.inputText));
        Toast.makeText(this,
                getResources().getText(R.string.toast_add_friend),
                Toast.LENGTH_SHORT).show();
    }

    private void sendAddFriendRequest(){
        List<ParseObject> PlayerList = new ArrayList<ParseObject>();

        ParseObject gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", mPlayer_Name);
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        gameScore.put("FriendID", mUser_Name);
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", mUser_objectId));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        gameScore = new ParseObject("User_Friends");
        gameScore.put("PlayerID", mUser_Name);
        gameScore.put("Player_objectId", ParseObject.createWithoutData("_User", mUser_objectId));
        gameScore.put("FriendID", mPlayer_Name);
        gameScore.put("Friend_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        gameScore.put("Status", 0);

        PlayerList.add(gameScore);

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                AddFriendRequestCompleted();
                sendAddFriendNotification();
            }
        });
    }

    private void checkIsFriend(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        obj = ParseObject.createWithoutData("_User", mUser_objectId);
        query.whereEqualTo("Friend_objectId", obj);
        //query.whereEqualTo("Status", 1);
        //query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    updateUserStatus(scoreList);


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    private void updateUserStatus(List<ParseObject> objList){

        if(objList.size() > 0){

            if(objList.get(0).getInt("Status") == 1){
                mBtnChat.setVisibility(View.VISIBLE);
                mBtnPersonalMessage.setVisibility(View.GONE);
                mBtnAddFriend.setVisibility(View.GONE);
                if(objList.get(0).getString("Chat_ObjectID") != null)
                    mChat_objectId = objList.get(0).getString("Chat_ObjectID");
            }
            else{
                mBtnChat.setVisibility(View.GONE);
                mBtnPersonalMessage.setVisibility(View.VISIBLE);
                mBtnAddFriend.setVisibility(View.VISIBLE);
                mBtnAddFriend.setClickable(false);
                mBtnAddFriend.setBackgroundColor(getResources().getColor(R.color.white));
                mBtnAddFriend.setTextColor(getResources().getColor(R.color.inputText));
                mBtnAddFriend.setText(getResources().getText(R.string.btn_added_friend));
            }
        }
        else{
            mBtnChat.setVisibility(View.GONE);
            mBtnPersonalMessage.setVisibility(View.VISIBLE);
            mBtnAddFriend.setVisibility(View.VISIBLE);
        }


        //means this user is my friend
//        if(count>0){
//            mBtnChat.setVisibility(View.VISIBLE);
//            mBtnPersonalMessage.setVisibility(View.GONE);
//            mBtnAddFriend.setVisibility(View.GONE);
//        }
//        else{
//
//        }


    }

    private void loadNoOfFriends(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User", mUser_objectId);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        //query.include("Friend_objectId");

        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                updateNoOfFriends(i);
            }
        });

    }

    private void loadNoOfPhotos(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        ParseObject obj = ParseObject.createWithoutData("_User", mUser_objectId);
        query.whereEqualTo("Submitter_objectId", obj);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                updateNoOfPhotos(i);
            }
        });

    }

    private void loadNoOfGroups(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        ParseObject obj = ParseObject.createWithoutData("_User", mUser_objectId);
        query.whereEqualTo("Member_objectId", obj);
        query.whereEqualTo("Member_Status", 1);

        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                updateNoOfGroups(i);
            }
        });

    }

    private void updateNoOfFriends(int count){
        String caption = getResources().getText(R.string.lbl_view) + " " +
                mUser_Name + "'s " + getResources().getText(R.string.lbl_friends) + " (" + count + ")";
        mTvUserFriends.setText(caption);
    }

    private void updateNoOfPhotos(int count){
        String caption = getResources().getText(R.string.lbl_view) + " " +
                mUser_Name + "'s " + getResources().getText(R.string.lbl_photos) + " (" + count + ")";
        mTvUserPhotos.setText(caption);
    }

    private void updateNoOfGroups(int count){
        String caption = getResources().getText(R.string.lbl_view) + " " +
                mUser_Name + "'s " + getResources().getText(R.string.lbl_groups) + " (" + count + ")";
        mTvUserGroups.setText(caption);
    }

    private void goToUserFeed(){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", mUser_objectId);
        intent.putExtra("Player_Name", mUser_Name);
        intent.putExtra("IsGroup", false);
        startActivity(intent);
    }

    private void goToUserFriends(){
        Intent groupMembersActivity = new Intent(this, activity_group_settings_members.class);
        groupMembersActivity.putExtra("User_ObjectID", mUser_objectId);
        groupMembersActivity.putExtra("User_Name", mUser_Name);
        groupMembersActivity.putExtra("Profile_Type", 1);
        startActivity(groupMembersActivity);
    }
    private void goToUserPhotos(){
        Intent groupMembersActivity = new Intent(this, activity_group_images.class);
        groupMembersActivity.putExtra("User_ObjectID", mUser_objectId);
        groupMembersActivity.putExtra("User_Name", mUser_Name);
        groupMembersActivity.putExtra("Profile_Type", 1);
        startActivity(groupMembersActivity);
    }
    private void goToUserGroups(){
        Intent groupMembersActivity = new Intent(this, activity_individual_groups.class);
        groupMembersActivity.putExtra("User_ObjectID", mUser_objectId);
        groupMembersActivity.putExtra("User_Name", mUser_Name);
        startActivity(groupMembersActivity);
    }
    private void goToNewPM(){
        Intent groupMembersActivity = new Intent(this, activity_new_pm.class);
        groupMembersActivity.putExtra("User_ObjectID", mUser_objectId);
        groupMembersActivity.putExtra("User_Name", mUser_Name);
        startActivity(groupMembersActivity);
    }

    private void onFriendTouched(){
        //First we need to see if any existing conversation between you and this contact

        if(!mChat_objectId.equals(""))
            GotoChatActivity(mChat_objectId, mUser_Name);
        else
            createNewChatSession(mUser_Name, mUser_objectId);

    }

    public void GotoChatActivity(String ChatID, String Username){
        Intent intent = new Intent(this, ChatActivity.class);

        //intent.putExtra("myObject", new Gson().toJson(mHTChatItemList.get(ChatID)));
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("ChatID", ChatID);
        bundle.putString("ChatUsername", Username);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void createNewUserList(String ObjID, String Friend_ObjectID, final String FriendID){


        final String chatID = ObjID;
        final String friendObjID = Friend_ObjectID;

        //gameScore.put("User_objectId", ParseObject.createWithoutData("_User", Player_objectId));
        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        final ParseObject NewPlayer = new ParseObject("Chat_UserList");
        final ParseObject NewPlayerSelf = new ParseObject("Chat_UserList");

        NewPlayer.put("Chat_ObjectID", chatID);
        NewPlayer.put("Chat_objectId", ParseObject.createWithoutData(
                "Chat_Master", chatID));
        NewPlayer.put("User_ObjectID", Friend_ObjectID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", Friend_ObjectID));
        PlayerList.add(NewPlayer);

        NewPlayerSelf.put("Chat_ObjectID", chatID);
        NewPlayerSelf.put("Chat_objectId", ParseObject.createWithoutData(
                "Chat_Master", chatID));
        NewPlayerSelf.put("User_ObjectID", mPlayer_objectId);
        NewPlayerSelf.put("User_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        PlayerList.add(NewPlayerSelf);


        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                // Log.i("Add new game completed", e.getMessage());
                GotoChatActivity(chatID, FriendID);

            }
        });
    }

    private void attachChatIDTOFriends(String ChatID, String FriendID){
        final String chatID = ChatID;
        ParseQuery query = new ParseQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        obj = ParseObject.createWithoutData("_User", mUser_objectId);
        query.whereEqualTo("Friend_objectId", obj);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("OBJECT ID IS ", object.getObjectId());
                } else {
                    //Log.d("score", "Retrieved the object.");
                    object.put("Chat_ObjectID", chatID);
                    object.put("Chat_objectId", ParseObject.createWithoutData(
                            "Chat_Master", chatID));
                    object.saveInBackground();
                }
            }
        });

        query = new ParseQuery("User_Friends");
        obj = ParseObject.createWithoutData("_User", mUser_objectId);
        query.whereEqualTo("Player_objectId", obj);
        obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Friend_objectId", obj);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("OBJECT ID IS ", object.getObjectId());
                } else {
                    //Log.d("score", "Retrieved the object.");
                    object.put("Chat_ObjectID", chatID);
                    object.put("Chat_objectId", ParseObject.createWithoutData(
                            "Chat_Master", chatID));
                    object.saveInBackground();
                }
            }
        });

    }

    private void createNewChatSession(String FriendID, final String Friend_ObjectID){

        final ParseObject gameScore = new ParseObject("Chat_Master");
        final String friendObjID = Friend_ObjectID;
        final String friendID = FriendID;

        gameScore.put("IsGroup", false);
        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Saved successfully.
                    createNewUserList(gameScore.getObjectId(), friendObjID, friendID);
                    attachChatIDTOFriends(gameScore.getObjectId(), friendObjID );
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });
    }
}
