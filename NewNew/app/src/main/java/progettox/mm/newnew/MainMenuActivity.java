package progettox.mm.newnew;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;



public class MainMenuActivity extends AppCompatActivity {
//        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    public String PlayerID;
    public String Player_objectId;
    private int SectionNumber;
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    private int mRequestCode = 101;
    public String NotificationType;
    public String GameID_Notify;
    public String mChatAlertType;
    public String ChatUsername;
    private Button mBtnMain;
    private Button mBtnChat;
    private Button mBtnGame;
    private App mAppSate;
    private Button mBtnNotifications;
    private ImageView mNotificationAlertIcon;
    private int mNotificationsCount;

    //public DrawerLayout mDrawerLayout;
    //public ListView listView;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    //private NavigationDrawerFragment mNavigationDrawerFragment;
    private frame_gamession_main mframe_gamession_main;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);

        //Check for Log In Credentials, to direct go to MainMenuActivity if Valid
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        String PID = sp.getString("PlayerID", "NIL");
        String ObjID = sp.getString("Player_ObjectID", "NIL");

        mAppSate = ((App)getApplicationContext());

        if (PID.equals("NIL") || ObjID.equals("NIL")){
            //if there are no log in credentials, force user back to log in page
            //TODO usually users come here directly by opening a notification
            //but the back does not work as it does not bring the user to the back stack
            finish();
        }
        else
        {
            //User is valid

            mAppSate.setPlayerID(PID);
            mAppSate.setPlayer_objectId(ObjID);
        }

        //this section is to check whether the user opened the game with a notification
        Bundle bundle = getIntent().getExtras();
        GameID_Notify = "";
        NotificationType = "";
        if (bundle != null) {
            GameID_Notify = bundle.getString("GameSessionID_Notify");
            NotificationType = bundle.getString("NotificationType");
            ChatUsername = bundle.getString("ChatUsername");
            mChatAlertType = bundle.getString("ChatAlertType");
        }


        App appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();
        appState.setGameSessionID_Notify(GameID_Notify);

        mTitle = getTitle();

        attachAdapter();

        mBtnMain = (Button) findViewById(R.id.firstPage_action);
        mBtnMain.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view) {
                mTitle = getString(R.string.title_section1);
                restoreActionBar();
                mViewPager.setCurrentItem(0);
            }

        });

        mBtnChat = (Button) findViewById(R.id.secondPage_action);
        mBtnChat.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view) {
                mTitle = getString(R.string.title_section2);
                restoreActionBar();
                //GotoChatScreen();
                mViewPager.setCurrentItem(1);
            }

        });

        mBtnNotifications = (Button) findViewById(R.id.fourthPage_action);
        mBtnNotifications.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view) {
                mTitle = getString(R.string.title_section4);
                restoreActionBar();
                //GotoChatScreen();
                mViewPager.setCurrentItem(2);
            }

        });

        mBtnGame = (Button) findViewById(R.id.thirdPage_action);
        mBtnGame.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view) {
                mTitle = getString(R.string.title_section3);
                restoreActionBar();
                mViewPager.setCurrentItem(3);
            }

        });

        mNotificationAlertIcon = (ImageView) findViewById(R.id.ic_notification_alert);
        mNotificationsCount = 0;
    }

    @Override
    public void onBackPressed() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("Would you like to exit the App?")
                .setTitle("Exit App");

        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ExitApp();
            }
        });

        builder.setNegativeButton("Stay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void checkForNewNotifications(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Notifications");
        ParseObject obj = ParseObject.createWithoutData("_User",Player_objectId);
        query.whereEqualTo("User_objectId", obj);
        query.whereEqualTo("IsSeen", false);;
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {
                if(i > 0){
                    mNotificationsCount = i;
                    triggerNotificationIcon(true);
                }

            }
        });
    }

    private void triggerNotificationIcon(Boolean status){
        if(status)
            mNotificationAlertIcon.setVisibility(View.VISIBLE);
        else
            mNotificationAlertIcon.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();  // Always call the superclass method first
        //mDrawerLayout.closeDrawers();

        checkForNewNotifications();

        App appState = ((App)getApplicationContext());
        boolean openGamePage = appState.getGameNotification();
        //if(openGamePage){
        if(NotificationType != null){

            if(NotificationType.equals("PLAY")){
                NotificationType = "";
                appState.setGameNotification(false);
                mTitle = getString(R.string.title_section3);
                restoreActionBar();
                mViewPager.setCurrentItem(3);
            }
            else if(NotificationType.equals("CHAT")){

                NotificationType = "";
                appState.setGameNotification(false);
                appState.setChatID(GameID_Notify);
                appState.setChatUsername(ChatUsername);
                appState.setChatAlertType(mChatAlertType);
                mTitle = getString(R.string.title_section2);
                restoreActionBar();
                mViewPager.setCurrentItem(1);

                Intent intent = new Intent(this, ChatActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("ChatID", GameID_Notify);
                bundle.putString("ChatUsername", ChatUsername);
                bundle.putString("ChatAlertType", mChatAlertType);
                intent.putExtras(bundle);
                startActivity(intent);

            }



        }

    }

    @Override
    protected void onResume() {
        super.onResume();  // Always call the superclass method first
        //mDrawerLayout.closeDrawers();
        App appState = ((App)getApplicationContext());
        boolean openGamePage = appState.getGameNotification();
        if(openGamePage){
            appState.setGameNotification(false);
            mTitle = getString(R.string.title_section3);
            restoreActionBar();
            mViewPager.setCurrentItem(3);
        }
        getSelfFriendList();
        LogIN();
    }

    @Override
    protected void onStop() {
        super.onStop();  // Always call the superclass method first

    }


    public void SubcribeParseChannels(String parseChan){
        Log.i("PC is", parseChan);

        ParsePush.subscribeInBackground(parseChan, new SaveCallback() {


            @Override
            public void done(ParseException e) {

                if (e == null) {
                    JSONObject data = null;
                    try {
                        data = new JSONObject("{\"alert\": \"Long Notification message!\", \"title\": \"Notification Title\"}");
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }

                    Log.i("Subcribe success", "");
//                    ParsePush push = new ParsePush();
//                    push.setChannel("Giants");
//                    push.setData(data);
//                    push.sendInBackground();
                } else {
                    //e.printStackTrace();
                    Log.i("Subcribe error", e.getMessage());
                }
            }
        });
    }

    private void clearNotifications(){
        mNotificationAlertIcon.setVisibility(View.GONE);

        //do nothing if there are now pending unseen notifications
        if(mNotificationsCount == 0)
            return;

        //here we update all the notificatino status to isSeen = true
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Notifications");
        ParseObject obj = ParseObject.createWithoutData("_User",Player_objectId);
        query.whereEqualTo("User_objectId", obj);
        query.whereEqualTo("IsSeen", false);;
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    for(int i = 0; i< groupList.size();i++){
                        groupList.get(i).put("IsSeen", true);
                        groupList.get(i).saveInBackground();
                    }
                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void GoToFeedGroup(String objID, String groupName, Boolean isGroup, Boolean isTutor){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("GroupObjectID", objID);
        intent.putExtra("GroupName", groupName);
        intent.putExtra("IsGroup", isGroup);
        intent.putExtra("IsTutor", isTutor);
        startActivity(intent);
    }

    public void GoToLogout(){
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("PlayerID", "NIL");
        ed.putString("PlayerPWD", "NIL");
        ed.putString("Player_ObjectID", "NIL");
        ed.apply();
        logOffUser();
        finish();
    }

    public void OpenTestMap(){
        Intent intent = new Intent(this, MapMainActivity.class);
        startActivity(intent);
    }

    public void OpenAzureTest(){
        Intent intent = new Intent(this, azure_test.class);
        startActivity(intent);
    }

    public void OpenGlobalFeed(int feedType){
        //0 is globalfeed, 1 is friendFeed, 2 is TutorFeed
        Intent intent = new Intent(this, globalfeed_main.class);
        intent.putExtra("Feed_Category", feedType);
        startActivity(intent);
    }

    public void goToCreateNewGroup(){
        Intent intent = new Intent(this, CreateNewGlobalFeedGroup.class);
        startActivity(intent);

    }

    public void goToNotifications(){
        triggerNotificationIcon(false);
        Intent intent = new Intent(this, activity_notifications.class);
        startActivity(intent);

    }

    public Boolean getNotificationStatus(){
        Boolean stat = false;
        if(mNotificationAlertIcon.getVisibility() == View.GONE)
            stat = false;
        else
            stat = true;

        return stat;
    }

    public void goToFindNewContacts(){
        Intent intent = new Intent(this, activity_individual_find.class);
        startActivity(intent);

    }

    public void goToIndividualPage(String objID, String Name){
        Intent intent = new Intent(this, activity_individual_settings.class);
        //Bundle bundle = new Bundle();

        intent.putExtra("User_ObjectID", objID);
        intent.putExtra("User_Name", Name);
        startActivity(intent);
    }

    public void goToCreateGroupChat(){
        Intent createNewPost = new Intent(this, CreateNewGroupChat.class);
        //createNewPost.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(createNewPost, mRequestCode);
    }

    public void attachAdapter(){
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());

        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mTitle = getString(R.string.title_section1);
                    restoreActionBar();
                    mBtnMain.setTextColor(Color.BLACK);
                    mBtnChat.setTextColor(Color.WHITE);
                    mBtnGame.setTextColor(Color.WHITE);
                    mBtnNotifications.setTextColor(Color.WHITE);
                    //mViewPager.
                } else if (position == 1) {
                    mTitle = getString(R.string.title_section2);
                    restoreActionBar();
                    mBtnChat.setTextColor(Color.BLACK);
                    mBtnMain.setTextColor(Color.WHITE);
                    mBtnGame.setTextColor(Color.WHITE);
                    mBtnNotifications.setTextColor(Color.WHITE);
                } else if (position == 2) {
                    mTitle = getString(R.string.title_section4);
                    restoreActionBar();
                    mBtnNotifications.setTextColor(Color.BLACK);
                    mBtnGame.setTextColor(Color.WHITE);
                    mBtnChat.setTextColor(Color.WHITE);
                    mBtnMain.setTextColor(Color.WHITE);
                    clearNotifications();
                }
                else if (position == 3) {
                    mTitle = getString(R.string.title_section3);
                    restoreActionBar();
                    mBtnNotifications.setTextColor(Color.WHITE);
                    mBtnGame.setTextColor(Color.BLACK);
                    mBtnChat.setTextColor(Color.WHITE);
                    mBtnMain.setTextColor(Color.WHITE);
                }
            }
//            @Override
//            public void onPageScrollStateChanged(int state)
//            {
//            }
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
//            {
//            }
        });
    }


    public void GotoGlobalFeedDetails(String GF_ID){
        App appState = ((App)getApplicationContext());
        appState.setGlobalFeedID(GF_ID);


        Intent intent = new Intent(this, globalfeed_secondary_activity.class);
        intent.putExtra("Feed_ObjectID", GF_ID);
        startActivity(intent);
    }

    public void GotoGlobalFeedDetails(String FeedID, String userName, String userObjID,
                                      String groupName,Boolean isGroup,
                                      String groupObjID, int type, String Message,
                                      String createdatDisplay, Date createdAt){
        App appState = ((App)getApplicationContext());
        appState.setGlobalFeedID(FeedID);


        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, globalfeed_secondary_activity.class);
        bundle.putString("Feed_ObjectID", FeedID);
        bundle.putString("User_Name", userName);
        bundle.putString("User_ObjectID", userObjID);
        bundle.putString("Message", Message);
        bundle.putInt("Type", type);
        bundle.putBoolean("IsGroup", isGroup);
        bundle.putString("Group_Name", groupName);
        bundle.putString("Group_ObjectID", groupObjID);
        bundle.putString("CreatedAtDisplay", createdatDisplay);
        bundle.putSerializable("CreatedAt", createdAt);

        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void OpenUserFeedPage(String username, String User_ObjectID){

        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", User_ObjectID);
        intent.putExtra("Player_Name", username);
        intent.putExtra("IsGroup", false);
        startActivity(intent);
    }

    public void GotoGameSessionDetails(String SessionID, String FirstCharacter, int Len, String GameName){

        App appState = ((App)getApplicationContext());
        appState.setGameSessionID(SessionID);
        appState.setWordCount(Len);
        appState.setFirstChar(FirstCharacter);
        appState.setGameSessionName(GameName);

        Intent intent = new Intent(this, GameSessionDetailsActivity.class);
        startActivity(intent);
    }

    public void GoToProfilePage(){
        Intent intent = new Intent(this, activity_user_profile.class);
        startActivity(intent);
    }

    public void GoToPMPage(){
        Intent intent = new Intent(this, activity_individual_pm.class);
        startActivity(intent);
    }

    public void GoToJoinGroupPage(){
        Intent intent = new Intent(this, activity_group_join.class);
        startActivity(intent);
    }

    public void GoToPMHistory(String PM_ObjectID, String UserObjID, String UserName){
        Intent intent = new Intent(this, activity_individual_pm_history.class);
        intent.putExtra("PM_ObjectID", PM_ObjectID);
        intent.putExtra("User_ObjectID", UserObjID);
        intent.putExtra("User_Name", UserName);
        startActivity(intent);
    }


//    @Override
//    public void onNavigationDrawerItemSelected(int position) {

//        if(position == 0){
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                .replace(R.id.containerDrawer, PlaceholderFragment.newInstance(position + 1))
//                .commit();
//        }
//        else if(position == 1) {
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.containerDrawer, PlaceholderFragment.newInstance(position + 1))
//                    .commit();
//        }
//        else if(position == 2){
//            frame_gamession_main newFragment = new frame_gamession_main();
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            FragmentTransaction transaction = fragmentManager.beginTransaction();
//            transaction.replace(R.id.containerDrawer, newFragment.newInstance(PlayerID,
//                    Player_objectId, position + 1));
//            //transaction.addToBackStack(null);
//            transaction.commit();
//        }

//        if(position == 0){
//            mViewPager.setCurrentItem(position);
//        }
//        else if(position == 1) {
//            mViewPager.setCurrentItem(position);
//        }
//        else if(position == 2){
//            mViewPager.setCurrentItem(position);
//        }
        //mViewPager.setCurrentItem(position);

//    }

    public void onSectionAttached(int number) {

        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);

                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section4);
                break;
            case 4:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_menu, menu);
            restoreActionBar();
            //return true;
        //}
        //mNavigationDrawerFragment.setMenuVisibility(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement


//        if (id == R.id.action_createnewgroup) {
//            Toast.makeText(this, "Creating a new Group Chat.", Toast.LENGTH_SHORT).show();
//            Intent createNewPost = new Intent(this, CreateNewGroupChat.class);
//            //createNewPost.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivityForResult(createNewPost, mRequestCode);
//            return true;
//        }

        if (id == R.id.action_createnewgame) {
            Toast.makeText(this, "Creating a new Game.", Toast.LENGTH_SHORT).show();
            Intent createNewGame = new Intent(this, CreateNewGameActivity.class);
            startActivityForResult(createNewGame, mRequestCode);
            return true;
        }

        if (id == R.id.action_search_all) {
            //Toast.makeText(this, "Creating a new Game.", Toast.LENGTH_SHORT).show();
            Intent createNewGame = new Intent(this, activity_search_main.class);
            startActivityForResult(createNewGame, mRequestCode);
            return true;
        }

//        if (id == R.id.action_feed_groups) {
//            Toast.makeText(this, "Showing your Groups.", Toast.LENGTH_SHORT).show();
//            Intent groupBrowser = new Intent(this, globalfeed_groups_browser.class);
//            startActivityForResult(groupBrowser, mRequestCode);
//            return true;
//        }

        //here we delete the SharedPreferences user credentials and take him/her to the
        //Log in Screen.
        if (id == R.id.action_logoff) {

            SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("PlayerID", "NIL");
            ed.putString("PlayerPWD", "NIL");
            ed.putString("Player_ObjectID", "NIL");
            ed.apply();
            logOffUser();
            finish();
            return true;
        }

        if (id == R.id.action_createnewpost) {

            Toast.makeText(this, "Creating a new Post.", Toast.LENGTH_SHORT).show();
            Intent createNewPost = new Intent(this, createNewGlobalFeed.class);
            startActivityForResult(createNewPost, mRequestCode);
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void logOffUser(){
        // Retrieve current user from Parse.com
        ParseUser currentUser = ParseUser.getCurrentUser();
        ParseUser.logOut();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        //Do your work here in ActivityA

        //This is for when coming back from creating a new game
        if(requestCode == mRequestCode && resultCode == RESULT_OK){
            String editTextString = data.getStringExtra("Status");
            if(editTextString.equals("Success") || editTextString.equals("WordSuccess")){
                //finish();
                //TODO this will hit when a new game is succesfully
                // created and comes back to main menu
                //to load the game session again
                //Fragment gameFrag = mSectionsPagerAdapter.getItem(2);
                //(frame_gamession_main)gameFrag.getActivity().
                attachAdapter();
                mTitle = getString(R.string.title_section3);
                restoreActionBar();
                mViewPager.setCurrentItem(2);
            }
//            else if(editTextString.equals("WordSuccess")){
//
//            }

        }


    }

    public void setSectionNumber(int index){
        SectionNumber = index;
    }

    public void ExitApp(){
        logOffUser();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



    public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.85f;
        private static final float MIN_ALPHA = 0.5f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA +
                        (scaleFactor - MIN_SCALE) /
                                (1 - MIN_SCALE) * (1 - MIN_ALPHA));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int NUM_VIEWS = 4;

        public void setN(int N) {
            this.NUM_VIEWS = N;
        }

        @Override
        public Fragment getItem(int position) {

            Fragment frag;
            if(position == 0){

                //frag =  PlaceholderFragment.newInstance(position + 1);
                frame_index_main newFragment = new frame_index_main();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, position + 1);
                newFragment.setArguments(args);
                frag =  newFragment;
            }
            else if(position == 1) {

                //frag = PlaceholderFragment.newInstance(position + 1);
                //frame_index_learn newFragment = new frame_index_learn();
                frame_index_chat_A newFragment = new frame_index_chat_A();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, position + 1);
                newFragment.setArguments(args);
                frag =  newFragment;
            }
            else if(position == 2){

                //frame_index_notifications newFragment = new frame_index_notifications();
                frame_index_contacts newFragment = new frame_index_contacts();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, position + 1);
                args.putString("PlayerID", PlayerID);
                args.putString("Player_objectId", Player_objectId);
                newFragment.setArguments(args);
                frag =  newFragment;

            }
            else if(position == 3){

//                frame_gamession_main newFragment = new frame_gamession_main();
//                Bundle args = new Bundle();
//                args.putInt(ARG_SECTION_NUMBER, position + 1);
//                args.putString("PlayerID", PlayerID);
//                args.putString("Player_objectId", Player_objectId);
//                newFragment.setArguments(args);
//                frag =  newFragment;

                frame_index_more newFragment = new frame_index_more();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, position + 1);
                args.putString("PlayerID", PlayerID);
                args.putString("Player_objectId", Player_objectId);
                newFragment.setArguments(args);
                frag =  newFragment;

            }
            else
            {
                frag =  PlaceholderFragment.newInstance(position + 1);
            }
            return frag;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return NUM_VIEWS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section4).toUpperCase(l);
                case 3:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";


        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            //setSectionNumber(sectionNumber);
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;

        }

        public PlaceholderFragment() {
        }

//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//
//            //container.
//            //View rootView = inflater.inflate(R.layout.fragment_main_menu, container, false);
//            View rootView = inflater.inflate(R.layout.fragment_game_session_main_list, container, false);
//
//            return rootView;
//        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainMenuActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));

        }
    }

    private void getSelfFriendList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User", Player_objectId);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    if(friendList.size()>0)
                        saveAndCacheFriendList(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    private void saveAndCacheFriendList(List<ParseObject> friendList){
        ArrayList<String> mFriendArray = new ArrayList<>();
        for (int i = 0; i < friendList.size(); i++) {
            try {
                mFriendArray.add(
                        friendList.get(i).getParseObject("Friend_objectId").getObjectId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Gson gson = new Gson();
        //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
        String JSON = gson.toJson(mFriendArray);

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("Friend_List", JSON);
        ed.apply();

    }

    private void LogIN(){

        String testURL = mAppSate.getWebAPIRootURL() + "token";
        LogIn newTask = new LogIn(testURL);
        newTask.execute();
    }

    public class LogIn extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        LogIn(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                JSONObject jsonParam = new JSONObject();
                jsonParam.put("grant_type", "password");
                jsonParam.put("username", "rick");
                jsonParam.put("password", "+60163320718");
                jsonParam.put("client_id", "Android");
                jsonParam.put("client_secret", "abc@123");

                Hashtable<String, String> temp = new Hashtable<>();
                temp.put("grant_type", "password");
                temp.put("username", "sridhar");
                temp.put("password", "sridhar");
                temp.put("client_id", "Android");
                temp.put("client_secret", "abc@123");

                String URLArgs = temp.toString();


                //URL url = new URL("http://192.168.1.181:8080/AngularJSAuthentication.API/token");
                URL url = new URL(URLHttp);

                urlConn = (HttpURLConnection) url.openConnection();
                //urlConn = url.openConnection();
                urlConn.setDoInput (true);
                urlConn.setDoOutput (true);
                urlConn.setUseCaches (false);
                urlConn.setRequestProperty("Content-Type","application/json");
                //urlConn.setRequestMethod("POST");
//                urlConn.setRequestProperty("Content-Length", "" +
//                        Integer.toString(jsonParam.length()));
                //urlConn.setRequestProperty("Content-Language", "en-US");
                urlConn.connect();
//Create JSONObject here


//                DataOutputStream wr = new DataOutputStream(urlConn.getOutputStream ());
//                wr.writeBytes(jsonParam.toString());
//                wr.flush();
//                wr.close();
                OutputStream os = urlConn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
//                URLArgs = "grant_type=password&username=rick&password=%2B60163320718&client_id=Android&client_secret=abc%40123";
                URLArgs = "grant_type=password&username=sridhar&password=sridhar&client_id=Android&client_secret=abc%40123";
                writer.write(URLArgs);
                writer.flush();
                writer.close();
                os.close();


                //Get Response
//                InputStream is = urlConn.getInputStream();
//                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//                String lines;
//                StringBuffer response = new StringBuffer();
//                while((lines = rd.readLine()) != null) {
//                    response.append(lines);
//                    response.append('\r');
//                }
//                rd.close();
                //return response.toString();


                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONObject obj = new JSONObject(result);


                String testArray = obj.getString("access_token");
                mAppSate.setToken(testArray);


                Log.d("My App", obj.toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

}
