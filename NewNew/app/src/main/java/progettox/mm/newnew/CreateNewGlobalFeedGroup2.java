package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * Created by Admin on 12/6/2015.
 */
public class CreateNewGlobalFeedGroup2 extends AppCompatActivity {

    private List<PlayerFriend> PlayerFriendsArray;
    public String PlayerID;
    public String Player_objectId;
    public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    private int mRequestCode = 100;
    public String[] FriendList;
    public String[] FriendObjIDList;
    public AppInfoAdapter adapter ;
    private ParseGeoPoint mGeoLocation;
    public App mAppState;
    private String mGroupName;
    private String mGroupDesc;
    private int mGroupType;
    private TextView mLoadingCaption;
    private ParseFile mTempAvatar;

    private TextView mLatitude;
    private TextView mLongtitude;
    private TextView mLocation;

    //baidu maps api usages
    private static final String TAG = "dzt";
    private LocationClient mLocationClient;
    private LocationClientOption.LocationMode tempMode = LocationClientOption.LocationMode.Battery_Saving;
    private String tempcoor="bd09ll";

    private void Initialize(){

        mLocationClient = ((App)getApplication()).mLocationClient; // Class LocationClient
        InitLocation();
        mLocationClient.start();

        prepareTempAvatar();

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            mGroupName = extras.getString("GroupName");
            mGroupDesc = extras.getString("GroupDesc");
            mGroupType = extras.getInt("GroupType");
        }

        mAppState = ((App)getApplicationContext());
        PlayerID = mAppState.getPlayerID();
        Player_objectId = mAppState.getPlayer_objectId();

        Button btnDone = (Button) findViewById(R.id.button1);
        btnDone.setVisibility(View.GONE);

        mLoadingCaption = (TextView) findViewById(R.id.lbl_loading_caption);
        mLongtitude = (TextView) findViewById(R.id.lbl_longtitude);
        mLatitude = (TextView) findViewById(R.id.lbl_latitude);
        mLocation = (TextView) findViewById(R.id.lbl_location);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_invite_listview);
        Initialize();
        //Start to populate the Friends List
        getFriendsList();

    }

    private void InitLocation(){
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);//���ö�λģʽ
        option.setCoorType(tempcoor);//���صĶ�λ����ǰٶȾ�γ�ȣ�Ĭ��ֵgcj02
        int span=1000;
        try {
            //span = Integer.valueOf(frequence.getText().toString());
        } catch (Exception e) {
            // TODO: handle exception
        }
        option.setScanSpan(span);//���÷���λ����ļ��ʱ��Ϊ5000ms
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
    }

    private void prepareTempAvatar(){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        final Bitmap tempAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.img_blank_black);
        tempAvatar.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] data = stream.toByteArray();
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        mTempAvatar = new ParseFile("Image.png", data);
        mTempAvatar.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                // If successful add file to user and signUpInBackground
                //if(null == e)
                    //createNewFeedGroup();
            }
        });
    }

    private void createNewFeedGroup(){
        final ParseObject GroupMaster = new ParseObject("GlobalFeed_Group_Master");
        GroupMaster.put("Name", mGroupName);
        GroupMaster.put("Canonical_Name", mGroupName.toLowerCase());
        GroupMaster.put("Description", mGroupDesc);
        GroupMaster.put("Type", mGroupType);
        GroupMaster.put("Creator_ObjectID", Player_objectId);
        GroupMaster.put("Creator_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        //TODO to enable creation of tutor group
        GroupMaster.put("IsTutorGroup", false);
        GroupMaster.put("GeoLocation", mGeoLocation);
        GroupMaster.put("Image", mTempAvatar);
        GroupMaster.put("Avatar", mTempAvatar);
        GroupMaster.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    addInvitesIntoFeed(GroupMaster.getObjectId());
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

    }

    private void addInvitesIntoFeed(final String GroupID){



        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        ParseObject NewPlayer = new ParseObject("GlobalFeed_Group_Members");
        //Add the Player who is creating the game frst
        NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", GroupID));
        NewPlayer.put("Member_ObjectID", Player_objectId);
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.put("Member_Status", 1); //by default, the creator is automatically a valid member
        PlayerList.add(NewPlayer);

        if(FriendObjIDList != null)
        {
            //followed by all the other friends he/she selected to join the game
            for (int i = 0; i<FriendObjIDList.length; i++){
                //insert new word into History table
                NewPlayer = new ParseObject("GlobalFeed_Group_Members");
                NewPlayer.put("Group_ObjectID", GroupID);
                NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                        "GlobalFeed_Group_Master", GroupID));
                NewPlayer.put("Member_ObjectID", FriendObjIDList[i]);
                NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                        "_User", FriendObjIDList[i]));
                NewPlayer.put("Member_Status", 0); //0 means pending adherance
                PlayerList.add(NewPlayer);
            }
        }




        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new game completed", "SUCCESS!");
                //uponConmpleteAddNewGame();
                //GoToGroup(GroupID);
                addInvitesIntoNotifications(GroupID);
                addFirstPostIntoGroup(GroupID);
            }
        });
    }

    private void addInvitesIntoNotifications(final String GroupID){

        if(FriendObjIDList == null)
            return;

        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        ParseObject NewPlayer;

        for (int i = 0; i<FriendObjIDList.length; i++){
            //insert new word into History table
            NewPlayer = new ParseObject("Notifications");
            //NewPlayer.put("Group_ObjectID", GroupID);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", FriendObjIDList[i]));
            NewPlayer.put("IsSeen", false);
            NewPlayer.put("Type", 2);
            NewPlayer.put("IsAccepted", false); //By default is False


            //NewPlayer.put("Member_ObjectID", FriendObjIDList[i]);
            NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Group_Master", GroupID));
            NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                    "_User", Player_objectId));

            PlayerList.add(NewPlayer);
        }

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new notifications", "SUCCESS!");

            }
        });


    }

    //Add a default 1st post saying who has created the group
    private void addFirstPostIntoGroup(final String GroupID){
        final ParseObject gameScore = new ParseObject("GlobalFeed_Master");

        gameScore.put("Message", PlayerID + " created this group");
        gameScore.put("User_ObjectID", Player_objectId);
        gameScore.put("User_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        gameScore.put("Group_ObjectID", GroupID);
        gameScore.put("Group_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", GroupID));
        gameScore.put("IsGroup", true);
        gameScore.put("Type", mAppState.getPostGroupInt());

        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {

                    GoToGroup(GroupID);
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });
    }

    private void GoToGroup(String objID){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("GroupObjectID", objID);
        intent.putExtra("IsGroup", true);
        intent.putExtra("GroupName", mGroupName);
        //TODO
        //intent.putExtra("IsTutor", getIsTutor(objID));
        intent.putExtra("IsTutor", false);
        startActivity(intent);
    }


    private void getFriendInvites(){

        //Skip this step if the user is new and has no friends
        if(adapter == null)
            return;

        StringBuilder result = new StringBuilder();
        StringBuilder resultObjId = new StringBuilder();
        int count = 0;
        for(int i=0;i<adapter.data.size();i++)
        {
            if(adapter.mCheckStates.get(i))
            {
                count++;
                result.append(PlayerFriendsArray.get(i).FriendID);
                result.append("\n");
                //FriendList[0] = PlayerFriendsArray.get(i).FriendID;
                resultObjId.append(PlayerFriendsArray.get(i).Friend_objectId);
                resultObjId.append("\n");

            }

        }

        if (count == 1){
            int friendsCount = 1;
            FriendList = new String[friendsCount];
            FriendObjIDList = new String[friendsCount];
            mAppState.SelectedFriendList(friendsCount);
            mAppState.SelectedFriendObjectIDList(friendsCount);
            FriendList[0] = result.substring(0, result.indexOf("\n"));
            FriendObjIDList[0] = resultObjId.substring(0, resultObjId.indexOf("\n"));

            mAppState.setSelectedFriendList(FriendList);
            mAppState.setSelectedFriendObjectIDList(FriendObjIDList);

        }
        else
        {
            //Toast.makeText(this, result, 1000).show();
            StringTokenizer tokens = new StringTokenizer(result.toString(), "\n");
            StringTokenizer tokensObjID = new StringTokenizer(resultObjId.toString(), "\n");
            int friendsCount = tokens.countTokens();
            FriendList = new String[friendsCount];
            FriendObjIDList = new String[friendsCount];
            mAppState.SelectedFriendList(friendsCount);
            mAppState.SelectedFriendObjectIDList(friendsCount);

            for(int i=0;i<friendsCount;i++) {
                FriendList[i] = tokens.nextToken();
                FriendObjIDList[i] = tokensObjID.nextToken();
            }

            //store into global class

            mAppState.setSelectedFriendList(FriendList);
            mAppState.setSelectedFriendObjectIDList(FriendObjIDList);
        }
    }

    private void displayJSONResults(List<ParseObject> data)
    {
        mLoadingCaption.setVisibility(View.GONE);
        try{
            PlayerFriendsArray = fromJson(data);

            Log.i("Finally", PlayerFriendsArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
            // Create the adapter to convert the array to views
            adapter = new AppInfoAdapter(this, R.layout.friends_invite_item, arrayOfUsers, this);
            adapter.addAll(PlayerFriendsArray);
            // Attach the adapter to a ListView
            ListView listView = (ListView) findViewById(R.id.listApplication);
            listView.setAdapter(adapter);

        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void getFriendsList(){

        mLoadingCaption.setVisibility(View.VISIBLE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User",Player_objectId);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    if(friendList.size() > 0)
                        displayJSONResults(friendList);
                    else
                        mLoadingCaption.setText(getResources().getText(R.string.group_lbl_no_friends));
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), i + 1, this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    public static class AppInfoHolder
    {
        //ImageView imgIcon;
        ImageView imgIcon;
        TextView txtTitle;
        CheckBox chkSelect;

    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.menu_source_icon);
                holder.txtTitle = (TextView) row.findViewById(R.id.menu_source_name);
                holder.chkSelect = (CheckBox) row.findViewById(R.id.menu_source_check_box);

                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.FriendID);
            holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.Friend_objectId, activity));
//            holder.imgIcon.setParseFile(appinfo.profilePic);
//
//            //TODO to improve this code, will not be good if hundreds of friends appear
//            holder.imgIcon.loadInBackground(new GetDataCallback() {
//                public void done(byte[] data, ParseException e) {
//                    // The image is loaded and displayed!
//                    //int oldHeight = imageView.getHeight();
//                    //int oldWidth = imageView.getWidth();
//                    Log.v("LOG!!!!!!", "imageView height = ");      // DISPLAYS 90 px
//                    Log.v("LOG!!!!!!", "imageView width = " );        // DISPLAYS 90 px
//                }
//            });
            //holder.imgIcon.setImageDrawable(appinfo.icon);
            // holder.chkSelect.setChecked(true);
            holder.chkSelect.setTag(position);
            holder.chkSelect.setChecked(mCheckStates.get(position, false));
            holder.chkSelect.setOnCheckedChangeListener(this);
            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }
//
//    public class PlayerFriend {
//        public String objectId;
//        public String PlayerID;
//        public String FriendID;
//        public String Friend_objectId;
//
//        public Drawable icon;
//        public String applicationName;
//        public ParseFile profilePic;
//
//        public PlayerFriend(ParseObject Obj, int c, final Activity activity) {
//            this.objectId = Obj.getObjectId();
//
//            this.PlayerID = Obj.getString("PlayerID");
//            this.FriendID = Obj.getString("FriendID");
//            this.profilePic = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
//            this.Friend_objectId = Obj.getParseObject("Friend_objectId").getObjectId();
//            //profilePic
//            //public static Drawable createFromResourceStream (Resources res, TypedValue value, InputStream is, String srcName, BitmapFactory.Options opts)
//
//
//            if(!IsBitmapCached(this.Friend_objectId, activity)){
//                ParseFile Picture = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
//                Picture.getDataInBackground(new GetDataCallback() {
//                    @Override
//                    public void done(byte[] data, ParseException e) {
//
//                        if (e == null) {
//                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
//                                    data.length);
//                            storeImage(PictureBmp, Friend_objectId, activity);
//
//                        } else {
//
//                        }
//
//                    }
//                });
//            }
//
//        }
//
//        private boolean IsBitmapCached(String FileName, Activity activity){
//            try {
//                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
//                // path to /data/data/yourapp/app_data/imageDir
//                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//
//                File f=new File(directory, FileName + ".png");
//                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
//                if (b != null)
//                    return true;
//
//            }
//            catch (FileNotFoundException e)
//            {
//                e.printStackTrace();
//            }
//
//            return false;
//        }
//
//        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
//            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
//            // path to /data/data/yourapp/app_data/imageDir
//            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//            // Create imageDir
//            File mypath=new File(directory, FileName + ".png");
//
//            FileOutputStream fos = null;
//            try {
//
//                fos = new FileOutputStream(mypath);
//
//                // Use the compress method on the BitMap object to write image to the OutputStream
//                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
//                fos.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            //return directory.getAbsolutePath();
//        }
//
//        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
//        {
//
//            try {
//                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
//                // path to /data/data/yourapp/app_data/imageDir
//                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//
//                File f=new File(directory, BitmapName + ".png");
//                return BitmapFactory.decodeStream(new FileInputStream(f));
//                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
//                //img.setImageBitmap(b);
//                //return b;
//            }
//            catch (FileNotFoundException e)
//            {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.global_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {
//            InitLocation();
//            mLocationClient.start();
            if(retrieveGeoLocation()){
                getFriendInvites();
                createNewFeedGroup();
                //prepareTempAvatar();
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private Boolean retrieveGeoLocation(){
        mGeoLocation = mAppState.getGeoLocPoint();
        if(mGeoLocation != null)
            return true;
        else
            return false;
    }

}
