package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Admin on 21/7/2015.
 */
public class PersonalMessage implements Serializable{

    private String objectId;
    private String PM_objectId;
    private String Sender_ObjectID;
    private String Sender_Name;
    private String Recipeint_ObjectID;
    private String Recipeint_Name;
    private String Message;
    private Boolean IsSeen;
    private ArrayList<String> Images;
    private Date CreatedAt;
    private String CreatedAtDisplay;
    private int noOfImages;

    //Properties/////////////////////////////START
    public ArrayList<String> getImages(){return this.Images;}
    public int getNoOfImages(){ return this.noOfImages;}
    public String getObjectId(){return this.objectId;}
    public String getMessage(){return this.Message;}
    public String getSender_ObjectID(){return this.Sender_ObjectID;}
    public String getSender_Name(){return this.Sender_Name;}
    public String getRecipeint_ObjectID(){return this.Recipeint_ObjectID;}
    public String getRecipeint_Name(){return this.Recipeint_Name;}
    public Boolean getIsSeen(){return this.IsSeen;}
    public String getCreatedAt(){
        String temp = this.CreatedAt.toString();
        int index = temp.indexOf("GMT");
        return temp.substring(0, index);
    }



    //Properties/////////////////////////////END

    public PersonalMessage(ParseObject Obj, final Activity activity) {
        this.objectId = Obj.getObjectId();
        this.PM_objectId = Obj.getParseObject("PM_objectId").getObjectId();
        this.Sender_Name = Obj.getParseObject("Sender_objectId").getString("username");
        this.Sender_ObjectID = Obj.getParseObject("Sender_objectId").getObjectId();
        this.Recipeint_Name = Obj.getParseObject("Recipient_objectId").getString("username");
        this.Recipeint_ObjectID = Obj.getParseObject("Recipient_objectId").getObjectId();
        this.CreatedAt = Obj.getCreatedAt();
        this.Message = Obj.getString("Message");
        this.noOfImages = 0;
        this.IsSeen = Obj.getBoolean("IsSeen");

        //load user avatar if it is not in cache
        if(!IsBitmapCached(this.Sender_ObjectID, activity)){
            ParseFile Picture = Obj.getParseObject("Sender_objectId").getParseFile("Avatar_Pic");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        storeImage(PictureBmp, Sender_ObjectID, activity);

                    } else {

                    }

                }
            });
        }

    }

    public void insertImagesThumbnail(ParseObject Obj, final Activity activity){
        if (noOfImages == 0)
        {
            this.Images = new ArrayList<String>();
        }
        this.Images.add( Obj.getObjectId());

        //
        //load post images if it is not in cache
        final String Filename = this.objectId + "_" + Obj.getObjectId() + "_Thumb";
        if(!IsBitmapCached(Filename ,activity)){
            ParseFile Picture = Obj.getParseFile("Image_Thumbnail");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        storeImage(PictureBmp, Filename, activity);

                    } else {

                    }

                }
            });
        }
        //


        this.noOfImages++;
    }

    public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }

        return null;
    }

    private boolean IsBitmapCached(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        //catch (FileNotFoundException e)
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }




}
