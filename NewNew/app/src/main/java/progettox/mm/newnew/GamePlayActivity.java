package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;


public class GamePlayActivity extends ActionBarActivity {



    private List<ParseObject> PlayerHistoryResults;
    private List<PlayerHistory> PlayerHistoryArray;
    public String PlayerID;
    public String Player_objectId;
    public String GameSessionID;
    public EditText mMainWord;
    public String mLastChar;
    public int mWordCount;
    public int mCurrentPlayerIndex;
    public int mNextPlayerIndex;
    public int mMaxPlayers;
    public String mNextPlayerID;
    public String mNextPlayerObjectID;
    public int mTurnInterval;
    public int mTotalWordsSubmitted;
    public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    private ProgressDialog mDialog;
    public InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_session_play);

        //retrieve and set game criteria
        Intent intent = getIntent();
        Bundle bundle = new Bundle();
        bundle = intent.getExtras();
        mLastChar = bundle.getString("LastChar");
        mWordCount = bundle.getInt("WordCount");

        //Retrieve global variables
        App appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();
        GameSessionID = appState.getGameSessionID();
        appState.setPageNumber(7);

        mMainWord = (EditText) findViewById(R.id.mainWord);

        //
        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Retrieving Game Sessions, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        //attach function to the submit button to call Parse to save the newly entered word
        ImageButton playButton = (ImageButton) findViewById(R.id.play_action);
        playButton.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view) {

                mDialog.show();
                checkForStartingLetter(mMainWord.getText().toString());
            }

        });

        //set UI for display criteria
        TextView wordCountCriteria =  (TextView) findViewById(R.id.txt_wordCount);
        TextView lastCharCriteria =  (TextView) findViewById(R.id.txt_LastChar);

        wordCountCriteria.setText(String.valueOf(mWordCount));
        lastCharCriteria.setText(mLastChar);

        //On Load, get the below details
        getCurrentPlayerMaxPlayer();
        getWordEntriesCount();

        //to force the keyboard to appear upon screen load//
        mMainWord.requestFocus();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        ////


    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();  // Always call the superclass method first
        hideSoftKeyboard();
    }

    public void getCurrentPlayerMaxPlayer(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_Master");
        query.getInBackground(GameSessionID, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    // object will be your game score
                    mCurrentPlayerIndex = object.getInt("CurrentPlayerIndex");
                    mMaxPlayers = object.getInt("TotalPlayers");
                    mTurnInterval = object.getInt("IntervalTurns");
                    //get next Player ID and Index
                    if (mCurrentPlayerIndex >= mMaxPlayers){
                        mNextPlayerIndex = 1;
                    }
                    else
                    {
                        mNextPlayerIndex = mCurrentPlayerIndex + 1;
                    }

                    getNextPlayer(mNextPlayerIndex);


                }
            }
        });
    }

    public void getNextPlayer(int TurnIndex){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_PlayerList");
        query.whereEqualTo("GameSession_ObjectID", GameSessionID);
        query.whereEqualTo("TurnIndex", TurnIndex);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> List, ParseException e) {
                if (e == null) {
                    //Log.d("No of repeated words", "Retrieved " + scoreList.size() + " word");
                    if(List.size() > 0){
                        //TODO this is a repeated word, send error message
                        mNextPlayerID = List.get(0).getString("PlayerID");
                        mNextPlayerObjectID = List.get(0).getString("Player_ObjectID");
                    }
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    //will be triggered when a successful word is submitted and saved in Parse
    public void updateMasterTable(){


        int limit = (mTotalWordsSubmitted + 1) % mTurnInterval;
        if(limit == 0){
            mWordCount++;
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_Master");

        // Retrieve the object by id
        query.getInBackground(GameSessionID, new GetCallback<ParseObject>() {
            public void done(ParseObject MasterTbl, ParseException e) {
                if (e == null) {
                    // Now let's update it with some new data. In this case, only cheatMode and score
                    // will get sent to the Parse Cloud. playerName hasn't changed.
                    MasterTbl.put("WordCount", mWordCount);
                    MasterTbl.put("CurrentPlayerID", mNextPlayerID);
                    MasterTbl.put("CurrentPlayerIndex", mNextPlayerIndex);
                    MasterTbl.put("CurrentPlayer_ObjectID", mNextPlayerObjectID);
                    MasterTbl.put("PreviousPlayerID", PlayerID);
                    MasterTbl.put("LastChar", mLastChar);
                    MasterTbl.saveInBackground();
                }
            }
        });

    }

    private void getWordEntriesCount(){

        //Log.d("PARSE", "Fetching of User in progress");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_History");
        query.whereEqualTo("GameSession_ObjectID", GameSessionID);
        //Log.d("ObjectId is", GameSessionID);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    mTotalWordsSubmitted = scoreList.size();


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void addWordtoParse(){


        //Close the soft keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mMainWord.getWindowToken(), 0);

        //insert new word into History table
        ParseObject gameScore = new ParseObject("GameSession_History");
        gameScore.put("score", 1337);
        gameScore.put("GameSession_ObjectID", GameSessionID);
        gameScore.put("GameSession_objectId", ParseObject.createWithoutData("GameSession_Master",GameSessionID));
        gameScore.put("Player_ObjectID", Player_objectId);
        gameScore.put("PlayerID", PlayerID);
        String word = mMainWord.getText().toString();
        gameScore.put("Word", word);
        gameScore.put("Stars", 0);
        gameScore.put("Points", 0);
        gameScore.saveInBackground();

        //get the last letter of the word
        mLastChar = word.substring(word.length()-1);

        updateMasterTable();

        uponConmpleteAddNewWord();

        mDialog.dismiss();
        PushNotifyPlayers();
        AlertDialogs.showAlertSuccessfulWordEntry(this);

        //finish();
    }

    public void PushNotifyPlayers()
    {
//        PushService.unsubscribe(this, GameSessionID);
        //ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
        //query.whereEqualTo("username", "rick");

//        ParseCloud.callFunctionInBackground("Pushtest", new HashMap<String, Object>(), new FunctionCallback<Object>() {
//            @Override
//            public void done(Object o, ParseException e) {
//
//            }
//        });

//        HashMap query = new HashMap<String, Object>();
//        query.put("Player", "rick");
//
//        ParseCloud.callFunctionInBackground("Pushtest", query, new FunctionCallback<Object>() {
//            @Override
//            public void done(Object o, ParseException e) {
//
//            }
//        });

//        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
//        installation.put("username", mNextPlayerID);
//        installation.saveInBackground();

//        ParsePush parsePush = new ParsePush();
//        ParseQuery pQuery = ParseInstallation.getQuery(); // <-- Installation query
//        pQuery.whereEqualTo("username", mNextPlayerID); // <-- you'll probably want to target someone that's not the current user, so modify accordingly
//        parsePush.sendMessageInBackground("PLAY " + GameSessionID, pQuery);

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("recipientId", mNextPlayerID);
        params.put("message", "PLAY " + GameSessionID + " " + PlayerID);
        ParseCloud.callFunctionInBackground("sendPushToUser", params, new FunctionCallback<String>() {
            @Override
            public void done(String o, ParseException e) {
                if (e == null) {
                    // Push sent successfully
                    Log.i("Cloud Push", " was successfully sent");
                }
            }
        });


    }

    public void uponConmpleteAddNewWord(){
        Intent intent = new Intent();
        intent.putExtra("Status", "Success");
        setResult(RESULT_OK, intent);
       // finish();
    }

    public void checkForRepatedWord(String word){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_History");
        query.whereEqualTo("GameSession_ObjectID", GameSessionID);
        query.whereEqualTo("Word", word);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> List, ParseException e) {
                if (e == null) {
                    //Log.d("No of repeated words", "Retrieved " + scoreList.size() + " word");
                    if(List.size() > 0){
                        mDialog.dismiss();
                        showAlertRepeatedWord();
                    }
                    else
                    {
                        //proceed to call Parse to enter this new word
                        addWordtoParse();
                    }
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public void showAlertRepeatedWord(){
        AlertDialogs.showAlertRepeatedWord(this);
    }

    public void checkForStartingLetter(String word){

        String startingLetter = word.substring(0,1);
        if(startingLetter.equals(mLastChar)){
            checkForWordLen(word);
        }
        else
        {
            mDialog.dismiss();
            AlertDialogs.showAlertIncorrectLetter(this, mLastChar);
        }


    }

    public void checkForWordLen(String word){
        if(word.length() == mWordCount) {
            //now we check for repeated word
            checkForRepatedWord(word);
        }
        else
        {
            mDialog.dismiss();
            AlertDialogs.showAlertIncorrectLength(this, mWordCount);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerHistoryArray = fromJson(data);

            Log.i("Finally", PlayerHistoryArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerHistory> arrayOfUsers = new ArrayList<PlayerHistory>();
            // Create the adapter to convert the array to views
            UsersAdapter adapter = new UsersAdapter(this, arrayOfUsers);
            adapter.addAll(PlayerHistoryArray);
            // Attach the adapter to a ListView
            ListView listView = (ListView) findViewById(R.id.lvItems);
            listView.setAdapter(adapter);

            ///////////
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    PlayerHistory listItem = PlayerHistoryArray.get(position);
//                    Log.i("Item selected is ", listItem.GameName);
//                }
//            });
            ///////////


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    public static class AlertDialogs{

        public AlertDialogs(){

        }

        public static void showAlertIncorrectLetter(Activity activity, String FirstChar){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter a word that starts with the letter " + FirstChar)
                    .setTitle("Invalid First Letter");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        public static void showAlertIncorrectLength(Activity activity, int len){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter a " + String.valueOf(len) + " letter word")
                    .setTitle("Invalid Number Of Letters");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        public static void showAlertRepeatedWord(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Sorry, the word was entered by another player. Please have another try.")
                    .setTitle("Please try Another Word");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        public static void showAlertSuccessfulWordEntry(final Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Good job! Your word has successfully been entered. Press Okay to go back.")
                    .setTitle("Congratulations!!");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    activity.finish();
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    public ArrayList<PlayerHistory> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerHistory> GameHistory = new ArrayList<PlayerHistory>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerHistory(jsonObjects.get(i), i+1));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return GameHistory;
    }

    public class PlayerHistory {
        public String objectId;
        public String GameSession_ObjectID;
        public String Player_ObjectID;
        public String PlayerID;
        public String Word;
        public int Stars;
        public Date CreatedAt;
        public String GameName;
        public String CurrentPlayerID;
        public int WordCount;
        public int TotalPlayers;
        public String LastCharacter;
        public int Points;
        public int index;

        public void setIndex(int count){
            index = count;
        }


        public PlayerHistory(ParseObject Obj, int c) {
            this.objectId = Obj.getObjectId();
            this.GameSession_ObjectID = Obj.getString("GameSession_ObjectID");
            this.Player_ObjectID = Obj.getString("Player_ObjectID");
            this.PlayerID = Obj.getString("PlayerID");
            this.Word = Obj.getString("Word");
            this.Stars = Obj.getInt("Stars");
            this.Points = Obj.getInt("Points");
            this.CreatedAt = Obj.getCreatedAt();
            this.GameName = Obj.getParseObject("GameSession_objectId").getString("Name");
            this.WordCount = Obj.getParseObject("GameSession_objectId").getInt("WordCount");
            this.TotalPlayers = Obj.getParseObject("GameSession_objectId").getInt("TotalPlayers");
            this.CurrentPlayerID = Obj.getParseObject("GameSession_objectId").getString("CurrentPlayerID");
            this.LastCharacter = Obj.getParseObject("GameSession_objectId").getString("LastChar");
            this.index = c;
        }

    }

    public class UsersAdapter extends ArrayAdapter<PlayerHistory> {
        public UsersAdapter(Context context, ArrayList<PlayerHistory> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            PlayerHistory user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_gamesessiondetails, parent, false);
            }
            // Lookup view for data population
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            TextView wordIndex = (TextView) convertView.findViewById(R.id.wordIndex);
            TextView diamond = (TextView) convertView.findViewById(R.id.Diamond);

            wordIndex.setText(String.valueOf(user.index));
            diamond.setText(String.valueOf(user.Stars));
            // Populate the data into the template view using the data object
            tvName.setText(user.Word);
            //tvName.setHint(user.GameSession_ObjectID);
            String SubHeader = "玩者: " + user.PlayerID + " 分数: " + user.Points;
            tvHome.setText(SubHeader);
            //tvName.setContentDescription(user.GameSession_ObjectID);


            //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
            View PrimaryColumn = convertView.findViewById(R.id.primary_target);
            PrimaryColumn.setTag(PrimaryColumn.getId(), user.objectId);
            //Then we set the onClick function here to pull the GameSession_objectId upon each click
            PrimaryColumn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(MainActivity.this,
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();

                        }
                    });

            View ShareButton = convertView.findViewById(R.id.secondary_action);
            ShareButton.setTag(ShareButton.getId(), user.GameSession_ObjectID);
            ShareButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            String outputText = "Share " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(MainActivity.this,
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                        }
                    });

            // Return the completed view to render on screen
            return convertView;
        }

    }
}
