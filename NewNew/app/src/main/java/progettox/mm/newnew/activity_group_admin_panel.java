package progettox.mm.newnew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.ListBlobItem;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
   * Created by Admin on 13/7/2015.
   */
 public class activity_group_admin_panel extends AppCompatActivity{

     private String mPlayer_objectId;
     private String mPlayer_Name;
     private String mGroup_objectId;
     private static final int CAMERA_REQUEST = 1888;
     private static final int AttachFileRequestCode = 2888;
     private Bitmap mUserPhoto;
     private ImageView mGroupProfilePic;
     private ParseFile mTempAvatar;
     private ParseFile mGroupPhotoBitmap;
     private ProgressDialog mDialog;
     static final int REQUEST_TAKE_PHOTO = 1;
     private String mCurrentPhotoPath;
     private App mAppState;
     private TextView mImageSaveLoading;
     private Button mOpenMemberRequests;

 //    private void getBlobImage(){
 //        try
 //        {
 //            // Define the connection-string with your values
 //            final String storageConnectionString =
 //                    "DefaultEndpointsProtocol=http;" +
 //                            "AccountName=samplewebapi2013" +
 //                            "AccountKey=PDuU6T86vS9w4+kRNipHsebnkIOKFUSTRj6WyQbChCM+kSc1cXu3A+k7jaleL/VdNxmy2Zo1QHbLQbGB+fszXw==";
 //
 //            final String StorageConnectionString = "DefaultEndpointsProtocol=http;AccountName=samplewebapi2013;AccountKey=PDuU6T86vS9w4+kRNipHsebnkIOKFUSTRj6WyQbChCM+kSc1cXu3A+k7jaleL/VdNxmy2Zo1QHbLQbGB+fszXw==;";
 //            // Retrieve storage account from connection-string.
 //            CloudStorageAccount storageAccount = CloudStorageAccount.parse(StorageConnectionString);
 //
 //            // Create the blob client.
 //            CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
 //
 //            // Retrieve reference to a previously created container.
 //            CloudBlobContainer container = blobClient.getContainerReference("tester1");
 //
 //            ContextWrapper cw = new ContextWrapper(getApplicationContext());
 //            // path to /data/data/yourapp/app_data/imageDir
 //            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
 //            // Create imageDir
 //            File mypath=new File(directory, "TestImage.png");
 //
 //            // Loop through each blob item in the container.
 //            for (ListBlobItem blobItem : container.listBlobs()) {
 //                // If the item is a blob, not a virtual directory.
 //                if (blobItem instanceof CloudBlob) {
 //                    // Download the item and save it to a file with the same name.
 //                    CloudBlob blob = (CloudBlob) blobItem;
 //                    //blob.download(new FileOutputStream("C:\\mydownloads\\" + blob.getName()));
 //                    blob.download(new FileOutputStream(mypath + blob.getName()));
 //                }
 //            }
 //
 //            mGroupProfilePic = (ImageView) findViewById(R.id.group_image);
 //            mGroupProfilePic.setImageBitmap(GlobalPost.LoadPNG("TestImage", this));
 //
 //        }
 //        catch (Exception e)
 //        {
 //            // Output the stack trace.
 //            e.printStackTrace();
 //        }
 //    }

     private void Initialize(){

         mAppState = ((App)getApplicationContext());
         mPlayer_Name = mAppState.getPlayerID();
         mPlayer_objectId = mAppState.getPlayer_objectId();

         //get group ObjectID
         Bundle extras = getIntent().getExtras();
         if(extras != null)
             mGroup_objectId = extras.getString("Group_ObjectID");

         mDialog = new ProgressDialog(this);
         mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
         mDialog.setMessage("Updating your profile pic.");
         mDialog.setIndeterminate(true);
         mDialog.setCanceledOnTouchOutside(false);


         mImageSaveLoading = (TextView) findViewById(R.id.lbl_loading_image);
         mGroupProfilePic = (ImageView) findViewById(R.id.group_image);
         Bitmap groupPhoto = GlobalPost.LoadPNG(mGroup_objectId, this);
         if(groupPhoto != null)
             mGroupProfilePic.setImageBitmap(groupPhoto);
         else
         {
             groupPhoto = BitmapFactory.decodeResource(getResources(),
                     R.drawable.bg_feed_generic);
             mGroupProfilePic.setImageBitmap(groupPhoto);
         }

         mOpenMemberRequests = (Button) findViewById(R.id.btn_open_member_requests);
         mOpenMemberRequests.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {
                 openMemberRequestsPage();

             }
         });

         ImageButton cameraBtn = (ImageButton) findViewById(R.id.camera_action);
         cameraBtn.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {
                 dispatchTakePictureIntent();

             }
         });

         ImageButton attachBtn = (ImageButton) findViewById(R.id.attach_action);
         attachBtn.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {

                 final Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                 startActivityForResult(galleryIntent,AttachFileRequestCode);
             }
         });

         //getBlobImage();
         loadMemberRequests();
     }

     private void loadMemberRequests(){


         ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
         ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master",mGroup_objectId);
         query.whereEqualTo("Group_objectId", obj);
         query.whereEqualTo("Member_Status", 0);

         query.countInBackground(new CountCallback() {
             @Override
             public void done(int i, ParseException e) {
                 //if (i > 0) {
                     updateNoOfMemberReq(i);
                 //}
             }
         });

     }

     private void updateNoOfMemberReq(int count){
         mOpenMemberRequests.setText(getResources().getText(R.string.lbl_member_request) + " (" +
            String.valueOf(count) + ") ");
     }

     private void openMemberRequestsPage(){
         Intent createNewPost = new Intent(this, activity_group_admin_mrequests.class);
         //Type 1 is GLOBAL FEED, Type 2 is GROUP FEED, Type 3 is USER/INDIVIDUAL FEED

         createNewPost.putExtra("Group_ObjectID", mGroup_objectId);

         startActivity(createNewPost);
     }

     @Override
     public void onSaveInstanceState(Bundle savedInstanceState) {

         // Always call the superclass so it can save the view hierarchy state
         super.onSaveInstanceState(savedInstanceState);

         if(mUserPhoto != null){

             //savedInstanceState.putBitmapArrayList("PostImage",mUserPhotoArray);
             savedInstanceState.putParcelable("PostImage", mUserPhoto);
         }
     }

     @Override
     public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_group_admin_panel);

         Initialize();
         // Check whether we're recreating a previously destroyed instance
         if (savedInstanceState != null) {
             // Restore value of members from saved state
             //Bitmap bmp = savedInstanceState.getParcelable("PostImage");
             Bitmap temp = savedInstanceState.getParcelable("PostImage");
             if(temp != null){
                 mUserPhoto = temp;
                 //imageView.setImageBitmap(mUserPhoto);
             }

         } else {
             // Probably initialize members with default values for a new instance
         }
     }

     private void dispatchTakePictureIntent() {
         Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         // Ensure that there's a camera activity to handle the intent
         if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
             // Create the File where the photo should go
             File photoFile = null;
             try {
                 photoFile = createImageFile();
             } catch (IOException ex) {
                 // Error occurred while creating the File
                 //...
             }
             // Continue only if the File was successfully created
             if (photoFile != null) {
                 takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                         Uri.fromFile(photoFile));
                 startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
             }
         }
     }

     private void galleryAddPic() {
         Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
         File f = new File(mCurrentPhotoPath);
         Uri contentUri = Uri.fromFile(f);
         mediaScanIntent.setData(contentUri);
         this.sendBroadcast(mediaScanIntent);
     }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        if(storageDir.isDirectory()){
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );


        }
        else{
            storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        }
// Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.getAbsolutePath();
        galleryAddPic();
        return image;
    }

     protected void onActivityResult(int requestCode, int resultCode, Intent data) {
         if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

 //            mDialog.show();
 //            Bitmap UserPhoto = (Bitmap) data.getExtras().get("data");
 //            mGroupProfilePic.setImageBitmap(UserPhoto);
 //            updateUserAvatar(UserPhoto);

             File file = new File(mCurrentPhotoPath);

             if(file.exists()){
                 Bitmap UserPhoto = BitmapFactory.decodeFile(file.getAbsolutePath());
                 int oriImageWidth =  UserPhoto.getWidth();
                 int oriImageHeight = UserPhoto.getHeight();

                 //get default height and width for group image for standardization
                 int imageWidth = mAppState.getGroupPhotoWidth();
                 int imageHeight = mAppState.getGroupPhotoHeight();

                 BitmapFactory.Options bounds = new BitmapFactory.Options();
                 bounds.inJustDecodeBounds = true;
                 BitmapFactory.decodeFile(file.getAbsolutePath(), bounds);

                 BitmapFactory.Options opts = new BitmapFactory.Options();
                 //Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                 //BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                 opts.inSampleSize = BitmapManager.calculateInSampleSize(oriImageWidth, oriImageHeight,
                         imageWidth, imageHeight);

                 opts.inJustDecodeBounds = false;
                 //return BitmapFactory.decodeResource(res, resId, options);
                 Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
 //                Bitmap bm = BitmapManager.decodeSampledBitmapFromResource(getResources(),
 //                            file.getAbsolutePath(), imageWidth, imageHeight);

                 ExifInterface exif = null;
                 try{
                     exif = new ExifInterface(file.getAbsolutePath());
                 }
                 catch(Exception e){

                 }

                 String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                 int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

                 int rotationAngle = 0;
                 if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
                 if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
                 if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

                 Matrix matrix = new Matrix();
                 matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
                 //UserPhoto = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
                 UserPhoto = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                 GlobalPost.StorePNG(UserPhoto, mGroup_objectId, this);

                 mGroupProfilePic.setImageBitmap(UserPhoto);


                 //from here we prep the bitmap to be saved to Parse
                 ByteArrayOutputStream stream = new ByteArrayOutputStream();
                 UserPhoto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                 byte[] dataStream = stream.toByteArray();

                 String imgFileName = mGroup_objectId;
                 //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
                 mGroupPhotoBitmap = new ParseFile (imgFileName + ".jpg", dataStream);
                 mImageSaveLoading.setVisibility(View.VISIBLE);
                 mGroupPhotoBitmap.saveInBackground(new SaveCallback() {
                     public void done(ParseException e) {
                         // If successful add file to user and signUpInBackground
                         if(null == e){
                             savetoParse();
                         }

                     }
                 });
             }



         }

         if (requestCode == AttachFileRequestCode && resultCode == RESULT_OK) {

             Uri selectedImageUri = data.getData();
             String[] fileColumn = {MediaStore.Images.Media.DATA};
             Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
                     null, null, null);
             imageCursor.moveToFirst();

             int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
             String picturePath = imageCursor.getString(fileColumnIndex);
 //
 //            Bitmap UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(), picturePath, 400, 400);
 //            mGroupProfilePic.setImageBitmap(UserPhoto);

             //get default height and width for group image for standardization
             int imageWidth = mAppState.getGroupPhotoWidth();
             int imageHeight = mAppState.getGroupPhotoHeight();


             Bitmap UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
                     picturePath, imageWidth, imageHeight);
             GlobalPost.StorePNG(UserPhoto, mGroup_objectId, this);
             mGroupProfilePic.setImageBitmap(UserPhoto);


             //from here we prep the bitmap to be saved to Parse
             ByteArrayOutputStream stream = new ByteArrayOutputStream();
             UserPhoto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
             byte[] dataStream = stream.toByteArray();

             String imgFileName = mGroup_objectId;
             //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
             mGroupPhotoBitmap = new ParseFile (imgFileName + ".jpg", dataStream);
             mImageSaveLoading.setVisibility(View.VISIBLE);
             mGroupPhotoBitmap.saveInBackground(new SaveCallback() {
                 public void done(ParseException e) {
                     // If successful add file to user and signUpInBackground
                     if(null == e){
                         savetoParse();
                     }

                 }
             });
         }
     }

 //    private void updateUserAvatar(final Bitmap img){
 //        String Username = mPlayer_Name;
 //        ByteArrayOutputStream stream = new ByteArrayOutputStream();
 //        //Bitmap tempAvatar = mUserPhoto;
 //        img.compress(Bitmap.CompressFormat.PNG, 100, stream);
 //        byte[] data = stream.toByteArray();
 //        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
 //        mTempAvatar = new ParseFile(Username + ".png", data);
 //        mTempAvatar.saveInBackground(new SaveCallback() {
 //            public void done(ParseException e) {
 //                // If successful add file to user and signUpInBackground
 //                if(null == e)
 //                    savetoParse(img);
 //            }
 //        });
 //    }

     private void savetoParse(){
         ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Master");
         query.whereEqualTo("objectId", mGroup_objectId);

         query.findInBackground(new FindCallback<ParseObject>() {
             public void done(List<ParseObject> scoreList, ParseException e) {
                 if (e == null) {
                     if(scoreList.size() > 0){
                         scoreList.get(0).put("Image", mGroupPhotoBitmap);
                         scoreList.get(0).saveInBackground();
                         mImageSaveLoading.setVisibility(View.GONE);
                     }
                 } else {
                     Log.d("score", "Error: " + e.getMessage());
                 }
             }
         });

     }

     private void completeUpdateAvatar(){
         Toast.makeText(this,
                 "User Image updated successfully",
                 Toast.LENGTH_SHORT).show();
     }

     @Override
     public void onResume() {
         super.onResume();  // Always call the superclass method first

         //mGroupProfilePic.setImageBitmap(GlobalPost.LoadPNG(mPlayer_objectId, this));
     }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
 }
