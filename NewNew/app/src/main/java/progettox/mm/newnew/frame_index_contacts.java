package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 31/7/2015.
 */
public class frame_index_contacts extends Fragment {

    public String mPlayerID;
    public String mPlayer_objectId;
    private View myFragmentView;
    private LoadImageTask mLoadImageTask;
    private TextView txtLoadingCaption;
    public App mAppState;
    public ListView listView;
    private ArrayList<PlayerFriend> PlayerFriendsArray;
    private FriendListAdapter mAdapter;

    public static frame_index_contacts newInstance(String param1, String param2) {
        frame_index_contacts fragment = new frame_index_contacts();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void Initialize(){
        mAppState = ((App)getActivity().getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Initialize();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.chat_main_list, container, false);
        txtLoadingCaption = (TextView)myFragmentView.findViewById(R.id.lbl_loading_caption);
        txtLoadingCaption.setText(getActivity().getResources().getText(R.string.lbl_contact_load));
        txtLoadingCaption.setVisibility(View.VISIBLE);
        listView = (ListView) myFragmentView.findViewById(R.id.lvItems);

        setHasOptionsMenu(true);

        return myFragmentView;
    }


    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

        //TODO set proper page numbering
        App appState = ((App)getActivity().getApplicationContext());
        appState.setPageNumber(3);


    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        getChatFriendsList();

    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), i+1, getActivity()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerFriendsArray = fromJson(data);

            Log.i("Finally", PlayerFriendsArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
            // Create the adapter to convert the array to views
            mAdapter = new FriendListAdapter(getActivity(), R.layout.chat_contact_item, arrayOfUsers, getActivity());
            mAdapter.addAll(PlayerFriendsArray);
            // Attach the adapter to a ListView
            listView.setAdapter(mAdapter);
            txtLoadingCaption.setVisibility(View.GONE);

        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void getChatFriendsList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        //query.whereEqualTo("PlayerID", mPlayer_objectId);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;

                    if(friendList.size()>0)
                        displayJSONResults(friendList);
                    else
                        txtLoadingCaption.setVisibility(View.GONE);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        //        String testURL = "http://samplewebapi2013.azurewebsites.net/api/JsonFile/GetFriendList?playerId=louie";
        //        getData newTask = new getData(testURL);
        //        newTask.execute();

    }

    public class FriendListAdapter extends ArrayAdapter<PlayerFriend> {
        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public FriendListAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final PlayerFriend user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(
                        getContext()).inflate(layoutResourceId, parent, false);
            }
            // Lookup view for data population
            TextView tvBody = (TextView) convertView.findViewById(R.id.tvBody);
            final ImageView ivProfileLeft = (ImageView) convertView.findViewById(R.id.ivProfileLeft);

            //now we determine what to display on the chat history list
            //first we determine what user and user image to put


            tvBody.setText(user.FriendID);
            //ivProfileLeft.setImageBitmap(user.loadImageFromStorage(user.Friend_objectId, activity));
            try{

                mLoadImageTask = new LoadImageTask(ivProfileLeft, user.Friend_objectId,
                        activity);
                mLoadImageTask.execute(ivProfileLeft);

            }
            catch(Exception e){
                Log.i("Group Settings Members", e.getMessage());
            }
            //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
            View PrimaryColumn = convertView.findViewById(R.id.primary_target);
            PrimaryColumn.setTag(PrimaryColumn.getId(), user.Friend_objectId);
            //Then we set the onClick function here to pull the GameSession_objectId upon each click
            PrimaryColumn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onFriendTouched(user.FriendID, user.Friend_objectId);
                        }
                    });






            // Return the completed view to render on screen
            return convertView;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.find_contacts, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search_contacts) {
            FindContacts();
            return true;
        }
        if (id == R.id.action_create_group) {
            CreateGroupChat();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }



    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //holder.PostImages[i].setImageBitmap(g.loadImageFromStorage(fileName, getActivity()));
            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
            //v = params[0];
            //return mFakeImageLoader.getImage();
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    private void CreateGroupChat(){
        ((MainMenuActivity)getActivity()).goToCreateGroupChat();
    }

    private void FindContacts(){
        ((MainMenuActivity)getActivity()).goToFindNewContacts();
    }

    private void onFriendTouched(String FriendID, String Friend_objectID){
        ((MainMenuActivity)getActivity()).goToIndividualPage(Friend_objectID, FriendID);
    }
}
