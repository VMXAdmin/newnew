package progettox.mm.newnew;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Admin on 13/5/2015.
 */
public class ParseReceiver extends ParsePushBroadcastReceiver {

    private void showOSNotification(Context context, Intent intent,
                                    String NotificationID, String NotificationType,
                                    String MessageSender){

        App appState = ((App)context.getApplicationContext());
        appState.setGameNotification(true);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("外语您好")
                        .setContentText("It's Your turn to Play!");
        mBuilder.setAutoCancel(true);

        if(NotificationType.equals("PLAY")){
            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(context, MainMenuActivity.class);
            //Create the bundle
            Bundle bundle = new Bundle();

            //Add your data to bundle
            bundle.putString("GameSessionID_Notify", NotificationID);
            bundle.putString("NotificationType", NotificationType);
            //bundle.putString("Player_objectId", UOBJID);

            //Add the bundle to the intent
            resultIntent.putExtras(bundle);



            // The stack builder object will contain an artificial back stack for the
            // started Activity.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(MainMenuActivity.class);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);

            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            mBuilder.setSound(uri);
            mBuilder.setVibrate(new long[] { 1000, 1000});
            mBuilder.setContentIntent(resultPendingIntent);

//        Notification note = mBuilder.build();
//        note.defaults |= Notification.DEFAULT_VIBRATE;
//        note.defaults |= Notification.DEFAULT_SOUND;
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


            // mId allows you to update the notification later on.
            mNotificationManager.notify(1, mBuilder.build());
        }
        else if(NotificationType.equals("CHAT")){
            mBuilder.setContentText("You have a new Message from " + MessageSender);
            final String ChatAlertType = "PUSH";
            //check if user is opening the chat page AND in the right chat Session
            int pageNo = appState.getPageNumber();
            String ChatID = appState.getChatID();

            //directly update the screen
            if(pageNo == 10 && ChatID.equals(NotificationID)){


                Intent i = new Intent(context, ChatActivity.class);
                i.putExtra("ChatID", NotificationID);
                i.putExtra("ChatUsername", MessageSender);
                i.putExtra("ChatAlertType", ChatAlertType);
                //i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                //i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

                return;
            }

            //directly update screen showing which contact sent in new messages
            if(pageNo == 9){

            }



            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(context, MainMenuActivity.class);
            //Create the bundle
            Bundle bundle = new Bundle();

            //Add your data to bundle
            bundle.putString("GameSessionID_Notify", NotificationID);
            bundle.putString("NotificationType", NotificationType);
            bundle.putString("ChatAlertType", ChatAlertType);
            bundle.putString("ChatUsername", MessageSender);
            //bundle.putString("Player_objectId", UOBJID);

            //Add the bundle to the intent
            resultIntent.putExtras(bundle);



            // The stack builder object will contain an artificial back stack for the
            // started Activity.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(MainMenuActivity.class);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);

            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            mBuilder.setSound(uri);
            mBuilder.setVibrate(new long[] { 1000, 1000});
            mBuilder.setContentIntent(resultPendingIntent);

//        Notification note = mBuilder.build();
//        note.defaults |= Notification.DEFAULT_VIBRATE;
//        note.defaults |= Notification.DEFAULT_SOUND;
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


            // mId allows you to update the notification later on.
            mNotificationManager.notify(1, mBuilder.build());
                    //((MainActivity)getActivity()).GotoGameActivity(PlayerID, PlayerObjectId);
        }



    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.i(TAG, "Received intent: " + intent.toString());
        String action = intent.getAction();
        Bundle extras = intent.getExtras();
        String jsonData = extras.getString("com.parse.Data");
        JSONObject jsonObject;
        String NotificationID = "";
        String NotificationType = "";
        String MessageSender = "";

        try {
            jsonObject = new JSONObject(jsonData);
            String msg = jsonObject.getString("alert");
            StringTokenizer tokens = new StringTokenizer(msg, " ");
            NotificationType = tokens.nextToken();
            NotificationID = tokens.nextToken();

            if(NotificationType.equals("CHAT")){
                MessageSender = tokens.nextToken();
            }



            //String dataString = jsonObject.getString("dataString");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Log.d("TEST", action.toString());
        if (action.equals(ParsePushBroadcastReceiver.ACTION_PUSH_RECEIVE)) {
            //JSONObject extras;
            try {

                showOSNotification(context, intent, NotificationID,
                        NotificationType, MessageSender);
                //showOSNotification(context, intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected Notification getNotification(Context context, Intent intent) {

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent piIntent=PendingIntent.getActivity(context,0,
                notificationIntent,0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle("My Notification")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Testing new Push Obj"));
        //.setContentText(extras.getString("alert")).setAutoCancel(true);
        mBuilder.setContentIntent(piIntent);

        return mBuilder.build();
    }

    protected void kissMyAss(){}
}
