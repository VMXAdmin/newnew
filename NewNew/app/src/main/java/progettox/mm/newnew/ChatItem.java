package progettox.mm.newnew;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Admin on 23/5/2015.
 */
public class ChatItem
        implements Serializable {

    private String Chat_ObjectID;
    private Date CreatedAt;
    private String ChatName;
    private String Chat_UserObjectID;
    private String PicturePath;
    private boolean IsGroup;
    private int Index;
    private ArrayList<ChatDetailsHolder> ChatDetails;
    private String Channel;


    public Bitmap getChatImage(){

        return null;
    }

    public String getChatUsername(){

        return this.ChatName;
    }

    public void createNewList(){
        if (ChatDetails == null)
        {
            this.ChatDetails = new ArrayList<ChatDetailsHolder>();
        }
    }

    public ArrayList<ChatDetailsHolder> getChatDetails(){
//        if (ChatDetails == null)
//        {
//            this.ChatDetails = new ArrayList<ChatDetailsHolder>();
//        }
        return ChatDetails;
    }

    public void insertChatDetails(ParseObject Obj, Activity activity){
        if (ChatDetails == null)
        {
            this.ChatDetails = new ArrayList<ChatDetailsHolder>();
        }
        this.ChatDetails.add( new ChatDetailsHolder(Obj, activity));

    }

    public String getChannel(){return this.Channel;}



    public ChatItem(ParseObject Obj, int c, final Activity activity) {
        //this.activity = act;
        this.Chat_ObjectID = Obj.getParseObject("Chat_objectId").getObjectId();
        this.CreatedAt = Obj.getParseObject("Chat_objectId").getCreatedAt();
        //this.ChatName = Obj.getParseObject("Chat_objectId").getString("Name");
        this.Index = c;
        this.IsGroup = Obj.getParseObject("Chat_objectId").getBoolean("IsGroup");

        if (this.IsGroup){

            this.ChatName = Obj.getParseObject("Chat_objectId").getString("Name");
            this.Channel = Obj.getParseObject("Chat_objectId").getString("Channel");

            if(!IsBitmapCached(this.Chat_ObjectID, activity)){
                ParseFile Picture = Obj.getParseObject("Chat_objectId").getParseFile("Picture");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Chat_ObjectID, activity);

                        } else {

                        }

                    }
                });
            }


            this.PicturePath = this.ChatName + ".png";
        }

    }

    private boolean IsBitmapCached(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);


            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        //catch (FileNotFoundException e)
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return false;
    }

    public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {


        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");


            FileOutputStream fos = null;
            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public Bitmap getChatImageBmp(){
        //return this.PictureBmp;
        return null;
    }

    public void updateChatName(String name, String objID){
        if(!this.IsGroup) {
            this.ChatName = name;
            //this.Chat_ObjectID = objID;
        }
    }

    public void updateChatAvatar(ParseFile Img, final String ImageName, final Activity activity){
        if(!this.IsGroup)
        {
            if(!IsBitmapCached(ImageName, activity)){
                ParseFile Picture = Img;
                //attempt to convert parseFile to Bitmap
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, ImageName, activity);
                        } else {

                        }

                    }
                });
            }

        }

    }

    public boolean getIsGroup(){return this.IsGroup;}

    public String getChatID(){ return this.Chat_ObjectID;}

}
