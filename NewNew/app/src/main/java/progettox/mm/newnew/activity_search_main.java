package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 30/6/2015.
 */
public class activity_search_main extends AppCompatActivity{

    private String mPlayer_objectId;
    private String mPlayer_Name;
    private EditText mSearchView;
    private ListView mListView;
    private TextView mTxtLoadingCaption;
    private ArrayAdapter<String> mAdapter;
    private LoadImageTask mLoadImageTask;
    private GroupAdapter mGroupAdapter;
    private ArrayList<Group> mGroupArray;
    private ArrayList<Group> mGroupArraySearched;
    private ArrayList<Group> mTemp;


    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // ignore
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // ignore
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.d("TAG", "*** Search value changed: " + s.toString());
            //getFilter().filter(s.toString());
            mTxtLoadingCaption.setVisibility(View.VISIBLE);
            mGroupAdapter.clear();
            if (mGroupArray != null) mGroupArray.clear();

            if(mSearchView.getText().toString().equals(""))
                return;

            LoadPersons(s.toString());
            LoadGroups(s.toString());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_main);

        Initialize();
    }

    private void Initialize(){

        App appState = ((App)getApplicationContext());
        mPlayer_Name = appState.getPlayerID();
        mPlayer_objectId = appState.getPlayer_objectId();

        mSearchView = (EditText) findViewById(R.id.search_view);
        mSearchView.addTextChangedListener(searchTextWatcher);

        mListView = (ListView) findViewById(R.id.list_view);
        mTxtLoadingCaption = (TextView) findViewById(R.id.lbl_loading_caption);
        mTxtLoadingCaption.setVisibility(View.GONE);
        // Construct the data source
        ArrayList<Group> arrayOfUsers = new ArrayList<Group>();
        // Create the adapter to convert the array to views
        mGroupAdapter = new GroupAdapter(this, R.layout.group_browser_item, arrayOfUsers, this);
        mTemp = new ArrayList<Group>();


    }

    public void onClickSearch(View view){
        mTxtLoadingCaption.setVisibility(View.VISIBLE);
        mGroupAdapter.clear();
        if (mGroupArray != null) mGroupArray.clear();

        if(mSearchView.getText().toString().equals(""))
            return;

        LoadPersons(mSearchView.getText().toString());
        LoadGroups(mSearchView.getText().toString());


    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        //LoadGroups();
    }

    private void setupSearchView() {
//        mSearchView.setIconifiedByDefault(false);
//        mSearchView.setOnQueryTextListener(this);
//        mSearchView.setSubmitButtonEnabled(false);
        //mSearchView.setQueryHint(getString(R.string.cheese_hunt_hint));
    }

//    public boolean onQueryTextChange(String newText) {
//        if (TextUtils.isEmpty(newText)) {
//            mListView.clearTextFilter();
//            mListView.setVisibility(View.INVISIBLE);
//        } else {
//            mListView.setFilterText(newText);
//            mListView.setVisibility(View.VISIBLE);
//        }
//        return true;
//    }
//
//    public boolean onQueryTextSubmit(String query) {
//        return false;
//    }

    private void LoadPersons(String searchTerm){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereContains("Canonical_username", searchTerm);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    if(objects.size() > 0){
                        Log.d("Groups", "Retrieved " + objects.size() + " scores");
                        InitializeAdapterAndLV_Persons(objects);
                    }
                    else
                    {
                        //txtLoadingCaption.setVisibility(View.GONE);
                        //mGroupAdapter.clear();
                        //mGroupAdapter.notifyDataSetChanged();
                        if(mGroupAdapter.isEmpty())
                            mTxtLoadingCaption.setText(getResources().getText(R.string.lbl_search_empty));
                    }
                } else {
                    // Something went wrong.
                }
            }
        });

    }

    private void LoadGroups(String searchTerm){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Master");
        query.whereContains("Canonical_Name", searchTerm);
        query.include("Group_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        InitializeAdapterAndLV(groupList);
                    }
                    else
                    {
                        //txtLoadingCaption.setVisibility(View.GONE);
                        //mGroupAdapter.clear();
                        //mGroupAdapter.notifyDataSetChanged();
                        if(mGroupAdapter.isEmpty())
                            mTxtLoadingCaption.setText(getResources().getText(R.string.lbl_search_empty));
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });

    }

    private void InitializeAdapterAndLV_Persons(List<ParseUser> data){
        mTemp = ConvertJSONtoObjects_Persons(data);
        if(mGroupArray == null) mGroupArray = new ArrayList<Group>();
        mGroupArray.addAll(mTemp);

//        // Construct the data source
//        ArrayList<Group> arrayOfUsers = new ArrayList<Group>();
//        // Create the adapter to convert the array to views
//        mGroupAdapter = new GroupAdapter(this, R.layout.group_browser_item, arrayOfUsers, this);
        //mGroupAdapter.clear();
        mGroupAdapter.clear();
        mGroupAdapter.addAll(mGroupArray);
        // Attach the adapter to a ListView
        //ListView listView = (ListView) findViewById(R.id.lv_groups);

        mListView.setAdapter(mGroupAdapter);

        if(mGroupAdapter.isEmpty()){
            mTxtLoadingCaption.setText(getResources().getText(R.string.lbl_search_empty));
        }
        else {
            mTxtLoadingCaption.setText(getResources().getText(R.string.lbl_search_keyword));
        }
    }

    private void InitializeAdapterAndLV(List<ParseObject> data){
        mTemp = ConvertJSONtoObjects(data);
        if(mGroupArray == null) mGroupArray = new ArrayList<Group>();
        mGroupArray.addAll(mTemp);

//        // Construct the data source
//        ArrayList<Group> arrayOfUsers = new ArrayList<Group>();
//        // Create the adapter to convert the array to views
//        mGroupAdapter = new GroupAdapter(this, R.layout.group_browser_item, arrayOfUsers, this);
//        mGroupAdapter.clear();
        mGroupAdapter.clear();
        mGroupAdapter.addAll(mGroupArray);
        // Attach the adapter to a ListView
        //ListView listView = (ListView) findViewById(R.id.lv_groups);
        mListView.setAdapter(mGroupAdapter);

        if(mGroupAdapter.isEmpty()){
            mTxtLoadingCaption.setText(getResources().getText(R.string.lbl_search_empty));
        }
        else {
            mTxtLoadingCaption.setText(getResources().getText(R.string.lbl_search_keyword));
        }



    }

    public ArrayList<Group> ConvertJSONtoObjects_Persons(List<ParseUser> jsonObjects) {
        ArrayList<Group> GameHistory = new ArrayList<Group>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new Group(jsonObjects.get(i), this, false));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    public ArrayList<Group> ConvertJSONtoObjects(List<ParseObject> jsonObjects) {
        ArrayList<Group> GameHistory = new ArrayList<Group>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new Group(jsonObjects.get(i), this, true));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    public static class GroupHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        ImageView imgIsTutor;
        LinearLayout linearLayout;

    }

    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                ArrayList<Group> orig = new ArrayList<Group>();
                final ArrayList<Group> results = new ArrayList<Group>();
                if (orig == null || orig.size() == 0)
                    orig = mGroupArray;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final Group g : orig) {
                            if (g.getGroupName().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                mGroupArraySearched = (ArrayList<Group>) results.values;
                mGroupAdapter.clear();
                mGroupAdapter.addAll(mGroupArraySearched);
                mGroupAdapter.notifyDataSetChanged();
                //notifyDataSetChanged();
            }
        };
    }

    private String getGroupName(String ObjId){
        for (int i = 0; i < mGroupArray.size(); i++){
            if(mGroupArray.get(i).getObjectID().equals(ObjId))
                return mGroupArray.get(i).getGroupName();
        }

        return "";
    }

    private void goToGroup(String objID){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("GroupObjectID", objID);
        intent.putExtra("GroupName", getGroupName(objID));
        intent.putExtra("IsGroup", true);
        intent.putExtra("IsTutor", false);
        startActivity(intent);
    }

    public void goToUserFeedPage(String username, String User_ObjectID){

        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", User_ObjectID);
        intent.putExtra("Player_Name", username);
        intent.putExtra("IsGroup", false);
        startActivity(intent);
    }

    private void onClickLayoutItem(String Notification_ObjectID){
        //int Type = mGroupArray.get(Notification_ObjectID).getNotificationType();
        int index = 0;
        for(int i=0;i<mGroupArray.size();i++){
            if(mGroupArray.get(i).getObjectID().equals(Notification_ObjectID)){
                index = i;
                break;
            }
        }


        //2 is Group join request (ppl ask you to join a group)
        if(mGroupArray.get(index).getType() == 2){
            String Group_objectId = mGroupArray.get(index).getObjectID();
            String Group_name = mGroupArray.get(index).getGroupName();
            //TODO check of isTutor Group or not, now is temporarily set to false
            goToGroup(Group_objectId);
        }
        //3 is somebody want to be your friend
        else if(mGroupArray.get(index).getType() == 1){
            String username = mGroupArray.get(index).getGroupName();
            String user_objectId = mGroupArray.get(index).getObjectID();

            goToUserFeedPage(username, user_objectId);
        }
       //TODO to do for 3, which is tutor

    }

    public class GroupAdapter extends ArrayAdapter<Group>
    {
        private Activity activity;

        public GroupAdapter(Context context, int layoutResourceId, ArrayList<Group> data, Activity act){
            super(context, layoutResourceId,data);

            activity = act;
        }

        private List<Group> getFilteredResults(CharSequence input){


            return null;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            Group item = getItem(position);
            GroupHolder holder= null;

            if (row == null){

                row = LayoutInflater.from(getContext()).inflate(R.layout.group_browser_item, parent, false);
                holder = new GroupHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.ivProfileLeft);
                holder.txtTitle = (TextView) row.findViewById(R.id.tvName);
                holder.imgIsTutor = (ImageView) row.findViewById(R.id.ivProfileRight);
                holder.linearLayout = (LinearLayout) row.findViewById(R.id.ll_group_item);

                row.setTag(holder);

            }
            else{
                holder = (GroupHolder)row.getTag();
            }

            //set clickable
            holder.linearLayout.setTag(holder.linearLayout.getId(), item.getObjectID());
            //holder.linearLayout.setTag(mGroupNameTag, item.getGroupName());

            holder.linearLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            onClickLayoutItem(String.valueOf(view.getTag(view.getId())));

                        }
                    });


            holder.txtTitle.setText(item.getGroupName());



            try{
                if(item.getIsTutorGroup()){
                    mLoadImageTask = new LoadImageTask(holder.imgIcon, item, item.getObjectID()+"_avatar", activity);
                    mLoadImageTask.execute(holder.imgIcon);

                    holder.imgIsTutor.setVisibility(View.VISIBLE);
                    holder.imgIsTutor.setBackgroundResource(R.drawable.ic_tutor);

                }
                else{
                    mLoadImageTask = new LoadImageTask(holder.imgIcon, item, item.getObjectID(), activity);
                    mLoadImageTask.execute(holder.imgIcon);

                    holder.imgIsTutor.setVisibility(View.INVISIBLE);
                }


            }
            catch(Exception e){
                Log.i("Feed_Main", e.getMessage());
            }




            return row;

        }


    }

    public class Group {
        private String objectId;
        private String name;
        private Boolean isTutorGroup;
        private int type;

        //PROPERTIES//////////////////
        public String getObjectID(){return this.objectId;}
        public String getGroupName(){return this.name;}
        public Boolean getIsTutorGroup(){return this.isTutorGroup;}
        public int getType(){return this.type;}
        //PROPERTIES//////////////////

        public Group(ParseObject Obj, final Activity activity, boolean isGroup) {
            this.objectId = Obj.getObjectId();
            if(isGroup){
                this.name = Obj.getString("Name");
                this.isTutorGroup = Obj.getBoolean("IsTutorGroup");
                this.type = 2;

            }
            else
            {
                this.type = 1;
                this.name = Obj.getString("username");
                this.isTutorGroup = false;
            }

            //first we cache the group image
            if(!IsBitmapCached(this.objectId, activity)){
                ParseFile Picture = Obj.getParseFile("Image");
                if(Picture != null){
                    Picture.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {

                            if (e == null) {
                                Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                        data.length);
                                storeImage(PictureBmp, objectId, activity);

                            } else {

                            }

                        }
                    });
                }

            }

            //if this group is a tutor group, we also need to cache the tutor's avatar image
            if(isTutorGroup){
                final String fileName = this.objectId + "_avatar";
                if(!IsBitmapCached(fileName, activity)){
                    ParseFile Picture = Obj.getParseFile("Avatar");
                    if(Picture != null){
                        Picture.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {

                                if (e == null) {
                                    Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                            data.length);
                                    storeImage(PictureBmp, fileName, activity);

                                } else {

                                }

                            }
                        });
                    }

                }
            }


        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return false;
        }

        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return null;
        }


    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, Group globalPost, String FileName, Activity act) {
            v = view;
            g = globalPost;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            Bitmap temp = g.loadImageFromStorage(fileName, activity);
            if (temp != null){
                return g.loadImageFromStorage(fileName, activity);
            }
            else
            {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }

}
