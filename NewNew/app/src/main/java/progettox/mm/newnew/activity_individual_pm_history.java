package progettox.mm.newnew;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 23/7/2015.
 */
public class activity_individual_pm_history extends AppCompatActivity {
    //private Map<String, PersonalMessage> mGroupArray = Collections.synchronizedMap(new LinkedHashMap<String, PersonalMessage>());
    private List<PersonalMessage> mGroupArray;
    public String mPlayerID;
    public String mPlayer_objectId;
    public String mUserID;
    public String mUser_objectId;
    private String mPM_objectId;
    public PMAdapter mGroupAdapter ;
    public App mAppState;
    private LoadImageTask mLoadImageTask;
    private TextView txtLoadingCaption;
    private ListView mListView;
    private ArrayList<String> mPMHistory_ObjectIDs;
    private ProgressDialog mDialogImageLoad;

    private void Initialize(){

        mAppState = ((App)getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        //mRightButton = (Button) findViewById(R.id.txt_action_member);
        mListView = (ListView) findViewById(R.id.lv_groups);
        txtLoadingCaption = (TextView)findViewById(R.id.lbl_loading_caption);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPM_objectId = extras.getString("PM_ObjectID");
            mUser_objectId = extras.getString("User_ObjectID");
            mUserID = extras.getString("User_Name");
        }

        mDialogImageLoad = new ProgressDialog(this);
        mDialogImageLoad.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialogImageLoad.setMessage("Loading your image.");
        mDialogImageLoad.setIndeterminate(true);
        mDialogImageLoad.setCanceledOnTouchOutside(false);

        Button pmReply = (Button) findViewById(R.id.btn_pm_reply);
        pmReply.setVisibility(View.VISIBLE);
        pmReply.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                replyPM();
            }
        });

        mPMHistory_ObjectIDs = new ArrayList<>();
        //setTitle
        setTitle(getResources().getText(R.string.lbl_pm_short) + ": " + mUserID);
    }

    private void replyPM(){
        Intent groupMembersActivity = new Intent(this, activity_new_pm.class);
        groupMembersActivity.putExtra("User_ObjectID", mUser_objectId);
        groupMembersActivity.putExtra("User_Name", mUserID);
        startActivity(groupMembersActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_pm);
        Initialize();
        //LoadAllPMs();

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        LoadAllPMs();
    }

    private void LoadAllPMs(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_History");
        ParseObject obj = ParseObject.createWithoutData("PM_Master", mPM_objectId);
        query.whereEqualTo("PM_objectId", obj);
        query.include("Sender_objectId");
        query.include("Recipient_objectId");
        query.include("PM_objectId");
        query.orderByDescending("createdAt");
        query.setLimit(20);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    FeedMasterDataIntoObjects(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void LoadPostImages(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_Images");
        query.whereContainedIn("PM_History_ObjectID", mPMHistory_ObjectIDs);
        query.include("PM_History_objectId");
        //query.orderByAscending("Index");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    //FeedMasterDataIntoObjects(scoreList);
                    updatePMObjects(scoreList);
                    //LoadPostLikes();

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updatePMObjects(List<ParseObject> jsonObjects){
        String temp;
        for (int i = 0; i < jsonObjects.size(); i++) {
            temp = jsonObjects.get(i).getString("PM_History_ObjectID");
            for (int j = 0; j < mGroupArray.size(); j++){
                if(temp.equals(mGroupArray.get(j).getObjectId())){
                    mGroupArray.get(j).insertImagesThumbnail(jsonObjects.get(i), this);
                }
            }
        }

        mGroupAdapter.notifyDataSetChanged();
    }



    public void FeedMasterDataIntoObjects(List<ParseObject> data) {
        //First create the main globalfeed objects
        mGroupArray = ConvertJSONtoArray(data);
        //mTotalFeedCount = mGlobalPost_List.size();

        //get last date
        //mLastPostDate = data.get(data.size() - 1).getCreatedAt();

        //get PM_History_ObjectIDs
        for (int i = data.size()-1; i >=0; i--) {
            try {
                mPMHistory_ObjectIDs.add(data.get(i).getObjectId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ArrayList<PersonalMessage> arrayOfUsers = new ArrayList<PersonalMessage>();
        // Create the adapter to convert the array to views
        mGroupAdapter = new PMAdapter(this, arrayOfUsers, this);
        mGroupAdapter.addAll(mGroupArray);

        mListView = (ListView) findViewById(R.id.lv_groups);
        mListView.setAdapter(mGroupAdapter);
        mListView.setSelection(mGroupArray.size()-1);
        txtLoadingCaption.setVisibility(View.GONE);
        LoadPostImages();

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if(mLastFirstVisibleItem<firstVisibleItem)
                {
                    //Log.i("SCROLLING DOWN","TRUE");
//                    if(mBottomFrame.getVisibility() == View.VISIBLE)
//                        mBottomFrame.setVisibility(View.GONE);

                }
                if(mLastFirstVisibleItem>firstVisibleItem)
                {
                    //Log.i("SCROLLING UP","TRUE");
//                    if(mBottomFrame.getVisibility() == View.GONE)
//                        mBottomFrame.setVisibility(View.VISIBLE);
                }
                mLastFirstVisibleItem=firstVisibleItem;

//                if(mTotalFeedCount > 0){
//                    int loadingTriggerCount = mTotalFeedCount - 7;
//                    if(firstVisibleItem >= loadingTriggerCount)
//                    {
//                        mTotalFeedCount+=5;
//                        loadGlobalFeedNext();
//                    }
//
//                }

            }
        });
    }



    public ArrayList<PersonalMessage> ConvertJSONtoArray(List<ParseObject> jsonObjects) {
        ArrayList<PersonalMessage> GameHistory = new ArrayList<PersonalMessage>();
        for (int i = jsonObjects.size()-1; i >=0; i--) {
            try {
                GameHistory.add(new PersonalMessage(jsonObjects.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



        return GameHistory;
    }

        static class ViewHolder {

        TextView postDate;
        ImageView imgIcon;
        TextView postMessage;
        ImageView[] PostImages = new ImageView[4];
        LinearLayout headerLayout;
        LinearLayout imagesLayout;
        LinearLayout masterLayout;
    }

    private void OpenUserFeedPage(int Position){
//        ((MainMenuActivity)getActivity()).OpenUserFeedPage(
//                mGlobalPost_List.get(Feed_objectId).getPlayerID(),
//                mGlobalPost_List.get(Feed_objectId).getPlayerObjectID());
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", mGroupArray.get(Position).getSender_ObjectID());
        intent.putExtra("Player_Name", mGroupArray.get(Position).getSender_Name());
        intent.putExtra("IsGroup", false);
        startActivity(intent);

    }

    public class PMAdapter extends ArrayAdapter<PersonalMessage> {
        Activity activity;
        public PMAdapter(Context context, ArrayList<PersonalMessage> users, Activity act) {
            super(context, 0, users);
            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final PersonalMessage user = getItem(position);
            ViewHolder holder = null;
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_individual_pm_history, parent, false);
                holder = new ViewHolder();
                holder.masterLayout = (LinearLayout) convertView.findViewById(R.id.layout_pm_master);
                holder.headerLayout = (LinearLayout) convertView.findViewById(R.id.post_header);
                holder.imagesLayout = (LinearLayout) convertView.findViewById(R.id.layout_pm_images);
                holder.imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);
                holder.postMessage = (TextView) convertView.findViewById(R.id.post_message);
                holder.postDate = (TextView) convertView.findViewById(R.id.post_date);
                holder.PostImages[0] = (ImageView) convertView.findViewById(R.id.post_pic1);
                holder.PostImages[1] = (ImageView) convertView.findViewById(R.id.post_pic2);
                holder.PostImages[2] = (ImageView) convertView.findViewById(R.id.post_pic3);
                holder.PostImages[3] = (ImageView) convertView.findViewById(R.id.post_pic4);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }


            holder.postDate.setText(user.getCreatedAt());

            //if next message comes from the same sender, we blank out the image and date/time
            if(position > 0){
                PersonalMessage userPrevious = getItem(position-1);
                if(user.getSender_ObjectID().equals(userPrevious.getSender_ObjectID())){
                    holder.headerLayout.setVisibility(View.GONE);

                }
                else{
                    holder.headerLayout.setVisibility(View.VISIBLE);

                }

            }

            if(user.getSender_ObjectID().equals(mPlayer_objectId))
                holder.masterLayout.setBackgroundColor(getResources().getColor(R.color.pm_recipient));
            else
                holder.masterLayout.setBackgroundColor(getResources().getColor(R.color.pm_sender));


            try{
//                holder.imgIcon.setImageBitmap(RoundedImageView.getCircleBitmap(
//                        user.loadImageFromStorage(user.getUsername(),
//                                getActivity())));

                mLoadImageTask = new LoadImageTask(holder.imgIcon, user.getSender_ObjectID(), activity);
                mLoadImageTask.execute(holder.imgIcon);
                holder.imgIcon.setTag(holder.imgIcon.getId(), position);
                holder.imgIcon.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                OpenUserFeedPage((int)view.getTag(view.getId()));

                            }
                        });
            }
            catch(Exception e){
                Log.i("Feed_Main", e.getMessage());
            }


            // Populate the Post's name and message
            holder.postMessage.setTag(holder.postMessage.getId(), user.getObjectId());
            holder.postMessage.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //TODO
                            //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                        }
                    });

            holder.postMessage.setText(user.getMessage());

            //TODO

            try{
                //only load the images if they are present
                if(user.getNoOfImages() > 0){
                    holder.imagesLayout.setVisibility(View.VISIBLE);
                    for (int i = 0; i<4; i++){
                        holder.PostImages[i].setVisibility(View.GONE);

                    }

                    for (int i = 0; i<user.getNoOfImages(); i++){
                        holder.PostImages[i].setVisibility(View.VISIBLE);
                        final String imageFilename = user.getObjectId() + "_" + user.getImages().get(i) + "_Thumb";
                        final int count = i;
                        //final String imageFilenameFull = user.getObjectId() + "_" + user.getImages().get(i);
                        try{

                            mLoadImageTask = new LoadImageTask(holder.PostImages[i], imageFilename, activity);
                            mLoadImageTask.execute(holder.PostImages[i]);


                        }
                        catch(Exception e){
                            Log.i("Feed_Main", e.getMessage());
                        }

                        final ImageView PIM =  holder.PostImages[i];
//                        if(PIM.getDrawable() == null){
//
//                        }
                        //PIM.setTag(PIM.getId(), user.objectId);
                        PIM.setOnClickListener(
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //TODO to pass in filename, then let the function load the image
                                        showFullThumbnail(user.getObjectId(), user.getImages().get(count));
                                        //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));

                                    }
                                });

                    }
                }
                else{
                    holder.imagesLayout.setVisibility(View.GONE);
                    for (int i = 0; i<4; i++){
                        holder.PostImages[i].setVisibility(View.GONE);

                    }

                }
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }


            // Return the completed view to render on screen
            return convertView;
        }

    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    private void DisplayFullImage(Bitmap bmp){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        //Bitmap IMG = GlobalPost.LoadPNG(imgFileName, this);
        ivPreview.setImageBitmap(bmp);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void DisplayFullImage(String imgFileName){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        Bitmap IMG = GlobalPost.LoadPNG(imgFileName, this);
        ivPreview.setImageBitmap(IMG);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void LoadFullImage(ParseObject Obj, final String Filename){

        final Activity act = this;
        //store image into cache
        if(!GlobalPost.CheckPNGCache(Filename, this)){
            ParseFile Picture = Obj.getParseFile("Image");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        GlobalPost.StorePNG(PictureBmp, Filename, act);
                        DisplayFullImage(PictureBmp);

                    } else {

                    }

                }
            });
        }
        else
        {
            DisplayFullImage(Filename);
        }

    }

    public void showFullThumbnail(final String FeedObjID, final String ImageObjID){

        final String Filename = FeedObjID + "_" + ImageObjID;

        if(GlobalPost.CheckPNGCache(Filename, this)){
            DisplayFullImage(Filename);
        }
        else{
            mDialogImageLoad.show();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_Images");
            query.whereEqualTo("objectId", ImageObjID);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                        if(scoreList.size()>0)
                            LoadFullImage(scoreList.get(0), Filename);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
