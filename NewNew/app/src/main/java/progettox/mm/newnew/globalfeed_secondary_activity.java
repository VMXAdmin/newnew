package progettox.mm.newnew;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 6/5/2015.
 */
public class globalfeed_secondary_activity extends AppCompatActivity {

    public String mPlayerID;
    public String mPlayer_objectId;
    private View myFragmentView;
    public GlobalPost mGlobalPost;
    //public Hashtable<String, GlobalPost> mGlobalPost_List;
    //public GlobalPostAdapter mAdapter;
    public CommentsAdapter mCommentsAdapter;
    //public GlobalPostAdapter mGlobalAdapter;
    private ProgressDialog mDialog;
    private ProgressDialog mDialog_New;
    public String mPostID;
    public ParseFile mProfilePic;
    private boolean mIsDirty;
    private LinearLayout mLayoutImages;
    private LoadImageTaskDrawable mLoadImageTaskDrawable;
    private TextView mLoadingCaption;
    private TextView mLoadingCommentsCaption;
    private LoadImageTask mLoadImageTask;

    public globalfeed_secondary_activity()
    {


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_globalfeed_secondary);

        App appState = ((App)getApplicationContext());
        mPostID = appState.getGlobalFeedID();
        mPlayerID = appState.getPlayerID();
        mPlayer_objectId = appState.getPlayer_objectId();

        ArrayList<CommentsHolder> arrayOfUsers = new ArrayList<CommentsHolder>();
        mCommentsAdapter = new CommentsAdapter(this, arrayOfUsers, this);

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Loading your Feed, Thanks for waiting.");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        mDialog_New = new ProgressDialog(this);
        mDialog_New.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog_New.setMessage("牛!");
        mDialog_New.setIndeterminate(true);
        mDialog_New.setCanceledOnTouchOutside(false);

        mIsDirty = false;

        mLayoutImages = (LinearLayout) findViewById(R.id.layout_images);
        mLoadingCaption = (TextView) findViewById(R.id.lbl_loading_images);
        mLoadingCommentsCaption = (TextView) findViewById(R.id.lbl_loading_comments);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //mPlayer_objectId = extras.getString("PlayerObjectID");

            //check for group
            Boolean isGroup = extras.getBoolean("IsGroup");
            if (isGroup != null) {
                if (isGroup) {
                    mGlobalPost = new GlobalPost(extras.getString("Feed_ObjectID"),
                            extras.getString("User_Name"),
                            extras.getString("User_ObjectID"),
                            extras.getString("Group_Name"),
                            true,
                            extras.getString("Group_ObjectID"),
                            extras.getInt("Type"),
                            extras.getString("Message"),
                            extras.getString("CreatedAtDisplay"),
                            (Date) extras.getSerializable("CreatedAt"));
                }
                else
                {
                    mGlobalPost = new GlobalPost(extras.getString("Feed_ObjectID"),
                            extras.getString("User_Name"),
                            extras.getString("User_ObjectID"),
                            "",
                            false,
                            "",
                            extras.getInt("Type"),
                            extras.getString("Message"),
                            extras.getString("CreatedAtDisplay"),
                            (Date) extras.getSerializable("CreatedAt"));
                }
            }
            else{
                mGlobalPost = new GlobalPost(extras.getString("Feed_ObjectID"),
                        extras.getString("User_Name"),
                        extras.getString("User_ObjectID"),
                        "",
                        false,
                        "",
                        extras.getInt("Type"),
                        extras.getString("Message"),
                        extras.getString("CreatedAtDisplay"),
                        (Date) extras.getSerializable("CreatedAt"));
            }
        }

        DisplayPrelimInfo();
        LoadPostImages();
    }

    private boolean checkDeviceCache(){

        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
        //SharedPreferences.Editor ed = sp.edit();
//        ed.putString("mHTChatItemList", "");
//        ed.apply();

        //check if we have cache of CHAT data
        String JSON = sp.getString("mGlobalPost_List", "");
        if(JSON == null || JSON.equals("")){
            //here we start to load and populate the user's chat data and history
            //preLoadFeedData();
            return false;
        }

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        LoadPostLikes();

    }



    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public void btnClick_SubmitNewComment(View view){
        hideSoftKeyboard();
        mDialog.show();
        EditText txtComment = (EditText) findViewById(R.id.txt_new_comment);
        //insert new word into History table
        final ParseObject GlobalFeed_Comments = new ParseObject("GlobalFeed_Comments");

        GlobalFeed_Comments.put("GlobalFeed_ObjectID", mPostID);
        GlobalFeed_Comments.put("GlobalFeed_objectId", ParseObject.createWithoutData("GlobalFeed_Master",mPostID));
        GlobalFeed_Comments.put("User_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        GlobalFeed_Comments.put("Comments", txtComment.getText().toString());
        GlobalFeed_Comments.put("No_of_Likes", 0);
        GlobalFeed_Comments.put("No_of_Comments", 0);
        //create new comments holder
        final CommentsHolder newHolder = new CommentsHolder(mPlayerID, mPlayer_objectId,
                txtComment.getText().toString(), this);
//        newHolder.setPlayerID(mPlayerID);
//        newHolder.setComment(txtComment.getText().toString());
//        newHolder.setProfilePic(mProfilePic);

        //mGlobalPost.addNewComment(newHolder);

        //mGlobalPost_List.get(mPostID).addNewComment(newHolder);

        GlobalFeed_Comments.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    newHolder.insertObjectID(GlobalFeed_Comments.getObjectId());
                    //mGlobalPost_List.get(mPostID).addNewComment(newHolder);
                    mGlobalPost.addNewComment(newHolder);
                    mCommentsAdapter.add(newHolder);
                    mDialog.dismiss();
                    mIsDirty = true;
                    updateCommentsDisplay();
                    addGroupNotifications(mPostID);
                    LoadPostLikes();
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

        //Clear the comment column after inserting a new entry
        txtComment.setText("");
    }

    private void addGroupNotifications(final String PostObjID){

        final ArrayList<String> groupMembers = new ArrayList<String>();

        //first we get all the members in the group
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        ParseObject obj = ParseObject.createWithoutData(
                "GlobalFeed_Master", PostObjID);
        query.whereEqualTo("GlobalFeed_objectId", obj);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    for(int i=0;i<scoreList.size();i++){
                        if(!scoreList.get(i).getParseObject("User_objectId").getObjectId().
                                equals(mPlayer_objectId)){
                            groupMembers.add(scoreList.get(i).getParseObject("User_objectId").getObjectId());
                        }
                    }

                    if(groupMembers.size() > 0)
                        addInvitesIntoNotifications(PostObjID, groupMembers);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void addInvitesIntoNotifications(final String FeedID,
                                             ArrayList<String> FriendList){

        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        String SubmitterObjectID = mGlobalPost.getPlayerObjectID();
        Boolean isGroup = false;
        if(mGlobalPost.getIsGroup())
            isGroup = true;

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", SubmitterObjectID));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 5);
        NewPlayer.put("IsAccepted", false); //By default is False


        NewPlayer.put("Feed_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Master", FeedID));
//            NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
//                    "GlobalFeed_Group_Master", GroupID));
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        if(isGroup) {
            NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Group_Master", mGlobalPost.getGroup_ObjectID()));
        }


        PlayerList.add(NewPlayer);

        //first we definitely need to nofify the TS


        Boolean isRepeat = false;
        for (int i = 0; i<FriendList.size(); i++){
            isRepeat = false;

            if(FriendList.get(i).equals(SubmitterObjectID)) continue;

            for (int j = 0; j < PlayerList.size();j++){
                if(PlayerList.get(j).getParseObject("User_objectId").getObjectId().equals(FriendList.get(i))){
                    isRepeat = true;
                    break;
                }

            }

            if(isRepeat) continue;

            //insert new word into History table
            NewPlayer = new ParseObject("Notifications");
            //NewPlayer.put("Group_ObjectID", GroupID);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", FriendList.get(i)));
            NewPlayer.put("IsSeen", false);
            NewPlayer.put("Type", 5);
            NewPlayer.put("IsAccepted", false); //By default is False


            NewPlayer.put("Feed_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Master", FeedID));
//            NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
//                    "GlobalFeed_Group_Master", GroupID));
            NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                    "_User", mPlayer_objectId));

            if(isGroup) {
                NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                        "GlobalFeed_Group_Master", mGlobalPost.getGroup_ObjectID()));
            }

            PlayerList.add(NewPlayer);
        }

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new notifications", "SUCCESS!");

            }
        });


    }

    private void updateCommentsDisplay(){
        TextView comments = (TextView) findViewById(R.id.no_of_comments);
        comments.setText(String.valueOf(mGlobalPost.getNoOfComments()));

    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void LoadPostImages(){
        mLoadingCaption.setVisibility(View.VISIBLE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        //ArrayList<ParseObject> obj = new ArrayList<ParseObject>();
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master", "RRoYFVnc7W"));
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master","pFN5oLckJB"));

        //String[] names = {"RRoYFVnc7W", "pFN5oLckJB"};
        //query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(names));
        query.whereEqualTo("GlobalFeed_ObjectID", mPostID);
        query.include("GlobalFeed_Master");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size()>0)
                        updateFeedImages(scoreList);
                    else
                    {
                        mLoadingCaption.setVisibility(View.GONE);
                        //LoadPostComments();
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void LoadPostLikes(){

        mLoadingCommentsCaption.setVisibility(View.VISIBLE);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Likes");
        //ArrayList<ParseObject> obj = new ArrayList<ParseObject>();
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master", "RRoYFVnc7W"));
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master","pFN5oLckJB"));

        //String[] names = {"RRoYFVnc7W", "pFN5oLckJB"};
        //query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(names));
        //query.include("GlobalFeed_Master");
        query.whereEqualTo("GlobalFeed_ObjectID", mPostID);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");

                    updateGlobalNewsFeedObject(scoreList, 2);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void LoadPostComments(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        //ArrayList<ParseObject> obj = new ArrayList<ParseObject>();
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master", "RRoYFVnc7W"));
        //obj.add(ParseObject.createWithoutData("GlobalFeed_Master","pFN5oLckJB"));

        //String[] names = {"RRoYFVnc7W", "pFN5oLckJB"};
        //query.whereContainedIn("GlobalFeed_ObjectID", Arrays.asList(names));
        //query.include("GlobalFeed_Master");
        query.whereEqualTo("GlobalFeed_ObjectID", mPostID);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    updateGlobalNewsFeedObject(scoreList, 3);
                    mLoadingCommentsCaption.setVisibility(View.GONE);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void insertAndLoadImagesIcon(final ParseObject Obj){

        mLoadingCaption.setVisibility(View.VISIBLE);
        if (mGlobalPost.noOfImages == 0)
        {
            mGlobalPost.Images = new ArrayList<String>();
        }
        mGlobalPost.Images.add( Obj.getObjectId());
        mGlobalPost.setImage_NoofComments(Obj.getObjectId(), Obj.getInt("No_of_Comments"));
        mGlobalPost.setImage_NoofLikes(Obj.getObjectId(), Obj.getInt("No_of_Likes"));
        final Activity activity = this;
        //
        //load post images if it is not in cache
        final String Filename = mGlobalPost.objectId + "_" + Obj.getObjectId() + "_Icon";
        if(!GlobalPost.CheckPNGCache(Filename ,this)){
            ParseFile Picture = Obj.getParseFile("Image_Icon");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {

                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        GlobalPost.StorePNG(PictureBmp, Filename, activity);
                        RefreshFeedImages(PictureBmp, Obj.getObjectId());
                    } else {

                    }

                }
            });
        }
        else
        {
            Bitmap PictureBmp = GlobalPost.LoadPNG(Filename, this);
            RefreshFeedImages(PictureBmp, Obj.getObjectId());
        }
        //


        mGlobalPost.noOfImages++;
    }

    private void insertAndLoadImages(final ParseObject Obj){

        mLoadingCaption.setVisibility(View.VISIBLE);
        if (mGlobalPost.noOfImages == 0)
        {
            mGlobalPost.Images = new ArrayList<String>();
        }
        mGlobalPost.Images.add( Obj.getObjectId());
        mGlobalPost.setImage_NoofComments(Obj.getObjectId(), Obj.getInt("No_of_Comments"));
        mGlobalPost.setImage_NoofLikes(Obj.getObjectId(), Obj.getInt("No_of_Likes"));
        final Activity activity = this;
        //
        //load post images if it is not in cache
        final String Filename = mGlobalPost.objectId + "_" + Obj.getObjectId();
        if(!GlobalPost.CheckPNGCache(Filename ,this)){
            ParseFile Picture = Obj.getParseFile("Image");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {

                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        GlobalPost.StorePNG(PictureBmp, Filename, activity);
                        RefreshFeedImages(PictureBmp, Obj.getObjectId());
                    } else {

                    }

                }
            });
        }
        else
        {
            Bitmap PictureBmp = GlobalPost.LoadPNG(Filename, this);
            RefreshFeedImages(PictureBmp, Obj.getObjectId());
        }
        //


        mGlobalPost.noOfImages++;
    }

    private void updateFeedImages(List<ParseObject> jsonObjects){
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                insertAndLoadImagesIcon(jsonObjects.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateGlobalNewsFeedObject(List<ParseObject> jsonObjects, int type){

        try{
            if(type == 2){
                if(mGlobalPost.getLikes() != null)
                    mGlobalPost.clearLikes();
            }
            else if(type == 3){
                if(mGlobalPost.getComments() != null)
                    mGlobalPost.clearComments();
            }



        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }


        for (int i = 0; i < jsonObjects.size(); i++) {
            try {

                if (type == 1){

                    insertAndLoadImagesIcon(jsonObjects.get(i));

                }
                else if(type == 2){


                    mGlobalPost.insertLikes(jsonObjects.get(i), this, mPlayer_objectId);

                }
                else if(type == 3){


                    mGlobalPost.insertComments(jsonObjects.get(i), this);

                }





            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(type == 1) LoadPostLikes();
        if(type == 2) LoadPostComments();
        if(type == 3) UpdateFeedDetails();

    }


    public void showFullThumbnail(Bitmap IMG){

        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        ivPreview.setImageBitmap(IMG);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();

    }

    private void likeComment(final String Comment_objectId){

        mDialog_New.show();
        ParseObject NewPlayer = new ParseObject("GF_Cmt_Likes");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Master", mPostID));
        NewPlayer.put("Comment_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Comments", Comment_objectId));
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                mDialog_New.dismiss();
                updateCommentsMaster_Likes(Comment_objectId);
                //UpdateParseObjectWithLikes(FeedID);
            }
        });

    }

    private void updateCommentsMaster_Likes(String Comment_ObjID){
        //update COMMENTS MASTER table
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        query.whereEqualTo("objectId", Comment_ObjID);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size()>0){
                        int NoofLikes = scoreList.get(0).getInt("No_of_Likes");
                        NoofLikes++;
                        scoreList.get(0).put("No_of_Likes", NoofLikes);
                        scoreList.get(0).saveInBackground(new SaveCallback() {
                            public void done(ParseException e) {
                               LoadPostComments();
                            }
                        });
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void likeFeed(final String FeedID){

        //updat display first to let user know it is responsive
        mGlobalPost.noOfLikes++;
        mGlobalPost.setLiked();
        UpdateFeedDetails();

        ParseObject NewPlayer = new ParseObject("GlobalFeed_Likes");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Master", FeedID));
        NewPlayer.put("GlobalFeed_ObjectID", FeedID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
                //UpdateParseObjectWithLikes(FeedID);
            }
        });
    }

    private void DisplayPrelimInfo(){
        TextView userName = (TextView) findViewById(R.id.user_name);
        ImageView imgIcon = (ImageView) findViewById(R.id.user_profile_pic);
        userName.setText(mGlobalPost.getUsername());
        imgIcon.setImageBitmap(mGlobalPost.loadImageFromStorage(mGlobalPost.getPlayerObjectID(), this));

        // Populate the Post's name and message
        TextView postName = (TextView) findViewById(R.id.post_name);
        TextView postMessage = (TextView) findViewById(R.id.post_message);
        postName.setText(mGlobalPost.getPostName());
        postMessage.setText(mGlobalPost.getMessage());

        TextView postDate = (TextView) findViewById(R.id.post_date);
        postDate.setText(mGlobalPost.getDate());


    }

    private void RefreshFeedImages(Bitmap bmp, String Image_object ){
        ImageView myImage = new ImageView(this);
        myImage.setAdjustViewBounds(true);
//        LinearLayout.LayoutParams lp =
//                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(20, 0, 20, 25);
        myImage.setLayoutParams(lp);
        myImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

        myImage.setImageBitmap(bmp);
        myImage.setTag(myImage.getId(), Image_object);
        //add ONCLICK listener for the inserted image
        myImage.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openImageSecondary(String.valueOf(view.getTag(view.getId())));

                    }
                });


        mLayoutImages.addView(myImage);

        mLoadingCaption.setVisibility(View.GONE);



    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void UpdateFeedDetails(){

//        TextView userName = (TextView) findViewById(R.id.user_name);
//        ImageView imgIcon = (ImageView) findViewById(R.id.user_profile_pic);
//        userName.setText(mGlobalPost.getUsername());
//        imgIcon.setImageBitmap(mGlobalPost.loadImageFromStorage(mGlobalPost.getPlayerObjectID(), this));
//
//        // Populate the Post's name and message
//        TextView postName = (TextView) findViewById(R.id.post_name);
//        TextView postMessage = (TextView) findViewById(R.id.post_message);
//        postName.setText(mGlobalPost.getPostName());
//        postMessage.setText(mGlobalPost.getMessage());

        TextView likes = (TextView) findViewById(R.id.no_of_likes);
        TextView comments = (TextView) findViewById(R.id.no_of_comments);

        likes.setText(String.valueOf(mGlobalPost.getNoOfLikes()));

        comments.setText(String.valueOf(mGlobalPost.getNoOfComments()));

        ImageButton btnLikes = (ImageButton) findViewById(R.id.btn_likes);
        btnLikes.setTag(btnLikes.getId(), mGlobalPost.getObjectId());
        if(!mGlobalPost.getIsLiked()){
            btnLikes.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_action_new));
            btnLikes.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            likeFeed(String.valueOf(view.getTag(view.getId())));

                        }
                    });
        }
        else
        {
            btnLikes.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_action_newed));
            btnLikes.setClickable(false);

        }

        //Load Comments into ListView
        ListView listView = (ListView) findViewById(R.id.lvComments);
        listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });


        mCommentsAdapter.clear();
        if(mGlobalPost.getNoOfComments() > 0){
//            ArrayList<CommentsHolder> arrayOfUsers = new ArrayList<CommentsHolder>();
//            mCommentsAdapter = new CommentsAdapter(this, arrayOfUsers);
            mCommentsAdapter.addAll(mGlobalPost.getComments());
            //listView.setAdapter(mCommentsAdapter);
        }

        listView.setAdapter(mCommentsAdapter);
        setListViewHeightBasedOnChildren(listView);
 //       Bitmap BPtemp;
        //only load the images if they are present
//        if(mGlobalPost.getNoOfImages() > 0){
//            mLayoutImages.removeAllViews();
//            for (int i = 0; i<mGlobalPost.getNoOfImages(); i++){
//
//                try{
////
//                    ImageView myImage = new ImageView(this);
//                    myImage.setAdjustViewBounds(true);
//                    LinearLayout.LayoutParams lp =
//                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//                    lp.setMargins(20, 0, 20, 25);
//                    myImage.setLayoutParams(lp);
//
////                    myImage.setLayoutParams(new LinearLayout.LayoutParams(
////                            LinearLayout.LayoutParams.MATCH_PARENT,
////                            LinearLayout.LayoutParams.MATCH_PARENT));
//
//                    String imageFilename = mGlobalPost.getObjectId() + "_" + mGlobalPost.getImages().get(i);
//
//
//                    BPtemp = BitmapManager.decodeSampledBitmap(imageFilename,
//                            mScreenWidth, mScreenHeight, this);
//                    myImage.setImageBitmap(BPtemp);
//                    mLayoutImages.addView(myImage);
////                    mLoadImageTaskDrawable = new LoadImageTaskDrawable(myImage, mLayoutImages,imageFilename, this);
////                    mLoadImageTaskDrawable.execute(myImage);
////                    mLayoutImages.addView(myImage);
//                }
//                catch(Exception e){
//                    Log.i("Feed_Details", e.getMessage());
//                }
//
//            }
//        }

        mDialog.dismiss();
    }

    public class LoadImageTaskDrawable extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private GlobalPost g;
        private int fileName;
        private Activity activity;

        LoadImageTaskDrawable(ImageView view, int imageID, Activity act) {
            v = view;
            fileName = imageID;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            return BitmapFactory.decodeResource(getResources(), fileName);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }



    public ArrayList<GlobalPost> ConvertJSONtoArray(List<ParseObject> jsonObjects){
        ArrayList<GlobalPost> GameHistory = new ArrayList<GlobalPost>();

        for (int i = 0; i < jsonObjects.size(); i++) {
            //if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)) {
            GameHistory.add(new GlobalPost(jsonObjects.get(i), this));

            //}
        }
        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    static class ViewHolder {

        TextView likes;
        TextView comments;
        ImageButton btnComments;
        ImageButton btnLikes;

        TextView userName;
        TextView commentDate;
        ImageView imgIcon;
        TextView comment;

    }

    public class CommentsAdapter extends ArrayAdapter<CommentsHolder> {

        private Activity activity;

        public CommentsAdapter(Context context, ArrayList<CommentsHolder> users, Activity act) {
            super(context, 0, users);
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            CommentsHolder user = getItem(position);
            ViewHolder holder = null;
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_globalfeed_comments, parent, false);
                holder = new ViewHolder();
                holder.btnComments = (ImageButton) convertView.findViewById(R.id.btn_comments);
                holder.btnLikes = (ImageButton) convertView.findViewById(R.id.btn_likes);
                holder.userName = (TextView) convertView.findViewById(R.id.txt_PlayerId);
                holder.imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);
                holder.likes = (TextView) convertView.findViewById(R.id.no_of_likes);
                holder.comments = (TextView) convertView.findViewById(R.id.no_of_comments);
                holder.comment = (TextView) convertView.findViewById(R.id.txt_Comment);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.userName.setText(user.getPlayerID());
            holder.comment.setText(user.getComment());

            try{
                mLoadImageTask = new LoadImageTask(holder.imgIcon, user.getPlayerObjectID(), activity);
                mLoadImageTask.execute(holder.imgIcon);
            }
            catch(Exception e){
                Log.i("Feed_Main", e.getMessage());
            }


            //TODO to include date display

            holder.likes.setText(String.valueOf(user.getNoOfLikes()));
            holder.comments.setText(String.valueOf(user.getNoOfComments()));

            holder.btnLikes.setTag(holder.btnLikes.getId(), user.getObjectID());
            holder.btnLikes.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            likeComment(String.valueOf(view.getTag(view.getId())));

                        }
                    });

            holder.btnComments.setTag(holder.btnComments.getId(), position);
            holder.btnComments.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(getActivity(),
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                            openCommentsSecondary(Integer.parseInt(String.valueOf(view.getTag(view.getId()))));

                        }
                    });

            // Return the completed view to render on screen
            return convertView;
        }

    }

    private void openImageSecondary(String Image_objectID){
        Intent intent = new Intent(this, activity_image_secondary.class);
        intent.putExtra("Post_ObjectID", mGlobalPost.getObjectId());
        intent.putExtra("Image_ObjectID", Image_objectID);
        intent.putExtra("User_ObjectID", mGlobalPost.getPlayerObjectID());
        intent.putExtra("Comment_Comments", mGlobalPost.getImage_NoOfComments(Image_objectID));
        intent.putExtra("Comment_Likes", mGlobalPost.getImage_NoOfLikes(Image_objectID));
        startActivity(intent);
    }

    private void openCommentsSecondary(int pos){
        Intent intent = new Intent(this, activity_comment_secondary.class);
        intent.putExtra("Post_ObjectID", mGlobalPost.getObjectId());
        intent.putExtra("Comment_ObjectID", mGlobalPost.getComments().get(pos).getObjectID());
        intent.putExtra("User_ObjectID", mGlobalPost.getComments().get(pos).getPlayerObjectID());
        intent.putExtra("User_Name", mGlobalPost.getComments().get(pos).getPlayerID());
        intent.putExtra("Comment_Date", mGlobalPost.getComments().get(pos).getDate());
        intent.putExtra("Comment_Body",mGlobalPost.getComments().get(pos).getComment());
        intent.putExtra("Comment_Comments", mGlobalPost.getComments().get(pos).getNoOfComments());
        intent.putExtra("Comment_Likes", mGlobalPost.getComments().get(pos).getNoOfLikes());
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first

    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if(id == R.id.home){
//            finish();
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
