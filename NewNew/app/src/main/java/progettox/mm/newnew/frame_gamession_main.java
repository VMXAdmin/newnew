package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.service.notification.NotificationListenerService;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;


public class frame_gamession_main extends Fragment {

    public List<PlayerFriend> PlayerFriendsArray;
    public String PlayerID;
    public String Player_objectId;
    private List<PlayerHistory> OtherHistoryArray;
    private List<PlayerHistory> PlayerHistoryArray;
    private View myFragmentView;
    private static final String ARG_SECTION_NUMBER = "section_number";
    public String GameSessionID_Notify;
    public AppInfoAdapter adapter;
    private ProgressDialog mDialog;

    public static frame_gamession_main newInstance(String param1, String param2, int sectionNumber) {
        frame_gamession_main fragment = new frame_gamession_main();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public frame_gamession_main() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            PlayerID = getArguments().getString("PlayerID");
            Player_objectId = getArguments().getString("Player_objectId");

        }

        mDialog = new ProgressDialog(getActivity());
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Retrieving Game Sessions, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        App appState = ((App)getActivity().getApplicationContext());
        GameSessionID_Notify = appState.getGameSessionID_Notify();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //myFragmentView = inflater.inflate(R.layout.fragment_gamesession_main, container, false);
        myFragmentView = inflater.inflate(R.layout.fragment_gamesession_refined, container, false);
        GetPlayerHistoryData();
        //mDialog.show();
        return myFragmentView;
    }



    private void displayJSONResults(List<ParseObject> data)
    {
        try{
            PlayerHistoryArray = fromJsonPlayer(data);
            OtherHistoryArray = fromJsonOther(data);

            //Log.i("Finally", PlayerHistoryArray.get(0).PlayerID);

            // Construct the data source for PlaywrGames (Player's Turn)
            ArrayList<PlayerHistory> arrayPlayerGames = new ArrayList<PlayerHistory>();
            // Create the adapter to convert the array to views
            UsersAdapter adapter = new UsersAdapter(getActivity(), arrayPlayerGames);
            adapter.addAll(PlayerHistoryArray);
            // Attach the adapter to a ListView
            //ListView listView = (ListView) findViewById(R.id.lvItems);
            //ListView listView = (ListView) getActivity().findViewById(R.id.lvItems);
            ListView listView = (ListView) myFragmentView.findViewById(R.id.lvPlayerTurn);
            listView.setAdapter(adapter);


            // Construct the data source for OtherGames (Other Player's Turn)
            ArrayList<PlayerHistory> arrayOtherGames = new ArrayList<PlayerHistory>();
            // Create the adapter to convert the array to views
            UsersAdapter adapterOther = new UsersAdapter(getActivity(), arrayOtherGames);
            adapterOther.addAll(OtherHistoryArray);
            // Attach the adapter to a ListView
            //ListView listView = (ListView) findViewById(R.id.lvItems);
            //ListView listView = (ListView) getActivity().findViewById(R.id.lvItems);
            ListView listViewOther = (ListView) myFragmentView.findViewById(R.id.lvOtherPlayerTurn);
            listViewOther.setAdapter(adapterOther);









            mDialog.dismiss();
            ///////////
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    PlayerHistory listItem = PlayerHistoryArray.get(position);
//                    Log.i("Item selected is ", listItem.GameName);
//                }
//            });
            ///////////


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    public void OpenGameSessionDetails(String SessionID, String FirstChar, int Len, String GameName){
        ((MainMenuActivity)getActivity()).GotoGameSessionDetails(SessionID, FirstChar, Len, GameName);
    }

    private void GetPlayerHistoryData(){

        Log.d("PARSE", "Fetching of User in progress");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_PlayerList");
        query.whereEqualTo("PlayerID", PlayerID);
        query.include("GameSession_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    displayJSONResults(scoreList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public void registerPushNotifications(PlayerHistory Object){
        //PushService.subscribe(getActivity(), "user_" + Object.GameSession_ObjectID, MainActivity.class);
        //PushService.setDefaultPushCallback(getActivity(), MainActivity.class);

        //ParsePush pp = new ParsePush();
        //ParsePush.subscribeInBackground(Object.GameSession_ObjectID);



        //Log.i("PC is", parseChan);
        String parseChan = "user_" + Object.GameSession_ObjectID;
        ParsePush.subscribeInBackground(parseChan, new SaveCallback() {


            @Override
            public void done(ParseException e) {

                if (e == null) {
                    JSONObject data = null;
                    try {
                        data = new JSONObject("{\"alert\": \"Long Notification message!\", \"title\": \"Notification Title\"}");
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }

                    Log.i("Subcribe success", "");
//                    ParsePush push = new ParsePush();
//                    push.setChannel("Giants");
//                    push.setData(data);
//                    push.sendInBackground();
                }
                else {
                    //e.printStackTrace();
                    Log.i("Subcribe error", e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerHistory> fromJsonPlayer(List<ParseObject> jsonObjects) {
        ArrayList<PlayerHistory> GameHistory = new ArrayList<PlayerHistory>();
        int count = 0;
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)){
                    GameHistory.add(new PlayerHistory(jsonObjects.get(i)));
                    registerPushNotifications(GameHistory.get(count));
                    count++;
                }

                //registerPushNotifications("user_" + GameHistory.get(i).GameSession_ObjectID);
                //((MainMenuActivity)getActivity()).SubcribeParseChannels("user_" + GameHistory.get(i).GameSession_ObjectID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    public ArrayList<PlayerHistory> fromJsonOther(List<ParseObject> jsonObjects) {
        ArrayList<PlayerHistory> GameHistory = new ArrayList<PlayerHistory>();
        int count = 0;
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                if (!jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)){
                    GameHistory.add(new PlayerHistory(jsonObjects.get(i)));
                    registerPushNotifications(GameHistory.get(count));
                    count++;
                }

                //registerPushNotifications("user_" + GameHistory.get(i).GameSession_ObjectID);
                //((MainMenuActivity)getActivity()).SubcribeParseChannels("user_" + GameHistory.get(i).GameSession_ObjectID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

        //update GSID_Notify to clear the alert message if user has opened it
        App appState = ((App)getActivity().getApplicationContext());
        GameSessionID_Notify = appState.getGameSessionID_Notify();
        appState.setPageNumber(5);

        GetPlayerHistoryData();
        //mDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        //GetPlayerHistoryData();
        //mDialog.show();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainMenuActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    public class PlayerHistory {
        public String objectId;
        public String GameSession_ObjectID;
        public String Player_ObjectID;
        public String PlayerID;
        public String Word;
        //public int Stars;
        public Date CreatedAt;
        public String GameName;
        public String CurrentPlayerID;
        public int WordCount;
        public int TotalPlayers;
        public String LastCharacter;


        public PlayerHistory(ParseObject Obj) {
            this.objectId = Obj.getObjectId();
            this.GameSession_ObjectID = Obj.getString("GameSession_ObjectID");
            this.Player_ObjectID = Obj.getString("Player_ObjectID");
            this.PlayerID = Obj.getString("PlayerID");
            this.Word = Obj.getString("Word");
            //this.Stars = Obj.getInt("Stars");
            this.CreatedAt = Obj.getCreatedAt();
            this.GameName = Obj.getParseObject("GameSession_objectId").getString("Name");
            this.WordCount = Obj.getParseObject("GameSession_objectId").getInt("WordCount");
            this.TotalPlayers = Obj.getParseObject("GameSession_objectId").getInt("TotalPlayers");
            this.CurrentPlayerID = Obj.getParseObject("GameSession_objectId").getString("CurrentPlayerID");
            this.LastCharacter = Obj.getParseObject("GameSession_objectId").getString("LastChar");

        }

//        public ArrayList<PlayerHistory> fromJson(List<ParseObject> jsonObjects) {
//            ArrayList<PlayerHistory> users = new ArrayList<PlayerHistory>();
//            for (int i = 0; i < jsonObjects.size(); i++) {
////                try {
////                    //users.add(new PlayerHistory(jsonObjects.getJSONObject(i)));
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
//            }
//            return users;
//        }
    }

    public ArrayList<PlayerFriend> ConvertJsonToArray(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), i+1, getActivity()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    private void AttachPlayerResults(List<ParseObject> data)
    {

        try{
            PlayerFriendsArray = ConvertJsonToArray(data);

            Log.i("Finally", PlayerFriendsArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
            // Create the adapter to convert the array to views
            adapter = new AppInfoAdapter(getActivity(), R.layout.friends_invite_item, arrayOfUsers, getActivity());
            adapter.addAll(PlayerFriendsArray);
            // Attach the adapter to a ListView

            //
            //LayoutInflater inflater = getActivity().getLayoutInflater();
            //View view = inflater.inflate(friends_game_list, parent, false);
            //ListView listView = (ListView) getActivity().findViewById(R.id.listFriends);
            //listView.setAdapter(adapter);

            new AlertDialog.Builder(getActivity()).setTitle("Participants for this Game")
                    .setAdapter(adapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item ) {
                            Toast.makeText(getActivity(), "Item Selected: " + item, Toast.LENGTH_SHORT).show();
                        }
                    }).show();


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    public void getFriendList(String GSID){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GameSession_PlayerList");
        query.whereEqualTo("GameSession_ObjectID", GSID);
        query.include("Player_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    AttachPlayerResults(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public class UsersAdapter extends ArrayAdapter<PlayerHistory> {
        public UsersAdapter(Context context, ArrayList<PlayerHistory> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            PlayerHistory user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_user, parent, false);
            }
            // Lookup view for data population
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);

            // Populate the data into the template view using the data object
            tvName.setText(user.GameName);
            //tvName.setHint(user.GameSession_ObjectID);
            String SubHeader = "玩者: " + user.CurrentPlayerID + " 字数: " + user.WordCount + " 头字: " + user.LastCharacter;
            tvHome.setText(SubHeader);
            //tvName.setContentDescription(user.GameSession_ObjectID);

            //set Alert icon to be visible for the notified Game Session ID
            ImageButton NotificationBtn = (ImageButton) convertView.findViewById(R.id.notify_action);
            if (!user.GameSession_ObjectID.equals(GameSessionID_Notify)){
                NotificationBtn.setVisibility(View.INVISIBLE);
            }
//            else
//            {
//                App appState = ((App)getActivity().getApplicationContext());
//                appState.setGameSessionID_Notify("");
//            }


            //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
            View PrimaryColumn = convertView.findViewById(R.id.primary_target);
            PrimaryColumn.setTag(PrimaryColumn.getId(), user.GameSession_ObjectID);

            final String FirstChar = user.LastCharacter;
            final int WordLen = user.WordCount;
            final String GameName = user.GameName;
            //Then we set the onClick function here to pull the GameSession_objectId upon each click
            PrimaryColumn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                            Toast.makeText(getActivity(),
//                                    outputText,
//                                    Toast.LENGTH_SHORT).show();
                            OpenGameSessionDetails(String.valueOf(view.getTag(view.getId())), FirstChar, WordLen, GameName);

                        }
                    });

            final String GameSessionID = user.GameSession_ObjectID;
            View ShareButton = convertView.findViewById(R.id.secondary_action);
            ShareButton.setTag(ShareButton.getId(), user.GameSession_ObjectID);
            ShareButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getFriendList(GameSessionID);
                        }
                    });

            // Return the completed view to render on screen
            return convertView;
        }

    }

    public static class AppInfoHolder
    {
        //ImageView imgIcon;
        ImageView imgIcon;
        TextView txtTitle;
        CheckBox chkSelect;

    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data,
                              Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.menu_source_icon);
                holder.txtTitle = (TextView) row.findViewById(R.id.menu_source_name);
                holder.chkSelect = (CheckBox) row.findViewById(R.id.menu_source_check_box);

                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.PlayerID);
            holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.PlayerObjectID, activity));
//            holder.imgIcon.setParseFile(appinfo.profilePic);
//
//            //TODO to improve this code, will not be good if hundreds of friends appear
//            holder.imgIcon.loadInBackground(new GetDataCallback() {
//                public void done(byte[] data, ParseException e) {
//                    // The image is loaded and displayed!
//                    //int oldHeight = imageView.getHeight();
//                    //int oldWidth = imageView.getWidth();
//                    Log.v("LOG!!!!!!", "imageView height = ");      // DISPLAYS 90 px
//                    Log.v("LOG!!!!!!", "imageView width = " );        // DISPLAYS 90 px
//                }
//            });
            //holder.imgIcon.setImageDrawable(appinfo.icon);
            // holder.chkSelect.setChecked(true);
            holder.chkSelect.setTag(position);
            holder.chkSelect.setChecked(mCheckStates.get(position, false));
            holder.chkSelect.setOnCheckedChangeListener(this);
            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }

    public class PlayerFriend {
        public String objectId;
        public String PlayerID;
        private String PlayerObjectID;
        //public String FriendID;
        //public String Friend_objectId;

        public Drawable icon;
        public String applicationName;
        public ParseFile profilePic;

        public PlayerFriend(ParseObject Obj, int c, final Activity activity) {
            this.objectId = Obj.getObjectId();

            this.PlayerID = Obj.getString("PlayerID");
            this.PlayerObjectID = Obj.getParseObject("Player_objectId").getObjectId();
            //this.FriendID = Obj.getString("FriendID");
            this.profilePic = Obj.getParseObject("Player_objectId").getParseFile("Avatar_Pic");
            //this.Friend_objectId = Obj.getParseObject("Friend_objectId").getObjectId();
            //profilePic
            //public static Drawable createFromResourceStream (Resources res, TypedValue value, InputStream is, String srcName, BitmapFactory.Options opts)


            if(!IsBitmapCached(this.PlayerObjectID, activity)){
                ParseFile Picture = Obj.getParseObject("Player_objectId").getParseFile("Avatar_Pic");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, PlayerObjectID, activity);

                        } else {

                        }

                    }
                });
            }

        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return false;
        }

        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return null;
        }


    }

}
