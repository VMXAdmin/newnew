package progettox.mm.newnew;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;


public class GameActivity extends FragmentActivity
            implements game_session_main.OnFragmentInteractionListener{

    public String PlayerID;
    public String Player_objectId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_container_game);

        //Get the bundle
        Bundle bundle = getIntent().getExtras();

        //Extract the data…
        PlayerID = bundle.getString("PlayerID");
        Player_objectId = bundle.getString("Player_objectId");
        //Log.i("ObjettId from main act", PlayerID);

        // Check whether the activity is using the layout version with
        // the fragment_container FrameLayout. If so, we must add the first fragment
        if (findViewById(R.id.fragment_game_main_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create an instance of ExampleFragment
            game_session_main GameFragment = new game_session_main();

            Bundle bundleFrag = new Bundle();
            bundleFrag.putString("PlayerID", PlayerID);
            bundleFrag.putString("Player_objectId", Player_objectId);
            // In case this activity was started with special instructions from an Intent,
            // pass the Intent's extras to the fragment as arguments
            GameFragment.setArguments(bundleFrag);

            // Add the fragment to the 'fragment_container' FrameLayout
//            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
//            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.add(R.id.fragment_game_main_container, GameFragment);

            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_game_main_container, GameFragment).commit();

        }
    }

    public void onFragmentInteraction(String id){
        Log.i("Item ID ", id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_fragment__log_in, container, false);
//            return rootView;
//        }
//    }
}
