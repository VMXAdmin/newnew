package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Admin on 16/7/2015.
 */
public class activity_group_invite_friends extends AppCompatActivity {

    public App mAppState;
    public String mPlayerID;
    public String mPlayer_objectId;
    private List<PlayerFriend> mPlayerFriendsArray;
    public String[] FriendList;
    public String[] FriendObjIDList;
    public AppInfoAdapter adapter ;

    private String mGroup_ObjectID;
    private String mGroup_Name;
    private int mGroup_Type;

    //Views
    private TextView mLoadingCaption;

    private void Initialize(){

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            mGroup_ObjectID = extras.getString("Group_ObjectID");
            mGroup_Name = extras.getString("Group_Name");
            mGroup_Type = extras.getInt("Group_Type");
        }

        mAppState = ((App)getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        mLoadingCaption = (TextView) findViewById(R.id.lbl_loading_caption);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_invite_friends);
        Initialize();
        //Start to populate the Friends List
        getFriendsList();

    }

    private void displayJSONResults(List<ParseObject> data)
    {
        //mLoadingCaption.setVisibility(View.GONE);
        try{
            mPlayerFriendsArray = fromJson(data);
            getGroupMemberList();

        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void filterFriendListNotMember(List<ParseObject> friendList){

        String Member_objectId = "";

        try{
            for(int i = 0; i < friendList.size(); i++){
                Member_objectId = friendList.get(i).getParseObject("Member_objectId").getObjectId();
                for(int j = 0; j<mPlayerFriendsArray.size();j++) {
                    if(Member_objectId.equals(mPlayerFriendsArray.get(j).getFriendObjectID())){

                        if(friendList.get(i).getInt("Member_Status") == 1)
                            mPlayerFriendsArray.get(j).setIsGroupInviteSent(2);
                        else if (friendList.get(i).getInt("Member_Status") == 0)
                            mPlayerFriendsArray.get(j).setIsGroupInviteSent(1);
                    }
                }

            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }



        updateListViewAdapter();

    }

    private void updateListViewAdapter(){
        // Construct the data source
        ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
        // Create the adapter to convert the array to views
        adapter = new AppInfoAdapter(this, R.layout.friends_invite_item, arrayOfUsers, this);
        adapter.addAll(mPlayerFriendsArray);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.listApplication);
        listView.setAdapter(adapter);

    }

    private void getGroupMemberList(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master",mGroup_ObjectID);
        query.whereEqualTo("Group_objectId", obj);
        //query.whereEqualTo("Member_Status", 1);
        query.include("Member_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    filterFriendListNotMember(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getFriendsList(){

        //Query only friends who are not already a member of the group
        //and put a caption saying so.

        mLoadingCaption.setVisibility(View.VISIBLE);
        mLoadingCaption.setText(getResources().getText(R.string.lbl_loading_friends));
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        ParseObject obj = ParseObject.createWithoutData("_User",mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        query.whereEqualTo("Status", 1);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    if(friendList.size() > 0){
                        mLoadingCaption.setText(getResources().getText(R.string.lbl_choose_friends));

                        displayJSONResults(friendList);
                    }

                    else
                        mLoadingCaption.setText(getResources().getText(R.string.group_lbl_no_friends));
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();

        try{
            for (int i = 0; i < jsonObjects.size(); i++) {
                try {
                    GameHistory.add(new PlayerFriend(jsonObjects.get(i), i + 1, this));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }



        return GameHistory;
    }

    public static class AppInfoHolder
    {
        //ImageView imgIcon;
        ImageView imgIcon;
        TextView txtTitle;
        CheckBox chkSelect;
        TextView caption;
    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.menu_source_icon);
                holder.txtTitle = (TextView) row.findViewById(R.id.menu_source_name);
                holder.chkSelect = (CheckBox) row.findViewById(R.id.menu_source_check_box);
                holder.caption = (TextView) row.findViewById(R.id.lbl_group_invite);
                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.FriendID);
            holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.Friend_objectId, activity));
//            holder.imgIcon.setParseFile(appinfo.profilePic);
//
//            //TODO to improve this code, will not be good if hundreds of friends appear
//            holder.imgIcon.loadInBackground(new GetDataCallback() {
//                public void done(byte[] data, ParseException e) {
//                    // The image is loaded and displayed!
//                    //int oldHeight = imageView.getHeight();
//                    //int oldWidth = imageView.getWidth();
//                    Log.v("LOG!!!!!!", "imageView height = ");      // DISPLAYS 90 px
//                    Log.v("LOG!!!!!!", "imageView width = " );        // DISPLAYS 90 px
//                }
//            });
            //holder.imgIcon.setImageDrawable(appinfo.icon);
            // holder.chkSelect.setChecked(true);

            holder.chkSelect.setTag(position);
            holder.chkSelect.setChecked(mCheckStates.get(position, false));
            holder.chkSelect.setOnCheckedChangeListener(this);

            try{
                if(appinfo.getisGroupInviteSent() == 2) {
                    holder.chkSelect.setVisibility(View.GONE);
                    holder.caption.setVisibility(View.GONE);
                }
                else if (appinfo.getisGroupInviteSent() == 1)
                {
                    holder.chkSelect.setVisibility(View.GONE);
                    holder.caption.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder.chkSelect.setVisibility(View.VISIBLE);
                    holder.caption.setVisibility(View.GONE);
                }
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }






            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }

    private void getSelectedFriends(){

        //Skip this step if the user is new and has no friends
        if(adapter == null)
            return;

        StringBuilder result = new StringBuilder();
        StringBuilder resultObjId = new StringBuilder();
        int count = 0;
        for(int i=0;i<adapter.data.size();i++)
        {
            if(adapter.mCheckStates.get(i))
            {
                count++;
                result.append(mPlayerFriendsArray.get(i).FriendID);
                result.append("\n");
                //FriendList[0] = PlayerFriendsArray.get(i).FriendID;
                resultObjId.append(mPlayerFriendsArray.get(i).Friend_objectId);
                resultObjId.append("\n");

            }

        }

        if (count == 1){
            int friendsCount = 1;
            FriendList = new String[friendsCount];
            FriendObjIDList = new String[friendsCount];
            mAppState.SelectedFriendList(friendsCount);
            mAppState.SelectedFriendObjectIDList(friendsCount);
            FriendList[0] = result.substring(0, result.indexOf("\n"));
            FriendObjIDList[0] = resultObjId.substring(0, resultObjId.indexOf("\n"));

            mAppState.setSelectedFriendList(FriendList);
            mAppState.setSelectedFriendObjectIDList(FriendObjIDList);

        }
        else
        {
            //Toast.makeText(this, result, 1000).show();
            StringTokenizer tokens = new StringTokenizer(result.toString(), "\n");
            StringTokenizer tokensObjID = new StringTokenizer(resultObjId.toString(), "\n");
            int friendsCount = tokens.countTokens();
            FriendList = new String[friendsCount];
            FriendObjIDList = new String[friendsCount];
            mAppState.SelectedFriendList(friendsCount);
            mAppState.SelectedFriendObjectIDList(friendsCount);

            for(int i=0;i<friendsCount;i++) {
                FriendList[i] = tokens.nextToken();
                FriendObjIDList[i] = tokensObjID.nextToken();
            }

            //store into global class

            mAppState.setSelectedFriendList(FriendList);
            mAppState.setSelectedFriendObjectIDList(FriendObjIDList);
        }

        if(count > 0)
            addInvitesIntoFeed();
    }

    private void addInvitesIntoFeed(){



        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        ParseObject NewPlayer = new ParseObject("GlobalFeed_Group_Members");

        if(FriendObjIDList != null)
        {
            //followed by all the other friends he/she selected to join the game
            for (int i = 0; i<FriendObjIDList.length; i++){
                //insert new word into History table
                NewPlayer = new ParseObject("GlobalFeed_Group_Members");
                NewPlayer.put("Group_ObjectID", mGroup_ObjectID);
                NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                        "GlobalFeed_Group_Master", mGroup_ObjectID));
                NewPlayer.put("Member_ObjectID", FriendObjIDList[i]);
                NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                        "_User", FriendObjIDList[i]));
                NewPlayer.put("Member_Status", 0); //0 means pending adherance
                PlayerList.add(NewPlayer);
            }
        }




        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new game completed", "SUCCESS!");
                //uponConmpleteAddNewGame();
                //GoToGroup(GroupID);
                addInvitesIntoNotifications();

            }
        });
    }

    private void addInvitesIntoNotifications(){

        if(FriendObjIDList == null)
            return;

        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        ParseObject NewPlayer;

        for (int i = 0; i<FriendObjIDList.length; i++){
            //insert new word into History table
            NewPlayer = new ParseObject("Notifications");
            //NewPlayer.put("Group_ObjectID", GroupID);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", FriendObjIDList[i]));
            NewPlayer.put("IsSeen", false);
            NewPlayer.put("Type", 2);
            NewPlayer.put("IsAccepted", false); //By default is False


            //NewPlayer.put("Member_ObjectID", FriendObjIDList[i]);
            NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Group_Master", mGroup_ObjectID));
            NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                    "_User", mPlayer_objectId));

            PlayerList.add(NewPlayer);
        }

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                completeInviteFriends();

            }
        });


    }

    private void completeInviteFriends(){


        Toast.makeText(this,
                getResources().getText(R.string.group_lbl_invited).toString(),
                Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.global_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        // Respond to the action bar's Up/Home button
        if (id == android.R.id.home)
        {
            finish();
            return true;
         }

        if (id == R.id.action_done) {
            try{
                getSelectedFriends();
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }

        }

        return super.onOptionsItemSelected(item);
    }

}
