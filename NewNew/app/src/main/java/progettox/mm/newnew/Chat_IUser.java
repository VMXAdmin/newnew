package progettox.mm.newnew;

/**
 * Created by Admin on 19/5/2015.
 */
public interface Chat_IUser {
    public String getUsername();
    public String getEmail();
    public String getUri();
}
