package progettox.mm.newnew;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Admin on 1/7/2015.
 */
public class frame_index_more extends Fragment {

    public String mPlayerID;
    public String mPlayer_objectId;
    //private LoadImageTask mLoadImageTask;
    private App mAppState;
    private ImageView userProfilePic;
    private ImageView mIVAlert;

    public static frame_index_more newInstance(String param1, String param2) {
        frame_index_more fragment = new frame_index_more();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_index_more, container, false);

        mIVAlert = (ImageView) view.findViewById(R.id.ic_pm_alert);

        userProfilePic = (ImageView) view.findViewById(R.id.user_profile_pic);
        TextView userName = (TextView) view.findViewById(R.id.lbl_my_name);

        userName.setText(mPlayerID);
        userProfilePic.setImageBitmap(GlobalPost.LoadPNG(mPlayer_objectId, getActivity()));

        Button btnOpenProfilePage = (Button) view.findViewById(R.id.btn_open_profile);
        btnOpenProfilePage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GoToProfilePage();
            }
        });

        Button btnOpenPMs = (Button) view.findViewById(R.id.btn_open_PM);
        btnOpenPMs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GoToPMPage();
            }
        });

        Button btnNotifications = (Button) view.findViewById(R.id.btn_open_notifications);
        btnNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GoToNotificationsPage();
            }
        });

        Button btnAzure = (Button) view.findViewById(R.id.btn_azure);
        btnAzure.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GoToAzurePage();
            }
        });

        Button btnSettings = (Button) view.findViewById(R.id.btn_open_settings);
        btnSettings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GoToAzurePage();
            }
        });

        Button btnLogOut = (Button) view.findViewById(R.id.btn_logoff);
        btnLogOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GoToLogout();
            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Initialize();

    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        userProfilePic.setImageBitmap(GlobalPost.LoadPNG(mPlayer_objectId, getActivity()));
        if(((MainMenuActivity)getActivity()).getNotificationStatus())
            mIVAlert.setVisibility(View.VISIBLE);
        else
            mIVAlert.setVisibility(View.GONE);
    }

    private void Initialize(){
        mAppState = ((App)getActivity().getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();



    }
    private void GoToNotificationsPage(){
        ((MainMenuActivity)getActivity()).goToNotifications();
    }

    private void GoToLogout(){
        ((MainMenuActivity)getActivity()).GoToLogout();
    }

    public void GoToPMPage(){
        ((MainMenuActivity)getActivity()).GoToPMPage();
    }

    public void GoToAzurePage(){
        ((MainMenuActivity)getActivity()).OpenAzureTest();
    }

    public void GoToProfilePage(){
        ((MainMenuActivity)getActivity()).GoToProfilePage();
    }
}
