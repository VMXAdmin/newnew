package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 6/5/2015.
 */
public class frame_index_learn extends Fragment {

    private int mRequestCode = 102;
    public String mPlayerID;
    public String mPlayer_objectId;
    public String Chat_ObjectID;
    private ProgressDialog mDialog;
    public App mAppState;
    public final static String TITLE = "CHAT";
    public ArrayList<ChatItem> mChatItemList;
    public Hashtable<String, ChatItem> mHTChatItemList;
    public ChatAdapter mAdapter;
    public ArrayList<Bitmap> mUserAvatars;
    public int mScreenHeight;
    public int mScreenWidth;
    public ListView listView;
    private String mChatID;
    private String mChatUsername;
    private String mActionType;
    private boolean mIsDirty;
    private TextView mLoadingCaption;

    // TODO: Rename and change types of parameters
    public static frame_index_learn newInstance(String param1, String param2) {
        frame_index_learn fragment = new frame_index_learn();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    public frame_index_learn()
    {


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAppState = ((App)getActivity().getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();
        mAppState.setGameNotification(false);
        mAppState.setPageNumber(9);

        mDialog = new ProgressDialog(getActivity());
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Retrieving Your Chats, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_main_list, container, false);
        listView = (ListView) view.findViewById(R.id.lvItems);
        mLoadingCaption = (TextView)view.findViewById(R.id.lbl_loading_caption);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

        //TODO retrieve values from appState
//        Bundle extras = getIntent().getExtras();
//        if (extras != null){
//            mChatID = extras.getString("ChatID");
//            mChatUsername = extras.getString("ChatUsername");
//            //mChannel = extras.getString("Channel");
//            mActionType = extras.getString("ChatAlertType");
//        }

        mChatID = mAppState.getChatID();
        mChatUsername = mAppState.getChatUsername();
        mActionType = mAppState.getChatAlertType();


        //If user opens push notification and comes here from MainMenuActivity
        //this code will further bring the user to the ChatActivity page to view the latest text msg
//        if(mActionType != null){
//            if(mActionType.equals("PUSH"))
//                GotoChatActivity(mChatID, mChatUsername, "" , mActionType);
//        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        //mDialog.show();
        mLoadingCaption.setVisibility(View.VISIBLE);
        loadListView();
        //checkChatCache();
        getChatSessionIDs();
        checkForChatUpdates();
    }

    public void checkForChatUpdates(){

        if(mHTChatItemList == null)
        {
            getChatSessionIDs();
            mIsDirty = true;
            return;
        }
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_UserList");
        query.whereEqualTo("User_ObjectID", mPlayer_objectId);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {

                if(mHTChatItemList.size() != i){
                    getChatSessionIDs();
                    mIsDirty = true;
                    //mDialog.show();
                }
            }
        });
    }

    private void checkChatCache(){
        SharedPreferences sp = getActivity().getSharedPreferences("OURINFO", 0);

        //check if we have cache of CHAT data
        String JSON = sp.getString("mHTChatItemList", "");
        if(JSON == null || JSON.equals("") || JSON.equals("null")){
            //here we start to load and populate the user's chat data and history
            getChatSessionIDs();
        }
        else
        {
            //here we just reload the JSON into our hashTable item
            mHTChatItemList = new Hashtable<String, ChatItem>();
            Gson gson = new Gson();
            Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            mHTChatItemList = gson.fromJson(JSON, stringStringMap);
            updateAdapter();
            mDialog.dismiss();
        }

        //getChatSessionIDs();

    }

    private void insertChatHistory(List<ParseObject> dataList){
//        String chatID = dataList.get(0).getString("Chat_ObjectID");
//        if ( mHTChatItemList.get(chatID).getChatDetails() != null){
//            mHTChatItemList.get(chatID).getChatDetails().clear();
//        }
//
//        for (int i=0; i<dataList.size(); i++){
//            mHTChatItemList.get(dataList.get(i).getString("Chat_ObjectID")).insertChatDetails(dataList.get(i), getActivity());
//        }

        String chatID = "";
        for (int i=0; i<dataList.size(); i++){
            chatID = dataList.get(0).getString("Chat_ObjectID");
            if ( mHTChatItemList.get(chatID).getChatDetails() != null){
                mHTChatItemList.get(chatID).getChatDetails().clear();
            }
        }

        for (int i=0; i<dataList.size(); i++){
            mHTChatItemList.get(dataList.get(i).getString("Chat_ObjectID")).insertChatDetails(dataList.get(i), getActivity());
        }


    }

    private void loadListView(){
        // Construct the data source
        ArrayList<ChatItem> arrayOfUsers = new ArrayList<ChatItem>();
        // Create the adapter to convert the array to views
        mAdapter = new ChatAdapter(getActivity().getBaseContext(), arrayOfUsers, getActivity());
        //adapter.addAll(mHTChatItemList.values());
        // Attach the adapter to a ListView
        listView.setAdapter(mAdapter);
        //mDialog.dismiss();
    }

    private void updateAdapter(){
        mAdapter.clear();
        mAdapter.addAll(mHTChatItemList.values());
        mAdapter.notifyDataSetChanged();
        mDialog.dismiss();
        mLoadingCaption.setVisibility(View.GONE);
    }

    private void updateChatItemUserAndImage(List<ParseObject> ObjList){

        for(int i=0;i<ObjList.size();i++){
            mHTChatItemList.get(ObjList.get(i).getString("Chat_ObjectID")).updateChatName(ObjList.get(i).getParseObject("Friend_objectId").getString("username"),
                    ObjList.get(i).getParseObject("Friend_objectId").getObjectId());
            mHTChatItemList.get(ObjList.get(i).getString("Chat_ObjectID")).updateChatAvatar(ObjList.get(i).getParseObject("Friend_objectId").getParseFile("Avatar_Pic"),
                    ObjList.get(i).getString("Chat_ObjectID"), getActivity());
        }


    }

    private void updateChatItemUserAndImage(ParseObject Obj, String ChatID){

        mHTChatItemList.get(ChatID).updateChatName(Obj.getString("FriendID"),
                Obj.getParseObject("Friend_objectId").getObjectId());
        mHTChatItemList.get(ChatID).updateChatAvatar(Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic"),
                ChatID, getActivity());
    }

    private void storeImage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getChatSessionDetails(List<ParseObject> dataList){

        mHTChatItemList = new Hashtable<String, ChatItem>();
        ArrayList<String> chatIDs = new ArrayList<>();

        for (int i=0; i<dataList.size(); i++){
            mHTChatItemList.put(dataList.get(i).getParseObject("Chat_objectId").getObjectId(),
                    new ChatItem(dataList.get(i), i, getActivity()));
            chatIDs.add(dataList.get(i).getParseObject("Chat_objectId").getObjectId());


        }

        //Now get get information to know which contact are we talking to and set the Name and Image
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        //query.whereEqualTo("PlayerID", mPlayerID);
        ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        //query.whereEqualTo("Chat_ObjectID", ChatID);
        query.whereContainedIn("Chat_ObjectID", chatIDs);
        query.include("Friend_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0) {
                        //set object's name and image

                        //updateChatItemUserAndImage(scoreList.get(0), ChatID);
                        updateChatItemUserAndImage(scoreList);


                    }
                    else
                    {
                        //mDialog.dismiss();
                        //TODO to display textView message saying no entries has been made yet
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        //Now we get information such as user avatar photo and username and last chat
        query = ParseQuery.getQuery("Chat_History");
        //query.whereEqualTo("Chat_ObjectID", ChatID);
        query.whereContainedIn("Chat_ObjectID", chatIDs);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0){

                        insertChatHistory(scoreList);
                        updateAdapter();


                    }

                    else
                    {
                        //mDialog.dismiss();
                        //TODO to display textView message saying no entries has been made yet
                        updateAdapter();
                        //cacheData();
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        //First we get all the chat IDs which is NOT group
//        for (int i=0; i<dataList.size(); i++){
//            //mChatItemList.add(new ChatItem(dataList.get(i), i));
//
//            mHTChatItemList.put(dataList.get(i).getParseObject("Chat_objectId").getObjectId(),
//                    new ChatItem(dataList.get(i), i, getActivity()));
//            final String ChatID = dataList.get(i).getParseObject("Chat_objectId").getObjectId();
//
//            //Now get get information to know which contact are we talking to and set the Name and Image
//            ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
//            //query.whereEqualTo("PlayerID", mPlayerID);
//            ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
//            query.whereEqualTo("Player_objectId", obj);
//            query.whereEqualTo("Chat_ObjectID", ChatID);
//            query.include("Friend_objectId");
//            query.findInBackground(new FindCallback<ParseObject>() {
//                public void done(List<ParseObject> scoreList, ParseException e) {
//                    if (e == null) {
//                        Log.d("score", "Retrieved " + scoreList.size() + " scores");
//                        if(scoreList.size() > 0) {
//                            //set object's name and image
//
//                            updateChatItemUserAndImage(scoreList.get(0), ChatID);
//
//
//                        }
//                        else
//                        {
//                            //mDialog.dismiss();
//                            //TODO to display textView message saying no entries has been made yet
//                        }
//
//                    } else {
//                        Log.d("score", "Error: " + e.getMessage());
//                    }
//                }
//            });
//
//            //Now we get information such as user avatar photo and username and last chat
//            query = ParseQuery.getQuery("Chat_History");
//            query.whereEqualTo("Chat_ObjectID", ChatID);
//            query.include("User_objectId");
//            query.findInBackground(new FindCallback<ParseObject>() {
//                public void done(List<ParseObject> scoreList, ParseException e) {
//                    if (e == null) {
//                        Log.d("score", "Retrieved " + scoreList.size() + " scores");
//                        if(scoreList.size() > 0){
//
//                            insertChatHistory(scoreList);
//                            updateAdapter();
//
//
//                        }
//
//                        else
//                        {
//                            //mDialog.dismiss();
//                            //TODO to display textView message saying no entries has been made yet
//                            updateAdapter();
//                            //cacheData();
//                        }
//
//                    } else {
//                        Log.d("score", "Error: " + e.getMessage());
//                    }
//                }
//            });
//        }




    }


    public static void createDirectory(File dir) throws IllegalStateException{
        if (!dir.exists()){
            if(!dir.mkdirs()){
                throw new IllegalStateException(
                        "Check if you've added permissions in AndroidManifest.xml: \n" +
                                "<uses-permission android:name=\"android.permission.WRITE_EXTERNAL_STORAGE\"/> \n"
                );
            }
        }
    }

    private void getChatSessionIDs(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_UserList");
        query.whereEqualTo("User_ObjectID", mPlayer_objectId);
        query.include("Chat_objectId");
        query.include("User_objectId");
        //query.addDescendingOrder("createAt");
        //query.orderByAscending("createAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0){
                        //PlayerHistoryResults = scoreList;
                        //displayJSONResults(scoreList);
                        getChatSessionDetails(scoreList);
                        //cacheUserAvatars(scoreList);
                    }
                    else
                    {
                        mDialog.dismiss();
                        mLoadingCaption.setVisibility(View.GONE);
                        //TODO to display textView message saying no entries has been made yet
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public class ChatAdapter extends ArrayAdapter<ChatItem> {

        private Activity activity;

        public ChatAdapter(Context context, ArrayList<ChatItem> users, Activity act) {
            super(context, 0, users);
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final ChatItem user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.chat_main_item, parent, false);
            }
            // Lookup view for data population
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvBody = (TextView) convertView.findViewById(R.id.tvBody);
            TextView tvCreatedAt = (TextView) convertView.findViewById(R.id.tvCreatedAt);
            //final ParseImageView ivProfileLeft = (ParseImageView) convertView.findViewById(R.id.ivProfileLeft);
            final ImageView ivProfileLeft = (ImageView) convertView.findViewById(R.id.ivProfileLeft);

            //now we determine what to display on the chat history list
            //first we determine what user and user image to put
            if(user.getChatDetails() != null) {
                Collections.sort(user.getChatDetails(), new ChatDetailsComparator());
                int index = user.getChatDetails().size() - 1;
                tvBody.setText(user.getChatDetails().get(index).getBody());
            }
            else
            {
                tvBody.setText("Be the first to chat up this group.");
            }


            tvName.setText(user.getChatUsername());
            //ivProfileLeft.setImageBitmap(user.getChatImageBmp());
            try{
                //ivProfileLeft.setBackgroundColor(getResources().getColor(R.color.inputText));
                ivProfileLeft.setImageBitmap(RoundedImageView.getCircleBitmap(user.loadImageFromStorage(user.getChatID(), activity)));
                //ivProfileLeft.setScaleType(ImageView.ScaleType.FIT_XY);
            }
            catch(Exception e){
                Log.i("Chat_Main", e.getMessage());
            }

//            ivProfileLeft.setParseFile(user.getChatImage());
//            ivProfileLeft.loadInBackground(new GetDataCallback() {
//                public void done(byte[] data, ParseException e) {
//
//                    ivProfileLeft.getLayoutParams().width = mScreenWidth/6;
//                    ivProfileLeft.getLayoutParams().height = mScreenWidth/6;
//                    ivProfileLeft.requestLayout();
//                }
//
//            });





            //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
            View PrimaryColumn = convertView.findViewById(R.id.primary_target);
            //PrimaryColumn.setTag(PrimaryColumn.getId(), user.getChatDetails().get(0).getObjectID());
            //Then we set the onClick function here to pull the GameSession_objectId upon each click
            PrimaryColumn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            GotoChatActivity(user.getChatID(), user.getChatUsername(),user.getChatID(),  user.getChannel(), "");
                        }
                    });
            // Return the completed view to render on screen
            return convertView;
        }

    }

    public void GotoChatActivity(String ChatID, String ChatPlayerID, String ChatPlayerObjectID, String Channel, String AlertType){

        Intent intent = new Intent(getActivity(), ChatActivity.class);

        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("ChatID", ChatID);
        bundle.putString("ChatUsername", ChatPlayerID);
        bundle.putString("ChatUserObjectID", ChatPlayerObjectID);
        bundle.putString("Channel", Channel);
        bundle.putString("ChatAlertType", AlertType);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }

    public class ChatDetailsComparator implements Comparator<ChatDetailsHolder>
    {
        public int compare(ChatDetailsHolder left, ChatDetailsHolder right) {
            //return left.name.compareTo(right.name);
            return left.getCreatedAt().compareTo(right.getCreatedAt());
        }
    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first

        if(this.mIsDirty){
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);


            SharedPreferences sp = getActivity().getSharedPreferences("OURINFO", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();

            this.mIsDirty = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);

            SharedPreferences sp = getActivity().getSharedPreferences("OURINFO", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();

            this.mIsDirty = false;
        }

    }
}
