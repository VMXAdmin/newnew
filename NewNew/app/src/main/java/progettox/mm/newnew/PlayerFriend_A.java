package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Admin on 11/7/2015.
 */
public class PlayerFriend_A {
    public int objectId;
    public String PlayerID;
    public String FriendID;
    public String Friend_objectId;

    public Drawable icon;
    public String applicationName;
    //public ParseFile profilePic;
    public Boolean isFriend;
    public int isGroupInviteSent; //0 means no invites sent, 1 means invites sent, 2 means is member
    public int FriendType; //0 means friend request sent, 1 means already is friends
    private Hashtable<String, String> Groups;

    public String getFriendObjectID(){return this.Friend_objectId;}
    public void setIsFriend(Boolean status) {this.isFriend = status;}
    public void setFriendStatus(int status) {this.FriendType = status;}
    public Boolean getIsFriend(){return this.isFriend;}
    public String getFriendID(){return this.FriendID;}
    public int getFriendType(){return this.FriendType;}

    public void setIsGroupInviteSent(int status) {this.isGroupInviteSent = status;}
    public int getisGroupInviteSent(){return this.isGroupInviteSent;}
    public Hashtable<String, String> getGroups() {return this.Groups;}
    public int getTotalNoOfGroups() {
        if(Groups != null) {
            return this.Groups.size();
        }

        return 0;
    }
    public String getFirstGroup(){
        if(Groups != null){
            Map.Entry<String,String> entry = Groups.entrySet().iterator().next();
            return entry.getValue();
        }

        return "";

    }
    public void insertNewGroup(String ObjID, String Name){
        if(Groups == null){
            Groups = new Hashtable<>();
        }

        Groups.put(ObjID, Name);
    }

//    public PlayerFriend_A(ParseUser Obj, final Activity activity) {
//        this.objectId = Obj.getObjectId();
//        this.isFriend = false;
//        this.isGroupInviteSent = 0;
//        this.PlayerID = "";
//
//        this.FriendID = Obj.getUsername();
//        //this.profilePic = Obj.getParseFile("Avatar_Pic");
//        this.Friend_objectId = Obj.getObjectId();
//
//
//
//        //profilePic
//        //public static Drawable createFromResourceStream (Resources res, TypedValue value, InputStream is, String srcName, BitmapFactory.Options opts)
//
//
//        if(!IsBitmapCached(this.Friend_objectId, activity)){
//            ParseFile Picture = Obj.getParseFile("Avatar_Pic");
//            Picture.getDataInBackground(new GetDataCallback() {
//                @Override
//                public void done(byte[] data, ParseException e) {
//
//                    if (e == null) {
//                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
//                                data.length);
//                        storeImage(PictureBmp, Friend_objectId, activity);
//
//                    } else {
//
//                    }
//
//                }
//            });
//        }
//
//    }

//    public PlayerFriend_A(JSONObject Obj, final Activity activity) {
//
//        try{
//            this.objectId = Obj.getInt("objectId");
//            this.isFriend = false;
//            this.isGroupInviteSent = 0;
//            this.PlayerID = "";
//            this.FriendID = Obj.getParseObject("Member_objectId").getString("username");
//            this.Friend_objectId = Obj.getString("Member_ObjectID");
//        }catch(Exception e){
//            Log.e("PlayerFriend_A", e.getMessage());
//        }
//
//        String PointerKey = "";
//        if(Obj.getParseObject("Member_objectId") != null){
//            PointerKey = "Member_objectId";
//            this.FriendID = Obj.getParseObject("Member_objectId").getString("username");
//            //this.profilePic = Obj.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
//            this.Friend_objectId = Obj.getString("Member_ObjectID");
//        }
//        else if (Obj.getParseObject("User_objectId") != null)
//        {
//            PointerKey = "User_objectId";
//            this.FriendID = Obj.getParseObject("User_objectId").getString("username");
//            //this.profilePic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
//            this.Friend_objectId = Obj.getParseObject("User_objectId").getObjectId();
//        }
//        else if (Obj.getParseObject("Friend_objectId") != null)
//        {
//            PointerKey = "Friend_objectId";
//            this.FriendID = Obj.getParseObject("Friend_objectId").getString("username");
//            //this.profilePic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
//            this.Friend_objectId = Obj.getParseObject("Friend_objectId").getObjectId();
//        }
//
//
//        //profilePic
//        //public static Drawable createFromResourceStream (Resources res, TypedValue value, InputStream is, String srcName, BitmapFactory.Options opts)
//
//
//        if(!IsBitmapCached(this.Friend_objectId, activity)){
//            ParseFile Picture = Obj.getParseObject(PointerKey).getParseFile("Avatar_Pic");
//            Picture.getDataInBackground(new GetDataCallback() {
//                @Override
//                public void done(byte[] data, ParseException e) {
//
//                    if (e == null) {
//                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
//                                data.length);
//                        storeImage(PictureBmp, Friend_objectId, activity);
//
//                    } else {
//
//                    }
//
//                }
//            });
//        }
//
//    }

//    public PlayerFriend_A(ParseObject Obj, int c, final Activity activity) {
//        this.objectId = Obj.getObjectId();
//        this.isFriend = false;
//        this.isGroupInviteSent = 0;
//        this.PlayerID = Obj.getString("PlayerID");
//        this.FriendID = Obj.getString("FriendID");
//
//        this.Friend_objectId = Obj.getParseObject("Friend_objectId").getObjectId();
//
//        if(!IsBitmapCached(this.Friend_objectId, activity)){
//            ParseFile Picture = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
//            Picture.getDataInBackground(new GetDataCallback() {
//                @Override
//                public void done(byte[] data, ParseException e) {
//
//                    if (e == null) {
//                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
//                                data.length);
//                        storeImage(PictureBmp, Friend_objectId, activity);
//
//                    } else {
//
//                    }
//
//                }
//            });
//        }
//
//    }

    private boolean IsBitmapCached(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
