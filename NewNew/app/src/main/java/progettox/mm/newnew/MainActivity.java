package progettox.mm.newnew;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.util.Hashtable;
import java.util.List;


public class MainActivity extends FragmentActivity {

    private Hashtable<String, ChatItem> mHTChatItemList;
    private int mNoOfChats;
    private int mChatIndex;
    private Fragment_LogIn mfirstFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fame_main);

        this.mNoOfChats = 0;
        this.mChatIndex = 0;
        // Check whether the activity is using the layout version with
        // the fragment_container FrameLayout. If so, we must add the first fragment
        if (findViewById(R.id.fragment_main_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create an instance of ExampleFragment
            mfirstFragment = new Fragment_LogIn();

            // In case this activity was started with special instructions from an Intent,
            // pass the Intent's extras to the fragment as arguments
            mfirstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_main_container, mfirstFragment).commit();


        }

        //
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();  // Always call the superclass method first

        App appState = ((App)getApplicationContext());
        appState.setPageNumber(1);

        //Check for Log In Credentials, to direct go to MainMenuActivity if Valid
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        String PlayerName = sp.getString("PlayerID", "NIL");
        String PWD = sp.getString("PlayerPWD", "NIL");
        String PlayerObjectID = sp.getString("Player_ObjectID", "NIL");

        if (!PlayerName.equals("NIL") && !PWD.equals("NIL")){
            //Do Parse Login
            mfirstFragment.ActivateLoading(true);
            ParseUserLoginSSO(PlayerName, PWD, PlayerObjectID);
        }


    }

    public void ParseUserLoginFSO(final String usernametxt, final String passwordtxt){
        ParseUser.logInInBackground(usernametxt, passwordtxt,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {

                            GotoMainMenuActivity(usernametxt, passwordtxt, user.getObjectId());
                        } else {
                            mfirstFragment.ActivateLoading(false);
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Wrong username or password, please try again or signup",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void ParseUserLoginSSO(final String usernametxt, final String passwordtxt, final String PlayerObjectID){
        // Send data to Parse.com for verification
        ParseUser.logInInBackground(usernametxt, passwordtxt,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            // If user exist and authenticated, send user to Welcome.class
//                            Intent intent = new Intent(
//                                    LoginSignupActivity.this,
//                                    Welcome.class);
//                            startActivity(intent);
//                            Toast.makeText(getApplicationContext(),
//                                    "Successfully Logged in",
//                                    Toast.LENGTH_LONG).show();
//                            finish();
                            GotoMainMenuActivity(usernametxt, passwordtxt, user.getObjectId());
                        } else {
                            mfirstFragment.ActivateLoading(false);
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Wrong username or password, please try again or signup",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void updateChatItemUserAndImage(ParseObject Obj, String ChatID){
        mHTChatItemList.get(ChatID).updateChatName(Obj.getString("FriendID"),
                Obj.getParseObject("Friend_objectId").getObjectId());
//        mHTChatItemList.get(ChatID).updateChatAvatar(Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic"),
//                Obj.getParseObject("Friend_objectId").getObjectId(), this);
        mHTChatItemList.get(ChatID).updateChatAvatar(Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic"),
                ChatID, this);
    }

    private void insertChatHistory(List<ParseObject> dataList, int count){
        for (int i=0; i<dataList.size(); i++){
            mHTChatItemList.get(dataList.get(i).getString("Chat_ObjectID")).insertChatDetails(dataList.get(i), this);
        }
        mChatIndex++;

        if (mChatIndex == mNoOfChats)
        {
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);

            SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();
        }


    }

    private void getChatSessionDetails(List<ParseObject> dataList,
                                       final String UID, final String UOBJID){

        //mChatItemList = new ArrayList<ChatItem>();
        mHTChatItemList = new Hashtable<String, ChatItem>();
        //final String ChatID = "";

        mNoOfChats = dataList.size();
        //First we get all the chat IDs which is NOT group
        for (int i=0; i<dataList.size(); i++){
            //mChatItemList.add(new ChatItem(dataList.get(i), i));

            final int count = i;
            mHTChatItemList.put(dataList.get(i).getParseObject("Chat_objectId").getObjectId(),
                    new ChatItem(dataList.get(i), i, this));
            final String ChatID = dataList.get(i).getParseObject("Chat_objectId").getObjectId();

            //Now get get information to know which contact are we talking to and set the Name and Image
            ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
            query.whereEqualTo("PlayerID", UID);
            query.whereEqualTo("Chat_ObjectID", ChatID);
            query.include("Friend_objectId");
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + scoreList.size() + " scores");
                        if(scoreList.size() > 0) {
                            //set object's name and image

                            updateChatItemUserAndImage(scoreList.get(0), ChatID);


                        }
                        else
                        {
                            //mDialog.dismiss();
                            //TODO to display textView message saying no entries has been made yet
                        }

                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });

            //Now we get information such as user avatar photo and username and last chat
//            ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_History");
            query = ParseQuery.getQuery("Chat_History");
            query.whereEqualTo("Chat_ObjectID", ChatID);
            query.include("User_objectId");
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + scoreList.size() + " scores");
                        if(scoreList.size() > 0){

                            insertChatHistory(scoreList, count);

                            //updateAdapter();


                        }

                        else
                        {

                            //TODO to display textView message saying no entries has been made yet
                           // updateAdapter();

                        }

                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }




    }

    private void preLoadUserChatData(final String UID, final String UOBJID){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_UserList");
        query.whereEqualTo("User_ObjectID", UOBJID);
        query.include("Chat_objectId");
        query.include("User_objectId");
        //query.addDescendingOrder("createAt");
        //query.orderByAscending("createAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0){
                        //PlayerHistoryResults = scoreList;
                        //displayJSONResults(scoreList);
                        getChatSessionDetails(scoreList, UID, UOBJID);
                        //cacheUserAvatars(scoreList);
                    }
                    else
                    {

                        //TODO to display textView message saying no entries has been made yet
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void GotoRegistrationActivity(){
        Intent intent = new Intent(this, activity_register_new.class);
        startActivity(intent);
    }

    public void onFirstTimeRegister(String UID, String PWD, String UOBJID){
        //register into Shared Object to be retrieved later///////////
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("PlayerID", UID);
        ed.putString("Player_ObjectID", UOBJID);
        ed.putString("PlayerPWD", PWD);
        ed.apply();
    }

    public void GotoMainMenuActivity(String UID, String PWD, String UOBJID){

        mfirstFragment.ActivateLoading(false);

        if(!IsBitmapCached(UID)){
            cacheSelfAvatarImage(UID, UOBJID);
        }

        //register into Shared Object to be retrieved later///////////
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
//        ed.putString("mHTChatItemList", "");
//        ed.apply();

        //check if we have cache of CHAT data
        String JSON = sp.getString("mHTChatItemList", "");
        if(JSON == null || JSON.equals("")){
            //here we start to load and populate the user's chat data and history
            preLoadUserChatData(UID, UOBJID);
        }



        ed.putString("PlayerID", UID);
        ed.putString("Player_ObjectID", UOBJID);
        ed.putString("PlayerPWD", PWD);
        ed.apply();
        //register into Shared Object to be retrieved later///////////

        //The 1st line is to register into Parse's Installation table to faciliate proper Push Notifications
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("username", UID);
        installation.saveInBackground();

        App appState = ((App)getApplicationContext());
        appState.setPlayerID(UID);
        appState.setPlayer_objectId(UOBJID);

        Intent intent = new Intent(this, MainMenuActivity.class);
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("PlayerID", UID);
        bundle.putString("Player_objectId", UOBJID);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void cacheSelfAvatarImage(final String UID, final String UOBJID){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", UID);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    //SaveImageToCache(objects.get(0), UID);
                    SaveImageToCache(objects.get(0), UOBJID);
                } else {
                    // Something went wrong.
                }
            }
        });
    }

    private void SaveImageToCache(ParseObject Obj, final String objcetID){
        ParseFile Picture = Obj.getParseFile("Avatar_Pic");
        Picture.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] data, ParseException e) {

                if (e == null) {
                    Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                            data.length);
                    storeImage(PictureBmp, objcetID);

                } else {

                }

            }
        });

    }

    private boolean IsBitmapCached(String FileName){
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void storeImage(Bitmap bitmapImage, String FileName) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_fragment__log_in, container, false);
            return rootView;
        }
    }

//    public class PushNotificationReceiver extends ParsePushBroadcastReceiver {
//
//        private static final String TAG = "PushNotificationReceiver";
//
//        public PushNotificationReceiver(){
//
//        }
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            //Log.i(TAG, "Received intent: " + intent.toString());
//            String action = intent.getAction();
//            Log.d("TEST", action.toString());
//            if (action.equals(ParsePushBroadcastReceiver.ACTION_PUSH_RECEIVE)) {
//                JSONObject extras;
//                try {
//                    extras = new JSONObject(intent.getStringExtra(ParsePushBroadcastReceiver.KEY_PUSH_DATA));
//
//                    // I get this on my log like this:
//                    // Received push notification. Alert: A test push from Parse!
//
//                    Log.i("ParsePush", "Received push notification. Alert: " + extras.getString("alert"));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        @Override
//        protected Notification getNotification(Context context, Intent intent) {
//
//            Intent notificationIntent = new Intent(context, MainActivity.class);
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//            PendingIntent piIntent=PendingIntent.getActivity(context,0,
//                    notificationIntent,0);
//
//            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
//                    .setContentTitle("My Notification")
//                    .setStyle(new NotificationCompat.BigTextStyle()
//                            .bigText("Testing new Push Obj"));
//                    //.setContentText(extras.getString("alert")).setAutoCancel(true);
//            mBuilder.setContentIntent(piIntent);
//
//            return mBuilder.build();
//        }
//    }


}
