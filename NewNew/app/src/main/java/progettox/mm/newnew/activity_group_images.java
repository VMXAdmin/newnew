package progettox.mm.newnew;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
  * Created by Admin on 18/7/2015.
  */
 public class activity_group_images extends AppCompatActivity {

    public String mUser_ObjectID;
    public String mUser_Name;
    private int mProfileType; //1 is user, 2 is group, 3 is tutor
    public String PlayerID;
    public String Player_objectId;
    private String mGroup_objectId;
    private LoadImageTask mLoadImageTask;
    public App mAppState;
    private ProgressDialog mDialogImageLoad;
    private Map<String, GroupImages> mLHM_Images = Collections.synchronizedMap(new LinkedHashMap<String, GroupImages>());

    private void Initialize(){
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            mProfileType = extras.getInt("Profile_Type");
            if(mProfileType == 2)
                mGroup_objectId = extras.getString("Group_ObjectID");
            else if(mProfileType == 1){
                mUser_ObjectID = extras.getString("User_ObjectID");
                mUser_Name = extras.getString("User_Name");
            }

        }

        mAppState = ((App)getApplicationContext());
        PlayerID = mAppState.getPlayerID();
        Player_objectId = mAppState.getPlayer_objectId();

        mDialogImageLoad = new ProgressDialog(this);
        mDialogImageLoad.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialogImageLoad.setMessage("Loading your image."); //TODO
        mDialogImageLoad.setIndeterminate(true);
        mDialogImageLoad.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
             super.onCreate(savedInstanceState);
             setContentView(R.layout.activity_group_images);
        Initialize();
        if(mProfileType == 2)
            loadGroupImages();
        else if(mProfileType == 1)
            loadUserImages();


//             GridView gridview = (GridView) findViewById(R.id.gridview);
//             gridview.setAdapter(new ImageAdapter(this));
//
//             gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                 public void onItemClick(AdapterView<?> parent, View v,
//                                         int position, long id) {
//                     Toast.makeText(activity_group_images.this, "" + position,
//                             Toast.LENGTH_SHORT).show();
//                 }
//             });
     }

    private void loadUserImages(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        ParseObject obj = ParseObject.createWithoutData("_User", mUser_ObjectID);
        query.whereEqualTo("Submitter_objectId", obj);
        query.include("GlobalFeed_objectId");
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size() > 0) {
                        FeedMasterDataIntoObjects(scoreList);
                    }

                }
            }
        });
    }

    private void loadGroupImages(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
        ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master", mGroup_objectId);
        query.whereEqualTo("Group_objectId", obj);
        query.include("GlobalFeed_objectId");
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                     if(scoreList.size() > 0) {
                         FeedMasterDataIntoObjects(scoreList);
                     }

                }
            }
        });
     }

    public void FeedMasterDataIntoObjects(List<ParseObject> data){

        mLHM_Images = ConvertJSONtoArray(data);

        ArrayList<GroupImages> arrayOfUsers = new ArrayList<GroupImages>();
        ImageAdapter gridAdapter = new ImageAdapter(this, arrayOfUsers, this);
        gridAdapter.addAll(mLHM_Images.values());


         GridView gridview = (GridView) findViewById(R.id.gridview);
         gridview.setAdapter(gridAdapter);

//         gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//             public void onItemClick(AdapterView<?> parent, View v,
//                                     int position, long id) {
//                 Toast.makeText(activity_group_images.this, "" + position + " " + id,
//                         Toast.LENGTH_SHORT).show();
//
//
//             }
//         });


    }

    public Map<String, GroupImages> ConvertJSONtoArray(List<ParseObject> jsonObjects){
        //Hashtable<String, GlobalPost> GameHistory = new Hashtable<String, GlobalPost>();
        Map<String, GroupImages> GameHistory = Collections.synchronizedMap(new LinkedHashMap<String, GroupImages>());

        for (int i = 0; i < jsonObjects.size(); i++) {
            //if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)) {
            GameHistory.put(jsonObjects.get(i).getObjectId(),
                    new GroupImages(jsonObjects.get(i), this));

            //}
        }
        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    static class ViewHolder {

        ImageView imgIcon;
        String objectID;
    }

     public class ImageAdapter extends ArrayAdapter<GroupImages> {
           private Context mContext;
         private ArrayList<GroupImages> itemArray;

//           public ImageAdapter(Context c) {
//               mContext = c;
//           }

           public int getCount() {
               return itemArray.size();
           }

           public GroupImages getItem(int position) {
               return itemArray.get(position);
           }

           public long getItemId(int position) {
               return 0;
           }

         Activity activity;
         public ImageAdapter(Context context, ArrayList<GroupImages> users, Activity act) {
             super(context, 0, users);
             itemArray = users;
           activity = act;
         }

           // create a new ImageView for each item referenced by the Adapter
          @Override
           public View getView(int position, View convertView, ViewGroup parent) {
               //ImageView imageView;
              ViewHolder holder = null;
              final GroupImages user = getItem(position);
               if (convertView == null) {
                   // if it's not recycled, initialize some attributes
                   convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_group_images, parent, false);
                   holder = new ViewHolder();
                   holder.imgIcon = (ImageView) convertView.findViewById(R.id.img_grid_photo);
                   holder.objectID = user.getImgObjectID();
                   convertView.setTag(holder);
//                   imageView = new ImageView(getContext());
//                   imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
//                   imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//                   imageView.setPadding(8, 8, 8, 8);
               } else {
                   //imageView = (ImageView) convertView;
                   holder = (ViewHolder) convertView.getTag();
               }

               //imageView.setImageResource(mThumbIds[position]);
              try{

                  mLoadImageTask = new LoadImageTask(holder.imgIcon, user.getImgFileName(), activity);
                  mLoadImageTask.execute(holder.imgIcon);
                  holder.imgIcon.setTag(holder.imgIcon.getId(), user.getImgObjectID());
                  holder.imgIcon.setOnClickListener(
                          new View.OnClickListener() {
                              @Override
                              public void onClick(View view) {
//                                  String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
//                                  Toast.makeText(activity_group_images.this, outputText,
//                                          Toast.LENGTH_SHORT).show();
                                  showFullThumbnail(String.valueOf(view.getTag(view.getId())));
                                  //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                                  //OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                              }
                          });


              }
              catch(Exception e){
                  Log.i("Feed_Main", e.getMessage());
              }
               //return imageView;
              return convertView;
           }

           // references to our images
//                  private Integer[] mThumbIds = {
//                          R.drawable.sample_2, R.drawable.sample_3,
//                          R.drawable.sample_4, R.drawable.sample_5,
//                          R.drawable.sample_6, R.drawable.sample_7,
//                          R.drawable.sample_0, R.drawable.sample_1,
//                          R.drawable.sample_2, R.drawable.sample_3,
//                          R.drawable.sample_4, R.drawable.sample_5,
//                          R.drawable.sample_6, R.drawable.sample_7,
//                          R.drawable.sample_0, R.drawable.sample_1,
//                          R.drawable.sample_2, R.drawable.sample_3,
//                          R.drawable.sample_4, R.drawable.sample_5,
//                          R.drawable.sample_6, R.drawable.sample_7
//                  };
       }

         private class GroupImages{
             private String feed_objectID;
             private String image_objectID;
             private String image_filename;
             private String image_filename_full;

             public String getImgFileName(){return this.image_filename;}
             public String getImgFileNameFull(){return this.image_filename_full;}
             public String getImgObjectID(){return this.image_objectID;}

             public GroupImages(ParseObject Obj, final Activity activity) {
                 this.feed_objectID = Obj.getParseObject("GlobalFeed_objectId").getObjectId();
                 this.image_objectID = Obj.getObjectId();
                 this.image_filename = this.feed_objectID + "_" + this.image_objectID + "_Icon";
                 this.image_filename_full = this.feed_objectID + "_" + this.image_objectID;

                 final String Filename = this.image_filename;
                 if(!IsBitmapCached(image_filename ,activity)){
                     ParseFile Picture = Obj.getParseFile("Image_Icon");
                     Picture.getDataInBackground(new GetDataCallback() {
                         @Override
                         public void done(byte[] data, ParseException e) {

                             if (e == null) {
                                 Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                         data.length);
                                 storeImage(PictureBmp, Filename, activity);

                             } else {

                             }

                         }
                     });
                 }
             }

             public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
             {

                 try {
                     ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                     // path to /data/data/yourapp/app_data/imageDir
                     File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                     File f=new File(directory, BitmapName + ".png");
                     return BitmapFactory.decodeStream(new FileInputStream(f));
                     //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                     //img.setImageBitmap(b);
                     //return b;
                 }
                 catch (Exception e)
                 {
                     e.printStackTrace();

                 }

                 return null;
             }

             private boolean IsBitmapCached(String FileName, Activity activity){
                 try {
                     ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                     // path to /data/data/yourapp/app_data/imageDir
                     File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                     File f=new File(directory, FileName + ".png");
                     Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                     if (b != null)
                         return true;

                 }
                 //catch (FileNotFoundException e)
                 catch (Exception e)
                 {
                     e.printStackTrace();
                 }

                 return false;
             }

             public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
                 ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                 // path to /data/data/yourapp/app_data/imageDir
                 File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                 // Create imageDir
                 File mypath=new File(directory, FileName + ".png");

                 FileOutputStream fos = null;
                 try {

                     fos = new FileOutputStream(mypath);

                     // Use the compress method on the BitMap object to write image to the OutputStream
                     bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                     fos.close();
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
                 //return directory.getAbsolutePath();
             }
         }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 150, 150, activity);

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    private void DisplayFullImage(Bitmap bmp){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        //Bitmap IMG = GlobalPost.LoadPNG(imgFileName, this);
        ivPreview.setImageBitmap(bmp);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void DisplayFullImage(String imgFileName){
        mDialogImageLoad.dismiss();
        final Dialog nagDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(false);
        nagDialog.setContentView(R.layout.theme_fullimage_dialog);
        Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
//        ParseImageView ivPreview = (ParseImageView)nagDialog.findViewById(R.id.iv_preview_image);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview = IMG;
//        ivPreview.setParseFile(IMG);
//        ivPreview.loadInBackground();
        Bitmap IMG = GlobalPost.LoadPNG(imgFileName, this);
        ivPreview.setImageBitmap(IMG);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    private void LoadFullImage(ParseObject Obj, final String Filename){

        final Activity act = this;
        //store image into cache
        if(!GlobalPost.CheckPNGCache(Filename, this)){
            ParseFile Picture = Obj.getParseFile("Image");
            Picture.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                data.length);
                        GlobalPost.StorePNG(PictureBmp, Filename, act);
                        DisplayFullImage(PictureBmp);

                    } else {

                    }

                }
            });
        }
        else
        {
            DisplayFullImage(Filename);
        }

    }

    public void showFullThumbnail(final String ImageObjID){

        final String Filename = mLHM_Images.get(ImageObjID).getImgFileNameFull();

        if(GlobalPost.CheckPNGCache(Filename, this)){
            DisplayFullImage(Filename);
        }
        else{
            mDialogImageLoad.show();
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Images");
            query.whereEqualTo("objectId", ImageObjID);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                        if(scoreList.size()>0)
                            LoadFullImage(scoreList.get(0), Filename);
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
