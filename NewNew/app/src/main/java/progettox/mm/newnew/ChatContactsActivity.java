package progettox.mm.newnew;

import android.app.Activity;
 import android.app.ProgressDialog;
 import android.content.Context;
 import android.content.ContextWrapper;
 import android.content.DialogInterface;
 import android.content.Intent;
 import android.graphics.Bitmap;
 import android.graphics.BitmapFactory;
 import android.graphics.Point;
 import android.graphics.drawable.Drawable;
 import android.os.AsyncTask;
 import android.os.Bundle;
 import android.support.v7.app.ActionBarActivity;
 import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
 import android.util.SparseBooleanArray;
 import android.view.Display;
 import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
 import android.view.ViewGroup;
 import android.view.WindowManager;
 import android.widget.ArrayAdapter;
 import android.widget.Button;
 import android.widget.ImageView;
 import android.widget.ListView;
 import android.widget.TextView;

 import com.google.android.gms.games.Player;
 import com.google.gson.Gson;
 import com.parse.FindCallback;
 import com.parse.GetCallback;
 import com.parse.GetDataCallback;
 import com.parse.ParseException;
 import com.parse.ParseFile;
 import com.parse.ParseImageView;
 import com.parse.ParseObject;
 import com.parse.ParseQuery;
 import com.parse.SaveCallback;

 import org.apache.http.HttpResponse;
 import org.apache.http.HttpStatus;
 import org.apache.http.StatusLine;
 import org.apache.http.client.HttpClient;
 import org.apache.http.client.methods.HttpGet;
 import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
 import org.json.JSONObject;

 import java.io.BufferedInputStream;
 import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
 import java.io.FileInputStream;
 import java.io.FileNotFoundException;
 import java.io.FileOutputStream;
 import java.io.IOException;
 import java.io.InputStream;
 import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
 import java.net.URL;
 import java.util.ArrayList;
 import java.util.Collections;
 import java.util.Date;
 import java.util.Hashtable;
 import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
  * Created by Admin on 21/5/2015.
  */
 public class ChatContactsActivity extends AppCompatActivity {

     public String mPlayerID;
     public String mPlayer_objectId;
     public String Chat_ObjectID;
     private ProgressDialog mDialog;
     public App mAppState;
     public final static String TITLE = "CONTACTS";

     public int mScreenHeight;
     public int mScreenWidth;
     public ListView listView;
     private ArrayList<PlayerFriend> PlayerFriendsArray;
     private FriendListAdapter mAdapter;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.chat_main_list);

     }

     private void onStartInitialize(){
         mAppState = ((App)getApplicationContext());
         mPlayerID = mAppState.getPlayerID();
         mPlayer_objectId = mAppState.getPlayer_objectId();

         //SET activity Title
         setTitle(TITLE);

         //since we're in the games territory, we want to go back to game session page
         //in main menu when user presses back
         mAppState.setGameNotification(false);
         mAppState.setPageNumber(11);

         mDialog = new ProgressDialog(this);
         mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
         mDialog.setMessage("Retrieving Your Contacts, please wait...");
         mDialog.setIndeterminate(true);
         mDialog.setCanceledOnTouchOutside(false);
         //mDialog.show();
         //getChatSessionIDs();

         WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
         Display display = wm.getDefaultDisplay();
         Point size = new Point();
         display.getSize(size);
         mScreenWidth = size.x;
         mScreenHeight = size.y;

         listView = (ListView) findViewById(R.id.lvItems);

 //        Button btnChat = (Button)findViewById(R.id.chat_action);
 //
 //        btnChat.setOnClickListener(new View.OnClickListener() {
 //
 //            @Override
 //            public void onClick(View v) {
 //                openChatPage();
 //            }
 //        });

     }

     public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
         ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
         for (int i = 0; i < jsonObjects.size(); i++) {
             try {
                 GameHistory.add(new PlayerFriend(jsonObjects.get(i), i+1, this));
             } catch (Exception e) {
                 e.printStackTrace();
             }
         }

         return GameHistory;
     }

     private void displayJSONResults(List<ParseObject> data)
     {

         try{
             PlayerFriendsArray = fromJson(data);

             Log.i("Finally", PlayerFriendsArray.get(0).PlayerID);

             // Construct the data source
             ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
             // Create the adapter to convert the array to views
             mAdapter = new FriendListAdapter(this, R.layout.chat_contact_item, arrayOfUsers, this);
             mAdapter.addAll(PlayerFriendsArray);
             // Attach the adapter to a ListView
             ListView listView = (ListView) findViewById(R.id.lvItems);
             listView.setAdapter(mAdapter);
             mDialog.dismiss();
             ///////////
 //            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
 //                @Override
 //                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
 //                    PlayerFriend listItem = PlayerFriendsArray.get(position);
 //                    Log.i("Item selected is ", listItem.GameName);
 //                }
 //            });
             ///////////


         }
         catch(Exception e){
             Log.i("Exception fromJson", e.getMessage());
         }

     }

     private void getChatFriendsList(){


         ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
         ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
         query.whereEqualTo("Player_objectId", obj);
         query.whereEqualTo("Status", 1);
         //query.whereEqualTo("PlayerID", mPlayer_objectId);
         query.include("Friend_objectId");

         query.findInBackground(new FindCallback<ParseObject>() {
             public void done(List<ParseObject> friendList, ParseException e) {
                 if (e == null) {
                     Log.d("score", "Retrieved " + friendList.size() + " scores");
                     //PlayerHistoryResults = scoreList;

                     if(friendList.size()>0)
                         displayJSONResults(friendList);
                     else
                         mDialog.dismiss();
                 } else {
                     Log.d("score", "Error: " + e.getMessage());
                 }
             }
         });

         //String testURL = "http://samplewebapi2013.azurewebsites.net/api/JsonFile/GetFriendList?playerId=louie";
//         String testURL = "http://192.168.0.11:1028/AngularJSAuthentication.API/token";
//         getData newTask = new getData(testURL);
//         newTask.execute();

                 //String testURL = "http://niuappsnf.azurewebsites.net/token";
//                 String testURL = "http://192.168.0.11:1028/AngularJSAuthentication.API/token";
//                 LogIn newTask = new LogIn(testURL);
//                 newTask.execute();

     }

     @Override
     protected void onStart() {
         super.onStart();  // Always call the superclass method first
         onStartInitialize();
     }

     @Override
     public void onResume() {
         super.onResume();  // Always call the superclass method first
         //receiveMessage();
         mDialog.show();
         getChatFriendsList();

     }

     public class PlayerFriend {
         public String objectId;
         public String PlayerID;
         public String FriendID;
         public String Friend_objectId;
         public String ChatID; //this will not be empty if there is an active chat
         public Boolean IsChatBuddies;
         public Drawable icon;
         public String applicationName;
         //public ParseFile profilePic;

         public PlayerFriend(ParseObject Obj, int c, final Activity activity) {
             this.objectId = Obj.getObjectId();

             this.PlayerID = Obj.getString("PlayerID");
             this.FriendID = Obj.getString("FriendID");
             //this.profilePic = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
             this.Friend_objectId = Obj.getParseObject("Friend_objectId").getObjectId();
             this.ChatID = Obj.getString("Chat_ObjectID");

             if (this.ChatID != null){
                 this.IsChatBuddies = true;
             }
             else {
                 this.IsChatBuddies = false;}

             if(!IsBitmapCached(this.Friend_objectId, activity)){
                 ParseFile Picture = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
                 Picture.getDataInBackground(new GetDataCallback() {
                     @Override
                     public void done(byte[] data, ParseException e) {

                         if (e == null) {
                             Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                     data.length);
                             storeImage(PictureBmp, Friend_objectId, activity);

                         } else {

                         }

                     }
                 });
             }
         }

         private boolean IsBitmapCached(String FileName, Activity activity){
             try {
                 ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                 // path to /data/data/yourapp/app_data/imageDir
                 File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                 File f=new File(directory, FileName + ".png");
                 Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                 if (b != null)
                     return true;

             }
             catch (FileNotFoundException e)
             {
                 e.printStackTrace();
             }

             return false;
         }

         public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
             ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
             // path to /data/data/yourapp/app_data/imageDir
             File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
             // Create imageDir
             File mypath=new File(directory, FileName + ".png");

             FileOutputStream fos = null;
             try {

                 fos = new FileOutputStream(mypath);

                 // Use the compress method on the BitMap object to write image to the OutputStream
                 bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                 fos.close();
             } catch (Exception e) {
                 e.printStackTrace();
             }
             //return directory.getAbsolutePath();
         }

         public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
         {

             try {
                 ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                 // path to /data/data/yourapp/app_data/imageDir
                 File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                 File f=new File(directory, BitmapName + ".png");
                 return BitmapFactory.decodeStream(new FileInputStream(f));
                 //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                 //img.setImageBitmap(b);
                 //return b;
             }
             catch (FileNotFoundException e)
             {
                 e.printStackTrace();
             }

             return null;
         }


     }

     public class FriendListAdapter extends ArrayAdapter<PlayerFriend> {
         Context context;
         int layoutResourceId;
         ArrayList<PlayerFriend> data = null;
         Activity activity;

         public FriendListAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
             super(context, layoutResourceId,data);
             this.layoutResourceId = layoutResourceId;
             this.context = context;
             this.data = data;
             this.activity = act;
         }

         @Override
         public View getView(int position, View convertView, ViewGroup parent) {
             // Get the data item for this position
             final PlayerFriend user = getItem(position);
             // Check if an existing view is being reused, otherwise inflate the view
             if (convertView == null) {
                 convertView = LayoutInflater.from(
                         getContext()).inflate(layoutResourceId, parent, false);
             }
             // Lookup view for data population
             TextView tvBody = (TextView) convertView.findViewById(R.id.tvBody);
             final ImageView ivProfileLeft = (ImageView) convertView.findViewById(R.id.ivProfileLeft);

             //now we determine what to display on the chat history list
             //first we determine what user and user image to put


             tvBody.setText(user.FriendID);
             ivProfileLeft.setImageBitmap(user.loadImageFromStorage(user.Friend_objectId, activity));
 //            ivProfileLeft.setParseFile(user.profilePic);
 //            ivProfileLeft.loadInBackground(new GetDataCallback() {
 //                public void done(byte[] data, ParseException e) {
 //
 //                    ivProfileLeft.getLayoutParams().width = mScreenWidth / 6;
 //                    ivProfileLeft.getLayoutParams().height = mScreenWidth / 6;
 //                    ivProfileLeft.requestLayout();
 //                }
 //
 //            });

             //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
             View PrimaryColumn = convertView.findViewById(R.id.primary_target);
             PrimaryColumn.setTag(PrimaryColumn.getId(), user.Friend_objectId);
             //Then we set the onClick function here to pull the GameSession_objectId upon each click
             PrimaryColumn.setOnClickListener(
                     new View.OnClickListener() {
                         @Override
                         public void onClick(View view) {
                             onFriendTouched(user.FriendID, user.Friend_objectId, user.ChatID);
                         }
                     });






             // Return the completed view to render on screen
             return convertView;
         }

     }

     private void createNewUserList(String ObjID, String Friend_ObjectID, final String FriendID){


         final String chatID = ObjID;
         final String friendObjID = Friend_ObjectID;

         //gameScore.put("User_objectId", ParseObject.createWithoutData("_User", Player_objectId));
         List<ParseObject> PlayerList = new ArrayList<ParseObject>();
         final ParseObject NewPlayer = new ParseObject("Chat_UserList");
         final ParseObject NewPlayerSelf = new ParseObject("Chat_UserList");

         NewPlayer.put("Chat_ObjectID", chatID);
         NewPlayer.put("Chat_objectId", ParseObject.createWithoutData(
                 "Chat_Master", chatID));
         NewPlayer.put("User_ObjectID", Friend_ObjectID);
         NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                 "_User", Friend_ObjectID));
         PlayerList.add(NewPlayer);

         NewPlayerSelf.put("Chat_ObjectID", chatID);
         NewPlayerSelf.put("Chat_objectId", ParseObject.createWithoutData(
                 "Chat_Master", chatID));
         NewPlayerSelf.put("User_ObjectID", mPlayer_objectId);
         NewPlayerSelf.put("User_objectId", ParseObject.createWithoutData(
                 "_User", mPlayer_objectId));
         PlayerList.add(NewPlayerSelf);


         ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

             @Override
             public void done(ParseException e) {
                 // TODO Auto-generated method stub
                 //do something
                // Log.i("Add new game completed", e.getMessage());
                 GotoChatActivity(chatID, FriendID);

             }
         });
     }

     private void attachChatIDTOFriends(String ChatID, String FriendID){
         final String chatID = ChatID;
         ParseQuery query = new ParseQuery("User_Friends");
         query.whereEqualTo("PlayerID", mPlayerID);
         query.whereEqualTo("FriendID", FriendID);
         query.getFirstInBackground(new GetCallback<ParseObject>() {
             public void done(ParseObject object, ParseException e) {
                 if (object == null) {
                     Log.d("OBJECT ID IS ", object.getObjectId());
                 } else {
                     //Log.d("score", "Retrieved the object.");
                     object.put("Chat_ObjectID", chatID);
                     object.put("Chat_objectId", ParseObject.createWithoutData(
                             "Chat_Master", chatID));
                     object.saveInBackground();
                 }
             }
         });

         query = new ParseQuery("User_Friends");
         query.whereEqualTo("PlayerID", FriendID);
         query.whereEqualTo("FriendID", mPlayerID);
         query.getFirstInBackground(new GetCallback<ParseObject>() {
             public void done(ParseObject object, ParseException e) {
                 if (object == null) {
                     Log.d("OBJECT ID IS ", object.getObjectId());
                 } else {
                     //Log.d("score", "Retrieved the object.");
                     object.put("Chat_ObjectID", chatID);
                     object.put("Chat_objectId", ParseObject.createWithoutData(
                             "Chat_Master", chatID));
                     object.saveInBackground();
                 }
             }
         });

     }

     private void createNewChatSession(String FriendID, String Friend_ObjectID){

         final ParseObject gameScore = new ParseObject("Chat_Master");
         final String friendObjID = Friend_ObjectID;
         final String friendID = FriendID;

         gameScore.put("IsGroup", false);
         gameScore.saveInBackground(new SaveCallback() {
             public void done(ParseException e) {
                 if (e == null) {
                     // Saved successfully.
                     createNewUserList(gameScore.getObjectId(), friendObjID, friendID);
                     attachChatIDTOFriends(gameScore.getObjectId(), friendID );
                 } else {
                     // The save failed.
                     Log.d("Error", e.getMessage());
                 }
             }
         });
     }

     private void onFriendTouched(String FriendID, String Friend_ObjectID, String ChatID){
         //First we need to see if any existing conversation between you and this contact
        if (ChatID != null){
            if(!ChatID.equals(""))
                GotoChatActivity(ChatID, FriendID);
            else
                createNewChatSession(FriendID, Friend_ObjectID);
        }
         else{
            //means you have no previous conversation with this contact
            createNewChatSession(FriendID, Friend_ObjectID);
        }
     }


     public void GotoChatActivity(String ChatID, String Username){
         Intent intent = new Intent(this, ChatActivity.class);

         //intent.putExtra("myObject", new Gson().toJson(mHTChatItemList.get(ChatID)));
         //Create the bundle
         Bundle bundle = new Bundle();

         //Add your data to bundle
         bundle.putString("ChatID", ChatID);
         bundle.putString("ChatUsername", Username);

         //Add the bundle to the intent
         intent.putExtras(bundle);

         startActivity(intent);
     }

     public class getData extends AsyncTask<String, String, String> {

         HttpURLConnection urlConnection;
         String URLHttp;

         getData(String urlTest){
             URLHttp = urlTest;
         }

         @Override
         protected String doInBackground(String... args) {

             StringBuilder result = new StringBuilder();

             try {



                 //URL url = new URL("https://api.github.com/users/dmnugent80/repos");
                 URL url = new URL(URLHttp);

                 urlConnection = (HttpURLConnection) url.openConnection();

                 InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                 BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                 String line;
                 while ((line = reader.readLine()) != null) {
                     result.append(line);
                 }

             }catch( Exception e) {
                 e.printStackTrace();
             }
             finally {
                 urlConnection.disconnect();
             }


             return result.toString();
         }

         @Override
         protected void onPostExecute(String result) {

             //Do something with the JSON string
             String temp = result;
             Log.i("JSONResults", temp);
             Gson gson = new Gson();
             try {

                 JSONObject obj = new JSONObject(result);

                 //JSONArray data = obj.getJSONArray(obj.getString("d"));
                 String testArray = obj.getString("d");
                 JSONArray data = new JSONArray(testArray);
                         //String friendID = data.getString("friendID");
                 //String playerID = data.getString("playerID");

                 Log.d("My App", obj.toString());

             } catch (Throwable t) {
                 Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
             }


         }

     }

    public class LogIn extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        LogIn(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                JSONObject jsonParam = new JSONObject();
                jsonParam.put("grant_type", "password");
                jsonParam.put("username", "sridhar");
                jsonParam.put("password", "sridhar");
                jsonParam.put("client_id", "Android");
                jsonParam.put("client_secret", "abc@123");

                Hashtable<String, String> temp = new Hashtable<>();
                temp.put("grant_type", "password");
                temp.put("username", "sridhar");
                temp.put("password", "sridhar");
                temp.put("client_id", "Android");
                temp.put("client_secret", "abc@123");

                String URLArgs = temp.toString();


                //URL url = new URL("http://192.168.1.181:8080/AngularJSAuthentication.API/token");
                URL url = new URL(URLHttp);

                urlConn = (HttpURLConnection) url.openConnection();
                //urlConn = url.openConnection();
                urlConn.setDoInput (true);
                urlConn.setDoOutput (true);
                urlConn.setUseCaches (false);
                urlConn.setRequestProperty("Content-Type","application/json");
                //urlConn.setRequestMethod("POST");
//                urlConn.setRequestProperty("Content-Length", "" +
//                        Integer.toString(jsonParam.length()));
                //urlConn.setRequestProperty("Content-Language", "en-US");
                urlConn.connect();
//Create JSONObject here


//                DataOutputStream wr = new DataOutputStream(urlConn.getOutputStream ());
//                wr.writeBytes(jsonParam.toString());
//                wr.flush();
//                wr.close();
                OutputStream os = urlConn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                URLArgs = "grant_type=password&username=sridhar&password=sridhar&client_id=Android&client_secret=abc%40123";
                writer.write(URLArgs);
                writer.flush();
                writer.close();
                os.close();


                //Get Response
//                InputStream is = urlConn.getInputStream();
//                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//                String lines;
//                StringBuffer response = new StringBuffer();
//                while((lines = rd.readLine()) != null) {
//                    response.append(lines);
//                    response.append('\r');
//                }
//                rd.close();
                //return response.toString();


                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }




                //InputStream in = new BufferedInputStream(urlConn.getInputStream());

//                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    result.append(line);
//                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONObject obj = new JSONObject(result);

                //JSONArray data = obj.getJSONArray(obj.getString("d"));
                //String testArray = obj.getString("d");
                //JSONArray data = new JSONArray(testArray);
                //String friendID = data.getString("friendID");
                //String playerID = data.getString("playerID");

                Log.d("My App", obj.toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

     @Override
     public boolean onCreateOptionsMenu(Menu menu) {

         getMenuInflater().inflate(R.menu.find_contacts, menu);

         return super.onCreateOptionsMenu(menu);
     }

     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         // Handle action bar item clicks here. The action bar will
         // automatically handle clicks on the Home/Up button, so long
         // as you specify a parent activity in AndroidManifest.xml.
         int id = item.getItemId();

         if (id == R.id.home) {
             finish();
             return true;
         }

         //noinspection SimplifiableIfStatement
         if (id == R.id.action_search_contacts) {
             Intent intent = new Intent(this, activity_individual_find.class);
             intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
             startActivity(intent);
             return true;
         }



         return super.onOptionsItemSelected(item);
     }

    public class LoginResponse{

        private String access_token;

        public void setAccess_token(String tkn) { this.access_token=tkn;}
        public String getAccess_token(){return this.access_token;}
    }
 }
