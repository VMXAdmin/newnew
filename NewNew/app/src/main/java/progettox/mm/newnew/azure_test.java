package progettox.mm.newnew;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Admin on 6/8/2015.
 */
public class azure_test extends AppCompatActivity {


    public String mPlayerID;
    public String mPlayer_objectId;
    public App mAppState;
    private TextView mResultTxtView;
    private ImageView mImage;
    private String WebAPI_ChatHistory_URL = "Api/ChatHistory/";
    private String WebAPI_ChatUserList_URL = "Api/ChatUserList/";

    private void Initialize() {

        mAppState = ((App) getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        mImage = (ImageView) findViewById(R.id.img_azure);
        mResultTxtView = (TextView)findViewById(R.id.txt_content);

        Button mBtnGroup = (Button) findViewById(R.id.btn_getchat_byid);
        mBtnGroup.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //getChatByColumnAndValue();
                        //get_ChatUserList_ByChatID(String.valueOf(2));
                        testChatWebAPI();

                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.azure_layout_test);
        Initialize();

    }

    private void get_ChatUserList_ByChatID(String Chat_objectId){

        String testURL =
                mAppState.getWebAPIRootURL() + WebAPI_ChatUserList_URL +
                        "GetByChatID/" + Chat_objectId;
        AT_get_ChatUserList_ByChatID newTask = new AT_get_ChatUserList_ByChatID(testURL);
        newTask.execute();
    }

    private void getChatById(){
        String testURL = "http://192.168.1.181:1028/AngularJSAuthentication.API/Api/ChatHistory/17";
        getChatById newTask = new getChatById(testURL);
        newTask.execute();
    }

    private void getChatByColumnAndValue(){
//        String testURL =
//                "http://192.168.1.181:1028/AngularJSAuthentication.API/Api/ChatHistory/GetByColumnNameAndValue?ColumnName=Chat_objectId&ColumnValue=8";
        String testURL =
                "http://192.168.1.181:1028/AngularJSAuthentication.API/Api/ChatHistory/GetByColumnNameAndValueMain?ColumnName=Chat_objectId&ColumnValue=8";
        getChatById newTask = new getChatById(testURL);
        newTask.execute();
    }

    public class AT_get_ChatUserList_ByChatID extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        AT_get_ChatUserList_ByChatID(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(URLHttp);
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONArray data = new JSONArray(result);

                Log.d("My App", data.get(0).toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

    public class getChatById extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        getChatById(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {

                //URL url = new URL("http://192.168.1.181:8080/AngularJSAuthentication.API/token");
                URL url = new URL(URLHttp);

                urlConn = (HttpURLConnection) url.openConnection();
                //urlConn = url.openConnection();
//                urlConn.setDoInput (true);
//                urlConn.setDoOutput (true);
//                urlConn.setUseCaches (false);
//                urlConn.setRequestMethod("GET");
//                urlConn.setRequestProperty("Content-Type","application/json");
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                //urlConn.connect();

//                OutputStream os = urlConn.getOutputStream();
//                BufferedWriter writer = new BufferedWriter(
//                        new OutputStreamWriter(os, "UTF-8"));
//                //URLArgs = "grant_type=password&username=sridhar&password=sridhar&client_id=Android&client_secret=abc%40123";
//                writer.write(URLArgs);
//                writer.flush();
//                writer.close();
//                os.close();


                //Get Response
//                InputStream is = urlConn.getInputStream();
//                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//                String lines;
//                StringBuffer response = new StringBuffer();
//                while((lines = rd.readLine()) != null) {
//                    response.append(lines);
//                    response.append('\r');
//                }
//                rd.close();
                //return response.toString();


                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }




                //InputStream in = new BufferedInputStream(urlConn.getInputStream());

//                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    result.append(line);
//                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                //GOOD EXAMPLE OF GETTING IMAGE - DO NOT DELETE
                //JSONObject obj = new JSONObject(result);
                JSONArray data = new JSONArray(result);
//                mResultTxtView.setText(obj.toString());
//                if(obj.getString("Image") != null){
//                    String tempStr = obj.getString("Image");
//                    byte[] x = Base64.decode(tempStr, Base64.DEFAULT);  //convert to base64
//                    Bitmap theImage = BitmapFactory.decodeByteArray(x,0,x.length);
//
//                    mImage.setImageBitmap(theImage);
//                }


                //GOOD EXAMPLE OF GETTING IMAGE

                //JSONArray data = obj.getJSONArray(obj.getString("d"));
                //String testArray = obj.getString("access_token");
                //mAppSate.setToken(testArray);
                //JSONArray data = new JSONArray(testArray);
                //String friendID = data.getString("friendID");
                //String playerID = data.getString("playerID");

                Log.d("My App", data.get(0).toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

    private void testChatWebAPI(){
        String testURL = "http://192.168.1.181:1028/AngularJSAuthentication.API/Api/ChatHistory/Add";
        insertChat newTask = new insertChat(testURL);
        newTask.execute();
    }

    public class insertChat extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        insertChat(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {

                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_feed_liked);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] bitmapdata = stream.toByteArray();
                String imgAzure = Base64.encodeToString(bitmapdata,0);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("objectId", 0);
                jsonParam.put("Chat_objectId", 8);
                jsonParam.put("User_objectId", 9);
                jsonParam.put("Body", "KONG LIANG");
                jsonParam.put("IsImage", "true");
                jsonParam.put("Image", imgAzure);
                jsonParam.put("FileName", null);
                java.util.Date utilDate = new java.util.Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                jsonParam.put("createdAt", sqlDate);
                jsonParam.put("updatedAt", sqlDate);

                String URLArgs = jsonParam.toString();


                //URL url = new URL("http://192.168.1.181:8080/AngularJSAuthentication.API/token");
                URL url = new URL(URLHttp);

                urlConn = (HttpURLConnection) url.openConnection();
                //urlConn = url.openConnection();
                urlConn.setDoInput (true);
                urlConn.setDoOutput (true);
                urlConn.setUseCaches (false);
                urlConn.setRequestProperty("Content-Type","application/json");
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                urlConn.connect();

                OutputStream os = urlConn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                //URLArgs = "grant_type=password&username=sridhar&password=sridhar&client_id=Android&client_secret=abc%40123";
                writer.write(URLArgs);
                writer.flush();
                writer.close();
                os.close();


                //Get Response
//                InputStream is = urlConn.getInputStream();
//                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//                String lines;
//                StringBuffer response = new StringBuffer();
//                while((lines = rd.readLine()) != null) {
//                    response.append(lines);
//                    response.append('\r');
//                }
//                rd.close();
                //return response.toString();


                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }




                //InputStream in = new BufferedInputStream(urlConn.getInputStream());

//                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    result.append(line);
//                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONObject obj = new JSONObject(result);

                //JSONArray data = obj.getJSONArray(obj.getString("d"));
                int ObjID = obj.getInt("objectId");
                //mAppSate.setToken(testArray);
                //JSONArray data = new JSONArray(testArray);
                //String friendID = data.getString("friendID");
                //String playerID = data.getString("playerID");

                Log.d("My App", obj.toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

}
