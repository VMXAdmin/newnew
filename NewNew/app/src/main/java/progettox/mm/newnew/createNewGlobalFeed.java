package progettox.mm.newnew;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URI;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class createNewGlobalFeed extends ActionBarActivity {



    //private List<ParseObject> PlayerHistoryResults;
    public String PlayerID;
    public String Player_objectId;

    //public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    static final int REQUEST_TAKE_PHOTO = 1;
    private String mCurrentPhotoPath;

    public TextView mImgLoadingText;
    public MenuItem mNextButton;

    public App appState;
    private ProgressDialog mDialog;
    private static final int CAMERA_REQUEST = 1888;
    private static final int AttachFileRequestCode = 2888;
    //private ImageView imageView;
    private ArrayList<Bitmap> mUserPhotoArray;
    private ArrayList<ParseFile> mParsePhotoArray;
    private ArrayList<ParseFile> mParsePhotoIconArray;
    private ArrayList<ParseFile> mParsePhotoThumbArray;
    //public Hashtable<String, GlobalPost> mGlobalPost_List;


    //for group feed
    private String mGroup_ObjectID;
    private String mGroup_Name;
    private int mGroup_Type; //2 is Open, 4 is CLOSED, 5 is SECRET
    private Boolean mIsGlobalFeed;
    private Boolean mIsUserFeed;
    private Boolean mIsGroupFeed;
    private Boolean mIsTutorFeed;
    private TextView mPostTarget;
    private LinearLayout mLayoutImages;
    private int mViewIndex;

    //for user feed
    private String mUser_ObjectID;
    private String mUser_Name;
    private int mImageIndex;
    //private Uri mImageUri;
    public int mScreenHeight;
    public int mScreenWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_globalfeed_form);

        //initialize var
        mIsGlobalFeed = false;
        mIsTutorFeed = false;
        mIsUserFeed = false;
        mIsGroupFeed = false;
        mImageIndex = 0;
        mViewIndex = 0;

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;

        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){

            //get to know which type of feed submission first
            int temp = bundle.getInt("Type");
            if(temp == 1) {
                mIsGlobalFeed = true;
            }
            else if (temp == 2) {
                mIsGroupFeed = true;
                mGroup_ObjectID = bundle.getString("GroupID");
                mGroup_Name = bundle.getString("GroupName");
                mGroup_Type = bundle.getInt("GroupType");
                mIsTutorFeed = bundle.getBoolean("IsTutor");
            }
            else if (temp == 3) {
                mIsUserFeed = true;
                mUser_ObjectID = bundle.getString("UserID");
                mUser_Name = bundle.getString("UserName");
            }


//            mGroup_ObjectID = bundle.getString("GroupID");
//            mGroup_Name = bundle.getString("GroupName");
//            if(mGroup_Name != null){
//                mIsGroupFeed = true;
//
//                //check if this is posting to globalFeed
//                if(mGroup_ObjectID.equals("global")){
//                    mIsGroupFeed = false;
//                    mIsGlobalFeed = true;
//                }
//
//            }
//            else
//            {
//                mIsGroupFeed = false;
//            }



        }
//        else
//        {
//            mIsGroupFeed = false;
//        }
//

        appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();

        //populateSpinners();

        //
        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Saving your new Post, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        //mGameName = (EditText) findViewById(R.id.txt_gameName);
        //initialize camera button
        //this.imageView = (ImageView)this.findViewById(R.id.img1);
        ImageButton cameraBtn = (ImageButton) findViewById(R.id.camera_action);
        cameraBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(cameraIntent, CAMERA_REQUEST);


                //openCamera();
                dispatchTakePictureIntent();
            }
        });

        ImageButton attachBtn = (ImageButton) findViewById(R.id.attach_action);
        attachBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent,AttachFileRequestCode);
            }
        });

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            //Bitmap bmp = savedInstanceState.getParcelable("PostImage");
            ArrayList<Bitmap> temp = savedInstanceState.getParcelableArrayList("PostImage");
            if(temp != null){
                mUserPhotoArray = temp;
                //imageView.setImageBitmap(mUserPhoto);
            }

        } else {
            // Probably initialize members with default values for a new instance
        }

        //set Target label
        mPostTarget = (TextView) findViewById(R.id.txt_target_name);
        if(mIsGroupFeed)
            mPostTarget.setText(mGroup_Name);
        else if (mIsUserFeed)
            mPostTarget.setText(mUser_Name);
        else if (mIsGlobalFeed)
            mPostTarget.setText("GLOBAL FEED"); //TODO chagne to String.xml

        mLayoutImages = (LinearLayout) findViewById(R.id.layout_images);

        mImgLoadingText = (TextView) findViewById(R.id.lbl_img_loading);
    }

//    private void openCamera(){
//        Intent cameraIntent = new Intent(this, activity_surface_camera.class);
//        startActivityForResult(cameraIntent, CAMERA_REQUEST);
//    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        if(mUserPhotoArray != null){

            //savedInstanceState.putBitmapArrayList("PostImage",mUserPhotoArray);
            savedInstanceState.putParcelableArrayList("PostImage", mUserPhotoArray);
        }
    }

//    private void TakeCameraPhoto(){
//
//    }



    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

        //retrieveFeedCache();


    }

//    private void retrieveFeedCache(){
//        Gson gson = new Gson();
//        Type stringStringMap = new TypeToken<Hashtable<String, GlobalPost>>(){}.getType();
//
//        SharedPreferences sp = getSharedPreferences("OURINFO", 0);
//        String JSON = sp.getString("mGlobalPost_List", "");
//        mGlobalPost_List = gson.fromJson(JSON, stringStringMap);
//    }

    private void addImageToArray(Bitmap Img){
        if(mUserPhotoArray == null)
            mUserPhotoArray = new ArrayList<Bitmap>();

        mUserPhotoArray.add(Img);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        if(storageDir.isDirectory()){
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );


        }
        else{
            storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        }
// Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.getAbsolutePath();
        galleryAddPic();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //...
                Log.e("Error", ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(mCurrentPhotoPath);
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

//            Bitmap UserPhoto = (Bitmap) data.getExtras().get("data");
//
//            ImageView myImage = new ImageView(this);
//            myImage.setLayoutParams(new LinearLayout.LayoutParams(
//                    400,
//                    400));
//
//            myImage.setImageBitmap(UserPhoto);
//            mLayoutImages.addView(myImage);
//
//            //now add the image to the Array
//            addImageToArray(UserPhoto);
            File file = new File(mCurrentPhotoPath);

            if(file.exists()){

                //int imageWidth = mScreenWidth * 0.8;
                Bitmap UserPhoto = BitmapFactory.decodeFile(file.getAbsolutePath());
                int oriImageWidth =  UserPhoto.getWidth();
                int oriImageHeight = UserPhoto.getHeight();

                int imageWidth = UserPhoto.getWidth();
                int imageHeight = UserPhoto.getHeight();
                //check if photo exceeds
                if(UserPhoto.getWidth() >= mScreenWidth){
                    Double imageWidthDb = mScreenWidth * 0.8;
                    Double imageHeightDb = UserPhoto.getHeight() * 0.8;
                    if(imageHeightDb >= mScreenHeight){
                        imageHeightDb = mScreenHeight * 0.8;

                    }
                    imageWidth = imageWidthDb.intValue();
                    imageHeight = imageHeightDb.intValue();
//                    UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                            file.getAbsolutePath(), imageWidth, imageHeight);


                }

                BitmapFactory.Options bounds = new BitmapFactory.Options();
                bounds.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), bounds);

                BitmapFactory.Options opts = new BitmapFactory.Options();
                //Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                //BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                opts.inSampleSize = BitmapManager.calculateInSampleSize(oriImageWidth, oriImageHeight,
                        imageWidth, imageHeight);

                opts.inJustDecodeBounds = false;
                //return BitmapFactory.decodeResource(res, resId, options);
                Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
//                Bitmap bm = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                            file.getAbsolutePath(), imageWidth, imageHeight);

                ExifInterface exif = null;
                try{
                    exif = new ExifInterface(file.getAbsolutePath());
                }
                catch(Exception e){

                }

                String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

                int rotationAngle = 0;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

                Matrix matrix = new Matrix();
                matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
                //UserPhoto = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
                UserPhoto = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);

//                Bitmap UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                        file.getAbsolutePath(), mScreenWidth*0.85, mScreenHeight);

                ImageView myImage = new ImageView(this);
                myImage.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                myImage.setAdjustViewBounds(true);
                myImage.setImageBitmap(UserPhoto);
                //mLayoutImages.addView(myImage);

                //now add the image to the Array
                addImageToArray(UserPhoto);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(20, 0, 20, 25);
                myImage.setLayoutParams(lp);

                //add to Parse background as well
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                UserPhoto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] dataStream = stream.toByteArray();

                String imgFileName = "Image" + "_" + mImageIndex;
                //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
                ParseFile imgFile = new ParseFile (imgFileName + ".jpg", dataStream);
                if(mParsePhotoArray == null)
                    mParsePhotoArray = new ArrayList<ParseFile>();
                mParsePhotoArray.add(mImageIndex, imgFile);

                //save the thumbnail now
                saveImageThumbnail(file.getAbsolutePath(), mImageIndex);


                mImgLoadingText.setVisibility(View.VISIBLE);
                mNextButton.setVisible(false);
                mParsePhotoArray.get(mImageIndex).saveInBackground(new SaveCallback() {
                    public void done(ParseException e) {
                        // If successful add file to user and signUpInBackground
                        if(null == e){
                            mImgLoadingText.setVisibility(View.GONE);
                            mNextButton.setVisible(true);
                        }

                    }
                });
                mImageIndex++;

                //now to add caption for this newly added image
                TextView myImageCaption = new TextView(this);
                myImageCaption.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));

                myImageCaption.setText("Image " + mImageIndex);
                myImageCaption.setTextColor(getResources().getColor(R.color.inputText));

                //now to add remove button in case user changes mind
                ImageButton imageRemoveButton = new ImageButton(this);
                LinearLayout.LayoutParams params =  new LinearLayout.LayoutParams(120,120);
                imageRemoveButton.setLayoutParams(params);
                imageRemoveButton.setBackgroundResource(R.color.white);
                imageRemoveButton.setAdjustViewBounds(true);
                imageRemoveButton.setScaleType(ImageButton.ScaleType.CENTER_CROP);
                imageRemoveButton.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_remove));



                imageRemoveButton.setTag(imageRemoveButton.getId(), myImage);
                imageRemoveButton.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
                                mLayoutImages.removeView(view);
                                mLayoutImages.removeView((View)(view.getTag(view.getId())));

                            }
                        });

                //mLayoutImages.addView(myImageCaption);

                mLayoutImages.addView(imageRemoveButton);
                mViewIndex++;
                mLayoutImages.addView(myImage);
                mViewIndex++;





            }



        }

        if (requestCode == AttachFileRequestCode && resultCode == RESULT_OK) {

            Uri selectedImageUri = data.getData();
            String[] fileColumn = {MediaStore.Images.Media.DATA};
            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
                    null, null, null);
            imageCursor.moveToFirst();

            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
            String picturePath = imageCursor.getString(fileColumnIndex);

            Bitmap UserPhoto = BitmapFactory.decodeFile(picturePath);
            //check if photo exceeds
            if(UserPhoto.getWidth() >= mScreenWidth){
                Double imageWidthDb = mScreenWidth * 0.8;
                Double imageHeightDb = UserPhoto.getHeight() * 0.8;
                if(imageHeightDb >= mScreenHeight){
                    imageHeightDb = mScreenHeight * 0.8;

                }

                int imageWidth = imageWidthDb.intValue();
                int imageHeight = imageHeightDb.intValue();
                UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
                        picturePath, imageWidth, imageHeight);


            }

//            UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                    picturePath, 350, 350);

            ImageView myImage = new ImageView(this);
            myImage.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            myImage.setAdjustViewBounds(true);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(20, 0, 20, 25);
            myImage.setLayoutParams(lp);

            myImage.setImageBitmap(UserPhoto);
            //mLayoutImages.addView(myImage);

            //now add the image to the Array
            addImageToArray(UserPhoto);





            //add to Parse background as well
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            UserPhoto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            byte[] dataStream = stream.toByteArray();

            String imgFileName = "Image" + "_" + mImageIndex;
            //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
            ParseFile imgFile = new ParseFile (imgFileName + ".jpg", dataStream);
            if(mParsePhotoArray == null)
                mParsePhotoArray = new ArrayList<ParseFile>();
            mParsePhotoArray.add(mImageIndex, imgFile);

            //save the thumbnail now
            saveImageThumbnail(picturePath, mImageIndex);

            mImgLoadingText.setVisibility(View.VISIBLE);
            mNextButton.setVisible(false);
            mParsePhotoArray.get(mImageIndex).saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    // If successful add file to user and signUpInBackground
                    if(null == e){
                        mImgLoadingText.setVisibility(View.GONE);
                        mNextButton.setVisible(true);
                    }

                }
            });
            mImageIndex++;

//now to add caption for this newly added image
            TextView myImageCaption = new TextView(this);
            myImageCaption.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));

            myImageCaption.setText("Image " + mImageIndex);
            myImageCaption.setTextColor(getResources().getColor(R.color.inputText));

            //now to add remove button in case user changes mind
            ImageButton imageRemoveButton = new ImageButton(this);
            LinearLayout.LayoutParams params =  new LinearLayout.LayoutParams(120,120);
            imageRemoveButton.setLayoutParams(params);
            imageRemoveButton.setBackgroundResource(R.color.white);
            imageRemoveButton.setAdjustViewBounds(true);
            imageRemoveButton.setScaleType(ImageButton.ScaleType.CENTER_CROP);
            imageRemoveButton.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_remove));



            imageRemoveButton.setTag(imageRemoveButton.getId(), myImage);
            imageRemoveButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
                            mLayoutImages.removeView(view);
                            mLayoutImages.removeView((View)(view.getTag(view.getId())));

                        }
                    });

            //mLayoutImages.addView(myImageCaption);

            mLayoutImages.addView(imageRemoveButton);
            mViewIndex++;
            mLayoutImages.addView(myImage);
            mViewIndex++;
        }
    }

    private void saveImageThumbnail(String Filename, int ImageIndex){
        //Bitmap temp = BitmapManager.decodeSampledBitmapFromPath(Filename, 100, 100, this);
        Bitmap temp = rotateImage(Filename, 100, 100);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        temp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] dataStream = stream.toByteArray();

        String imgFileName = "Image" + "_" + ImageIndex;
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        ParseFile imgFile = new ParseFile (imgFileName + ".jpg", dataStream);
        if(mParsePhotoThumbArray == null)
            mParsePhotoThumbArray = new ArrayList<ParseFile>();
        mParsePhotoThumbArray.add(ImageIndex, imgFile);

        //we need to store another sized version of hte image for display in GALLERY
        //temp = BitmapManager.decodeSampledBitmapFromPath(Filename, 200, 200, this);
        temp = rotateImage(Filename, 200, 200);
        stream = new ByteArrayOutputStream();
        temp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        dataStream = stream.toByteArray();

        imgFileName = "Image" + "_" + ImageIndex;
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        imgFile = new ParseFile (imgFileName + ".jpg", dataStream);
        if(mParsePhotoIconArray == null)
            mParsePhotoIconArray = new ArrayList<ParseFile>();
        mParsePhotoIconArray.add(ImageIndex, imgFile);


    }

    private Bitmap rotateImage(String OriPath, int width, int height){

        Bitmap OriImg = BitmapManager.decodeSampledBitmapFromPath(OriPath, width, height, this);

        ExifInterface exif = null;
        try{
            exif = new ExifInterface(OriPath);
        }
        catch(Exception e){

        }

        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) OriImg.getWidth() / 2, (float) OriImg.getHeight() / 2);
        //UserPhoto = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        return Bitmap.createBitmap(OriImg, 0, 0, OriImg.getWidth(), OriImg.getHeight(), matrix, true);
    }



//    public void btnClick_addNewGame(View view){
//
//        //get Views
//        EditText Name = (EditText) findViewById(R.id.txt_post_name);
//        EditText Content = (EditText) findViewById(R.id.txt_post_content);
//
//        if(Name.getText().toString().equals("")){
//            AlertDialogs.ShowAlertEmptyName(this);
//            return;
//        }
//
//        if(Content.getText().toString().equals("")){
//            AlertDialogs.ShowAlertEmptyBody(this);
//            return;
//        }
//
//
//        mDialog.show();
//        addNewPost();
//    }
//
//    public void btnClick_goBack(View view){
//        finish();
//    }

//    private void cacheGlobalPost(){
//        Gson gson = new Gson();
//        //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
//        String JSON = gson.toJson(mGlobalPost_List);
//
//        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
//        SharedPreferences.Editor ed = sp.edit();
//        ed.putString("mGlobalPost_List", JSON);
//        ed.apply();
//    }



    private void AddImagesToParse(final String ObjectId){

        if(mUserPhotoArray == null){
            mDialog.dismiss();
            finish();
            return;
        }

        //update GameSessionID
        appState.setGameSessionID(ObjectId);
        final List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        String imgFileName = "";
        ParseObject NewPlayer;

        for(int i = 0; i< mUserPhotoArray.size();i++){
            NewPlayer = new ParseObject("GlobalFeed_Images");
            NewPlayer.put("GlobalFeed_ObjectID", ObjectId);
            NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData("GlobalFeed_Master",
                    ObjectId)); //same for all
            NewPlayer.put("Submitter_objectId", ParseObject.createWithoutData("_User",
                    Player_objectId));
            NewPlayer.put("Index", i);
            NewPlayer.put("Image", mParsePhotoArray.get(i));
            NewPlayer.put("Image_Icon", mParsePhotoIconArray.get(i));
            NewPlayer.put("Image_Thumbnail", mParsePhotoThumbArray.get(i));

            //indicate whether the image belongs to group or individual
            if(mIsGroupFeed)
                NewPlayer.put("Group_objectId", ParseObject.createWithoutData("GlobalFeed_Group_Master",
                        mGroup_ObjectID)); //same for all

            if(mIsUserFeed)
                NewPlayer.put("Member_objectId", ParseObject.createWithoutData("_User",
                        mUser_ObjectID)); //same for all

            PlayerList.add(NewPlayer);
        }

        final Activity act = this;

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if(e == null){
                    //do something
                    Log.i("Add new notifications", "SUCCESS!");
                    //mGlobalPost_List.get(ObjectId).insertImageArray(PlayerList, act);
                    mDialog.dismiss();
                    //cacheGlobalPost();
                    finish();

                }
                else
                {
                    Log.i("Save Image Error", e.getMessage());
                }


            }
        });

    }
//
//    public void uponConmpleteAddNewGame(){
//        Intent intent = new Intent();
//        intent.putExtra("Status", "Success");
//        setResult(RESULT_OK, intent);
//        mDialog.dismiss();
//        finish();
//    }


    private void addNewPost(){


        //get Views
        EditText Name = (EditText) findViewById(R.id.txt_post_name);
        EditText Content = (EditText) findViewById(R.id.txt_post_content);
        final Activity act = this;

        //insert new word into History table
        final ParseObject gameScore = new ParseObject("GlobalFeed_Master");
        gameScore.put("Name", Name.getText().toString());
        gameScore.put("Message", Content.getText().toString());
        gameScore.put("User_ObjectID", Player_objectId);
        gameScore.put("User_objectId", ParseObject.createWithoutData("_User", Player_objectId));
        //check if this post if for GROUP or USER TIMELINE
        if(mIsGroupFeed){
            gameScore.put("Group_ObjectID", mGroup_ObjectID);
            gameScore.put("Group_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Group_Master", mGroup_ObjectID));
            gameScore.put("IsGroup", true);
            //TODO to put in correct group type, 3 for closed, 4 for secret i dunno
            if(mIsTutorFeed)
                gameScore.put("Type", appState.getPostTutor());
            else
                gameScore.put("Type", mGroup_Type);
        }
        else if (mIsUserFeed){
            gameScore.put("Friend_ObjectID", mUser_ObjectID);
            gameScore.put("Friend_objectId", ParseObject.createWithoutData(
                    "_User", mUser_ObjectID));
            gameScore.put("IsGroup", false);
            gameScore.put("Type", appState.getPostFriendInt());
        }
        else{
            gameScore.put("Type", appState.getPostGlobalInt());
            gameScore.put("IsGroup", false);
        }

        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Saved successfully.
                    //mGlobalPost_List.put(gameScore.getObjectId(), new GlobalPost(gameScore, PlayerID));
                    AddImagesToParse(gameScore.getObjectId());
                    if(mIsGroupFeed)
                        addGroupNotifications(mGroup_ObjectID, gameScore.getObjectId());
                    else if(mIsUserFeed)
                        addUserNotification(mUser_ObjectID, gameScore.getObjectId());


                    //GlobalPost = new GlobalPost();
                    //here we update the main HashTable and cache it


                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

    }

    private void addUserNotification(final String ObjID, final String FeedObjID){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", ObjID));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 9);
        NewPlayer.put("IsAccepted", false); //By default is False
        NewPlayer.put("Feed_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Master", FeedObjID));
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    private void addGroupNotifications(final String ObjID, final String FeedObjID){

        final ArrayList<String> groupMembers = new ArrayList<String>();

        //first we get all the members in the group
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        ParseObject obj = ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", ObjID);
        query.whereEqualTo("Group_objectId", obj);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    for(int i=0;i<scoreList.size();i++){
                        if(!scoreList.get(i).getString("Member_ObjectID").
                                equals(Player_objectId)){
                            groupMembers.add(scoreList.get(i).getString("Member_ObjectID"));
                        }
                    }

                    addInvitesIntoNotifications(ObjID, FeedObjID, groupMembers);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void addInvitesIntoNotifications(final String GroupID, final String FeedID,
                                             ArrayList<String> FriendList){

        List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        ParseObject NewPlayer;

        for (int i = 0; i<FriendList.size(); i++){
            //insert new word into History table
            NewPlayer = new ParseObject("Notifications");
            //NewPlayer.put("Group_ObjectID", GroupID);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", FriendList.get(i)));
            NewPlayer.put("IsSeen", false);
            NewPlayer.put("Type", 6);
            NewPlayer.put("IsAccepted", false); //By default is False


            NewPlayer.put("Feed_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Master", FeedID));
            NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Group_Master", GroupID));
            NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                    "_User", Player_objectId));

            PlayerList.add(NewPlayer);
        }

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                //do something
                Log.i("Add new notifications", "SUCCESS!");

            }
        });


    }

    public static class AlertDialogs{

        public AlertDialogs(){

        }

        public static void ShowAlertEmptyName(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter a name for your Post")
                    .setTitle("Invalid Post Name");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        public static void ShowAlertEmptyBody(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter some text for your Message")
                    .setTitle("Invalid Post Content");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        if (!mNavigationDrawerFragment.isDrawerOpen()) {
//            // Only show items in the action bar relevant to this screen
//            // if the drawer is not showing. Otherwise, let the drawer
//            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.main_menu, menu);
//            restoreActionBar();
//            return true;
//        }
        getMenuInflater().inflate(R.menu.global_post, menu);
        mNextButton = (MenuItem) menu.findItem(R.id.action_post);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.home){
            finish();
            return true;
        }


        if (id == R.id.action_post) {

            EditText Name = (EditText) findViewById(R.id.txt_post_name);
            EditText Content = (EditText) findViewById(R.id.txt_post_content);

            if(Content.getText().toString().equals("")){
                AlertDialogs.ShowAlertEmptyBody(this);
                return false;
            }


            mDialog.show();
            addNewPost();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
