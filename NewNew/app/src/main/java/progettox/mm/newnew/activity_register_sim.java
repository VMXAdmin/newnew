package progettox.mm.newnew;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sinch.verification.CodeInterceptionException;
import com.sinch.verification.Config;
import com.sinch.verification.InvalidInputException;
import com.sinch.verification.ServiceErrorException;
import com.sinch.verification.SinchVerification;
import com.sinch.verification.Verification;
import com.sinch.verification.VerificationListener;

import java.util.List;

/**
 * Created by Admin on 30/7/2015.
 */
public class activity_register_sim extends AppCompatActivity {

    private ProgressDialog mDialog;
    private EditText mCountryCode;
    private EditText mPhoneNumber;
    private String mSimNumber;
    private Boolean mIsNewRegistration;
    private ProgressDialog mSimSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_sim);
        Initialize();
        getCountryCode();
    }

    private void ActivateLoading(Boolean status){
        if(status)
            mDialog.show();
        else
            mDialog.dismiss();
    }

    private void goToRegisterPage2(){
        if(mIsNewRegistration){
            Intent intent = new Intent(this, activity_register_userid.class);
            intent.putExtra("Sim", mSimNumber);
            startActivity(intent);
        }
        else{
            getUserName();
        }

    }

    public void ParseUserLoginSSO(final String usernametxt, final String passwordtxt){
        // Send data to Parse.com for verification
        mSimSignIn.show();
        ParseUser.logInInBackground(usernametxt, passwordtxt,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            mSimSignIn.dismiss();
                            GotoGameActivity(usernametxt, passwordtxt, user.getObjectId());
                        } else {

                        }
                    }
                });
    }

    private void getUserName(){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("Sim_Number", mSimNumber);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    if(objects.size() > 0){
                        Log.d("Groups", "Retrieved " + objects.size() + " scores");
                        ParseUserLoginSSO(objects.get(0).getUsername(),
                                mSimNumber);
                    }
                    else {

                    }
                } else {
                    // Something went wrong.
                }
            }
        });
    }

    private void Initialize(){
        mIsNewRegistration = false;

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Verifying your sim card number. Please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        mSimSignIn = new ProgressDialog(this);
        mSimSignIn.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mSimSignIn.setMessage("Loggin you in...");
        mSimSignIn.setIndeterminate(true);
        mSimSignIn.setCanceledOnTouchOutside(false);

        mCountryCode = (EditText) findViewById(R.id.txt_country_code);
        mPhoneNumber = (EditText) findViewById(R.id.phoneNumber);
        Button verifySim = (Button) findViewById(R.id.btn_sim_verify);
        verifySim.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                verifySimCard();
            }
        });
        TextView simCaption = (TextView) findViewById(R.id.txt_sim_caption);

        Bundle bundle = getIntent().getExtras();
        if(bundle.getInt("Type") == 0)
            mIsNewRegistration = true;

        if(!mIsNewRegistration){
            simCaption.setText(getResources().getText(R.string.title_sim_login));
            setTitle(getResources().getText(R.string.title_sim_signin));
        }

    }

    private void getCountryCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];break;  }
        }

        mCountryCode.setText("+" + CountryZipCode);

    }

    private void verifySimCard(){
        ActivateLoading(true);
        String phoneNum = mPhoneNumber.getText().toString();
        mSimNumber = mCountryCode.getText().toString() + phoneNum;
        Config config = SinchVerification.config().
                applicationKey("227ce225-df8a-426a-ba89-48c466fe1859").context(this).build();
        VerificationListener listener = new MyVerificationListener();
        Verification verification = SinchVerification.createFlashCallVerification(config, mSimNumber, listener);
        verification.initiate();
    }

    private class MyVerificationListener implements VerificationListener {
        @Override
        public void onInitiated() {}

        @Override
        public void onInitiationFailed(Exception e) {
            ActivateLoading(false);
            if (e instanceof InvalidInputException) {
                Toast.makeText(getApplicationContext(), "Incorrect number provided", Toast.LENGTH_LONG).show();
            } else if (e instanceof ServiceErrorException) {
                Toast.makeText(getApplicationContext(),"Sinch service error",Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(),"Other system error, check your network state", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onVerified() {
            ActivateLoading(false);
            new AlertDialog.Builder(activity_register_sim.this)
                    .setMessage("Verification Successful!")
                    .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                            goToRegisterPage2();
                        }
                    })
                    .show();

        }

        @Override
        public void onVerificationFailed(Exception e) {
            ActivateLoading(false);
            if (e instanceof CodeInterceptionException) {
                Toast.makeText(getApplicationContext(),"Intercepting the verification call automatically failed",Toast.LENGTH_LONG).show();
            } else if (e instanceof ServiceErrorException) {
                Toast.makeText(getApplicationContext(), "Sinch service error",Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(),"Other system error, check your network state", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void GotoGameActivity(String UID, String PWD, String UOBJID){


        //register into Shared Object to be retrieved later///////////
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("PlayerID", UID);
        ed.putString("Player_ObjectID", UOBJID);
        ed.putString("PlayerPWD", PWD);
        ed.apply();
        //register into Shared Object to be retrieved later///////////

        //The 1st line is to register into Parse's Installation table to faciliate proper Push Notifications
        //ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if(e != null)
                    Log.i("ParseINstallation", e.getMessage());
            }
        });

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("username", UID);
        //installation.saveInBackground();
        installation.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if(e != null)
                    Log.i("ParseINstallation", e.getMessage());
            }
        });

        App appState = ((App)getApplicationContext());
        appState.setPlayerID(UID);
        appState.setPlayer_objectId(UOBJID);

        Intent intent = new Intent(this, MainMenuActivity.class);
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("PlayerID", UID);
        bundle.putString("Player_objectId", UOBJID);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }
}
