package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 16/6/2015.
 */
public class frame_index_notifications extends Fragment {

    public String PlayerID;
    public String Player_objectId;
    private View myFragmentView;
    public Hashtable<String, Notification> mNotifications_HT;
    public NotificationAdapter mAdapter;
    //public CommentsAdapter mCommentsAdapter;
    //private ProgressDialog mDialog;
    public int mScreenHeight;
    public int mScreenWidth;
    private boolean mIsDirty;
    private LoadImageTask mLoadImageTask;
    private TextView txtLoadingCaption;


    public static frame_index_notifications newInstance(String param1, String param2) {
        frame_index_notifications fragment = new frame_index_notifications();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void Initialize(){

        txtLoadingCaption.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            PlayerID = getArguments().getString("PlayerID");
            Player_objectId = getArguments().getString("Player_objectId");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_index_notifications, container, false);
        txtLoadingCaption = (TextView)myFragmentView.findViewById(R.id.lbl_loading_caption);
        return myFragmentView;
    }


    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

        //TODO set proper page numbering
        App appState = ((App)getActivity().getApplicationContext());
        appState.setPageNumber(3);

        loadUserNotifications();
        txtLoadingCaption.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first


    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first

    }

    private void loadUserNotifications(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Notifications");
        ParseObject obj = ParseObject.createWithoutData("_User",Player_objectId);
        query.whereEqualTo("User_objectId", obj);
        query.include("Feed_objectId");
        query.include("Group_objectId");
        query.include("Member_objectId");
        query.include("User_objectId");
        query.include("PM_History_objectId");
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0) {
                        checkForUserAvatarChages(scoreList);
                        FeedMasterDataIntoObjects(scoreList);
                    }
                    else
                        txtLoadingCaption.setVisibility(View.GONE);


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void checkForUserAvatarChages(List<ParseObject> data){
        ArrayList<String> arrObjId = new ArrayList<>();
        for(int i=0;i<data.size();i++){
            if(data.get(i).getInt("Type") == 11 && data.get(i).getBoolean("IsSeen") == false){
                arrObjId.add(data.get(i).getParseObject("User_objectId").getObjectId());
                cacheNewUserAvatar(data.get(i));
            }
        }



    }

    private void cacheNewUserAvatar(ParseObject User){

        User.put("IsSeen", true);
        User.saveInBackground();

        final Activity act = getActivity();
        ParseFile Picture = User.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
        final String PlayerObjectID = User.getParseObject("Member_objectId").getObjectId();
        Picture.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] data, ParseException e) {

                if (e == null) {
                    Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                            data.length);
                    //storeImage(PictureBmp, PlayerObjectID, activity);
                    GlobalPost.StorePNG(PictureBmp, PlayerObjectID, act);

                } else {

                }

            }
        });

    }

    public void FeedMasterDataIntoObjects(List<ParseObject> data){
        //First create the main globalfeed objects
        mNotifications_HT = ConvertJSONtoArray(data);

        ArrayList<Notification> arrayOfUsers = new ArrayList<Notification>();
        // Create the adapter to convert the array to views
        mAdapter = new NotificationAdapter(getActivity(), arrayOfUsers);
        mAdapter.addAll(mNotifications_HT.values());

        ListView listView = (ListView) myFragmentView.findViewById(R.id.lvItems);
        listView.setAdapter(mAdapter);
        //LoadPostImages();
        //mDialog.dismiss();
        //Next we have to load Any Images that might have been attached
        txtLoadingCaption.setVisibility(View.GONE);
    }

    public Hashtable<String, Notification> ConvertJSONtoArray(List<ParseObject> jsonObjects){
        Hashtable<String, Notification> GameHistory = new Hashtable<String, Notification>();

        for (int i = 0; i < jsonObjects.size(); i++) {
            //if (jsonObjects.get(i).getParseObject("GameSession_objectId").getString("CurrentPlayerID").equals(PlayerID)) {
            GameHistory.put(jsonObjects.get(i).getObjectId(),
                    new Notification(jsonObjects.get(i), getActivity()));

            //}
        }
        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Notification g;
        private String fileName;

        LoadImageTask(ImageView view, Notification globalPost, String FileName) {
            v = view;
            g = globalPost;
            fileName = FileName;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            Bitmap temp = g.loadImageFromStorage(fileName, getActivity());
            if (temp != null){
                return g.loadImageFromStorage(fileName, getActivity());
            }
            else
            {
                return null;
            }

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }

        }

    }

    static class ViewHolder{
        ImageView notificationPic;
        TextView notificationName;
        TextView notificationDate;
        TextView notificationGroup;
        TextView notificationDetails;
        LinearLayout notificationLayout;
        Button notificationAccept;
    }

    public class NotificationAdapter extends ArrayAdapter<Notification> {
        public NotificationAdapter(Context context, ArrayList<Notification> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            Notification user = getItem(position);
            ViewHolder holder = null;
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_index_notifications, parent, false);
                holder = new ViewHolder();
                holder.notificationAccept = (Button) convertView.findViewById(R.id.btn_notifications_accept);
                holder.notificationName = (TextView) convertView.findViewById(R.id.notification_name);
                holder.notificationGroup = (TextView) convertView.findViewById(R.id.notification_group);
                holder.notificationPic = (ImageView) convertView.findViewById(R.id.notification_pic);
                holder.notificationDetails = (TextView) convertView.findViewById(R.id.notification_details);
                holder.notificationDate = (TextView) convertView.findViewById(R.id.notification_date);
                holder.notificationLayout = (LinearLayout) convertView.findViewById(R.id.notification_Layout);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }

            //Load Notification Avatar Pic
            try{
                mLoadImageTask = new LoadImageTask(holder.notificationPic, user, user.getNotificationAvatarID());
                mLoadImageTask.execute(holder.notificationPic);
            }
            catch(Exception e){
                Log.i("Feed_Main", e.getMessage());
            }

            //Load Notification Subject
            holder.notificationName.setText(user.getNotificationName());
            //Load Notification Date
            holder.notificationDate.setText(user.getNotificationDate());
            //Load Notification Message
            holder.notificationDetails.setText(user.getNotificationMessage());

//            if(user.getNotificationType() == 3 || user.getNotificationType() == 4
//                    || ){
//
//                holder.notificationGroup.setVisibility(View.GONE);
//
//            }

            if(user.getNotificationType() == 6){
                holder.notificationGroup.setVisibility(View.VISIBLE);
                holder.notificationGroup.setText(user.getGroupName());
                holder.notificationGroup.setTag(holder.notificationGroup.getId(), user.getObjectId());
                holder.notificationGroup.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onClickLayoutItem(String.valueOf(view.getTag(view.getId())));
                            }
                        });
            }
            else if(user.getNotificationType() == 7 || user.getNotificationType() == 8){
                holder.notificationGroup.setVisibility(View.VISIBLE);
                holder.notificationGroup.setText(user.getGroupName());
                holder.notificationGroup.setTag(holder.notificationGroup.getId(), user.getObjectId());
                holder.notificationGroup.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onClickLayoutItem(String.valueOf(view.getTag(view.getId())));
                            }
                        });
            }
            else if(user.getNotificationType() == 5){
                if(user.getIsGroup()){
                    holder.notificationGroup.setVisibility(View.VISIBLE);
                    holder.notificationGroup.setText(user.getGroupName());
                    holder.notificationGroup.setTag(holder.notificationGroup.getId(), user.getObjectId());
                    holder.notificationGroup.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    onClickLayoutItem(String.valueOf(view.getTag(view.getId())));
                                }
                            });
                }
            }
            else
            {
                holder.notificationGroup.setVisibility(View.GONE);
            }



            //Tag the Comments Button
            //Button btnComments = (Button) convertView.findViewById(R.id.btn_comments);
            holder.notificationLayout.setTag(holder.notificationLayout.getId(), user.getObjectId());
            holder.notificationLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onClickLayoutItem(String.valueOf(view.getTag(view.getId())));
                        }
                    });


            //enable the ACCEPT button if it is related to
            //type 2 Group join request
            //type 3 Friend request (be a friend)
            if(user.getNotificationType() == 2 || user.getNotificationType() == 3 ||
                    user.getNotificationType() == 7){

                if(!user.isAccepted){
                    holder.notificationAccept.setVisibility(View.VISIBLE);
                    holder.notificationAccept.setTag(holder.notificationAccept.getId(), user.getObjectId());
                    holder.notificationAccept.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    view.setVisibility(View.INVISIBLE);
                                    onClickAcceptButton(String.valueOf(view.getTag(view.getId())));
                                }
                            });
                }
                else{
                    holder.notificationAccept.setVisibility(View.VISIBLE);
                    holder.notificationAccept.setText(getActivity().getResources().getText(R.string.notifications_accepted));
                    holder.notificationAccept.setTextColor(getActivity().getResources().getColor(R.color.inputText));
                    holder.notificationAccept.setBackgroundColor(Color.TRANSPARENT);
                }


            }
            else
            {
                holder.notificationAccept.setVisibility(View.INVISIBLE);
            }

            // Return the completed view to render on screen
            return convertView;
        }

    }

    public void OpenPMHistory(String PM_ObjectID, String UserObjID, String Username){
        ((MainMenuActivity)getActivity()).GoToPMHistory(PM_ObjectID, UserObjID, Username);
    }

    public void OpenFeedDetails(String Notification_ObjectID){
        String FeedID = mNotifications_HT.get(Notification_ObjectID).getFeedObjectID();
        String UserName = mNotifications_HT.get(Notification_ObjectID).getMemberName();
        String UserObjectID = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
        String Message = mNotifications_HT.get(Notification_ObjectID).getMessage();
        Boolean isGroup = mNotifications_HT.get(Notification_ObjectID).getIsGroup();
        String GroupName = mNotifications_HT.get(Notification_ObjectID).getGroupName();
        String GroupObjectID = mNotifications_HT.get(Notification_ObjectID).getGroupObjectID();
        Date createdAt = mNotifications_HT.get(Notification_ObjectID).getCreatedAt();
        String createdAtDisplay = mNotifications_HT.get(Notification_ObjectID).getNotificationDate();

        ((MainMenuActivity)getActivity()).GotoGlobalFeedDetails(FeedID,
                UserName, UserObjectID, GroupName, isGroup, GroupObjectID, 0, Message,
                createdAtDisplay, createdAt);
    }

    private void onClickLayoutItem(String Notification_ObjectID){
        int Type = mNotifications_HT.get(Notification_ObjectID).getNotificationType();
        //2 is Group join request (ppl ask you to join a group)
        if(Type == 2){
            String Group_objectId = mNotifications_HT.get(Notification_ObjectID).getGroupObjectID();
            String Group_name = mNotifications_HT.get(Notification_ObjectID).getGroupName();
            //TODO check of isTutor Group or not, now is temporarily set to false
            ((MainMenuActivity)getActivity()).GoToFeedGroup(Group_objectId, Group_name,
                    true, false);
        }
        //3 is somebody want to be your friend
        else if(Type == 3){
            String username = mNotifications_HT.get(Notification_ObjectID).getMemberName();
            String user_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            ((MainMenuActivity)getActivity()).OpenUserFeedPage(username, user_objectId);
        }
        //4 is that somebody ACCEPTED your friend request
        else if(Type == 4){
            String username = mNotifications_HT.get(Notification_ObjectID).getMemberName();
            String user_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            ((MainMenuActivity)getActivity()).OpenUserFeedPage(username, user_objectId);
        }
        //this is where someone posted a new comment in a post you have started/commented
        else if(Type == 5){
            String Feed_objectId = mNotifications_HT.get(Notification_ObjectID).getFeedObjectID();
            OpenFeedDetails(Notification_ObjectID);
        }
        //this is where someone posted a new message in a group that your in as well
        else if(Type == 6){
            String Feed_objectId = mNotifications_HT.get(Notification_ObjectID).getFeedObjectID();
            OpenFeedDetails(Notification_ObjectID);
        }
        //someone pressed the JOIN GROUP button and you as the creator can see this request
        else if(Type == 7){
            String username = mNotifications_HT.get(Notification_ObjectID).getMemberName();
            String user_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            ((MainMenuActivity)getActivity()).OpenUserFeedPage(username, user_objectId);
        }
        //you as the group creator APPROVED this someone who requested for join access
        else if(Type == 8){
            String username = mNotifications_HT.get(Notification_ObjectID).getMemberName();
            String user_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            ((MainMenuActivity)getActivity()).OpenUserFeedPage(username, user_objectId);
        }
        //this is where someone posted a new message in your personal timeline
        else if(Type == 9){
            String Feed_objectId = mNotifications_HT.get(Notification_ObjectID).getFeedObjectID();
            OpenFeedDetails(Notification_ObjectID);
        }
        //this is where someone posted a new message in your personal timeline
        else if(Type == 10){
            String PM_objectId = mNotifications_HT.get(Notification_ObjectID).getPMObjectID();
            String username = mNotifications_HT.get(Notification_ObjectID).getMemberName();
            String user_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            OpenPMHistory(PM_objectId, user_objectId, username);
        }
        //this is where someone changed a new profile pic, send noti to friends only
        else if(Type == 11){
            String PM_objectId = mNotifications_HT.get(Notification_ObjectID).getPMObjectID();
            String username = mNotifications_HT.get(Notification_ObjectID).getMemberName();
            String user_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            OpenPMHistory(PM_objectId, user_objectId, username);
        }

    }

    private void clearNotificationsAsDone(String Notification_ObjectID){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Notifications");
        query.whereEqualTo("objectId", Notification_ObjectID);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    for(int i = 0; i< groupList.size();i++){
                        groupList.get(i).put("IsAccepted", true);
                        groupList.get(i).saveInBackground();
                    }
                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void onClickAcceptButton(String Notification_ObjectID){
        int Type = mNotifications_HT.get(Notification_ObjectID).getNotificationType();

        //2 is Group join request (ppl ask you to join a group)
        if(Type == 2){
            String Group_objectId = mNotifications_HT.get(Notification_ObjectID).getGroupObjectID();
            //now we update the GlobalFeed_Group_Members table
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
            query.whereEqualTo("Group_ObjectID", Group_objectId);
            query.whereEqualTo("Member_ObjectID", Player_objectId);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> groupList, ParseException e) {
                    if (e == null) {
                        for(int i = 0; i< groupList.size();i++){
                            groupList.get(i).put("Member_Status", 1);
                            groupList.get(i).saveInBackground();
                        }
                    } else {
                        Log.d("Groups", "Error: " + e.getMessage());
                    }
                }
            });

            //now we change the notification table's IsAccepted value to TRUE
            clearNotificationsAsDone(Notification_ObjectID);
        }
        //3 is somebody want to be your friend
        else if(Type == 3){
            String Friend_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            //now we update the GlobalFeed_Group_Members table
            ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
            ParseObject obj = ParseObject.createWithoutData("_User",Player_objectId);
            query.whereEqualTo("Player_objectId", obj);
            obj = ParseObject.createWithoutData("_User",Friend_objectId);
            query.whereEqualTo("Friend_objectId", obj);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> groupList, ParseException e) {
                    if (e == null) {
                        for(int i = 0; i< groupList.size();i++){
                            groupList.get(i).put("Status", 1);
                            groupList.get(i).saveInBackground();
                        }
                    } else {
                        Log.d("Groups", "Error: " + e.getMessage());
                    }
                }
            });

            //do the vice versa
            query = ParseQuery.getQuery("User_Friends");
            obj = ParseObject.createWithoutData("_User",Friend_objectId);
            query.whereEqualTo("Player_objectId", obj);
            obj = ParseObject.createWithoutData("_User",Player_objectId);
            query.whereEqualTo("Friend_objectId", obj);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> groupList, ParseException e) {
                    if (e == null) {
                        for(int i = 0; i< groupList.size();i++){
                            groupList.get(i).put("Status", 1);
                            groupList.get(i).saveInBackground();
                        }
                    } else {
                        Log.d("Groups", "Error: " + e.getMessage());
                    }
                }
            });

            //here we add to notifications to inform the person that you have accepted his/her friend request
            ParseObject NewPlayer = new ParseObject("Notifications");
            //NewPlayer.put("Group_ObjectID", GroupID);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", Friend_objectId));
            NewPlayer.put("IsSeen", false);
            NewPlayer.put("Type", 4);
            NewPlayer.put("IsAccepted", false); //By default is False

            NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                    "_User", Player_objectId));
            NewPlayer.saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    //RequestCompleted();
                }
            });

            //now we change the notification table's IsAccepted value to TRUE
            clearNotificationsAsDone(Notification_ObjectID);
        }
        else if(Type == 7){
            String Group_objectId = mNotifications_HT.get(Notification_ObjectID).getGroupObjectID();
            String Requestor_objectId = mNotifications_HT.get(Notification_ObjectID).getMemberObjectID();
            //now we update the GlobalFeed_Group_Members table
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
            query.whereEqualTo("Group_ObjectID", Group_objectId);
            query.whereEqualTo("Member_ObjectID", Requestor_objectId);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> groupList, ParseException e) {
                    if (e == null) {
                        for(int i = 0; i< groupList.size();i++){
                            groupList.get(i).put("Member_Status", 1);
                            groupList.get(i).saveInBackground();
                        }
                    } else {
                        Log.d("Groups", "Error: " + e.getMessage());
                    }
                }
            });

            //here we add to notifications to inform the person that you have accepted his/her Group joining request
            ParseObject NewPlayer = new ParseObject("Notifications");
            //NewPlayer.put("Group_ObjectID", GroupID);
            NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                    "_User", Requestor_objectId));
            NewPlayer.put("IsSeen", false);
            NewPlayer.put("Type", 8);
            NewPlayer.put("IsAccepted", false); //By default is False
            NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                    "GlobalFeed_Group_Master", Group_objectId));
            NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                    "_User", Player_objectId));
            NewPlayer.saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    //RequestCompleted();
                }
            });

            //now we change the notification table's IsAccepted value to TRUE
            clearNotificationsAsDone(Notification_ObjectID);

        }

    }

    private class Notification{
        private String ObjectId;
        private String Player_Name;
        private String Player_objectId;
        private Boolean isSeen;
        private int Type;
        private Date CreatedAt;
        private Boolean isGroup;
        private String Feed_objectId;
        private String PM_objectId;
        private String Group_objectId;
        private String Group_Name;
        private String Member_objectId;
        private String Member_Name;
        private String FileName;
        private String NotificationName;
        private Boolean isAccepted;
        private String Message;
        private String NotificationMessage;
        //PROPERTIES/////////////////////////////
        public Boolean getIsGroup(){return this.isGroup;}
        public String getFeedObjectID(){return this.Feed_objectId;}
        public String getPMObjectID(){return this.PM_objectId;}
        public String getGroupName(){return this.Group_Name;}
        public Boolean getIsAccepted(){return this.isAccepted;}
        public String getMemberName(){return this.Member_Name;}
        public String getMemberObjectID(){return this.Member_objectId;}
        public String getGroupObjectID(){return this.Group_objectId;}
        public int getNotificationType(){return this.Type;}
        public String getNotificationAvatarID(){return this.FileName;}
        public String getNotificationName(){return this.NotificationName;}
        public Date getCreatedAt(){return this.CreatedAt;}
        public String getMessage(){return this.Message;}
        public String getNotificationDate(){

            //if(this.CreatedAt.)

            String temp = this.CreatedAt.toString();
            int index = temp.indexOf("GMT");


            return temp.substring(0, index);

            //return this.CreatedAt;
        }
        public String getObjectId(){return this.ObjectId;}
        public String getNotificationMessage(){
            //TODO to put all these captions into STrings.xml so taht language
            if(this.Type == 1){

            }
            else if(this.Type == 2){
                return getActivity().getResources().getText(R.string.notifications_msg_two).toString();
            }
            else if(this.Type == 3){
                return getActivity().getResources().getText(R.string.notifications_msg_three).toString();
            }
            else if(this.Type == 4){
                return getActivity().getResources().getText(R.string.notifications_msg_four).toString();
            }
            else if(this.Type == 5){
                return getActivity().getResources().getText(R.string.notifications_msg_five).toString();
            }
            else if(this.Type == 6){
                return getActivity().getResources().getText(R.string.notifications_msg_six).toString();
            }
            else if(this.Type == 7){
                return getActivity().getResources().getText(R.string.notifications_msg_seven).toString();
            }
            else if(this.Type == 8){
                return getActivity().getResources().getText(R.string.notifications_msg_eight).toString();
            }
            else if(this.Type == 9){
                return getActivity().getResources().getText(R.string.notifications_msg_nine).toString();
            }
            else if(this.Type == 10){
                return getActivity().getResources().getText(R.string.notifications_msg_ten).toString();
            }

            return "";
        }
        //PROPERTIES/////////////////////////////

        public Notification(ParseObject Obj , final Activity activity){
            this.ObjectId = Obj.getObjectId();
            this.CreatedAt = Obj.getCreatedAt();
            this.Player_Name = Obj.getParseObject("User_objectId").getString("username");
            this.Player_objectId = Obj.getParseObject("User_objectId").getObjectId();
            this.isSeen = Obj.getBoolean("IsSeen");
            this.Type = Obj.getInt("Type");
            this.isAccepted = Obj.getBoolean("IsAccepted");
            this.isGroup = false;

            if(this.Type == 1){

            }
            //join a group request
            else if(this.Type == 2){
                this.isGroup = true;
                this.Group_objectId = Obj.getParseObject("Group_objectId").getObjectId();
                this.Group_Name = Obj.getParseObject("Group_objectId").getString("Name");
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Group_objectId;
                this.NotificationName = this.Group_Name;
            }
            //friend request
            else if(this.Type == 3){
                this.isGroup = false;
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;
            }
            //friend request ACCEPTED notice
            else if(this.Type == 4){
                this.isGroup = false;
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;
            }
            //Add new comment to a post you commented on
            else if(this.Type == 5){
                //if the comment is to a post which belongs to a group
                if(Obj.getParseObject("Group_objectId") != null){
                    this.Group_objectId = Obj.getParseObject("Group_objectId").getObjectId();
                    this.Group_Name = Obj.getParseObject("Group_objectId").getString("Name");
                    this.isGroup = true;
                }

                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;
                this.Feed_objectId = Obj.getParseObject("Feed_objectId").getObjectId();
                this.Message = Obj.getParseObject("Feed_objectId").getString("Message");

            }
            //Add new post to group
            else if(this.Type == 6){
                this.isGroup = true;
                this.Group_objectId = Obj.getParseObject("Group_objectId").getObjectId();
                this.Group_Name = Obj.getParseObject("Group_objectId").getString("Name");
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;
                this.Feed_objectId = Obj.getParseObject("Feed_objectId").getObjectId();
                this.Message = Obj.getParseObject("Feed_objectId").getString("Message");
            }
            //member want to join new group
            else if(this.Type == 7){
                this.isGroup = true;
                this.Group_objectId = Obj.getParseObject("Group_objectId").getObjectId();
                this.Group_Name = Obj.getParseObject("Group_objectId").getString("Name");
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;

            }
            //member want to join new group ACCEPTED
            else if(this.Type == 8){
                this.isGroup = true;
                this.Group_objectId = Obj.getParseObject("Group_objectId").getObjectId();
                this.Group_Name = Obj.getParseObject("Group_objectId").getString("Name");
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;

            }
            //Add new post to USER personal feed (aka timeline)
            else if(this.Type == 9){
                this.isGroup = false;
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;
                this.Feed_objectId = Obj.getParseObject("Feed_objectId").getObjectId();
                this.Message = Obj.getParseObject("Feed_objectId").getString("Message");
            }
            //Someone send you a PM (Personal Message)
            else if(this.Type == 10){
                this.isGroup = false;
                this.Member_objectId = Obj.getParseObject("Member_objectId").getObjectId();
                this.Member_Name = Obj.getParseObject("Member_objectId").getString("username");
                this.FileName = this.Member_objectId;
                this.NotificationName = this.Member_Name;
                this.PM_objectId = Obj.getParseObject("PM_History_objectId").getString("PM_ObjectID");
                this.Message = Obj.getParseObject("PM_History_objectId").getString("Message");
            }

            //load user avatar if it is not in cache
            if(!IsBitmapCached(this.FileName, activity)){
                ParseFile Picture = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
                if(this.Type == 1){

                }
                else if(this.Type == 2){
                    Picture = Obj.getParseObject("Group_objectId").getParseFile("Image");
                    if (Picture == null)
                        return;
                }
                else if(this.Type == 3){
                    Picture = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
                }
                else if(this.Type == 4){
                    Picture = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
                }
                else if(this.Type == 5){
                    Picture = Obj.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
                }
                else if(this.Type == 6){
                    Picture = Obj.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
                }
                else if(this.Type == 7){
                    Picture = Obj.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
                }
                else if(this.Type == 8){
                    Picture = Obj.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
                }
                else if(this.Type == 9){
                    Picture = Obj.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
                }
                else if(this.Type == 10){
                    Picture = Obj.getParseObject("Member_objectId").getParseFile("Avatar_Pic");
                }

                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, FileName, activity);

                        } else {

                        }

                    }
                });
            }
        }



        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (Exception e)
            {
                e.printStackTrace();

            }

            return null;
        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            //catch (FileNotFoundException e)
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return false;
        }

    }

}
