package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 27/7/2015.
 */
public class activity_comment_secondary extends AppCompatActivity {

    private String mPlayerID;
    private String mPlayer_objectId;
    private CommentsAdapter mCommentsAdapter;
    private List<CommentsHolder> mCommentsArray;
    private String mPost_objectId;
    private String mComment_objectId;
    private String mComment_User_objectId;
    private String mComment_User_Name;
    private String mComment_Date;
    private String mComment_Body;
    private Boolean mComment_IsLiked;
    private int mNo_of_Likes;
    private int mNo_of_Comments;

    private TextView mLoadingCaption;
    private TextView mTxtLikes;
    private TextView mTxtComments;
    private ImageButton mBtnLikes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_globalfeed_secondary);
        Initialize();
    }

    private void Initialize(){
        App appState = ((App) getApplicationContext());
        mPost_objectId = appState.getGlobalFeedID();
        mPlayerID = appState.getPlayerID();
        mPlayer_objectId = appState.getPlayer_objectId();

        mLoadingCaption = (TextView) findViewById(R.id.lbl_loading_images);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try{
                mPost_objectId = extras.getString("Post_ObjectID");
                mComment_objectId = extras.getString("Comment_ObjectID");
                mComment_User_objectId = extras.getString("User_ObjectID");
                mComment_User_Name = extras.getString("User_Name");
                mComment_Date = extras.getString("Comment_Date");
                mComment_Body = extras.getString("Comment_Body");
                mNo_of_Comments = extras.getInt("Comment_Comments");
                mNo_of_Likes = extras.getInt("Comment_Likes");
                //mComment_IsLiked = extras.getBoolean("Comment_IsLiked");
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }

        }
    }

    private void updateUI(){
        TextView userName = (TextView) findViewById(R.id.user_name);
        ImageView imgIcon = (ImageView) findViewById(R.id.user_profile_pic);
        TextView postMessage = (TextView) findViewById(R.id.post_message);
        TextView postDate = (TextView) findViewById(R.id.post_date);
        mTxtLikes = (TextView) findViewById(R.id.no_of_likes);
        mTxtComments = (TextView) findViewById(R.id.no_of_comments);
        mTxtLikes.setText(String.valueOf(mNo_of_Likes));
        mTxtComments.setText(String.valueOf(mNo_of_Comments));
        mBtnLikes = (ImageButton) findViewById(R.id.btn_likes);

        try{

            userName.setText(mComment_User_Name);
            imgIcon.setImageBitmap(GlobalPost.LoadPNG(mComment_User_objectId, this));
            imgIcon.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            OpenUserFeedPage();
                        }
                    });

            postMessage.setText(mComment_Body);
            postDate.setText(mComment_Date);

            //set ONCLICK listener for the like button
            mBtnLikes.setTag(mBtnLikes.getId(), mComment_objectId);
            mBtnLikes.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_action_new));
            mBtnLikes.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            likeComment(String.valueOf(view.getTag(view.getId())));

                        }
                    });
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }



    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        updateUI();
        checkIfYouLiked();
        loadComments();
    }

    private void checkIfYouLiked(){

        if(mNo_of_Likes> 0){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("GF_Cmt_Likes");
            ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Comments", mComment_objectId);
            query.whereEqualTo("Comment_objectId", obj);
            obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
            query.whereEqualTo("Comment_objectId", obj);
            //query.include("User_objectId");
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        if(scoreList.size() > 0){
                            mComment_IsLiked = true;
                            mBtnLikes.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                                    R.drawable.ic_action_newed));
                        }

                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    public void loadComments(){
        //Log.d("PARSE", "Fetching of User in progress");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GF_Cmt_Cmts");
        ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Comments", mComment_objectId);
        query.whereEqualTo("Comment_objectId", obj);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {

                    if(scoreList.size() > 0)
                        updateLVandAdapter(scoreList);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updateLVandAdapter(List<ParseObject> scoreList){

        try{
            mCommentsArray = ConvertJSONtoArray(scoreList);
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }


        //Load Comments into ListView
        ArrayList<CommentsHolder> arrayOfUsers = new ArrayList<CommentsHolder>();
        mCommentsAdapter = new CommentsAdapter(this, arrayOfUsers, this);
        mCommentsAdapter.addAll(mCommentsArray);
        ListView listView = (ListView) findViewById(R.id.lvComments);
        listView.setAdapter(mCommentsAdapter);
        setListViewHeightBasedOnChildren(listView);
        listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
    }

    private void likeComment(final String Comment_objectId){

        mNo_of_Likes++;
        updateLikesDisplay();

        ParseObject NewPlayer = new ParseObject("GF_Cmt_Likes");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("GlobalFeed_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Master", mPost_objectId));
        NewPlayer.put("Comment_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Comments", Comment_objectId));
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", mPlayer_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                updateCommentsMaster_Likes(Comment_objectId);
                //UpdateParseObjectWithLikes(FeedID);
            }
        });

    }

    private void updateCommentsMaster_Likes(String Comment_ObjID){
        //update COMMENTS MASTER table
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        query.whereEqualTo("objectId", Comment_ObjID);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size()>0){
                        int NoofLikes = scoreList.get(0).getInt("No_of_Likes");
                        NoofLikes++;
                        scoreList.get(0).put("No_of_Likes", NoofLikes);
                        scoreList.get(0).saveInBackground(new SaveCallback() {
                            public void done(ParseException e) {
                                //LoadPostComments();
                            }
                        });
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    public void btnClick_SubmitNewComment(View view){
        hideSoftKeyboard();
        EditText txtComment = (EditText) findViewById(R.id.txt_new_comment);
        //insert new word into History table
        final ParseObject GlobalFeed_Cmt_Comments = new ParseObject("GF_Cmt_Cmts");


        GlobalFeed_Cmt_Comments.put("GlobalFeed_objectId", ParseObject.createWithoutData("GlobalFeed_Master", mPost_objectId));
        GlobalFeed_Cmt_Comments.put("Comment_objectId", ParseObject.createWithoutData("GlobalFeed_Comments", mComment_objectId));
        GlobalFeed_Cmt_Comments.put("User_objectId", ParseObject.createWithoutData("_User", mPlayer_objectId));
        GlobalFeed_Cmt_Comments.put("Comments", txtComment.getText().toString());

        GlobalFeed_Cmt_Comments.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    mNo_of_Comments++;
                    updateCommentsMaster();
                    loadComments();

                    updateCommentsDisplay();
                    //TODO
                    //addGroupNotifications(mPostID);
                } else {
                    // The save failed.
                    Log.d("Error", e.getMessage());
                }
            }
        });

        //Clear the comment column after inserting a new entry
        txtComment.setText("");


    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void updateCommentsMaster(){
        //update COMMENTS MASTER table
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Comments");
        query.whereEqualTo("objectId", mComment_objectId);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    if(scoreList.size()>0){
                        scoreList.get(0).put("No_of_Comments", mNo_of_Comments);
                        scoreList.get(0).saveInBackground();
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updateLikesDisplay(){

        mTxtLikes.setText(String.valueOf(mNo_of_Likes));

    }

    private void updateCommentsDisplay(){

        mTxtComments.setText(String.valueOf(mNo_of_Comments));

    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public ArrayList<CommentsHolder> ConvertJSONtoArray(List<ParseObject> jsonObjects){
        ArrayList<CommentsHolder> GameHistory = new ArrayList<CommentsHolder>();

        try{
            for (int i = 0; i < jsonObjects.size(); i++) {
                GameHistory.add(new CommentsHolder(jsonObjects.get(i), this));
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }

        //ParsePush.subscribeInBackground("rick");
        return GameHistory;
    }

    public class CommentsAdapter extends ArrayAdapter<CommentsHolder> {

        private Activity activity;

        public CommentsAdapter(Context context, ArrayList<CommentsHolder> users, Activity act) {
            super(context, 0, users);
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            CommentsHolder user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_comments_secondary, parent, false);
            }

            try{
                // Populate the Post'er's Image and Name
                TextView userName = (TextView) convertView.findViewById(R.id.txt_PlayerId);
                TextView comment = (TextView) convertView.findViewById(R.id.txt_Comment);
                ImageView imgIcon = (ImageView) convertView.findViewById(R.id.user_profile_pic);
                userName.setText(user.getPlayerID());
                comment.setText(user.getComment());
                imgIcon.setImageBitmap(user.loadImageFromStorage(user.getPlayerObjectID(), activity));
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }



            // Return the completed view to render on screen
            return convertView;
        }

    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            Bitmap bmp = null;
            try{
                bmp = BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);

            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }

            return bmp;

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void OpenUserFeedPage(){
        if(mComment_User_objectId != null && mComment_User_Name != null){
            Intent intent = new Intent(this, activity_feed_targeted.class);
            intent.putExtra("PlayerObjectID", mComment_User_objectId);
            intent.putExtra("Player_Name", mComment_User_Name);
            intent.putExtra("IsGroup", false);
            startActivity(intent);
        }
    }
}
