package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 21/7/2015.
 */
public class activity_individual_pm extends AppCompatActivity {

    private List<PM_Master> mGroupArray;
    public String mPlayerID;
    public String mPlayer_objectId;
    public GroupAdapter mGroupAdapter ;
    public App mAppState;
    private LoadImageTask mLoadImageTask;
    private TextView txtLoadingCaption;
    private ListView mListView;

    private void Initialize(){

        mAppState = ((App)getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        //mRightButton = (Button) findViewById(R.id.txt_action_member);
        mListView = (ListView) findViewById(R.id.lv_groups);
        txtLoadingCaption = (TextView)findViewById(R.id.lbl_loading_caption);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_pm);
        Initialize();
        //LoadAllPMs();

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        LoadAllPMs();
    }

    private void LoadAllPMs(){
//        ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_History");
//        ParseObject obj = ParseObject.createWithoutData("_User",mPlayer_objectId);
//        query.whereEqualTo("Recipient_objectId", obj);
//        query.include("Sender_objectId");
//        query.orderByDescending("createdAt");
//        query.findInBackground(new FindCallback<ParseObject>() {
//            public void done(List<ParseObject> friendList, ParseException e) {
//                if (e == null) {
//                    Log.d("score", "Retrieved " + friendList.size() + " scores");
//                    //PlayerHistoryResults = scoreList;
//                    filterUniqueSenders(friendList);
//                } else {
//                    Log.d("score", "Error: " + e.getMessage());
//                }
//            }
//        });

        ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_UserList");
        ParseObject obj = ParseObject.createWithoutData("_User",mPlayer_objectId);
        query.whereEqualTo("User_objectId", obj);
        //query.include("Sender_objectId");
        //query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    //filterUniqueSenders(friendList);
                    getPMHistory(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


    }

    private void getPMHistory(List<ParseObject> jsonObjects){
        ArrayList<ParseObject> temp = new ArrayList<>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                temp.add(i, jsonObjects.get(i).getParseObject("PM_objectId"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_History");
        //ParseObject obj = ParseObject.createWithoutData("_User",mPlayer_objectId);
        query.whereContainedIn("PM_objectId", temp);
        query.include("PM_objectId");
        query.include("Sender_objectId");
        query.include("Recipient_objectId");
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    filterUniqueSenders(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    private void filterUniqueSenders(List<ParseObject> friendList){
        Hashtable<String, ParseObject> Sender = new Hashtable<>();
        try{
            for (int i = 0; i<friendList.size();i++){
                if(Sender.get(friendList.get(i).getParseObject("PM_objectId").getObjectId()) == null){
                    Sender.put(friendList.get(i).getParseObject("PM_objectId").getObjectId(),
                            friendList.get(i));
                }
            }

            mGroupArray = ConvertJSONtoObjects(Sender.values());
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }

        InitializeAdapterAndLV();
    }

    public ArrayList<PM_Master> ConvertJSONtoObjects(Collection<ParseObject> jsonObjects) {
        ArrayList<ParseObject> temp = new ArrayList(jsonObjects);
        ArrayList<PM_Master> GameHistory = new ArrayList<PM_Master>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PM_Master(temp.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    private void InitializeAdapterAndLV(){
        //mGroupArray = ConvertJSONtoObjects(data);
        ArrayList<PM_Master> arrayOfUsers = new ArrayList<PM_Master>();
        // Create the adapter to convert the array to views
        mGroupAdapter = new GroupAdapter(this, R.layout.item_individual_pm, arrayOfUsers, this);
        mGroupAdapter.addAll(mGroupArray);
        // Attach the adapter to a ListView
        //ListView listView = (ListView) findViewById(R.id.lv_groups);
        mListView.setAdapter(mGroupAdapter);

        //mDialog.dismiss();
        txtLoadingCaption.setVisibility(View.GONE);


    }

    public static class GroupHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtDesc;
        LinearLayout linearLayout;
        TextView txtDate;
    }

    public class GroupAdapter extends ArrayAdapter<PM_Master> {
        private Activity activity;

        public GroupAdapter(Context context, int layoutResourceId, ArrayList<PM_Master> data, Activity act) {
            super(context, layoutResourceId, data);

            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;
            PM_Master item = getItem(position);
            GroupHolder holder = null;

            if (row == null) {

                row = LayoutInflater.from(getContext()).inflate(R.layout.item_individual_pm, parent, false);
                holder = new GroupHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.ivProfileLeft);
                holder.txtTitle = (TextView) row.findViewById(R.id.tvName);
                holder.txtDesc = (TextView) row.findViewById(R.id.tvDesc);
                holder.txtDate = (TextView) row.findViewById(R.id.tvDate);
                holder.linearLayout = (LinearLayout) row.findViewById(R.id.ll_group_item);

                row.setTag(holder);

            } else {
                holder = (GroupHolder) row.getTag();
            }

            //set clickable
            //holder.linearLayout.setTag(holder.linearLayout.getId(), item.getPM_ObjectID());
            holder.linearLayout.setTag(holder.linearLayout.getId(), position);
            holder.linearLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            GoToPMHistoryPage((int)view.getTag(view.getId()));

                        }
                    });


            holder.txtTitle.setText(item.getPMContactName());
            //holder.txtDesc.setText(item.getMessage());
            holder.txtDesc.setText(item.getSenderName() + ": " + item.getMessage());

            try {

                mLoadImageTask = new LoadImageTask(holder.imgIcon, item.getPMContact_ObjectID(), activity);
                mLoadImageTask.execute(holder.imgIcon);


            } catch (Exception e) {
                Log.i("Feed_Main", e.getMessage());
            }

            holder.txtDate.setText(item.getMessageDate());
            return row;
        }

    }

    private void GoToPMHistoryPage(int Pos){
        Intent intent = new Intent(this, activity_individual_pm_history.class);
        intent.putExtra("PM_ObjectID", mGroupArray.get(Pos).getPM_ObjectID());
        intent.putExtra("User_ObjectID", mGroupArray.get(Pos).getPMContact_ObjectID());
        intent.putExtra("User_Name", mGroupArray.get(Pos).getPMContactName());
        startActivity(intent);

    }


    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

//            Bitmap temp = g.loadImageFromStorage(fileName, activity);
//            if (temp != null){
//                return g.loadImageFromStorage(fileName, activity);
//            }
//            else
//            {
//                return null;
//            }
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }



    public class PM_Master{
        private String Sender_ObjectID;
        private String Sender_Name;
        private String Recipient_ObjectID;
        private String Recipient_Name;
        private String PM_ObjectID;
        private String Message;
        private Boolean IsSeen;
        private Date CreatedAt;

        public String getPM_ObjectID(){return this.PM_ObjectID;}
        public String getObjectId(){return this.Sender_ObjectID;}
        public String getMessage(){return this.Message;}
        public Date getCreatedAt(){return this.CreatedAt;}
        public String getSenderName() { return this.Sender_Name;}
        public String getSender_ObjectID() { return this.Sender_ObjectID;}
        public String getMessageDate() {
            String temp = this.CreatedAt.toString();
            int index = temp.indexOf("GMT");
            return temp.substring(0, index);
        }

        public String getPMContactName() {
            //if you are the last person to message, your name will appear under Sender
            //but in PM, we need to display our contacts and not ourselves
            if(this.Sender_ObjectID.equals(mPlayer_objectId)){
                return this.Recipient_Name;
            }
            else
            {
                return this.Sender_Name;
            }
        }
        public String getPMContact_ObjectID(){
            //if you are the last person to message, your name will appear under Sender
            //but in PM, we need to display our contacts and not ourselves
            if(this.Sender_ObjectID.equals(mPlayer_objectId)){
                return this.Recipient_ObjectID;
            }
            else
            {
                return this.Sender_ObjectID;
            }
        }

        public PM_Master(ParseObject Obj, final Activity activity) {
            this.PM_ObjectID = Obj.getParseObject("PM_objectId").getObjectId();
            this.Sender_Name = Obj.getParseObject("Sender_objectId").getString("username");
            this.Sender_ObjectID = Obj.getParseObject("Sender_objectId").getObjectId();
            this.Recipient_Name = Obj.getParseObject("Recipient_objectId").getString("username");
            this.Recipient_ObjectID = Obj.getParseObject("Recipient_objectId").getObjectId();


            this.CreatedAt = Obj.getCreatedAt();
            this.Message = Obj.getString("Message");
            this.IsSeen = Obj.getBoolean("IsSeen");

            //load user avatar if it is not in cache
            if(!IsBitmapCached(this.Sender_ObjectID, activity)){
                ParseFile Picture = Obj.getParseObject("Sender_objectId").getParseFile("Avatar_Pic");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Sender_ObjectID, activity);

                        } else {

                        }

                    }
                });
            }

        }

        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (Exception e)
            {
                e.printStackTrace();

            }

            return null;
        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            //catch (FileNotFoundException e)
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
