package progettox.mm.newnew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.io.ByteArrayOutputStream;

/**
 * Created by Admin on 30/7/2015.
 */
public class activity_register_userid extends AppCompatActivity{

    private ProgressDialog mDialog;
    private EditText mUser_Name;
    //private EditText mUser_Password;
    private String mSimNumber;
    private ParseFile mTempAvatar;
    private App mAppState;
    private ProgressDialog mSimSignIn;

    //baidu maps api usages
    private static final String TAG = "dzt";
    private LocationClient mLocationClient;
    private LocationClientOption.LocationMode tempMode = LocationClientOption.LocationMode.Battery_Saving;
    private String tempcoor="bd09ll";
    private ParseGeoPoint mGeoLocation;

    private Boolean retrieveGeoLocation(){
        mGeoLocation = mAppState.getGeoLocPoint();
        if(mGeoLocation != null)
            return true;
        else
            return false;
    }

    private void InitLocation(){
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);//���ö�λģʽ
        option.setCoorType(tempcoor);//���صĶ�λ����ǰٶȾ�γ�ȣ�Ĭ��ֵgcj02
        int span=1000;
        try {
            //span = Integer.valueOf(frequence.getText().toString());
        } catch (Exception e) {
            // TODO: handle exception
        }
        option.setScanSpan(span);//���÷���λ����ļ��ʱ��Ϊ5000ms
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_id);
        Initialize();

    }

    private void Initialize(){

        mAppState = ((App) getApplicationContext());

        Bundle bundle = getIntent().getExtras();
        mSimNumber = bundle.getString("Sim");

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Creating your account...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        mUser_Name = (EditText) findViewById(R.id.txt_user_name);
        //mUser_Password = (EditText) findViewById(R.id.txt_user_pwd);
        Button completeReg = (Button) findViewById(R.id.btn_complete_reg);
        completeReg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                completeRegistration();
            }
        });

        //Initialize BAIDU map geo location grab
        mLocationClient = ((App)getApplication()).mLocationClient; // Class LocationClient
        InitLocation();
        mLocationClient.start();
        retrieveGeoLocation();
    }

    private void ActivateLoading(Boolean status){
        if(status)
            mDialog.show();
        else
            mDialog.dismiss();
    }

    private void completeRegistration(){
        final String username = mUser_Name.getText().toString();
        final String pwd = mSimNumber;

        if(retrieveGeoLocation()){
            ActivateLoading(true);
            final ParseUser user = new ParseUser();
            user.setUsername(username);
            user.setPassword(pwd);
            //user.setEmail("test@test.com");


            user.put("GeoLoc", mGeoLocation);
            user.put("Sim_Number", mSimNumber);
            TelephonyManager telephonyManager =
                    (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

            user.put("DeviceID", telephonyManager.getDeviceId());
            //now to insert canonical name with all lower case characters
            user.put("Canonical_username", username.toLowerCase());

            //now to put a temp user image


            //user.put("Avatar_Pic", mTempAvatar);

            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        mDialog.dismiss();
                        prepareTempAvatar(user.getObjectId());
                        GotoGameActivity(username,
                                pwd,
                                user.getObjectId());
                    } else {
                        // The save failed.
                        Log.d("Error", e.getMessage());
                    }
                }
            });
        }

    }

    private void prepareTempAvatar(final String imgFileName){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        final Bitmap tempAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.img_blank_black);
        tempAvatar.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] data = stream.toByteArray();
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        mTempAvatar = new ParseFile(imgFileName + ".png", data);
        mTempAvatar.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                // If successful add file to user and signUpInBackground
                if(null == e)
                    saveImagetoParse(imgFileName, tempAvatar);
            }
        });
    }

    private void saveImagetoParse(String ObjID, Bitmap Pic ){
        ParseUser user = ParseUser.getCurrentUser();
        //ParseUser user = ParseUser.
        user.put("Avatar_Pic", mTempAvatar);
        user.put("Avatar_Pic_Full", mTempAvatar);
        user.saveInBackground();

        GlobalPost.StorePNG(Pic, ObjID, this);

    }

    public void GotoGameActivity(String UID, String PWD, String UOBJID){


        //register into Shared Object to be retrieved later///////////
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString("PlayerID", UID);
        ed.putString("Player_ObjectID", UOBJID);
        ed.putString("PlayerPWD", PWD);
        ed.apply();
        //register into Shared Object to be retrieved later///////////

        //The 1st line is to register into Parse's Installation table to faciliate proper Push Notifications
        //ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if(e != null)
                    Log.i("ParseINstallation", e.getMessage());
            }
        });

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("username", UID);
        //installation.saveInBackground();
        installation.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if(e != null)
                    Log.i("ParseINstallation", e.getMessage());
            }
        });

        App appState = ((App)getApplicationContext());
        appState.setPlayerID(UID);
        appState.setPlayer_objectId(UOBJID);

        Intent intent = new Intent(this, MainMenuActivity.class);
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("PlayerID", UID);
        bundle.putString("Player_objectId", UOBJID);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }
}
