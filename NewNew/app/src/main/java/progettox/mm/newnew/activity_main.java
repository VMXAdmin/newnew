package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

/**
 * Created by Admin on 30/7/2015.
 */
public class activity_main extends Activity{


    private ProgressDialog mDialog;

    private void goToSignInPage(){
        Intent intent = new Intent(this, activity_register_sim.class);
        intent.putExtra("Type", 1);
        startActivity(intent);
    }

    private void goToRegisterPage(){
        Intent intent = new Intent(this, activity_register_sim.class);
        intent.putExtra("Type", 0);
        startActivity(intent);
    }

    private void goToMainPage(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void Initialize(){
        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Logging you in. Please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);
        
        Button SignIn = (Button) findViewById(R.id.btn_sign_in);
        Button Register = (Button) findViewById(R.id.btn_register);
        Button devLogin = (Button) findViewById(R.id.btn_dev_login);

        SignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToSignInPage();
            }
        });

        Register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToRegisterPage();
            }
        });


        devLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToMainPage();
            }
        });
    }

    private void ActivateLoading(Boolean status){
        if(status)
            mDialog.show();
        else
            mDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        Initialize();
    }

    @Override
    protected void onStart() {
        super.onStart();  // Always call the superclass method first

        App appState = ((App)getApplicationContext());
        appState.setPageNumber(1);

        //Check for Log In Credentials, to direct go to MainMenuActivity if Valid
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        String PlayerName = sp.getString("PlayerID", "NIL");
        String PWD = sp.getString("PlayerPWD", "NIL");
        String PlayerObjectID = sp.getString("Player_ObjectID", "NIL");

        if (!PlayerName.equals("NIL") && !PWD.equals("NIL")){
            //Do Parse Login
            ActivateLoading(true);
            ParseUserLoginSSO(PlayerName, PWD, PlayerObjectID);
        }


    }

    public void ParseUserLoginSSO(final String usernametxt, final String passwordtxt, final String PlayerObjectID){
        // Send data to Parse.com for verification
        ParseUser.logInInBackground(usernametxt, passwordtxt,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            GotoMainMenuActivity(usernametxt, passwordtxt, user.getObjectId());
                        } else {
                            ActivateLoading(false);
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Wrong username or password, please try again or signup",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void GotoMainMenuActivity(String UID, String PWD, String UOBJID){

        ActivateLoading(false);

        //register into Shared Object to be retrieved later///////////
        SharedPreferences sp = getSharedPreferences("OURINFO", MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();

        ed.putString("PlayerID", UID);
        ed.putString("Player_ObjectID", UOBJID);
        ed.putString("PlayerPWD", PWD);
        ed.apply();
        //register into Shared Object to be retrieved later///////////

        //The 1st line is to register into Parse's Installation table to faciliate proper Push Notifications
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("username", UID);
        installation.saveInBackground();

        App appState = ((App)getApplicationContext());
        appState.setPlayerID(UID);
        appState.setPlayer_objectId(UOBJID);

        Intent intent = new Intent(this, MainMenuActivity.class);
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("PlayerID", UID);
        bundle.putString("Player_objectId", UOBJID);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }

}
