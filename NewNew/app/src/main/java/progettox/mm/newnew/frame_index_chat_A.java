package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 12/8/2015.
 */
public class frame_index_chat_A extends Fragment {

    private int mRequestCode = 102;
    public String mPlayerID;
    public String mPlayer_objectId;
    public String Chat_ObjectID;
    private ProgressDialog mDialog;
    public App mAppState;
    public final static String TITLE = "CHAT";
    public ArrayList<ChatItem> mChatItemList;
    public Hashtable<String, ChatItem> mHTChatItemList;
    public ChatAdapter mAdapter;
    public ArrayList<Bitmap> mUserAvatars;
    public int mScreenHeight;
    public int mScreenWidth;
    public ListView listView;
    private String mChatID;
    private String mChatUsername;
    private String mActionType;
    private boolean mIsDirty;
    private TextView mLoadingCaption;
    private String WebAPI_ChatHistory_URL = "Api/ChatHistory/";
    private String WebAPI_ChatUserList_URL = "Api/ChatUsesrList/";

    // TODO: Rename and change types of parameters
    public static frame_index_chat_A newInstance(String param1, String param2) {
        frame_index_chat_A fragment = new frame_index_chat_A();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    public frame_index_chat_A()
    {


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAppState = ((App)getActivity().getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();
        mAppState.setGameNotification(false);
        mAppState.setPageNumber(9);

        mDialog = new ProgressDialog(getActivity());
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Retrieving Your Chats, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_main_list, container, false);
        listView = (ListView) view.findViewById(R.id.lvItems);
        mLoadingCaption = (TextView)view.findViewById(R.id.lbl_loading_caption);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

        mChatID = mAppState.getChatID();
        mChatUsername = mAppState.getChatUsername();
        mActionType = mAppState.getChatAlertType();

        loadListView();
        get_ChatUserList_ByColumnAndValue("User_objectId", mPlayer_objectId, true, true);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        mLoadingCaption.setVisibility(View.VISIBLE);
        //loadListView();
        //checkChatCache();
        //getChatSessionIDs();
        //checkForChatUpdates();


    }

    public void checkForChatUpdates(){

        //TODO
        if(mHTChatItemList == null)
        {
            getChatSessionIDs();
            mIsDirty = true;
            return;
        }
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_UserList");
        query.whereEqualTo("User_ObjectID", mPlayer_objectId);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int i, ParseException e) {

                if(mHTChatItemList.size() != i){
                    getChatSessionIDs();
                    mIsDirty = true;
                    //mDialog.show();
                }
            }
        });
    }

    private void checkChatCache(){
        SharedPreferences sp = getActivity().getSharedPreferences("OURINFO", 0);

        //check if we have cache of CHAT data
        String JSON = sp.getString("mHTChatItemList", "");
        if(JSON == null || JSON.equals("") || JSON.equals("null")){
            //here we start to load and populate the user's chat data and history
            getChatSessionIDs();
        }
        else
        {
            //here we just reload the JSON into our hashTable item
            mHTChatItemList = new Hashtable<String, ChatItem>();
            Gson gson = new Gson();
            Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            mHTChatItemList = gson.fromJson(JSON, stringStringMap);
            //updateAdapter();
            mDialog.dismiss();
        }

        //getChatSessionIDs();

    }

    private void insertChatHistory(List<ParseObject> dataList){
//        String chatID = dataList.get(0).getString("Chat_ObjectID");
//        if ( mHTChatItemList.get(chatID).getChatDetails() != null){
//            mHTChatItemList.get(chatID).getChatDetails().clear();
//        }
//
//        for (int i=0; i<dataList.size(); i++){
//            mHTChatItemList.get(dataList.get(i).getString("Chat_ObjectID")).insertChatDetails(dataList.get(i), getActivity());
//        }

        String chatID = "";
        for (int i=0; i<dataList.size(); i++){
            chatID = dataList.get(0).getString("Chat_ObjectID");
            if ( mHTChatItemList.get(chatID).getChatDetails() != null){
                mHTChatItemList.get(chatID).getChatDetails().clear();
            }
        }

        for (int i=0; i<dataList.size(); i++){
            mHTChatItemList.get(dataList.get(i).getString("Chat_ObjectID")).insertChatDetails(dataList.get(i), getActivity());
        }


    }

    private void loadListView(){

        //ArrayList<ChatItem> arrayOfUsers = new ArrayList<ChatItem>();

        //mAdapter = new ChatAdapter(getActivity().getBaseContext(), arrayOfUsers, getActivity());
        //adapter.addAll(mHTChatItemList.values());

        //listView.setAdapter(mAdapter);

    }

//    private void updateAdapter(){
//        mAdapter.clear();
//        mAdapter.addAll(mHTChatItemList.values());
//        mAdapter.notifyDataSetChanged();
//        mDialog.dismiss();
//        mLoadingCaption.setVisibility(View.GONE);
//    }

    private void updateChatItemUserAndImage(List<ParseObject> ObjList){

        for(int i=0;i<ObjList.size();i++){
            mHTChatItemList.get(ObjList.get(i).getString("Chat_ObjectID")).updateChatName(ObjList.get(i).getParseObject("Friend_objectId").getString("username"),
                    ObjList.get(i).getParseObject("Friend_objectId").getObjectId());
            mHTChatItemList.get(ObjList.get(i).getString("Chat_ObjectID")).updateChatAvatar(ObjList.get(i).getParseObject("Friend_objectId").getParseFile("Avatar_Pic"),
                    ObjList.get(i).getString("Chat_ObjectID"), getActivity());
        }


    }

    private void updateChatItemUserAndImage(ParseObject Obj, String ChatID){

        mHTChatItemList.get(ChatID).updateChatName(Obj.getString("FriendID"),
                Obj.getParseObject("Friend_objectId").getObjectId());
        mHTChatItemList.get(ChatID).updateChatAvatar(Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic"),
                ChatID, getActivity());
    }

    private void storeImage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getChatSessionDetails(List<ParseObject> dataList){

        mHTChatItemList = new Hashtable<String, ChatItem>();
        ArrayList<String> chatIDs = new ArrayList<>();

        for (int i=0; i<dataList.size(); i++){
            mHTChatItemList.put(dataList.get(i).getParseObject("Chat_objectId").getObjectId(),
                    new ChatItem(dataList.get(i), i, getActivity()));
            chatIDs.add(dataList.get(i).getParseObject("Chat_objectId").getObjectId());


        }

        //Now get get information to know which contact are we talking to and set the Name and Image
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        //query.whereEqualTo("PlayerID", mPlayerID);
        ParseObject obj = ParseObject.createWithoutData("_User", mPlayer_objectId);
        query.whereEqualTo("Player_objectId", obj);
        //query.whereEqualTo("Chat_ObjectID", ChatID);
        query.whereContainedIn("Chat_ObjectID", chatIDs);
        query.include("Friend_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0) {
                        //set object's name and image

                        //updateChatItemUserAndImage(scoreList.get(0), ChatID);
                        updateChatItemUserAndImage(scoreList);


                    }
                    else
                    {
                        //mDialog.dismiss();
                        //TODO to display textView message saying no entries has been made yet
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        //Now we get information such as user avatar photo and username and last chat
        query = ParseQuery.getQuery("Chat_History");
        //query.whereEqualTo("Chat_ObjectID", ChatID);
        query.whereContainedIn("Chat_ObjectID", chatIDs);
        query.include("User_objectId");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0){

                        insertChatHistory(scoreList);
                        //updateAdapter();


                    }

                    else
                    {
                        //mDialog.dismiss();
                        //TODO to display textView message saying no entries has been made yet
                        //updateAdapter();
                        //cacheData();
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getChatSessionIDs(){

        //TODO
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat_UserList");
        query.whereEqualTo("User_ObjectID", mPlayer_objectId);
        query.include("Chat_objectId");
        query.include("User_objectId");
        //query.addDescendingOrder("createAt");
        //query.orderByAscending("createAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size() > 0){
                        //PlayerHistoryResults = scoreList;
                        //displayJSONResults(scoreList);
                        getChatSessionDetails(scoreList);
                        //cacheUserAvatars(scoreList);
                    }
                    else
                    {
                        mDialog.dismiss();
                        mLoadingCaption.setVisibility(View.GONE);
                        //TODO to display textView message saying no entries has been made yet
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });




    }

    public class ChatAdapter extends ArrayAdapter<ChatItem_A> {

        private Activity activity;

        public ChatAdapter(Context context, ArrayList<ChatItem_A> users, Activity act) {
            super(context, 0, users);
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final ChatItem_A user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.chat_main_item, parent, false);
            }
            // Lookup view for data population
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView tvBody = (TextView) convertView.findViewById(R.id.tvBody);
            TextView tvCreatedAt = (TextView) convertView.findViewById(R.id.tvCreatedAt);
            //final ParseImageView ivProfileLeft = (ParseImageView) convertView.findViewById(R.id.ivProfileLeft);
            final ImageView ivProfileLeft = (ImageView) convertView.findViewById(R.id.ivProfileLeft);

            //now we determine what to display on the chat history list
            //first we determine what user and user image to put
            tvBody.setText(user.getDisplayBody());
            tvName.setText(user.getDisplayName());
            try{
                if(!user.getIsGroup()) {
                    if(user.loadImageFromStorage(user.getDisplayImageObjID(), activity) != null){
                        ivProfileLeft.setImageBitmap(RoundedImageView.getCircleBitmap(user.loadImageFromStorage(user.getDisplayImageObjID(), activity)));
                    }else{

                        ivProfileLeft.setImageDrawable(getResources().getDrawable(R.drawable.bg_feed_generic));
                    }

                }
                    else{
                    String tempStr = user.getPictureString();
                    byte[] x = Base64.decode(tempStr, Base64.DEFAULT);  //convert to base64
                    Bitmap theImage = BitmapFactory.decodeByteArray(x,0,x.length);

                    ivProfileLeft.setImageBitmap(RoundedImageView.getCircleBitmap(theImage));
                }
            }
            catch(Exception e){
                Log.i("Chat_Main", e.getMessage());
            }

            //TODO consider tapping a user and not typing anything yet
//            if(user.getChatDetails() != null) {
//                Collections.sort(user.getChatDetails(), new ChatDetailsComparator());
//                int index = user.getChatDetails().size() - 1;
//                tvBody.setText(user.getChatDetails().get(index).getBody());
//            }
//            else
//            {
//                tvBody.setText("Be the first to chat up this group.");
//            }

            //First set a unique for each view to that we can attach our key -> GameSession_objectId into it for onClick retrieval
            View PrimaryColumn = convertView.findViewById(R.id.primary_target);
            //PrimaryColumn.setTag(PrimaryColumn.getId(), user.getChatDetails().get(0).getObjectID());
            //Then we set the onClick function here to pull the GameSession_objectId upon each click
            PrimaryColumn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            GotoChatActivity(user.getChatID(), user.getDisplayName(),
                                    user.getDisplayObjectId(),  user.getChannel(), "");
                        }
                    });
            // Return the completed view to render on screen
            return convertView;
        }

    }

    public void GotoChatActivity(int ChatID, String ChatPlayerID, String ChatPlayerObjectID, String Channel, String AlertType){

        Intent intent = new Intent(getActivity(), activity_chat_A.class);

        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putInt("ChatID", ChatID);
        bundle.putString("ChatUsername", ChatPlayerID);
        bundle.putString("ChatUserObjectID", ChatPlayerObjectID);
        bundle.putString("Channel", Channel);
        bundle.putString("ChatAlertType", AlertType);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivity(intent);
    }

    public class ChatDetailsComparator implements Comparator<ChatDetailsHolder>
    {
        public int compare(ChatDetailsHolder left, ChatDetailsHolder right) {
            //return left.name.compareTo(right.name);
            return left.getCreatedAt().compareTo(right.getCreatedAt());
        }
    }

    @Override
    public void onStop() {
        super.onStop();  // Always call the superclass method first

        if(this.mIsDirty){
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);


            SharedPreferences sp = getActivity().getSharedPreferences("OURINFO", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();

            this.mIsDirty = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //save to cache if there are changes to mHT
        if(this.mIsDirty){
            Gson gson = new Gson();
            //Type stringStringMap = new TypeToken<Hashtable<String, ChatItem>>(){}.getType();
            String JSON = gson.toJson(mHTChatItemList);

            SharedPreferences sp = getActivity().getSharedPreferences("OURINFO", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("mHTChatItemList", JSON);
            ed.apply();

            this.mIsDirty = false;
        }

    }

    private void loadAllChats(){
         //get_ChatUserList_ByColumnAndValue
    }

    private void getChatHistories(JSONArray data){
        //ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<String> temp2 = new ArrayList<>();
        for (int i = 0; i < data.length(); i++) {
            try {
                JSONObject obj = (JSONObject)data.get(i);
                //temp.add(i, obj.getInt("Chat_objectId"));
                temp2.add(i, String.valueOf(obj.getInt("Chat_objectId")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //TODO
        get_ChatHistory_ByColumnList("Chat_objectId", temp2, true, true, true, false);

    }

    private void insertJSONintoObjects(JSONArray data){
        Hashtable<Integer, JSONObject> Sender = new Hashtable<>();
        try{
            for (int i = 0; i<data.length();i++){
                JSONObject obj = (JSONObject)data.get(i);
                if(Sender.get(obj.getInt("Chat_objectId")) == null){
                    Sender.put(obj.getInt("Chat_objectId"),
                            obj);
                }
            }

            ArrayList<JSONObject> temp = new ArrayList(Sender.values());
            ArrayList<ChatItem_A> GameHistory = new ArrayList<ChatItem_A>();
            //mGroupArray = ConvertJSONtoObjects(Sender.values());
            for (int i = 0; i < Sender.size(); i++) {
                try {
                    GameHistory.add(new ChatItem_A(temp.get(i), i, getActivity()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            InitializeAdapterAndLV(GameHistory);

        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }

        //InitializeAdapterAndLV();
    }

    private void InitializeAdapterAndLV(ArrayList<ChatItem_A> data){
        //mGroupArray = ConvertJSONtoObjects(data);
        ArrayList<ChatItem_A> arrayOfUsers = new ArrayList<ChatItem_A>();
        // Create the adapter to convert the array to views
        mAdapter = new ChatAdapter(getActivity().getBaseContext(), arrayOfUsers, getActivity());
        mAdapter.addAll(data);
        listView.setAdapter(mAdapter);
        mLoadingCaption.setVisibility(View.GONE);
    }

    //AZURE WEB API//////////////////////////////////////START

    private void get_ChatHistory_ByColumnList(String ColName, ArrayList<String> ColValue, Boolean getChatObj,
                                                  Boolean getSenderObj, Boolean getRecipientObj, Boolean isAscending
                                                  ){
        StringBuilder sb = new StringBuilder();
        for (String s : ColValue)
        {
            sb.append("\"");
            sb.append(s);
            sb.append("\",");
        }
        String colVal = sb.toString();
        colVal = colVal.substring(0, colVal.length()-1);

        String testURL =
                mAppState.getWebAPIRootURL() + WebAPI_ChatHistory_URL +
                        "GetChatHistoryByColumnList?ColumnName=" + ColName +
                        //"&ColumnValue=[\"1\",\"3\"]" + //ColValue +
                        "&ColumnValue=[" + colVal + "]" +
                        "&getUserObj=" + getSenderObj +
                        "&getChatObj=" + getChatObj +
                        "&getRecipientObj=" + getRecipientObj +
                        "&isAscending=" + isAscending;
        AT_get_ChatHistory_ByColumnAndList newTask = new AT_get_ChatHistory_ByColumnAndList(testURL);
        newTask.execute();
    }

//    private void get_ChatHistory_ByColumnAndValue(String ColName, String ColValue, Boolean getChatObj,
//                                                  Boolean getSenderObj, Boolean getRecipientObj, Boolean isAscending,
//                                                  int Count){
//
//        String testURL =
//                mAppState.getWebAPIRootURL() + WebAPI_ChatHistory_URL +
//                        "GetByColumnNameAndValueMain?ColumnName=" + ColName +
//                        "&ColumnValue=" + ColValue;
//        AT_get_ChatHistory_ByColumnAndValue newTask = new AT_get_ChatHistory_ByColumnAndValue(testURL);
//        newTask.execute();
//    }

    private void get_ChatUserList_ByChatID(String Chat_objectId){

        String testURL =
                mAppState.getWebAPIRootURL() + WebAPI_ChatUserList_URL +
                        "GetChatUsesrListByUserId/" + Chat_objectId;
        AT_get_ChatUserList_ByChatID newTask = new AT_get_ChatUserList_ByChatID(testURL);
        newTask.execute();
    }

    private void get_ChatUserList_ByColumnAndValue(String ColName, String ColValue, Boolean getChatObj,
                                                   Boolean getUserObj){

        String testURL =
                mAppState.getWebAPIRootURL() + WebAPI_ChatUserList_URL +
                        "GetUsersByColumn?ColumnName=" + ColName +
                        "&ColumnValue=" + ColValue +
                        "&getChatObj=" + getChatObj +
                        "&getUserObjID=" + getUserObj;
        AT_get_ChatUserList_ByColumnAndValue newTask = new AT_get_ChatUserList_ByColumnAndValue(testURL);
        newTask.execute();
    }

    public class AT_get_ChatHistory_ByColumnList extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        AT_get_ChatHistory_ByColumnList(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(URLHttp);
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONArray data = new JSONArray(result);

                Log.d("My App", data.get(0).toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

    public class AT_get_ChatHistory_ByColumnAndList extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        AT_get_ChatHistory_ByColumnAndList(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(URLHttp);
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONArray data = new JSONArray(result);
                insertJSONintoObjects(data);
                Log.d("My App", data.get(0).toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

    public class AT_get_ChatUserList_ByChatID extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        AT_get_ChatUserList_ByChatID(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(URLHttp);
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONArray data = new JSONArray(result);

                Log.d("My App", data.get(0).toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

    public class AT_get_ChatUserList_ByColumnAndValue extends AsyncTask<String, String, String> {

        HttpURLConnection urlConn;
        String URLHttp;

        AT_get_ChatUserList_ByColumnAndValue(String urlTest){
            URLHttp = urlTest;
        }

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(URLHttp);
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setRequestProperty("Authorization", "Bearer " + mAppState.getToken());

                int HttpResult =urlConn.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConn.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        result.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+result.toString());

                }else{
                    //System.out.println(urlConn.getResponseMessage());
                    InputStream error = urlConn.getErrorStream();
                    Log.i("Server error", urlConn.getErrorStream().toString());
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConn.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            String temp = result;
            Log.i("JSONResults", temp);
            Gson gson = new Gson();
            try {

                JSONArray data = new JSONArray(result);

                getChatHistories(data);
                Log.d("My App", data.get(0).toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }


        }

    }

    //AZURE WEB API//////////////////////////////////////END

    public class ChatItem_A
            implements Serializable {

        private int Chat_ObjectID;
        private String CreatedAt;
        private String ChatName;
        private String Body;
        private String Chat_UserObjectID;
        private String PictureString;
        private boolean IsGroup;
        private int Index;
        private ArrayList<ChatDetailsHolder> ChatDetails;
        private String Channel;
        private String Sender_ObjectID;
        private String Sender_Name;
        private String Recipient_ObjectID;
        private String Recipient_Name;
        private String Contact_ObjectID;
        private String Contact_Name;

        //PROPERTIES//////////////////////
        public String getPictureString() {return this.PictureString;}
        public String getDisplayBody(){ return this.Body;}
        public String getDisplayName() {return this.Contact_Name;}
        public String getDisplayObjectId() {return this.Contact_ObjectID;}
        public String getDisplayImageObjID() {
            if(this.IsGroup)
                return this.Contact_ObjectID;
            else
                return this.Contact_ObjectID;
        }

        //PROPERTIES//////////////////////

        public Bitmap getChatImage(){

            return null;
        }

        public String getChatUsername(){

            return this.ChatName;
        }

        public void createNewList(){
            if (ChatDetails == null)
            {
                this.ChatDetails = new ArrayList<ChatDetailsHolder>();
            }
        }

        public ArrayList<ChatDetailsHolder> getChatDetails(){
//        if (ChatDetails == null)
//        {
//            this.ChatDetails = new ArrayList<ChatDetailsHolder>();
//        }
            return ChatDetails;
        }

        public void insertChatDetails(ParseObject Obj, Activity activity){
            if (ChatDetails == null)
            {
                this.ChatDetails = new ArrayList<ChatDetailsHolder>();
            }
            this.ChatDetails.add( new ChatDetailsHolder(Obj, activity));

        }

        public String getChannel(){return this.Channel;}





        public ChatItem_A(JSONObject Obj, int c, final Activity activity) {
            //this.activity = act;
            try{
                this.Chat_ObjectID = Obj.getInt("Chat_objectId");
                this.CreatedAt = Obj.getString("createdAt");
                this.Body = Obj.getString("Body");
                //this.ChatName = Obj.getParseObject("Chat_objectId").getString("Name");
                this.Index = c;

                if(Obj.get("UserObject")!= null){
                    JSONObject Sender = (JSONObject)Obj.get("UserObject");
                    this.Sender_ObjectID = Sender.getString("Id");
                    this.Sender_Name = Sender.getString("UserName");
                }
                if(Obj.get("RecipientObject")!= null){
                    JSONObject Recipient = (JSONObject)Obj.get("RecipientObject");
                    this.Recipient_ObjectID = Recipient.getString("Id");
                    this.Recipient_Name = Recipient.getString("UserName");
                }

                this.Contact_ObjectID = Sender_ObjectID;
                this.Contact_Name = Sender_Name;
                if(this.Sender_ObjectID.equals(mPlayer_objectId)){
                    this.Contact_ObjectID = Recipient_ObjectID;
                    this.Contact_Name = Recipient_Name;
                }

                this.IsGroup = false;
                if(Obj.get("ChatMaster")!= null){
                    JSONObject ChatMaster = (JSONObject)Obj.get("ChatMaster");
                    this.IsGroup = ChatMaster.getBoolean("IsGroup");
                }

                if (this.IsGroup) {

                    if(Obj.get("ChatMaster")!= null){
                        JSONObject ChatMaster = (JSONObject)Obj.get("ChatMaster");
                        this.Contact_ObjectID = String.valueOf(ChatMaster.getInt("objectId"));
                        this.Contact_Name = ChatMaster.getString("Name");


                        if(!ChatMaster.getString("Picture").equals(null)){
                            this.PictureString = ChatMaster.getString("Picture");

                        }
                    }
                }
                //TODO when image column is ready in IdentityUsers Table
//                else{
//                    if(this.Sender_ObjectID.equals(mPlayer_objectId)){
//                        JSONObject Recipient = (JSONObject)Obj.get("RecipientObject");
//                        this.PictureString = Recipient.getString("Id");
//                    }
//                    else
//                    {
//
//                    }
//                }



            }catch(Exception e){
                Log.e("PlayerFriend_A", e.getMessage());
            }
        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);


                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            //catch (FileNotFoundException e)
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }

            return false;
        }

        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {


            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                // Create imageDir
                File mypath=new File(directory, FileName + ".png");


                FileOutputStream fos = null;
                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        public Bitmap getChatImageBmp(){
            //return this.PictureBmp;
            return null;
        }

        public void updateChatName(String name, String objID){
            if(!this.IsGroup) {
                this.ChatName = name;
                //this.Chat_ObjectID = objID;
            }
        }

        public void updateChatAvatar(ParseFile Img, final String ImageName, final Activity activity){
            if(!this.IsGroup)
            {
                if(!IsBitmapCached(ImageName, activity)){
                    ParseFile Picture = Img;
                    //attempt to convert parseFile to Bitmap
                    Picture.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {

                            if (e == null) {
                                Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                        data.length);
                                storeImage(PictureBmp, ImageName, activity);
                            } else {

                            }

                        }
                    });
                }

            }

        }

        public boolean getIsGroup(){return this.IsGroup;}

        public int getChatID(){ return this.Chat_ObjectID;}

    }

}
