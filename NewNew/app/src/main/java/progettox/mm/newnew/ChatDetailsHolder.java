package progettox.mm.newnew;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Admin on 23/5/2015.
 */
public class ChatDetailsHolder
        implements Serializable {
    private String Username;
    //private ParseFile UsersPic;
    //private Bitmap UsersPicBmp;
    private String Users_objectId;
    private String Body;
    private Date CreatedAt;
    private String createdAt;
    private String ObjectID;
    private int objectId;
    public ChatDetailsHolder(){}

    public ChatDetailsHolder(JSONObject Obj, final Activity activity){


        try{
            this.objectId = Obj.getInt("objectId");
            this.Users_objectId = Obj.getString("User_objectId");
            JSONObject Sender = (JSONObject)Obj.get("UserObject");
            this.Username = Sender.getString("UserName");
            this.Body = Obj.getString("Body");
            this.createdAt = Obj.getString("createdAt");

            if(Obj.getBoolean("IsImage")){
                if(!IsBitmapCached(String.valueOf(this.objectId), activity)){

                    String tempStr = Obj.getString("Image");
                    byte[] x = Base64.decode(tempStr, Base64.DEFAULT);  //convert to base64
                    Bitmap PictureBmp = BitmapFactory.decodeByteArray(x,0,x.length);
                    storeImage(PictureBmp, String.valueOf(objectId), activity);

                }
            }

            //TODO if Sridhar can provide image in UserDirectory table, we check and cache the image here
        }
        catch(Exception e){
            Log.i("Parse error ", e.getMessage());
        }

    }

    public ChatDetailsHolder(ParseObject Obj, final Activity activity){


        try{
            this.ObjectID = Obj.getObjectId();
            this.Users_objectId = Obj.getString("User_ObjectID");
            this.Username = Obj.getParseObject("User_objectId").getString("username");
            //this.UsersPic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
            this.Body = Obj.getString("body");
            this.CreatedAt = Obj.getCreatedAt();

            if(Obj.getParseFile("Image") != null){
                if(!IsBitmapCached(this.ObjectID, activity)){
                    ParseFile UsersPic = Obj.getParseFile("Image");
                    UsersPic.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {

                            if (e == null) {
                                Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                        data.length);
                                storeImage(PictureBmp, ObjectID, activity);

                            } else {

                            }

                        }
                    });
                }
            }


            if(!IsBitmapCached(this.Users_objectId, activity)){
                ParseFile UsersPic = Obj.getParseObject("User_objectId").getParseFile("Avatar_Pic");
                UsersPic.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Users_objectId, activity);

                        } else {

                        }

                    }
                });
            }

        }
        catch(Exception e){
            Log.i("Parse error ", e.getMessage());
        }

    }

    private boolean IsBitmapCached(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            try{
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;
            }catch (Exception e){
                return false;
            }



        }
       //catch (FileNotFoundException e)
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public Date getCreatedAt(){return this.CreatedAt;}
    //public ParseFile getProfilePic(){ return this.UsersPic;}
    //public Bitmap getProfilePic(){ return this.UsersPicBmp;}
    public String getUsername(){ return this.Username;}
    public String getBody(){ return this.Body;}
    public String getObjectID(){ return this.ObjectID;}
    public String getobjectId(){ return String.valueOf(this.objectId);}
    public String getUserObjectID(){ return this.Users_objectId;}
//
//        public void setProfilePic(ParseFile Obj){ this.profilePic = Obj;}
//        public void setPlayerID(String ID){this.PlayerID = ID;}
//        public void setComment(String Comment){this.Message = Comment;}
}
