package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.games.Player;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 18/7/2015.
 */
public class activity_group_admin_mrequests extends AppCompatActivity {

    private List<PlayerFriend> PlayerFriendsArray;
    public String PlayerID;
    public String Player_objectId;
    public AppInfoAdapter adapter ;
    public App mAppState;
    private String mGroup_objectId;
    private LoadImageTask mLoadImageTask;

    private void Initialize(){
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            mGroup_objectId = extras.getString("Group_ObjectID");
        }

        mAppState = ((App)getApplicationContext());
        PlayerID = mAppState.getPlayerID();
        Player_objectId = mAppState.getPlayer_objectId();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_invite_listview);
        Initialize();
        getMembersList();

    }

    private void updateListViewAdapter(){
        // Construct the data source
        ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
        // Create the adapter to convert the array to views
        adapter = new AppInfoAdapter(this, R.layout.item_group_settings_members, arrayOfUsers, this);
        adapter.addAll(PlayerFriendsArray);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.listApplication);
        listView.setAdapter(adapter);

    }

    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerFriendsArray = fromJson(data);
            updateListViewAdapter();
        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void getMembersList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master",mGroup_objectId);
        query.whereEqualTo("Group_objectId", obj);
        query.whereEqualTo("Member_Status", 0);
        query.include("Member_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    displayJSONResults(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    private String getUserName(String ObjID){
        String temp = "";
        for (int i = 0; i<PlayerFriendsArray.size(); i++){
            if(PlayerFriendsArray.get(i).getFriendObjectID().equals(ObjID)){
                temp = PlayerFriendsArray.get(i).getFriendID();
                break;
            }

        }

        return temp;
    }


    private void sendAddFriendNotification(String Friend_ObjectID){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", Friend_ObjectID));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 8);
        NewPlayer.put("IsAccepted", false); //By default is False

        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", mGroup_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    private void OpenUserFeedPage(String Friend_objectId){
//        ((MainMenuActivity)getActivity()).OpenUserFeedPage(
//                mGlobalPost_List.get(Feed_objectId).getPlayerID(),
//                mGlobalPost_List.get(Feed_objectId).getPlayerObjectID());
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("PlayerObjectID", Friend_objectId);
        intent.putExtra("Player_Name", getUserName(Friend_objectId));
        intent.putExtra("IsGroup", false);
        startActivity(intent);

    }

    private void approveJoinRequest(final String Member_objectId){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        ParseObject obj = ParseObject.createWithoutData("GlobalFeed_Group_Master",mGroup_objectId);
        query.whereEqualTo("Group_objectId", obj);
        query.whereEqualTo("Member_ObjectID", Member_objectId);


        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                   if(friendList.size() == 1){
                       friendList.get(0).put("Member_Status", 1);
                       friendList.get(0).saveInBackground();
                       //removeApprovedMember(Member_objectId);
                   }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        //sent notification to user to tell them their request has been approved
        sendAddFriendNotification(Member_objectId);
    }

    private void removeApprovedMember(String Member_objectID){
        for(int i = 0; i < PlayerFriendsArray.size();i++){
            if(PlayerFriendsArray.get(i).getFriendObjectID().equals(Member_objectID)){
                PlayerFriendsArray.remove(i);
                break;
            }
        }

        adapter.notifyDataSetChanged();

    }

    public static class AppInfoHolder
    {
        //ImageView imgIcon;
        ImageView imgIcon;
        TextView txtTitle;
        Button chkSelect;

    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.menu_source_icon);
                holder.txtTitle = (TextView) row.findViewById(R.id.menu_source_name);
                holder.chkSelect = (Button) row.findViewById(R.id.txt_action_member);

                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.FriendID);
            holder.txtTitle.setTag(holder.txtTitle.getId(), appinfo.getFriendObjectID());
            holder.txtTitle.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                            //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                            OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                        }
                    });

            //holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.Friend_objectId, activity));
            try{

                mLoadImageTask = new LoadImageTask(holder.imgIcon, appinfo.Friend_objectId,
                        activity);
                mLoadImageTask.execute(holder.imgIcon);
                holder.imgIcon.setTag(holder.imgIcon.getId(), appinfo.getFriendObjectID());
                holder.imgIcon.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));

                                //OpenFeedDetails(String.valueOf(view.getTag(view.getId())));
                                OpenUserFeedPage(String.valueOf(view.getTag(view.getId())));

                            }
                        });

            }
            catch(Exception e){
                Log.i("Group Settings Members", e.getMessage());
            }

            holder.chkSelect.setText(getResources().getText(R.string.lbl_approve_join));
            holder.chkSelect.setTag(holder.chkSelect.getId(), appinfo.getFriendObjectID());
            holder.chkSelect.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView temp = (TextView) view;
                            temp.setText(getResources().getText(R.string.lbl_approved_join));
                            temp.setClickable(false);
                            temp.setBackgroundColor(getResources().getColor(R.color.white));
                            approveJoinRequest(String.valueOf(view.getTag(view.getId())));

                        }
                    });
            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            //holder.PostImages[i].setImageBitmap(g.loadImageFromStorage(fileName, getActivity()));
            //return g.loadImageFromStorage(fileName, activity);
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
            //v = params[0];
            //return mFakeImageLoader.getImage();
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            v.setImageBitmap(result);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        // Respond to the action bar's Up/Home button
        if (id == android.R.id.home)
        {
            //NavUtils.navigateUpFromSameTask(this);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
