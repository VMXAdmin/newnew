package progettox.mm.newnew;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Admin on 19/5/2015.
 */
@ParseClassName("Chat_History")
public class Message extends ParseObject {
    public String getUserId() {
        return getString("User_ObjectID");
    }

    public String getBody() {
        return getString("body");
    }

    public void setUserId(String userId) {
        put("User_ObjectID", userId);
    }

    public void setUserObjectId(String userId) {
        put("User_objectId", ParseObject.createWithoutData("_User", userId));
    }

    public void setChatId(String chatId) {
        put("Chat_ObjectID", chatId);
    }

    public void setChatObjectId(String chatId) {
        put("Chat_objectId", ParseObject.createWithoutData("Chat_Master", chatId));
    }

    public void setBody(String body) {
        put("body", body);
    }
}
