package progettox.mm.newnew;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Admin on 6/5/2015.
 */
public class frame_index_main extends Fragment{

    public String mPlayerID;
    public String mPlayer_objectId;
    private View myFragmentView;
    private List<Group> mGroupArray;
    private LoadImageTask mLoadImageTask;
    private GroupAdapter mGroupAdapter;
    private App mAppState;
    //private ProgressDialog mDialog;
    private ListView listView;
    private TextView txtLoadingCaption;


    // TODO: Rename and change types of parameters
    public static frame_index_main newInstance(String param1, String param2) {
        frame_index_main fragment = new frame_index_main();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public frame_index_main()
    {


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_index_main, container, false);
        listView = (ListView) view.findViewById(R.id.lv_groups);
        Button btnOpenGlobalFeed = (Button) view.findViewById(R.id.btn_open_globalfeed);
        btnOpenGlobalFeed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToGlobalFeed(0);
            }
        });

        Button btnOpenTutorFeed = (Button) view.findViewById(R.id.btn_open_tutorfeed);
        btnOpenTutorFeed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToGlobalFeed(2);
            }
        });

        Button btnOpenFriendFeed = (Button) view.findViewById(R.id.btn_open_friendfeed);
        btnOpenFriendFeed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToGlobalFeed(1);
            }
        });




        Button btnCreateNewGroup = (Button) view.findViewById(R.id.btn_create_newGroup);
        btnCreateNewGroup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GoToCreateNewGroup();
            }
        });



        Button btnJoinNewGroup = (Button) view.findViewById(R.id.btn_join_newGroup);
        btnJoinNewGroup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(),
//                        "This feature is working in progress",
//                        Toast.LENGTH_SHORT).show();
                goToJoinGroupPage();

            }
        });

        txtLoadingCaption = (TextView)view.findViewById(R.id.lbl_loading_caption);
        return view;
    }

    private void openTestMap(){
        ((MainMenuActivity)getActivity()).OpenTestMap();
    }

    private void Initialize(){
        mAppState = ((App)getActivity().getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        txtLoadingCaption.setVisibility(View.VISIBLE);

//        mDialog = new ProgressDialog(getActivity());
//        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        mDialog.setMessage("Loading your groups, please wait...");
//        mDialog.setIndeterminate(true);
//        mDialog.setCanceledOnTouchOutside(false);

        LoadUserGroups();
        //mDialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void LoadUserGroups(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        query.whereEqualTo("Member_ObjectID", mPlayer_objectId);
        query.whereEqualTo("Member_Status", 1);

        query.include("Group_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        InitializeAdapterAndLV(groupList);
                    }
                    else
                    {
                        txtLoadingCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });

    }

    private void InitializeAdapterAndLV(List<ParseObject> data){
        mGroupArray = ConvertJSONtoObjects(data);

        // Construct the data source
        ArrayList<Group> arrayOfUsers = new ArrayList<Group>();
        // Create the adapter to convert the array to views
        mGroupAdapter = new GroupAdapter(getActivity(), R.layout.group_browser_item, arrayOfUsers, getActivity());
        mGroupAdapter.addAll(mGroupArray);
        // Attach the adapter to a ListView
        //ListView listView = (ListView) findViewById(R.id.lv_groups);
        listView.setAdapter(mGroupAdapter);

        //mDialog.dismiss();
        txtLoadingCaption.setVisibility(View.GONE);
    }

    public ArrayList<Group> ConvertJSONtoObjects(List<ParseObject> jsonObjects) {
        ArrayList<Group> GameHistory = new ArrayList<Group>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new Group(jsonObjects.get(i), getActivity()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }



    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        Initialize();
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

    }

    public static class GroupHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        ImageView imgIsTutor;
        LinearLayout linearLayout;

    }

    private Boolean getIsTutor(String objID){
        for (int i = 0; i < mGroupArray.size(); i++){
            if(mGroupArray.get(i).getObjectID().equals(objID))
                return mGroupArray.get(i).getIsTutorGroup();
        }

        return false;

    }

    private String getGroupName(String ObjId){
        for (int i = 0; i < mGroupArray.size(); i++){
            if(mGroupArray.get(i).getObjectID().equals(ObjId))
                return mGroupArray.get(i).getGroupName();
        }

        return "";
    }



    public class GroupAdapter extends ArrayAdapter<Group>
    {
        private Activity activity;

        public GroupAdapter(Context context, int layoutResourceId, ArrayList<Group> data, Activity act){
            super(context, layoutResourceId,data);

            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            Group item = getItem(position);
            GroupHolder holder= null;

            if (row == null){

                row = LayoutInflater.from(getContext()).inflate(R.layout.group_browser_item, parent, false);
                holder = new GroupHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.ivProfileLeft);
                holder.txtTitle = (TextView) row.findViewById(R.id.tvName);
                holder.imgIsTutor = (ImageView) row.findViewById(R.id.ivProfileRight);
                holder.linearLayout = (LinearLayout) row.findViewById(R.id.ll_group_item);

                row.setTag(holder);

            }
            else{
                holder = (GroupHolder)row.getTag();
            }

            //set clickable
            holder.linearLayout.setTag(holder.linearLayout.getId(), item.getObjectID());
            //holder.linearLayout.setTag(mGroupNameTag, item.getGroupName());

            holder.linearLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            GoToGroup(String.valueOf(view.getTag(view.getId())));

                        }
                    });


            holder.txtTitle.setText(item.getGroupName());



            try{
                if(item.getIsTutorGroup()){
                    mLoadImageTask = new LoadImageTask(holder.imgIcon, item, item.getObjectID()+"_avatar", activity);
                    mLoadImageTask.execute(holder.imgIcon);

                    holder.imgIsTutor.setVisibility(View.VISIBLE);
                    holder.imgIsTutor.setBackgroundResource(R.drawable.ic_tutor);

                }
                else{
                    mLoadImageTask = new LoadImageTask(holder.imgIcon, item, item.getObjectID(), activity);
                    mLoadImageTask.execute(holder.imgIcon);

                    holder.imgIsTutor.setVisibility(View.INVISIBLE);
                }


            }
            catch(Exception e){
                Log.i("Feed_Main", e.getMessage());
            }




            return row;

        }


    }


//    public class Group {
//        private String objectId;
//        private String name;
//        private Boolean isTutorGroup;
//
//
//        //PROPERTIES//////////////////
//        public String getObjectID(){return this.objectId;}
//        public String getGroupName(){return this.name;}
//        public Boolean getIsTutorGroup(){return this.isTutorGroup;}
//        //PROPERTIES//////////////////
//
//        public Group(ParseObject Obj, final Activity activity) {
//            this.objectId = Obj.getParseObject("Group_objectId").getObjectId();
//            this.name = Obj.getParseObject("Group_objectId").getString("Name");
//            this.isTutorGroup = Obj.getParseObject("Group_objectId").getBoolean("IsTutorGroup");
//
//            //first we cache the group image
//            if(!IsBitmapCached(this.objectId, activity)){
//                ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Image");
//                if(Picture != null){
//                    Picture.getDataInBackground(new GetDataCallback() {
//                        @Override
//                        public void done(byte[] data, ParseException e) {
//
//                            if (e == null) {
//                                Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
//                                        data.length);
//                                storeImage(PictureBmp, objectId, activity);
//
//                            } else {
//
//                            }
//
//                        }
//                    });
//                }
//
//            }
//
//            //if this group is a tutor group, we also need to cache the tutor's avatar image
//            if(isTutorGroup){
//                final String fileName = this.objectId + "_avatar";
//                if(!IsBitmapCached(fileName, activity)){
//                    ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Avatar");
//                    if(Picture != null){
//                        Picture.getDataInBackground(new GetDataCallback() {
//                            @Override
//                            public void done(byte[] data, ParseException e) {
//
//                                if (e == null) {
//                                    Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
//                                            data.length);
//                                    storeImage(PictureBmp, fileName, activity);
//
//                                } else {
//
//                                }
//
//                            }
//                        });
//                    }
//
//                }
//            }
//
//
//        }
//
//        private boolean IsBitmapCached(String FileName, Activity activity){
//            try {
//                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
//                // path to /data/data/yourapp/app_data/imageDir
//                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//
//                File f=new File(directory, FileName + ".png");
//                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
//                if (b != null)
//                    return true;
//
//            }
//            catch (FileNotFoundException e)
//            {
//                e.printStackTrace();
//            }
//
//            return false;
//        }
//
//        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
//            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
//            // path to /data/data/yourapp/app_data/imageDir
//            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//            // Create imageDir
//            File mypath=new File(directory, FileName + ".png");
//
//            FileOutputStream fos = null;
//            try {
//
//                fos = new FileOutputStream(mypath);
//
//                // Use the compress method on the BitMap object to write image to the OutputStream
//                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
//                fos.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            //return directory.getAbsolutePath();
//        }
//
//        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
//        {
//
//            try {
//                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
//                // path to /data/data/yourapp/app_data/imageDir
//                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//
//                File f=new File(directory, BitmapName + ".png");
//                return BitmapFactory.decodeStream(new FileInputStream(f));
//                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
//                //img.setImageBitmap(b);
//                //return b;
//            }
//            catch (FileNotFoundException e)
//            {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//
//    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, Group globalPost, String FileName, Activity act) {
            v = view;
            g = globalPost;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

//            Bitmap temp = g.loadImageFromStorage(fileName, activity);
//            if (temp != null){
//                return g.loadImageFromStorage(fileName, activity);
//            }
//            else
//            {
//                return null;
//            }
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }

    public void goToJoinGroupPage(){
//        Intent intent = new Intent(this, CreateNewGlobalFeedGroup.class);
//        startActivity(intent);
        ((MainMenuActivity)getActivity()).GoToJoinGroupPage();
    }


    public void goToGlobalFeed(int FeedCategory){
//        Intent intent = new Intent(this, CreateNewGlobalFeedGroup.class);
//        startActivity(intent);
        ((MainMenuActivity)getActivity()).OpenGlobalFeed(FeedCategory);
    }

    private void GoToGroup(String objID){
//        Intent intent = new Intent(this, activity_feed_targeted.class);
//        intent.putExtra("GroupObjectID", objID);
//        intent.putExtra("GroupName", getGroupName(objID));
//        intent.putExtra("IsGroup", true);
//        intent.putExtra("IsTutor", getIsTutor(objID));
//        startActivity(intent);
        ((MainMenuActivity)getActivity()).GoToFeedGroup(objID, getGroupName(objID),
                true, getIsTutor(objID));

    }

    public void GoToCreateNewGroup(){
//        Intent intent = new Intent(this, CreateNewGlobalFeedGroup.class);
//        startActivity(intent);
        ((MainMenuActivity)getActivity()).goToCreateNewGroup();
    }

}
