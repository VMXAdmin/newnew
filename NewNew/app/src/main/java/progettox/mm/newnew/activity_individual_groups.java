package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 21/7/2015.
 */
public class activity_individual_groups extends AppCompatActivity {

    private List<Group> mGroupArray;
    public String PlayerID;
    public String Player_objectId;
    public String mUser_ObjectID;
    public String mUser_Name;
    public GroupAdapter mGroupAdapter ;
    public App mAppState;
    private LoadImageTask mLoadImageTask;
    private TextView txtLoadingCaption;
    private ListView mListView;
    private Boolean mIsSelf;
    private String mGroup_objectId;
    //private Button mRightButton;
    //private int mProfileType; //1 is user, 2 is group, 3 is tutor

    private void Initialize(){



        Bundle extras = getIntent().getExtras();
        if (extras != null){
//            mProfileType = extras.getInt("Profile_Type");
//            if(mProfileType == 2)
//                mGroup_objectId = extras.getString("Group_ObjectID");
//            else if(mProfileType == 1){
                mUser_ObjectID = extras.getString("User_ObjectID");
                mUser_Name = extras.getString("User_Name");
            //}

        }
        mIsSelf = false;


        mAppState = ((App)getApplicationContext());
        PlayerID = mAppState.getPlayerID();
        Player_objectId = mAppState.getPlayer_objectId();

        //mRightButton = (Button) findViewById(R.id.txt_action_member);
        mListView = (ListView) findViewById(R.id.lv_groups);
        txtLoadingCaption = (TextView)findViewById(R.id.lbl_loading_caption);

        if(mUser_ObjectID.equals(Player_objectId))
            mIsSelf = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_groups);
        Initialize();

        getGroupList();



    }

    private void updateSelfGroups(){
        for (int i = 0; i<mGroupArray.size(); i++){
           mGroupArray.get(i).setMemberStatus(2);
        }

        updateListViewAdapter();
    }

    private void filterGroupList(List<ParseObject> friendList){

        try{
            if(friendList.size() > 0){
                for (int i = 0; i<mGroupArray.size(); i++){
                    for(int j = 0; j<friendList.size();j++){
                        String Friend_ObjectID = friendList.get(j).getParseObject("Group_objectId").getObjectId();
                        if(Friend_ObjectID.equals(mGroupArray.get(i).getObjectID())){

                            if(friendList.get(j).getInt("Member_Status") == 1)
                            {
                                mGroupArray.get(i).setMemberStatus(2);
                            }
                            else
                            {
                                mGroupArray.get(i).setMemberStatus(1);

                            }

                        }
                    }
                }
            }
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }




        updateListViewAdapter();
    }

    private void getSelfGroupList(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        query.whereEqualTo("Member_ObjectID", Player_objectId);
        //query.whereEqualTo("Member_Status", 1);
        query.include("Group_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        filterGroupList(groupList);
                    }
                    else
                    {
                        txtLoadingCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void getGroupList(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        query.whereEqualTo("Member_ObjectID", mUser_ObjectID);
        query.whereEqualTo("Member_Status", 1);
        query.include("Group_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    if(groupList.size() > 0){
                        Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                        InitializeAdapterAndLV(groupList);
                    }
                    else
                    {
                        txtLoadingCaption.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updateListViewAdapter(){
        // Construct the data source
        ArrayList<Group> arrayOfUsers = new ArrayList<Group>();
        // Create the adapter to convert the array to views
        mGroupAdapter = new GroupAdapter(this, R.layout.group_browser_item, arrayOfUsers, this);
        mGroupAdapter.addAll(mGroupArray);
        // Attach the adapter to a ListView
        //ListView listView = (ListView) findViewById(R.id.lv_groups);
        mListView.setAdapter(mGroupAdapter);

        //mDialog.dismiss();
        txtLoadingCaption.setVisibility(View.GONE);

    }

    private void InitializeAdapterAndLV(List<ParseObject> data){
        mGroupArray = ConvertJSONtoObjects(data);

        if(!mIsSelf)
            getSelfGroupList();
        else
            updateSelfGroups();


    }

    public ArrayList<Group> ConvertJSONtoObjects(List<ParseObject> jsonObjects) {
        ArrayList<Group> GameHistory = new ArrayList<Group>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new Group(jsonObjects.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

    public void GoToFeedGroup(Group Obj){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("GroupObjectID", Obj.getObjectID());
        intent.putExtra("GroupName", Obj.getGroupName());
        intent.putExtra("IsGroup", true);
        intent.putExtra("IsTutor", Obj.getIsTutorGroup());
        startActivity(intent);
    }

    private void joinGroup(final Group Obj){

        final ParseObject gameScore = new ParseObject("GlobalFeed_Group_Members");
        gameScore.put("Group_ObjectID", Obj.getObjectID());
        gameScore.put("Group_objectId", ParseObject.createWithoutData("GlobalFeed_Group_Master",
                Obj.getObjectID()));
        gameScore.put("Member_ObjectID", Player_objectId);
        gameScore.put("Member_objectId", ParseObject.createWithoutData("_User",
                Player_objectId));
        gameScore.put("Member_Status", 0);
        gameScore.put("IsNotify", true); //by default, group notifications are set to ON
        gameScore.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
                sendJoinGroupNotification(Obj.getCreatorObjectID(), Obj.getObjectID());
            }
        });
    }


    private void sendJoinGroupNotification(String groupCreator, String GroupObjID){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", groupCreator));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 7);
        NewPlayer.put("IsAccepted", false); //By default is False
        NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", GroupObjID));
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });


    }

    private void sendJoinGroupNotification_2(String groupCreator){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", groupCreator));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 7);
        NewPlayer.put("IsAccepted", false); //By default is False
        NewPlayer.put("Group_objectId", ParseObject.createWithoutData(
                "GlobalFeed_Group_Master", mGroup_objectId));
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    public static class GroupHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtDesc;
        TextView txtNoOfMembers;
        Button rightButton;
        LinearLayout linearLayout;

    }

    public class GroupAdapter extends ArrayAdapter<Group> {
        private Activity activity;

        public GroupAdapter(Context context, int layoutResourceId, ArrayList<Group> data, Activity act) {
            super(context, layoutResourceId, data);

            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;
            Group item = getItem(position);
            GroupHolder holder = null;

            if (row == null) {

                row = LayoutInflater.from(getContext()).inflate(R.layout.item_individual_groups, parent, false);
                holder = new GroupHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.ivProfileLeft);
                holder.txtTitle = (TextView) row.findViewById(R.id.tvName);
                holder.txtDesc = (TextView) row.findViewById(R.id.tvDesc);
                holder.txtNoOfMembers = (TextView) row.findViewById(R.id.tvMembers);
                holder.rightButton = (Button) row.findViewById(R.id.txt_action_member);
                holder.linearLayout = (LinearLayout) row.findViewById(R.id.ll_group_item);

                row.setTag(holder);

            } else {
                holder = (GroupHolder) row.getTag();
            }

            //set clickable
            holder.linearLayout.setTag(holder.linearLayout.getId(), item);
            //holder.linearLayout.setTag(mGroupNameTag, item.getGroupName());

            holder.linearLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            GoToFeedGroup((Group) view.getTag(view.getId()));

                        }
                    });


            holder.txtTitle.setText(item.getGroupName());
            holder.txtDesc.setText(item.getGroupDescription());
            holder.txtNoOfMembers.setText(String.valueOf(item.getTotalNoOfMembers()) + " " +
            getResources().getText(R.string.lbl_members));

            try {

                mLoadImageTask = new LoadImageTask(holder.imgIcon, item.getObjectID(), activity);
                mLoadImageTask.execute(holder.imgIcon);


            } catch (Exception e) {
                Log.i("Feed_Main", e.getMessage());
            }

            try{
                if (item.getMemberStatus() == 2) {
                    holder.rightButton.setVisibility(View.GONE);
                    holder.rightButton.setClickable(false);
                } else if (item.getMemberStatus() == 1) {
                    holder.rightButton.setVisibility(View.VISIBLE);
                    holder.rightButton.setText(getResources().getText(R.string.btn_joined_group));
                    holder.rightButton.setTextColor(getResources().getColor(R.color.inputText));
                    holder.rightButton.setBackgroundColor(getResources().getColor(R.color.white));
                    holder.rightButton.setClickable(false);
                } else if (item.getMemberStatus() == 0) {
                    holder.rightButton.setVisibility(View.VISIBLE);
                    holder.rightButton.setText(getResources().getText(R.string.btn_group_join));
                    holder.rightButton.setTag(holder.rightButton.getId(), item);
                    holder.rightButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView temp = (TextView) view;
                            temp.setText(getResources().getText(R.string.btn_joined_group));
                            temp.setClickable(false);
                            temp.setTextColor(getResources().getColor(R.color.inputText));
                            temp.setBackgroundColor(getResources().getColor(R.color.white));
                            joinGroup((Group) view.getTag(view.getId()));
                        }
                    });


                }
            }catch(Exception e){
                Log.e("Error", e.getMessage());
            }

            return row;
        }

    }


    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, String FileName, Activity act) {
            v = view;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

//            Bitmap temp = g.loadImageFromStorage(fileName, activity);
//            if (temp != null){
//                return g.loadImageFromStorage(fileName, activity);
//            }
//            else
//            {
//                return null;
//            }
            return BitmapManager.decodeSampledBitmap(fileName, 50, 50, activity);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
