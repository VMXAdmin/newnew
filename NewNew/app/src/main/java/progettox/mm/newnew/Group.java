package progettox.mm.newnew;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 21/7/2015.
 */
public class Group implements Serializable{
    private String objectId;
    private String name;
    private Boolean isTutorGroup;
    private int NoOfMembers;
    private String Desc;
    private String Creator_ObjectID;
    private int MemberStatus; //0 means not a member, 1 means requested, 2 means is a member
//    private List<String> Member_Names;
//    private List<String> Member_objectIds;
    private Hashtable <String, String> Members;

    //PROPERTIES//////////////////
    public String getCreatorObjectID(){return this.Creator_ObjectID;}
    public int getMemberStatus(){return this.MemberStatus;}
    public void setMemberStatus(int i){this.MemberStatus = i;}
    public int getTotalNoOfMembers(){return this.NoOfMembers;}
    public String getGroupDescription() {return this.Desc;}
    public String getObjectID(){return this.objectId;}
    public String getGroupName(){return this.name;}
    public Boolean getIsTutorGroup(){return this.isTutorGroup;}
    public int getTotalNoOfMembersHT() {return this.Members.size();}
    public String getFirstMember(){
        Map.Entry<String,String> entry = Members.entrySet().iterator().next();
        return entry.getValue();
    }
    public void insertNewMember(String ObjID, String Name){
        if(Members == null){
            Members = new Hashtable<>();
        }

        Members.put(ObjID, Name);
    }
    //PROPERTIES//////////////////

    public Group(ParseObject Obj, final Activity activity) {
        this.Creator_ObjectID = Obj.getParseObject("Group_objectId").getString("Creator_ObjectID");
        this.objectId = Obj.getParseObject("Group_objectId").getObjectId();
        this.name = Obj.getParseObject("Group_objectId").getString("Name");
        this.isTutorGroup = Obj.getParseObject("Group_objectId").getBoolean("IsTutorGroup");
        this.Desc = Obj.getParseObject("Group_objectId").getString("Description");
        this.NoOfMembers = Obj.getParseObject("Group_objectId").getInt("TotalMembers");
        this.MemberStatus = 0;


        //first we cache the group image
        if(!IsBitmapCached(this.objectId, activity)){
            ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Image");
            if(Picture != null){
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, objectId, activity);

                        } else {

                        }

                    }
                });
            }

        }

        //if this group is a tutor group, we also need to cache the tutor's avatar image
        if(isTutorGroup){
            final String fileName = this.objectId + "_avatar";
            if(!IsBitmapCached(fileName, activity)){
                ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Avatar");
                if(Picture != null){
                    Picture.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {

                            if (e == null) {
                                Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                        data.length);
                                storeImage(PictureBmp, fileName, activity);

                            } else {

                            }

                        }
                    });
                }

            }
        }


    }

    private boolean IsBitmapCached(String FileName, Activity activity){
        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, FileName + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            if (b != null)
                return true;

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, FileName + ".png");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return directory.getAbsolutePath();
    }

    public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
    {

        try {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            File f=new File(directory, BitmapName + ".png");
            return BitmapFactory.decodeStream(new FileInputStream(f));
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            //img.setImageBitmap(b);
            //return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return null;
    }



}
