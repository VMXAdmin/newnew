package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 10/6/2015.
 */
public class globalfeed_groups_browser extends AppCompatActivity {

    private List<Group> mGroupArray;
    private LoadImageTask mLoadImageTask;
    public String mPlayerID;
    public String mPlayer_objectId;
    private GroupAdapter mGroupAdapter;
    private App mAppState;
    private ProgressDialog mDialog;
    private static final int mGroupNameTag = 10001000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.globalfeed_group_browser);

    }

    private void Initialize(){
        mAppState = ((App)getApplicationContext());
        mPlayerID = mAppState.getPlayerID();
        mPlayer_objectId = mAppState.getPlayer_objectId();

        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Loading your groups, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        LoadUserGroups();
        mDialog.show();
    }

    private void LoadUserGroups(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GlobalFeed_Group_Members");
        query.whereEqualTo("Member_ObjectID", mPlayer_objectId);
        query.whereEqualTo("Member_Status", 1);
        query.include("Group_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupList, ParseException e) {
                if (e == null) {
                    Log.d("Groups", "Retrieved " + groupList.size() + " scores");
                    InitializeAdapterAndLV(groupList);
                } else {
                    Log.d("Groups", "Error: " + e.getMessage());
                }
            }
        });

    }

    public void onClick_OpenGlobalFeed(View view){
        Intent intent = new Intent(this, globalfeed_main.class);
        startActivity(intent);
    }

    private void InitializeAdapterAndLV(List<ParseObject> data){
        mGroupArray = ConvertJSONtoObjects(data);

        // Construct the data source
        ArrayList<Group> arrayOfUsers = new ArrayList<Group>();
        // Create the adapter to convert the array to views
        mGroupAdapter = new GroupAdapter(this, R.layout.group_browser_item, arrayOfUsers, this);
        mGroupAdapter.addAll(mGroupArray);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.lv_groups);
        listView.setAdapter(mGroupAdapter);

        mDialog.dismiss();
    }

    public ArrayList<Group> ConvertJSONtoObjects(List<ParseObject> jsonObjects) {
        ArrayList<Group> GameHistory = new ArrayList<Group>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new Group(jsonObjects.get(i), this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }



    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first
        Initialize();
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

    }

    public static class GroupHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
        ImageView imgIsTutor;
        LinearLayout linearLayout;

    }

    private Boolean getIsTutor(String objID){
        for (int i = 0; i < mGroupArray.size(); i++){
            if(mGroupArray.get(i).getObjectID().equals(objID))
                return mGroupArray.get(i).getIsTutorGroup();
        }

        return false;

    }

    private String getGroupName(String ObjId){
        for (int i = 0; i < mGroupArray.size(); i++){
            if(mGroupArray.get(i).getObjectID().equals(ObjId))
                return mGroupArray.get(i).getGroupName();
        }

        return "";
    }

    private void GoToGroup(String objID){
        Intent intent = new Intent(this, activity_feed_targeted.class);
        intent.putExtra("GroupObjectID", objID);
        intent.putExtra("GroupName", getGroupName(objID));
        intent.putExtra("IsGroup", true);
        intent.putExtra("IsTutor", getIsTutor(objID));
        startActivity(intent);
    }

    public class GroupAdapter extends ArrayAdapter<Group>
    {
        private Activity activity;

        public GroupAdapter(Context context, int layoutResourceId, ArrayList<Group> data, Activity act){
            super(context, layoutResourceId,data);

            activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            Group item = getItem(position);
            GroupHolder holder= null;

            if (row == null){

                row = LayoutInflater.from(getContext()).inflate(R.layout.group_browser_item, parent, false);
                holder = new GroupHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.ivProfileLeft);
                holder.txtTitle = (TextView) row.findViewById(R.id.tvName);
                holder.imgIsTutor = (ImageView) row.findViewById(R.id.ivProfileRight);
                holder.linearLayout = (LinearLayout) row.findViewById(R.id.ll_group_item);

                row.setTag(holder);

            }
            else{
                holder = (GroupHolder)row.getTag();
            }

            //set clickable
            holder.linearLayout.setTag(holder.linearLayout.getId(), item.getObjectID());
            //holder.linearLayout.setTag(mGroupNameTag, item.getGroupName());

            holder.linearLayout.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            GoToGroup(String.valueOf(view.getTag(view.getId())));

                        }
                    });


            holder.txtTitle.setText(item.getGroupName());



            try{
                if(item.getIsTutorGroup()){
                    mLoadImageTask = new LoadImageTask(holder.imgIcon, item, item.getObjectID()+"_avatar", activity);
                    mLoadImageTask.execute(holder.imgIcon);

                    holder.imgIsTutor.setVisibility(View.VISIBLE);
                    holder.imgIsTutor.setBackgroundResource(R.drawable.ic_tutor);

                }
                else{
                    mLoadImageTask = new LoadImageTask(holder.imgIcon, item, item.getObjectID(), activity);
                    mLoadImageTask.execute(holder.imgIcon);

                    holder.imgIsTutor.setVisibility(View.INVISIBLE);
                }


            }
            catch(Exception e){
                Log.i("Feed_Main", e.getMessage());
            }




            return row;

        }


    }


    public class Group {
        private String objectId;
        private String name;
        private Boolean isTutorGroup;


        //PROPERTIES//////////////////
        public String getObjectID(){return this.objectId;}
        public String getGroupName(){return this.name;}
        public Boolean getIsTutorGroup(){return this.isTutorGroup;}
        //PROPERTIES//////////////////

        public Group(ParseObject Obj, final Activity activity) {
            this.objectId = Obj.getParseObject("Group_objectId").getObjectId();
            this.name = Obj.getParseObject("Group_objectId").getString("Name");
            this.isTutorGroup = Obj.getParseObject("Group_objectId").getBoolean("IsTutorGroup");

            //first we cache the group image
            if(!IsBitmapCached(this.objectId, activity)){
                ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Image");
                if(Picture != null){
                    Picture.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {

                            if (e == null) {
                                Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                        data.length);
                                storeImage(PictureBmp, objectId, activity);

                            } else {

                            }

                        }
                    });
                }

            }

            //if this group is a tutor group, we also need to cache the tutor's avatar image
            if(isTutorGroup){
                final String fileName = this.objectId + "_avatar";
                if(!IsBitmapCached(fileName, activity)){
                    ParseFile Picture = Obj.getParseObject("Group_objectId").getParseFile("Avatar");
                    if(Picture != null){
                        Picture.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {

                                if (e == null) {
                                    Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                            data.length);
                                    storeImage(PictureBmp, fileName, activity);

                                } else {

                                }

                            }
                        });
                    }

                }
            }


        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return false;
        }

        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return null;
        }


    }

    public class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {
        private ImageView v;
        private Group g;
        private String fileName;
        private Activity activity;

        LoadImageTask(ImageView view, Group globalPost, String FileName, Activity act) {
            v = view;
            g = globalPost;
            fileName = FileName;
            activity = act;
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {

            Bitmap temp = g.loadImageFromStorage(fileName, activity);
            if (temp != null){
                return g.loadImageFromStorage(fileName, activity);
            }
            else
            {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null){
                v.setImageBitmap(result);
            }
            else
            {
                //v.setImageBitmap(R.drawable.bg_feed_generic);
                v.setBackgroundResource(R.drawable.bg_feed_generic);
            }
        }

    }


    public void onClickCreateNewGroup(View view){
        Intent intent = new Intent(this, CreateNewGlobalFeedGroup.class);
        startActivity(intent);

    }
}
