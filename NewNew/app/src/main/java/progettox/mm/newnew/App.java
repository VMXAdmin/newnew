package progettox.mm.newnew;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.GeofenceClient;
import com.baidu.location.LocationClient;
import com.parse.Parse;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


public class App extends Application {

    public LocationClient mLocationClient;
    public GeofenceClient mGeofenceClient;
    public MyLocationListener mMyLocationListener;

    private String Access_Token;
    private String PlayerID;
    private String Player_objectId;
    private String GameSessionName;
    private String GameSessionID;
    private String GameSessionID_Notify;
    public String[] SelectedFriendList;
    public String[] SelectedFriendObjectIDList;
    public String FirstChar;
    public int WordCount;
    public boolean GameNotification;
    public String GlobalFeedID;
    public String ChatID;
    private String ChatUsername;
    private String ChatAlertType;
    private int PageNumber;
    private double locationLongtitude;
    private double locationLatitude;
    private ParseGeoPoint GeoLocPoint;
    private String WebAPIURL = "http://192.168.0.10:1029/AngularJSAuthentication.API/";

    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            //Receive Location
            StringBuffer sb = new StringBuffer(256);
            sb.append("time : ");
            sb.append(location.getTime());
            sb.append("\nerror code : ");
            sb.append(location.getLocType());
            sb.append("\nlatitude : ");
            sb.append(location.getLatitude());
            sb.append("\nlontitude : ");
            sb.append(location.getLongitude());
            sb.append("\nradius : ");
            sb.append(location.getRadius());
            if (location.getLocType() == BDLocation.TypeGpsLocation){
                sb.append("\nspeed : ");
                sb.append(location.getSpeed());
                sb.append("\nsatellite : ");
                sb.append(location.getSatelliteNumber());
                sb.append("\ndirection : ");
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                sb.append(location.getDirection());
            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation){
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                //��Ӫ����Ϣ
                sb.append("\noperationers : ");
                sb.append(location.getOperators());
            }
            //logMsg(sb.toString());
            Log.i("BaiduLocationApiDem", sb.toString());

            if(location.getLocType() == 161){

                GeoLocPoint = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
                locationLongtitude = location.getLongitude();
                locationLatitude = location.getLatitude();
                mLocationClient.stop();
            }
        }


    }

    public ParseGeoPoint getGeoLocPoint() {return this.GeoLocPoint;}

    public int getGroupPhotoWidth() {return 1200;}
    public int getGroupPhotoHeight() {return 800;}

    public int getPostGlobalInt() {return 1;}
    public int getPostGroupInt(){return 2;}
    public int getPostFriendInt(){return 3;}
    public int getPostGroupClosedInt(){return 4;}
    public int getPostGroupSecretInt(){return 5;}
    public int getPostTutor(){return 6;}

    public String getGroupType(int type){
        if(type == 0)
            return getResources().getText(R.string.group_type_open).toString();
        else if (type == 1)
            return getResources().getText(R.string.group_type_closed).toString();
        else if (type == 2)
            return getResources().getText(R.string.group_type_secret).toString();

        return "";
    }

    public String getWebAPIRootURL() {
        return this.WebAPIURL;
    }

    public boolean getGameNotification()
    { return GameNotification;}

    public void setGameNotification(boolean status){
        GameNotification = status;
    }

    public String[] getSelectedFriendList(){
        return SelectedFriendList;
    }
    public void setSelectedFriendList(String[] s){
        SelectedFriendList = s;
    }

    public void SelectedFriendList(int size){
        SelectedFriendList = new String[size];
    }

    public String[] getSelectedFriendObjectIDList(){
        return SelectedFriendObjectIDList;
    }
    public void setSelectedFriendObjectIDList(String[] s){
        SelectedFriendObjectIDList = s;
    }

    public void SelectedFriendObjectIDList(int size){
        SelectedFriendObjectIDList = new String[size];
    }
    public String getChatID(){
        return ChatID;
    }
    public void setChatID(String s){
        ChatID = s;
    }

    public String getChatUsername(){
        return ChatUsername;
    }
    public void setChatUsername(String s){
        ChatUsername = s;
    }

    public String getChatAlertType(){
        return ChatAlertType;
    }
    public void setChatAlertType(String s){
        ChatAlertType = s;
    }

    public String getGlobalFeedID(){
        return GlobalFeedID;
    }
    public void setGlobalFeedID(String s){
        GlobalFeedID = s;
    }

    public String getPlayerID(){
        return PlayerID;
    }
    public void setPlayerID(String s){
        PlayerID = s;
    }

    public String getPlayer_objectId(){
        return Player_objectId;
    }
    public void setPlayer_objectId(String s){
        Player_objectId = s;
    }

    public String getGameSessionID_Notify(){
        return GameSessionID_Notify;
    }
    public void setGameSessionID_Notify(String s){
        GameSessionID_Notify = s;
    }

    public String getGameSessionID(){
        return GameSessionID;
    }
    public void setGameSessionID(String s){
        GameSessionID = s;
    }

    public String getGameSessionName(){
        return GameSessionName;
    }
    public void setGameSessionName(String s){
        GameSessionName = s;
    }

    public String getFirstChar(){
        return FirstChar;
    }
    public void setFirstChar(String s){
        FirstChar = s;
    }

    public int getWordCount(){
        return WordCount;
    }
    public void setWordCount(int s){
        WordCount = s;
    }

    public int getPageNumber(){
        return PageNumber;
    }
    public void setPageNumber(int s){
        PageNumber = s;
    }

    public String getToken(){
        return Access_Token;
    }
    public void setToken(String s){
        Access_Token = s;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        Parse.enableLocalDatastore(this);
        // Register your parse models here
        ParseObject.registerSubclass(Message.class);
        Parse.initialize(this, "XCPKhRiyskUrl6UptV1vD3dZPIyPxYlBAOzYP82S", "cH7f3IIxlo1No5g7eZNLToS1W0waNISC0a51yLl3");

        //Initialize Values
        GameNotification = false;
        GameSessionID_Notify = "";

        mLocationClient = new LocationClient(this.getApplicationContext());
        mMyLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mMyLocationListener);


    }



}
