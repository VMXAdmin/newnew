package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 21/7/2015.
 */
public class activity_new_pm extends AppCompatActivity {

    public String PlayerID;
    public String Player_objectId;
    private Boolean mHasPMHistory;
    private String mPM_objectID;

    //public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    static final int REQUEST_TAKE_PHOTO = 1;
    private String mCurrentPhotoPath;

    public TextView mImgLoadingText;
    public MenuItem mNextButton;

    public App appState;
    private ProgressDialog mDialog;
    private static final int CAMERA_REQUEST = 1888;
    private static final int AttachFileRequestCode = 2888;
    //private ImageView imageView;
    private ArrayList<Bitmap> mUserPhotoArray;
    private ArrayList<ParseFile> mParsePhotoArray;
    private ArrayList<ParseFile> mParsePhotoThumbArray;

    private String mUser_ObjectID;
    private String mUser_Name;
    private int mImageIndex;
    //private Uri mImageUri;
    public int mScreenHeight;
    public int mScreenWidth;

    private TextView mPostTarget;
    private LinearLayout mLayoutImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_globalfeed_form);

        //initialize var
        //mIsGlobalFeed = false;
        //mIsUserFeed = false;
        //mIsGroupFeed = false;
        mImageIndex = 0;
        //mViewIndex = 0;

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;

        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){

            mUser_ObjectID = bundle.getString("User_ObjectID");
            mUser_Name = bundle.getString("User_Name");

        }

        appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();
        mHasPMHistory = false;
        mPM_objectID = "";
        //populateSpinners();

        //
        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage(getResources().getText(R.string.lbl_pm_caption)); //lbl_pm_caption
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);

        TextView headerText = (TextView) findViewById(R.id.txt_header);
        headerText.setText(getResources().getText(R.string.lbl_post_pm));

        ImageButton cameraBtn = (ImageButton) findViewById(R.id.camera_action);
        cameraBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(cameraIntent, CAMERA_REQUEST);


                //openCamera();
                dispatchTakePictureIntent();
            }
        });

        ImageButton attachBtn = (ImageButton) findViewById(R.id.attach_action);
        attachBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent,AttachFileRequestCode);
            }
        });

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            //Bitmap bmp = savedInstanceState.getParcelable("PostImage");
            ArrayList<Bitmap> temp = savedInstanceState.getParcelableArrayList("PostImage");
            if(temp != null){
                mUserPhotoArray = temp;
                //imageView.setImageBitmap(mUserPhoto);
            }

        } else {
            // Probably initialize members with default values for a new instance
        }

        //set Target label
        mPostTarget = (TextView) findViewById(R.id.txt_target_name);
        mPostTarget.setText(mUser_Name);

        mLayoutImages = (LinearLayout) findViewById(R.id.layout_images);

        mImgLoadingText = (TextView) findViewById(R.id.lbl_img_loading);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        if(mUserPhotoArray != null){

            //savedInstanceState.putBitmapArrayList("PostImage",mUserPhotoArray);
            savedInstanceState.putParcelableArrayList("PostImage", mUserPhotoArray);
        }
    }

    @Override
    public void onStart() {
        super.onStart();  // Always call the superclass method first

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

    }

    private void checkPMHistory(){
        mNextButton.setVisible(false);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_UserList");
        ParseObject obj = ParseObject.createWithoutData("_User", mUser_ObjectID);
        query.whereEqualTo("User_objectId", obj);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    if(scoreList.size()>0)
                        checkUserHistory(scoreList);
                    else
                        mNextButton.setVisible(true);

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void checkUserHistory(List<ParseObject> scoreList){
        ArrayList<ParseObject> temp = new ArrayList<>();
        for(int i=0;i<scoreList.size();i++){
//            temp.add(i, ParseObject.createWithoutData("PM_objectId",
//                    scoreList.get(i).getParseObject("PM_objectId").getObjectId()));
            temp.add(i, scoreList.get(i).getParseObject("PM_objectId"));
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("PM_UserList");
        ParseObject obj = ParseObject.createWithoutData("_User", Player_objectId);
        query.whereEqualTo("User_objectId", obj);
        query.whereContainedIn("PM_objectId", temp);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {

                    if(scoreList.size()>0){
                        mHasPMHistory = true;
                        mPM_objectID = scoreList.get(0).getParseObject("PM_objectId").getObjectId();
                        mNextButton.setVisible(true);
                    }
                    else
                    {
                        mNextButton.setVisible(true);
                    }


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void addImageToArray(Bitmap Img){
        if(mUserPhotoArray == null)
            mUserPhotoArray = new ArrayList<Bitmap>();

        mUserPhotoArray.add(Img);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        if(storageDir.isDirectory()){
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );


        }
        else{
            storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        }
// Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.getAbsolutePath();
        galleryAddPic();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //...
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void saveImageThumbnail(String Filename, int ImageIndex){
        Bitmap temp = BitmapManager.decodeSampledBitmapFromPath(Filename, 100, 100, this);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        temp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] dataStream = stream.toByteArray();

        String imgFileName = "Image" + "_" + ImageIndex;
        //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
        ParseFile imgFile = new ParseFile (imgFileName + ".jpg", dataStream);
        if(mParsePhotoThumbArray == null)
            mParsePhotoThumbArray = new ArrayList<ParseFile>();
        mParsePhotoThumbArray.add(ImageIndex, imgFile);



    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(mCurrentPhotoPath);
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);

//            Bitmap UserPhoto = (Bitmap) data.getExtras().get("data");
//
//            ImageView myImage = new ImageView(this);
//            myImage.setLayoutParams(new LinearLayout.LayoutParams(
//                    400,
//                    400));
//
//            myImage.setImageBitmap(UserPhoto);
//            mLayoutImages.addView(myImage);
//
//            //now add the image to the Array
//            addImageToArray(UserPhoto);
            File file = new File(mCurrentPhotoPath);

            if(file.exists()){

                //int imageWidth = mScreenWidth * 0.8;
                Bitmap UserPhoto = BitmapFactory.decodeFile(file.getAbsolutePath());
                int oriImageWidth =  UserPhoto.getWidth();
                int oriImageHeight = UserPhoto.getHeight();

                int imageWidth = UserPhoto.getWidth();
                int imageHeight = UserPhoto.getHeight();
                //check if photo exceeds
                if(UserPhoto.getWidth() >= mScreenWidth){
                    Double imageWidthDb = mScreenWidth * 0.8;
                    Double imageHeightDb = UserPhoto.getHeight() * 0.8;
                    if(imageHeightDb >= mScreenHeight){
                        imageHeightDb = mScreenHeight * 0.8;

                    }
                    imageWidth = imageWidthDb.intValue();
                    imageHeight = imageHeightDb.intValue();
//                    UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                            file.getAbsolutePath(), imageWidth, imageHeight);


                }

                BitmapFactory.Options bounds = new BitmapFactory.Options();
                bounds.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), bounds);

                BitmapFactory.Options opts = new BitmapFactory.Options();
                //Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                //BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
                opts.inSampleSize = BitmapManager.calculateInSampleSize(oriImageWidth, oriImageHeight,
                        imageWidth, imageHeight);

                opts.inJustDecodeBounds = false;
                //return BitmapFactory.decodeResource(res, resId, options);
                Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
//                Bitmap bm = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                            file.getAbsolutePath(), imageWidth, imageHeight);

                ExifInterface exif = null;
                try{
                    exif = new ExifInterface(file.getAbsolutePath());
                }
                catch(Exception e){

                }

                String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

                int rotationAngle = 0;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

                Matrix matrix = new Matrix();
                matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
                //UserPhoto = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
                UserPhoto = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);

//                Bitmap UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
//                        file.getAbsolutePath(), mScreenWidth*0.85, mScreenHeight);

                ImageView myImage = new ImageView(this);
                myImage.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                myImage.setAdjustViewBounds(true);
                myImage.setImageBitmap(UserPhoto);
                //mLayoutImages.addView(myImage);

                //now add the image to the Array
                addImageToArray(UserPhoto);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(20, 0, 20, 25);
                myImage.setLayoutParams(lp);

                //add to Parse background as well
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                UserPhoto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] dataStream = stream.toByteArray();

                String imgFileName = "Image" + "_" + mImageIndex;
                //ParseFile imgFile = new ParseFile (ObjectId + "Img1.png", data);
                ParseFile imgFile = new ParseFile (imgFileName + ".jpg", dataStream);
                if(mParsePhotoArray == null)
                    mParsePhotoArray = new ArrayList<ParseFile>();
                mParsePhotoArray.add(mImageIndex, imgFile);

                //save the thumbnail now
                saveImageThumbnail(file.getAbsolutePath(), mImageIndex);

                mImgLoadingText.setVisibility(View.VISIBLE);
                mNextButton.setVisible(false);
                mParsePhotoArray.get(mImageIndex).saveInBackground(new SaveCallback() {
                    public void done(ParseException e) {
                        // If successful add file to user and signUpInBackground
                        if(null == e){
                            mImgLoadingText.setVisibility(View.GONE);
                            mNextButton.setVisible(true);
                        }

                    }
                });
                mImageIndex++;

                //now to add caption for this newly added image
                TextView myImageCaption = new TextView(this);
                myImageCaption.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));

                myImageCaption.setText("Image " + mImageIndex);
                myImageCaption.setTextColor(getResources().getColor(R.color.inputText));

                //now to add remove button in case user changes mind
                ImageButton imageRemoveButton = new ImageButton(this);
                LinearLayout.LayoutParams params =  new LinearLayout.LayoutParams(120,120);
                imageRemoveButton.setLayoutParams(params);
                imageRemoveButton.setBackgroundResource(R.color.white);
                imageRemoveButton.setAdjustViewBounds(true);
                imageRemoveButton.setScaleType(ImageButton.ScaleType.CENTER_CROP);
                imageRemoveButton.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_remove));



                imageRemoveButton.setTag(imageRemoveButton.getId(), myImage);
                imageRemoveButton.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
                                mLayoutImages.removeView(view);
                                mLayoutImages.removeView((View)(view.getTag(view.getId())));

                            }
                        });

                mLayoutImages.addView(imageRemoveButton);
                mLayoutImages.addView(myImage);
            }
        }

        if (requestCode == AttachFileRequestCode && resultCode == RESULT_OK) {

            Uri selectedImageUri = data.getData();
            String[] fileColumn = {MediaStore.Images.Media.DATA};
            Cursor imageCursor = getContentResolver().query(selectedImageUri, fileColumn,
                    null, null, null);
            imageCursor.moveToFirst();

            int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
            String picturePath = imageCursor.getString(fileColumnIndex);

            Bitmap UserPhoto = BitmapFactory.decodeFile(picturePath);
            //check if photo exceeds
            if(UserPhoto.getWidth() >= mScreenWidth){
                Double imageWidthDb = mScreenWidth * 0.8;
                Double imageHeightDb = UserPhoto.getHeight() * 0.8;
                if(imageHeightDb >= mScreenHeight){
                    imageHeightDb = mScreenHeight * 0.8;

                }

                int imageWidth = imageWidthDb.intValue();
                int imageHeight = imageHeightDb.intValue();
                UserPhoto = BitmapManager.decodeSampledBitmapFromResource(getResources(),
                        picturePath, imageWidth, imageHeight);


            }

            ImageView myImage = new ImageView(this);
            myImage.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            myImage.setAdjustViewBounds(true);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(20, 0, 20, 25);
            myImage.setLayoutParams(lp);

            myImage.setImageBitmap(UserPhoto);
            addImageToArray(UserPhoto);

            //add to Parse background as well
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            UserPhoto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            byte[] dataStream = stream.toByteArray();

            String imgFileName = "Image" + "_" + mImageIndex;
            ParseFile imgFile = new ParseFile (imgFileName + ".jpg", dataStream);
            if(mParsePhotoArray == null)
                mParsePhotoArray = new ArrayList<ParseFile>();
            mParsePhotoArray.add(mImageIndex, imgFile);

            //save the thumbnail now
            saveImageThumbnail(picturePath, mImageIndex);

            mImgLoadingText.setVisibility(View.VISIBLE);
            mNextButton.setVisible(false);
            mParsePhotoArray.get(mImageIndex).saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    // If successful add file to user and signUpInBackground
                    if(null == e){
                        mImgLoadingText.setVisibility(View.GONE);
                        mNextButton.setVisible(true);
                    }

                }
            });
            mImageIndex++;

            //now to add caption for this newly added image
            TextView myImageCaption = new TextView(this);
            myImageCaption.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));

            myImageCaption.setText("Image " + mImageIndex);
            myImageCaption.setTextColor(getResources().getColor(R.color.inputText));

            //now to add remove button in case user changes mind
            ImageButton imageRemoveButton = new ImageButton(this);
            LinearLayout.LayoutParams params =  new LinearLayout.LayoutParams(120,120);
            imageRemoveButton.setLayoutParams(params);
            imageRemoveButton.setBackgroundResource(R.color.white);
            imageRemoveButton.setAdjustViewBounds(true);
            imageRemoveButton.setScaleType(ImageButton.ScaleType.CENTER_CROP);
            imageRemoveButton.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_remove));



            imageRemoveButton.setTag(imageRemoveButton.getId(), myImage);
            imageRemoveButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //String outputText = "Primary Touch " + String.valueOf(view.getTag(view.getId()));
                            mLayoutImages.removeView(view);
                            mLayoutImages.removeView((View)(view.getTag(view.getId())));

                        }
                    });

            //mLayoutImages.addView(myImageCaption);

            mLayoutImages.addView(imageRemoveButton);
            mLayoutImages.addView(myImage);
        }
    }

    private void AddImagesToParse(final String ObjectId){

        if(mUserPhotoArray == null){
            mDialog.dismiss();
            finish();
            return;
        }

        //update GameSessionID
        appState.setGameSessionID(ObjectId);
        final List<ParseObject> PlayerList = new ArrayList<ParseObject>();
        String imgFileName = "";
        ParseObject NewPlayer;

        for(int i = 0; i< mUserPhotoArray.size();i++){
            NewPlayer = new ParseObject("PM_Images");
            NewPlayer.put("PM_History_ObjectID", ObjectId);
            NewPlayer.put("PM_History_objectId", ParseObject.createWithoutData("PM_History",
                    ObjectId)); //same for all
            NewPlayer.put("Image", mParsePhotoArray.get(i));
            NewPlayer.put("Image_Thumbnail", mParsePhotoThumbArray.get(i));
            PlayerList.add(NewPlayer);
        }

        final Activity act = this;

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if(e == null){
                    mDialog.dismiss();
                    finish();
                }
                else
                {
                    Log.i("Save Image Error", e.getMessage());
                }


            }
        });

    }

    private void addNewPost(){




        //get Views
        EditText Content = (EditText) findViewById(R.id.txt_post_content);

        if(mHasPMHistory){
//insert new word into History table
            final ParseObject gameScore = new ParseObject("PM_History");
            gameScore.put("PM_ObjectID", mPM_objectID);
            gameScore.put("PM_objectId", ParseObject.createWithoutData("PM_Master",
                    mPM_objectID));
            gameScore.put("Message", Content.getText().toString());
            gameScore.put("IsSeen", false);
            gameScore.put("Sender_objectId", ParseObject.createWithoutData("_User", Player_objectId));
            gameScore.put("Recipient_objectId", ParseObject.createWithoutData("_User", mUser_ObjectID));

            gameScore.saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    if (e == null) {

                        AddImagesToParse(gameScore.getObjectId());
                        addUserNotification(gameScore.getObjectId());

                    } else {
                        // The save failed.
                        Log.d("Error", e.getMessage());
                    }
                }
            });
        }
        else{
            final ParseObject gameScore = new ParseObject("PM_Master");
            gameScore.put("Creator_objectId", ParseObject.createWithoutData("_User",
                    Player_objectId));

            gameScore.saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    if (e == null) {

                        AddUsersToUserList(gameScore.getObjectId());


                    } else {
                        // The save failed.
                        Log.d("Error", e.getMessage());
                    }
                }
            });
        }
    }

    private void AddUsersToUserList(String PM_objID){
        List<ParseObject> PlayerList = new ArrayList<ParseObject>();

        ParseObject gameScore = new ParseObject("PM_UserList");
        gameScore.put("PM_objectId", ParseObject.createWithoutData("PM_Master", PM_objID));
        gameScore.put("User_objectId", ParseObject.createWithoutData("_User", Player_objectId));
        PlayerList.add(gameScore);

        gameScore = new ParseObject("PM_UserList");
        gameScore.put("PM_objectId", ParseObject.createWithoutData("PM_Master", PM_objID));
        gameScore.put("User_objectId", ParseObject.createWithoutData("_User", mUser_ObjectID));
        PlayerList.add(gameScore);

        PlayerList.add(gameScore);

        ParseObject.saveAllInBackground(PlayerList, new SaveCallback() {

            @Override
            public void done(ParseException e) {

            }
        });

        mHasPMHistory = true;
        mPM_objectID = PM_objID;
        addNewPost();


    }

    private void addUserNotification(final String FeedObjID){

        ParseObject NewPlayer = new ParseObject("Notifications");
        //NewPlayer.put("Group_ObjectID", GroupID);
        NewPlayer.put("User_objectId", ParseObject.createWithoutData(
                "_User", mUser_ObjectID));
        NewPlayer.put("IsSeen", false);
        NewPlayer.put("Type", 10);
        NewPlayer.put("IsAccepted", false); //By default is False
        NewPlayer.put("PM_History_objectId", ParseObject.createWithoutData(
                "PM_History", FeedObjID));
        NewPlayer.put("Member_objectId", ParseObject.createWithoutData(
                "_User", Player_objectId));
        NewPlayer.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                //RequestCompleted();
            }
        });
    }

    public static class AlertDialogs{

        public AlertDialogs(){

        }

        public static void ShowAlertEmptyName(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter a name for your Post")
                    .setTitle("Invalid Post Name");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        public static void ShowAlertEmptyBody(Activity activity){
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("Please enter some text for your Message")
                    .setTitle("Invalid Post Content");

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        if (!mNavigationDrawerFragment.isDrawerOpen()) {
//            // Only show items in the action bar relevant to this screen
//            // if the drawer is not showing. Otherwise, let the drawer
//            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.main_menu, menu);
//            restoreActionBar();
//            return true;
//        }
        getMenuInflater().inflate(R.menu.global_next, menu);
        mNextButton = menu.findItem(R.id.action_next);
        //check for existing PM between you and the recipient
        checkPMHistory();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.home){
            finish();
            return true;
        }


        if (id == R.id.action_next) {
//get Views
            EditText Name = (EditText) findViewById(R.id.txt_post_name);
            EditText Content = (EditText) findViewById(R.id.txt_post_content);

//            if(Name.getText().toString().equals("")){
//                AlertDialogs.ShowAlertEmptyName(this);
//                return false;
//            }

            if(Content.getText().toString().equals("")){
                AlertDialogs.ShowAlertEmptyBody(this);
                return false;
            }


            mDialog.show();
            addNewPost();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }




}
