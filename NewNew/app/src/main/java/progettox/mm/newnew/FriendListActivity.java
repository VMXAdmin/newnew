package progettox.mm.newnew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


public class FriendListActivity extends Activity {



    //private List<ParseObject> PlayerHistoryResults;
    private List<PlayerFriend> PlayerFriendsArray;
    public String PlayerID;
    public String Player_objectId;
    public String GameSessionID;
    public String mLastChar;
    public int mWordCount;
    public final static String EXTRA_MESSAGE = "progettox.mm.newnew.GamePlayActivity";
    private int mRequestCode = 100;
    public String[] FriendList;
    public String[] FriendObjIDList;
    public AppInfoAdapter adapter ;
    private ProgressDialog mDialog;
    public App appState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_invite_listview);

        appState = ((App)getApplicationContext());
        PlayerID = appState.getPlayerID();
        Player_objectId = appState.getPlayer_objectId();
        GameSessionID = appState.getGameSessionID();


        //final ListView listApplication = (ListView)findViewById(R.id.listApplication);
        Button b= (Button) findViewById(R.id.button1);
        b.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                StringBuilder result = new StringBuilder();
                StringBuilder resultObjId = new StringBuilder();
                int count = 0;
                for(int i=0;i<adapter.data.size();i++)
                {
                    if(adapter.mCheckStates.get(i))
                    {
                        count++;
                        result.append(PlayerFriendsArray.get(i).FriendID);
                        result.append("\n");
                        //FriendList[0] = PlayerFriendsArray.get(i).FriendID;
                        resultObjId.append(PlayerFriendsArray.get(i).Friend_objectId);
                        resultObjId.append("\n");

                    }

                }

                if (count == 1){
                    int friendsCount = 1;
                    FriendList = new String[friendsCount];
                    FriendObjIDList = new String[friendsCount];
                    appState.SelectedFriendList(friendsCount);
                    appState.SelectedFriendObjectIDList(friendsCount);
                    FriendList[0] = result.substring(0, result.indexOf("\n"));
                    FriendObjIDList[0] = resultObjId.substring(0, resultObjId.indexOf("\n"));

                    appState.setSelectedFriendList(FriendList);
                    appState.setSelectedFriendObjectIDList(FriendObjIDList);

                }
                else
                {
                    //Toast.makeText(this, result, 1000).show();
                    StringTokenizer tokens = new StringTokenizer(result.toString(), "\n");
                    StringTokenizer tokensObjID = new StringTokenizer(resultObjId.toString(), "\n");
                    int friendsCount = tokens.countTokens();
                    FriendList = new String[friendsCount];
                    FriendObjIDList = new String[friendsCount];
                    appState.SelectedFriendList(friendsCount);
                    appState.SelectedFriendObjectIDList(friendsCount);

                    for(int i=0;i<friendsCount;i++) {
                        FriendList[i] = tokens.nextToken();
                        FriendObjIDList[i] = tokensObjID.nextToken();
                    }

                    //store into global class

                    appState.setSelectedFriendList(FriendList);
                    appState.setSelectedFriendObjectIDList(FriendObjIDList);
                }

                finish();

            }

        });


        mDialog = new ProgressDialog(this);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setMessage("Retrieving Your Friends, please wait...");
        mDialog.setIndeterminate(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
        //Start to populate the Friends List
        getFriendsList();

//        ApplicationInfo applicationInfo = getApplicationInfo();
//        PackageManager pm = getPackageManager();
//        List<PackageInfo> pInfo = new ArrayList<PackageInfo>();
//        pInfo.addAll(pm.getInstalledPackages(0));
//        app_info = new AppInfo[pInfo.size()];
//
//        int counter = 0;
//        for(PackageInfo item: pInfo){
//            try{
//
//                applicationInfo = pm.getApplicationInfo(item.packageName, 1);
//
//                app_info[counter] = new AppInfo(pm.getApplicationIcon(applicationInfo),
//                        String.valueOf(pm.getApplicationLabel(applicationInfo)));
//
//                System.out.println(counter);
//
//            }
//            catch(Exception e){
//                System.out.println(e.getMessage());
//            }
//
//            counter++;
//        }
//
//        adapter = new AppInfoAdapter(this, R.layout.friends_invite_item, app_info);
//        listApplication.setAdapter(adapter);

    }


    public void goToGamePlayPage(){
//        Intent intent = new Intent(this, GamePlayActivity.class);
//        startActivity(intent);

        Intent intent = new Intent(this, GamePlayActivity.class);
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("LastChar", mLastChar);
        bundle.putInt("WordCount", mWordCount);

        //Add the bundle to the intent
        intent.putExtras(bundle);

        startActivityForResult(intent, mRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        //Do your work here in ActivityA
        //getExtras().get("Type"));
//        String message = data.getStringExtra(EXTRA_MESSAGE);
//        if(message != null){
//            if(message.equals("Success")){
//                finish();
//            }
//        }

        if(requestCode == mRequestCode && resultCode == RESULT_OK){
            String editTextString = data.getStringExtra("Status");
            if(editTextString.equals("Success")){
                finish();
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayJSONResults(List<ParseObject> data)
    {

        try{
            PlayerFriendsArray = fromJson(data);

            Log.i("Finally", PlayerFriendsArray.get(0).PlayerID);

            // Construct the data source
            ArrayList<PlayerFriend> arrayOfUsers = new ArrayList<PlayerFriend>();
            // Create the adapter to convert the array to views
            adapter = new AppInfoAdapter(this, R.layout.friends_invite_item, arrayOfUsers, this);
            adapter.addAll(PlayerFriendsArray);
            // Attach the adapter to a ListView
            ListView listView = (ListView) findViewById(R.id.listApplication);
            listView.setAdapter(adapter);
            mDialog.dismiss();
            ///////////
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    PlayerFriend listItem = PlayerFriendsArray.get(position);
//                    Log.i("Item selected is ", listItem.GameName);
//                }
//            });
            ///////////


        }
        catch(Exception e){
            Log.i("Exception fromJson", e.getMessage());
        }

    }

    private void getFriendsList(){


        ParseQuery<ParseObject> query = ParseQuery.getQuery("User_Friends");
        query.whereEqualTo("PlayerID", PlayerID);
        query.include("Friend_objectId");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> friendList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + friendList.size() + " scores");
                    //PlayerHistoryResults = scoreList;
                    displayJSONResults(friendList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    public ArrayList<PlayerFriend> fromJson(List<ParseObject> jsonObjects) {
        ArrayList<PlayerFriend> GameHistory = new ArrayList<PlayerFriend>();
        for (int i = 0; i < jsonObjects.size(); i++) {
            try {
                GameHistory.add(new PlayerFriend(jsonObjects.get(i), i+1, this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return GameHistory;
    }

//    public class AppInfo {
//        public Drawable icon;
//        public String applicationName;
//
//        public AppInfo(){
//            super();
//        }
//
//        public AppInfo(Drawable icon, String applicationName){
//            super();
//            this.icon = icon;
//            this.applicationName = applicationName;
//        }
//
//
//    }

    public static class AppInfoHolder
    {
        //ImageView imgIcon;
        ImageView imgIcon;
        TextView txtTitle;
        CheckBox chkSelect;

    }

    public class AppInfoAdapter extends ArrayAdapter<PlayerFriend> implements CompoundButton.OnCheckedChangeListener
    {  SparseBooleanArray mCheckStates;

        Context context;
        int layoutResourceId;
        ArrayList<PlayerFriend> data = null;
        Activity activity;

        public AppInfoAdapter(Context context, int layoutResourceId, ArrayList<PlayerFriend> data, Activity act){
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
            mCheckStates = new SparseBooleanArray(data.size());
            this.activity = act;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View row = convertView;
            AppInfoHolder holder= null;

            if (row == null){

                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new AppInfoHolder();

                holder.imgIcon = (ImageView) row.findViewById(R.id.menu_source_icon);
                holder.txtTitle = (TextView) row.findViewById(R.id.menu_source_name);
                holder.chkSelect = (CheckBox) row.findViewById(R.id.menu_source_check_box);

                row.setTag(holder);

            }
            else{
                holder = (AppInfoHolder)row.getTag();
            }


            PlayerFriend appinfo = data.get(position);

            holder.txtTitle.setText(appinfo.FriendID);
            holder.imgIcon.setImageBitmap(appinfo.loadImageFromStorage(appinfo.Friend_objectId, activity));
//            holder.imgIcon.setParseFile(appinfo.profilePic);
//
//            //TODO to improve this code, will not be good if hundreds of friends appear
//            holder.imgIcon.loadInBackground(new GetDataCallback() {
//                public void done(byte[] data, ParseException e) {
//                    // The image is loaded and displayed!
//                    //int oldHeight = imageView.getHeight();
//                    //int oldWidth = imageView.getWidth();
//                    Log.v("LOG!!!!!!", "imageView height = ");      // DISPLAYS 90 px
//                    Log.v("LOG!!!!!!", "imageView width = " );        // DISPLAYS 90 px
//                }
//            });
            //holder.imgIcon.setImageDrawable(appinfo.icon);
            // holder.chkSelect.setChecked(true);
            holder.chkSelect.setTag(position);
            holder.chkSelect.setChecked(mCheckStates.get(position, false));
            holder.chkSelect.setOnCheckedChangeListener(this);
            return row;

        }
        public boolean isChecked(int position) {
            return mCheckStates.get(position, false);
        }

        public void setChecked(int position, boolean isChecked) {
            mCheckStates.put(position, isChecked);

        }

        public void toggle(int position) {
            setChecked(position, !isChecked(position));

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            mCheckStates.put((Integer) buttonView.getTag(), isChecked);

        }

    }

    public class PlayerFriend {
        public String objectId;
        public String PlayerID;
        public String FriendID;
        public String Friend_objectId;

        public Drawable icon;
        public String applicationName;
        public ParseFile profilePic;
        
        public PlayerFriend(ParseObject Obj, int c, final Activity activity) {
            this.objectId = Obj.getObjectId();

            this.PlayerID = Obj.getString("PlayerID");
            this.FriendID = Obj.getString("FriendID");
            this.profilePic = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
            this.Friend_objectId = Obj.getParseObject("Friend_objectId").getObjectId();
            //profilePic
            //public static Drawable createFromResourceStream (Resources res, TypedValue value, InputStream is, String srcName, BitmapFactory.Options opts)


            if(!IsBitmapCached(this.Friend_objectId, activity)){
                ParseFile Picture = Obj.getParseObject("Friend_objectId").getParseFile("Avatar_Pic");
                Picture.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {

                        if (e == null) {
                            Bitmap PictureBmp = BitmapFactory.decodeByteArray(data, 0,
                                    data.length);
                            storeImage(PictureBmp, Friend_objectId, activity);

                        } else {

                        }

                    }
                });
            }

        }

        private boolean IsBitmapCached(String FileName, Activity activity){
            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, FileName + ".png");
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                if (b != null)
                    return true;

            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return false;
        }

        public void storeImage(Bitmap bitmapImage, String FileName, Activity activity) {
            ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, FileName + ".png");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return directory.getAbsolutePath();
        }

        public Bitmap loadImageFromStorage(String BitmapName, Activity activity)
        {

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File f=new File(directory, BitmapName + ".png");
                return BitmapFactory.decodeStream(new FileInputStream(f));
                //ImageView img=(ImageView)findViewById(R.id.imgPicker);
                //img.setImageBitmap(b);
                //return b;
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }

            return null;
        }

    }

}
